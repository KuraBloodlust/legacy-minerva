VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Scheme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IScheme

Private m_ActiveCaptionForeColor(3) As Long
Private m_ActiveCaptionGradient1(3) As Long
Private m_ActiveCaptionGradient2(3) As Long
Private m_InactiveCaptionForeColor(3) As Long
Private m_InactiveCaptionGradient1(3) As Long
Private m_InactiveCaptionGradient2(3) As Long

Private m_ActiveTabIcon(3) As Boolean
Private m_ActiveTabForeColor(3) As Long
Private m_ActiveTabGradient1(3) As Long
Private m_ActiveTabGradient2(3) As Long
Private m_ActiveTabGradientAngle(3) As Long

Private m_InactiveTabIcon(3) As Boolean
Private m_InactiveTabForeColor(3) As Long
Private m_InactiveTabGradient1(3) As Long
Private m_InactiveTabGradient2(3) As Long
Private m_InactiveTabGradientAngle(3) As Long

Private m_FocusTabIcon(3) As Boolean
Private m_FocusTabForeColor(3) As Long
Private m_FocusTabGradient1(3) As Long
Private m_FocusTabGradient2(3) As Long
Private m_FocusTabGradientAngle(3) As Long

Private m_CaptionStyle(3) As Long
Private m_FrameStyle(3) As Long
Private m_BackColor(3) As Long
Private m_FrameColor(3) As Long
Private m_FrameWidth(3) As Long
Private m_TabStripBackColor(3) As Long
Private m_SplitbarColor(3) As Long
Private m_EditorAreaBackColor(3) As Long
Private m_Captions(3) As Boolean
Private m_CaptionIcons(3) As Boolean

Private Property Get IScheme_ActiveCaptionForeColor() As Long
   IScheme_ActiveCaptionForeColor = m_ActiveCaptionForeColor(scheme)
End Property
Public Sub setActiveCaptionForeColor(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_ActiveCaptionForeColor(WindowsScheme) = Color
End Sub

Private Property Get IScheme_ActiveCaptionGradient1() As Long
   IScheme_ActiveCaptionGradient1 = m_ActiveCaptionGradient1(scheme)
End Property
Public Sub setActiveCaptionGradient1(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_ActiveCaptionGradient1(WindowsScheme) = Color
End Sub

Private Property Get IScheme_ActiveCaptionGradient2() As Long
   IScheme_ActiveCaptionGradient2 = m_ActiveCaptionGradient2(scheme)
End Property
Public Sub setActiveCaptionGradient2(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_ActiveCaptionGradient2(WindowsScheme) = Color
End Sub

Private Property Get IScheme_ActiveTabIcon() As Boolean
   IScheme_ActiveTabIcon = m_ActiveTabIcon(scheme)
End Property
Public Sub setActiveTabIcon(ByVal ActiveTabIcon As Boolean, ByVal WindowsScheme As Long)
   m_ActiveTabIcon(WindowsScheme) = ActiveTabIcon
End Sub

Private Property Get IScheme_InactiveTabIcon() As Boolean
   IScheme_InactiveTabIcon = m_InactiveTabIcon(scheme)
End Property
Public Sub setInactiveTabIcon(ByVal InactiveTabIcon As Boolean, ByVal WindowsScheme As Long)
   m_InactiveTabIcon(WindowsScheme) = InactiveTabIcon
End Sub

Private Property Get IScheme_ActiveTabForeColor() As Long
   IScheme_ActiveTabForeColor = m_ActiveTabForeColor(scheme)
End Property
Public Sub setActiveTabForeColor(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_ActiveTabForeColor(WindowsScheme) = Color
End Sub

Private Property Get IScheme_ActiveTabGradient1() As Long
   IScheme_ActiveTabGradient1 = m_ActiveTabGradient1(scheme)
End Property
Public Sub setActiveTabGradient1(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_ActiveTabGradient1(WindowsScheme) = Color
End Sub

Private Property Get IScheme_ActiveTabGradient2() As Long
   IScheme_ActiveTabGradient2 = m_ActiveTabGradient2(scheme)
End Property
Public Sub setActiveTabGradient2(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_ActiveTabGradient2(WindowsScheme) = Color
End Sub

Private Property Get IScheme_ActiveTabGradientAngle() As Long
   IScheme_ActiveTabGradientAngle = m_ActiveTabGradientAngle(scheme)
End Property
Public Sub setActiveTabGradientAngle(ByVal Angel As Long, ByVal WindowsScheme As Long)
   m_ActiveTabGradientAngle(WindowsScheme) = Angel
End Sub

Private Property Get IScheme_BackColor() As Long
   IScheme_BackColor = m_BackColor(scheme)
End Property
Public Sub setBackColor(ByVal BackColor As Long, ByVal WindowsScheme As Long)
   m_BackColor(WindowsScheme) = BackColor
End Sub

Private Property Get IScheme_CaptionStyle() As vbCaptionStyle
   IScheme_CaptionStyle = m_CaptionStyle(scheme)
End Property
Public Sub setCaptionStyle(ByVal CaptionStyle As Long, ByVal WindowsScheme As Long)
   m_CaptionStyle(WindowsScheme) = CaptionStyle
End Sub

Private Property Get IScheme_FocusTabIcon() As Boolean
   IScheme_FocusTabIcon = m_FocusTabIcon(scheme)
End Property
Public Sub setFocusTabIcon(ByVal FocusTabIcon As Boolean, ByVal WindowsScheme As Long)
   m_FocusTabIcon(WindowsScheme) = FocusTabIcon
End Sub

Private Property Get IScheme_FrameStyle() As vbFrameStyle
   IScheme_FrameStyle = m_FrameStyle(scheme)
End Property
Public Sub setFrameStyle(ByVal FrameStyle As Long, ByVal WindowsScheme As Long)
   m_FrameStyle(WindowsScheme) = FrameStyle
End Sub

Private Property Get IScheme_EditorAreaBackColor() As Long
   IScheme_EditorAreaBackColor = m_EditorAreaBackColor(scheme)
End Property
Public Sub setEditorAreaBackColor(ByVal EditorAreaBackColor As Long, ByVal WindowsScheme As Long)
   m_EditorAreaBackColor(WindowsScheme) = EditorAreaBackColor
End Sub

Private Property Get IScheme_FocusTabForeColor() As Long
   IScheme_FocusTabForeColor = m_FocusTabForeColor(scheme)
End Property
Public Sub setFocusTabForeColor(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_FocusTabForeColor(WindowsScheme) = Color
End Sub

Private Property Get IScheme_FocusTabGradient1() As Long
   IScheme_FocusTabGradient1 = m_FocusTabGradient1(scheme)
End Property
Public Sub setFocusTabGradient1(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_FocusTabGradient1(WindowsScheme) = Color
End Sub

Private Property Get IScheme_FocusTabGradient2() As Long
   IScheme_FocusTabGradient2 = m_FocusTabGradient2(scheme)
End Property
Public Sub setFocusTabGradient2(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_FocusTabGradient2(WindowsScheme) = Color
End Sub

Private Property Get IScheme_FocusTabGradientAngle() As Long
   IScheme_FocusTabGradientAngle = m_FocusTabGradientAngle(scheme)
End Property
Public Sub setFocusTabGradientAngle(ByVal Angel As Long, ByVal WindowsScheme As Long)
   m_FocusTabGradientAngle(WindowsScheme) = Angel
End Sub


Private Property Get IScheme_FrameColor() As Long
   IScheme_FrameColor = m_FrameColor(scheme)
End Property
Public Sub setFrameColor(ByVal FrameColor As Long, ByVal WindowsScheme As Long)
   m_FrameColor(WindowsScheme) = FrameColor
End Sub

Private Property Get IScheme_FrameWidth() As Long
   IScheme_FrameWidth = m_FrameWidth(scheme)
End Property
Public Sub setFrameWidth(ByVal FrameWidth As Long, ByVal WindowsScheme As Long)
   m_FrameWidth(WindowsScheme) = FrameWidth
End Sub

Private Property Get IScheme_InactiveCaptionForeColor() As Long
   IScheme_InactiveCaptionForeColor = m_InactiveCaptionForeColor(scheme)
End Property
Public Sub setInactiveCaptionForeColor(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_InactiveCaptionForeColor(WindowsScheme) = Color
End Sub

Private Property Get IScheme_InactiveCaptionGradient1() As Long
   IScheme_InactiveCaptionGradient1 = m_InactiveCaptionGradient1(scheme)
End Property
Public Sub setInactiveCaptionGradient1(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_InactiveCaptionGradient1(WindowsScheme) = Color
End Sub

Private Property Get IScheme_InactiveCaptionGradient2() As Long
   IScheme_InactiveCaptionGradient2 = m_InactiveCaptionGradient2(scheme)
End Property
Public Sub setInactiveCaptionGradient2(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_InactiveCaptionGradient2(WindowsScheme) = Color
End Sub

Private Property Get IScheme_InactiveTabForeColor() As Long
   IScheme_InactiveTabForeColor = m_InactiveTabForeColor(scheme)
End Property
Public Sub setInactiveTabForeColor(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_InactiveTabForeColor(WindowsScheme) = Color
End Sub

Private Property Get IScheme_InactiveTabGradient1() As Long
   IScheme_InactiveTabGradient1 = m_InactiveTabGradient1(scheme)
End Property
Public Sub setInactiveTabGradient1(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_InactiveTabGradient1(WindowsScheme) = Color
End Sub

Private Property Get IScheme_InactiveTabGradient2() As Long
   IScheme_InactiveTabGradient2 = m_InactiveTabGradient2(scheme)
End Property
Public Sub setInactiveTabGradient2(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_InactiveTabGradient2(WindowsScheme) = Color
End Sub

Private Property Get IScheme_InactiveTabGradientAngle() As Long
   IScheme_InactiveTabGradientAngle = m_InactiveTabGradientAngle(scheme)
End Property
Public Sub setInactiveTabGradientAngle(ByVal Angel As Long, ByVal WindowsScheme As Long)
   m_InactiveTabGradientAngle(WindowsScheme) = Angel
End Sub

Private Property Get IScheme_SplitbarColor() As Long
   IScheme_SplitbarColor = m_SplitbarColor(scheme)
End Property
Public Sub setSplitbarColor(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_SplitbarColor(WindowsScheme) = Color
End Sub

Private Property Get IScheme_TabStripBackColor() As Long
   IScheme_TabStripBackColor = m_TabStripBackColor(scheme)
End Property
Public Sub setTabStripBackColor(ByVal Color As Long, ByVal WindowsScheme As Long)
   m_TabStripBackColor(WindowsScheme) = Color
End Sub

Private Property Get IScheme_CaptionIcons() As Boolean
   IScheme_CaptionIcons = m_CaptionIcons(scheme)
End Property
Public Sub setCaptionIcons(ByVal CaptionIcons As Boolean, ByVal WindowsScheme As Long)
   m_CaptionIcons(WindowsScheme) = CaptionIcons
End Sub

Private Property Get IScheme_Captions() As Boolean
   IScheme_Captions = m_Captions(scheme)
End Property
Public Sub setCaptions(ByVal Captions As Boolean, ByVal WindowsScheme As Long)
   m_Captions(WindowsScheme) = Captions
End Sub

