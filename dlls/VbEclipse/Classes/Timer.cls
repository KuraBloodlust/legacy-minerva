VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Timer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function GetTickCount Lib "kernel32" () As Long

'----------------------------------------------------------------------
'
' <center><b>AB-Software Timer Class</b></center><br><br>
'
' Complete replacement for the timer control, in 38 lines of code
' with no API calls or ActiveX controls! Can be started, stopped
' and paused for a given amount of time. It has no limit for the
' time intervals it can measure and nicest of all, it can be used
' outside forms, within other classes.<br><br>
'
' <hr>
'
' THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
' EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
' OF MERCHANTABILITY, FITNESS FOR A PARIVULAR PURPOSE AND
' NONIFRINGEMENT. IN NO EVENT SHALL THE AUTORS OR COPYRIGHT HOLDERS
' BE LIABLE FPR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WETHER IN
' AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
' OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
' IN THE SOFTWARE.
'
'----------------------------------------------------------------------
'
' #Author     Andreas Baechle
' #Created    06/08/2004
' #Version    1.0
' #Copyright  Copyright (c) 2004, AB-Software
'
'----------------------------------------------------------------------
' @Interval
'----------------------------------------------------------------------

' --------------------------------------------------------------------
' Events
' --------------------------------------------------------------------

Public Event Timer() ' The timer event.

' --------------------------------------------------------------------
' Variables
' --------------------------------------------------------------------

Private m_Interval As Long  ' The interval in seconds.
Private m_Enabled As Boolean   ' <b>True</b> if timer is stopped; <b>False</b> otherwise.

' --------------------------------------------------------------------
' Starts the timer with an interval.
'
' @lngInterval The timer interval.
' --------------------------------------------------------------------
Public Property Let Enabled(ByVal blnEnabled As Boolean)
    m_Enabled = blnEnabled
    
    If m_Enabled Then Run
End Property


Public Property Get Enabled() As Boolean
    Enabled = m_Enabled
End Property

' --------------------------------------------------------------------
' Loops between timer events.
' --------------------------------------------------------------------
Private Sub Run()
   
   Dim n As Long

   Do

      If (GetTickCount Mod Me.Interval) = 0 Then
                  
         RaiseEvent Timer
         
      End If
      
      DoEvents
         
   Loop Until m_Enabled = False
   
End Sub

' --------------------------------------------------------------------
' Sets the interval.
'
' @lngInterval The new interval.
' --------------------------------------------------------------------
Public Property Let Interval(lngInterval As Long)
    
    m_Interval = Abs(lngInterval)
    
    If Enabled And m_Interval = 0 Then
       Enabled = False
    End If
    
End Property

' --------------------------------------------------------------------
' Returns the interval.
' --------------------------------------------------------------------
Public Property Get Interval() As Long
    Interval = m_Interval
End Property
