VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSettingsRenderer_Desc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Renderer-Klasse f�r allgemeine Registry-Kategorieelemente

Option Explicit

Public MyForm As New frmDataElement_Desc
Public MySEditor As cEdSettingsEditor

Public Sub PreInitialize()
Load MyForm
End Sub

Public Sub Initialize()
Set MyForm = MyForm.GetInstance
Set MyForm.MyRenderer = Me
MyForm.Visible = False
End Sub

Public Function Render(ByRef snode As cGAMRegistryNode, ByRef picSEditor As PictureBox, ByRef x As Long, ByRef y As Long, ByRef SubCatLevel As Integer, ByRef DisplayCollection As CHive)


If snode.NodeType = "category" Then


    MyForm.picDataEditor.FontSize = 8
    MyForm.picDataEditor.FontBold = True
    MyForm.picDataEditor.FontItalic = False
    MyForm.Padding = 10
    
    MyForm.picDataEditor.BackColor = &H8000000D
    MyForm.picDataEditor.ForeColor = &H8000000E
    
ElseIf Left(LCase(snode.NodeType), 7) = "custom:" Then
    MyForm.picDataEditor.FontSize = 7
    MyForm.picDataEditor.FontBold = False
    MyForm.picDataEditor.FontItalic = False
    MyForm.Padding = 5

    MyForm.picDataEditor.BackColor = &HA6EEB7
    MyForm.picDataEditor.ForeColor = vbBlack
Else
    MyForm.picDataEditor.FontSize = 7
    MyForm.picDataEditor.FontBold = False
    MyForm.picDataEditor.FontItalic = False
    MyForm.Padding = 3
    

     MyForm.picDataEditor.BackColor = &HE0E0E0
    MyForm.picDataEditor.ForeColor = vbBlack
End If



If SubCatLevel > 1 Then


ElseIf SubCatLevel = 1 Then

ElseIf SubCatLevel = 0 Then


End If

Dim pb As PictureBox

Set MyForm.MyNode = snode
Set pb = MyForm.MainContainer

SetParentAPI pb.hwnd, picSEditor.hwnd
pb.Left = x
pb.Top = y
pb.Width = picSEditor.ScaleWidth - pb.Left

y = y + pb.Height


MyForm.Initialize SubCatLevel

DisplayCollection.Add pb
End Function


Public Function DataToXML(snode As cGAMRegistryNode, XMLNode As IXMLDOMElement, Dom As DOMDocument, Project As cGAMProject) As Boolean

End Function

Public Function XMLToData(snode As cGAMRegistryNode, XMLNode As IXMLDOMNode, Dom As DOMDocument, Project As cGAMProject)

End Function
