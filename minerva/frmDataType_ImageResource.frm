VERSION 5.00
Begin VB.Form frmDataType_ImageResource 
   Caption         =   "Form1"
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3165
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows-Standard
   Begin VB.PictureBox picImage 
      AutoSize        =   -1  'True
      Height          =   300
      Left            =   480
      Picture         =   "frmDataType_ImageResource.frx":0000
      ScaleHeight     =   240
      ScaleWidth      =   240
      TabIndex        =   3
      Top             =   1440
      Width           =   300
   End
   Begin VB.PictureBox picDataEditor 
      Appearance      =   0  '2D
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   120
      ScaleHeight     =   855
      ScaleWidth      =   4455
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      Begin VB.CommandButton cmdBrowse 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4080
         TabIndex        =   4
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox txtField 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   3975
      End
      Begin VB.Label lblCaption 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Feldname:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   900
      End
   End
End
Attribute VB_Name = "frmDataType_ImageResource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public MyNode As cGAMRegistryNode
Public MyRenderer As Object

Public Function GetInstance() As frmDataType_String
    Set GetInstance = New frmDataType_String
End Function

Public Function MainContainer() As PictureBox
    Set MainContainer = picDataEditor
End Function

Private Sub cmdBrowse_Click()
 If PromptForSprite("Select Image Resource", "images", False) Then
    txtField.Text = RetSprites(1)
 End If
 
 
End Sub

Private Sub picDataEditor_Resize()
txtField.Width = picDataEditor.ScaleWidth - 2 * txtField.Left - cmdBrowse.Width
cmdBrowse.Left = txtField.Left + txtField.Width
End Sub

Public Sub Initialize(iSubCatLevel As Integer)
txtField.Text = MyNode.Value
lblCaption.Caption = MyNode.FriendlyName & " [" & MyNode.NodeName & "]"
End Sub

Private Sub txtField_Change()
MyNode.Value = txtField.Text
End Sub
