VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmScriptLog 
   Caption         =   "R-PG Log"
   ClientHeight    =   4020
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   4470
   Icon            =   "frmScriptLog.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4020
   ScaleWidth      =   4470
   StartUpPosition =   3  'Windows-Standard
   Begin MSComctlLib.StatusBar STB 
      Align           =   2  'Unten ausrichten
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   3765
      Width           =   4470
      _ExtentX        =   7885
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlLvw 
      Left            =   840
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmScriptLog.frx":23D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmScriptLog.frx":27F1
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmScriptLog.frx":2C14
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmScriptLog.frx":2E89
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmScriptLog.frx":32A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmScriptLog.frx":383E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView LVW 
      Height          =   3735
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   6588
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "imlLvw"
      SmallIcons      =   "imlLvw"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Message"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Timestamp"
         Object.Width           =   3528
      EndProperty
   End
End
Attribute VB_Name = "frmScriptLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private ListItem    As ListItem
Private Size(1)     As Long

Public Sub LogEntry(Msg As String, lt As eLogTypes)
  Set ListItem = LVW.ListItems.Add(, , Msg, lt, lt)
  ListItem.SubItems(1) = Now
  LVW.ListItems(LVW.ListItems.Count).Selected = True
  LVW.ListItems(LVW.ListItems.Count).EnsureVisible
  DoEvents
End Sub

Private Sub Form_Resize()
  LVW.Width = ScaleWidth
  Size(0) = ScaleHeight - stb.Height
  Size(1) = LVW.Width - (LVW.ColumnHeaders(2).Width + stb.Height)
  If Size(0) > 0 Then LVW.Height = Size(0)
  If Size(1) > 0 Then LVW.ColumnHeaders(1).Width = Size(1)
End Sub

