VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSelMap 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "r-pg player - [welcome]"
   ClientHeight    =   5805
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7845
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmSelMap.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmSelMap.frx":08CA
   ScaleHeight     =   5805
   ScaleWidth      =   7845
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkCamera 
      Caption         =   "Information Camera"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5640
      TabIndex        =   25
      Top             =   4920
      Width           =   2055
   End
   Begin MSComctlLib.ListView lvwProjects 
      Height          =   1095
      Left            =   360
      TabIndex        =   19
      Top             =   2640
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   1931
      View            =   2
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "imlProjects"
      SmallIcons      =   "imlProjectsSmall"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Frame FraResourceLink 
      Caption         =   "Resource Link"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   360
      TabIndex        =   20
      Top             =   3720
      Width           =   4935
      Begin VB.TextBox txtResLink 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   21
         Text            =   "maps\start.rmap"
         Top             =   960
         Width           =   4695
      End
      Begin VB.Label lblBPackageRpak 
         Caption         =   "b) Package.rpak://Maps\Filename.rmap    <-- The Package is specified"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   690
         Width           =   4695
      End
      Begin VB.Label lblAMapsFilename 
         Caption         =   "a) Maps\Filename.rmap    <-- Minerva will search for it"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   480
         Width           =   4695
      End
      Begin VB.Label lblEnterAValid 
         Caption         =   "Enter a valid resource link in one of two possible formats:"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   240
         Width           =   4695
      End
   End
   Begin MSComctlLib.TabStrip tsStartMode 
      Height          =   2895
      Left            =   240
      TabIndex        =   18
      Top             =   2280
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   5106
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Project"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Resource Link"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkInformationOSD 
      Caption         =   "Information OSD"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5640
      TabIndex        =   17
      Top             =   4700
      Width           =   2055
   End
   Begin MSComctlLib.ImageList imlProjectsSmall 
      Left            =   4560
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelMap.frx":0C0C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlProjects 
      Left            =   4920
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelMap.frx":14E6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkLogging 
      Caption         =   "Error logging"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5640
      TabIndex        =   14
      Top             =   5160
      Value           =   1  'Checked
      Width           =   2055
   End
   Begin VB.CheckBox chkLogToFile 
      Caption         =   "Log to minerva.log"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5640
      TabIndex        =   13
      Top             =   5400
      Width           =   2055
   End
   Begin VB.Frame fraGFX 
      Caption         =   "GFX Settings"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Left            =   5640
      TabIndex        =   4
      Top             =   1680
      Width           =   2055
      Begin VB.CheckBox chkFullscreen 
         Caption         =   "Fullscreen"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   2400
         Width           =   1815
      End
      Begin VB.CheckBox chkScale 
         Caption         =   "Scale 2x (SW) or Zoom 2x (D3D)"
         Height          =   495
         Left            =   120
         TabIndex        =   11
         Top             =   1920
         Width           =   1575
      End
      Begin VB.ComboBox cboRes 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmSelMap.frx":1DC0
         Left            =   120
         List            =   "frmSelMap.frx":1DCA
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1560
         Width           =   1815
      End
      Begin VB.OptionButton optEngine1 
         Caption         =   "DirectX8 (D3D)"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   480
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.OptionButton optEngine2 
         Caption         =   "Software (DX8 Flip)"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   1815
      End
      Begin VB.OptionButton optEngine3 
         Caption         =   "Software"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Resolution:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Engine:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   1335
      End
   End
   Begin MSComDlg.CommonDialog cdl 
      Left            =   5520
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "Map Files (*.rmap)|*.rmap"
   End
   Begin VB.CheckBox chkDebugCol 
      Caption         =   "Debug Collision"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5640
      TabIndex        =   2
      Top             =   4460
      Width           =   2055
   End
   Begin VB.CommandButton cmdQuit 
      Caption         =   "Quit"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3240
      TabIndex        =   1
      Top             =   5280
      Width           =   2175
   End
   Begin VB.CommandButton cmdLaunch 
      Caption         =   "Launch Game"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   0
      Top             =   5280
      Width           =   2175
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   1500
      Left            =   0
      Picture         =   "frmSelMap.frx":1DE0
      ScaleHeight     =   1500
      ScaleWidth      =   9600
      TabIndex        =   3
      Top             =   0
      Width           =   9600
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "welcome"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   120
      TabIndex        =   16
      Top             =   1560
      Width           =   1395
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "please select the project you would like to try out."
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00808080&
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   1920
      Width           =   4695
   End
End
Attribute VB_Name = "frmSelMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Projs As New CHive




Private Sub chkLogging_Click()
  chkLogToFile.Enabled = chkLogging.Value
End Sub

Private Sub cmdLaunch_Click()

    If lvwProjects.SelectedItem Is Nothing Then
        MsgBox "Please select a project before you continue!", vbInformation, "No Map selected"
        Exit Sub
    End If
    
    Dim ScreenWidth As Long, ScreenHeight As Long
    Scrn_w = Left(cboRes.Text, InStr(1, cboRes.Text, "*") - 1)
    Scrn_h = Mid(cboRes.Text, InStr(1, cboRes.Text, "*") + 1)
    
    frmMain.Width = (Val(IIf(chkScale.Value, Scrn_w * 2, Scrn_w)) * Screen.TwipsPerPixelX) + (frmMain.Width - frmMain.ScaleWidth * Screen.TwipsPerPixelX)
    frmMain.Height = (Val(IIf(chkScale.Value, Scrn_h * 2, Scrn_h)) * Screen.TwipsPerPixelY) + (frmMain.Height - frmMain.ScaleHeight * Screen.TwipsPerPixelY)
    
    FullScrn = (chkFullscreen.Value = 1)
    Debug_Collision = (chkDebugCol.Value = 1)
    InfoOSD = (chkInformationOSD.Value = 1)
    InfoCamera = (chkCamera.Value = 1)
    Logging = (chkLogging.Value = 1)
    LogToGUI = True
    LogToFile = (chkLogToFile.Value = 1)
    InitLogging
    
    CurProj = lvwProjects.SelectedItem.Key
    
    Select Case tsStartMode.SelectedItem.Index
        Case 1
            SelMap = "maps\start.rmap"
        Case 2
            SelMap = txtResLink.Text
    End Select
    
    
    If chkScale.Value = 1 Then
        ZoomF = 2
    Else
        ZoomF = 1
    End If
    
    If optEngine1.Value = True Then engine.Engine_i = 1
    If optEngine2.Value = True Then engine.Engine_i = 2
    If optEngine3.Value = True Then engine.Engine_i = 3
    
    Unload Me
    
End Sub

Private Sub cmdQuit_Click()
    ShowCursor True
    End
End Sub

Private Sub Form_Load()
tsStartMode_Click
    
    ListProjects
    cboRes.ListIndex = 0
    
End Sub



Public Sub ListProjects()

Dim x As String
Dim prj As cGAMProject

lvwProjects.ListItems.Clear
Set Projs = New CHive

x = Dir(UpOneFolder(App.Path) & "\Projects\", vbDirectory)
Do Until x = ""
If Not x = "." And Not x = ".." Then
Set prj = New cGAMProject


    Projs.Add prj, x

End If
x = Dir()
Loop

Dim p As Integer
Do
p = p + 1
If p > Projs.Count Then Exit Do

If Projs(p).LoadHeader(UpOneFolder(App.Path) & "\Projects\" & Projs.Key(p) & "\" & Projs.Key(p) & ".rpak", "", True) Then
    lvwProjects.ListItems.Add , Projs.Key(p), Projs(p).Package.Title & " [" & Projs.Key(p) & "]", 1, 1
Else
    MsgBox "The project folder '" & Projs.Key(p) & "' could not be loaded." & vbCrLf & "(Reason: '" & Projs(p).ErrDescription & "')" & vbCrLf & vbCrLf & "Make sure it is valid or delete the directory to remove this message.", vbInformation, "Invalid Project Folder"
    
    Projs.Remove p
    p = p - 1
End If
Loop


Set prj = Nothing
End Sub

Private Sub lvwProjects_DblClick()
cmdLaunch_Click
End Sub

Private Sub tsStartMode_Click()
Select Case tsStartMode.SelectedItem.Index

    Case 1
        lvwProjects.Height = (FraResourceLink.Top + FraResourceLink.Height - lvwProjects.Top)
        FraResourceLink.Visible = False
    Case 2
        lvwProjects.Height = (FraResourceLink.Top - lvwProjects.Top)
        FraResourceLink.Visible = True
End Select
End Sub
