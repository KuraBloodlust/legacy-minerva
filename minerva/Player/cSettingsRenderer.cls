VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSettingsRenderer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Renderer-Klasse f�r allgemeine Registry-Kategorieelemente

Option Explicit


Public Sub PreInitialize()

End Sub

Public Sub Initialize()

End Sub

Public Function Render(ByRef snode As cGAMRegistryNode, ByRef picSEditor As PictureBox, ByRef x As Long, ByRef y As Long, ByRef SubCatLevel As Integer, ByRef DisplayCollection As CHive)

End Function

Public Function DataToXML(snode As cGAMRegistryNode, XMLNode As IXMLDOMElement, Dom As DOMDocument, Project As cGAMProject) As Boolean
XMLNode.Text = snode.Value
End Function

Public Function XMLToData(snode As cGAMRegistryNode, XMLNode As IXMLDOMNode, Dom As DOMDocument, Project As cGAMProject) As Boolean
On Error Resume Next
snode.Value = XMLNode.Text
End Function

