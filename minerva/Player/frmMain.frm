VERSION 5.00
Begin VB.Form frmMain 
   BackColor       =   &H00000000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "r-pg player - [game window]"
   ClientHeight    =   7200
   ClientLeft      =   8880
   ClientTop       =   6525
   ClientWidth     =   9600
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMain.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   480
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   640
   StartUpPosition =   2  'CenterScreen
   Begin VB.Label lblLoading 
      AutoSize        =   -1  'True
      BackColor       =   &H00000000&
      Caption         =   "R-PG: Minerva is now loading..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0FFC0&
      Height          =   285
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   3555
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then
If FrameSkip > 0 Then FrameSkip = FrameSkip - 1
End If

If KeyCode = vbKeyF4 Then
FrameSkip = FrameSkip + 1
End If

If modPlayer.InitializeComplete = True Then
    GlobalEvent "OnKeyDown"
End If

If KeyCode = vbKeyEscape Then Form_Unload 0
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If modPlayer.InitializeComplete = True Then
    GlobalEvent "OnKeyUp"
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
engine.Unload
End
End Sub

Private Sub Form_Resize()
If Me.WindowState = vbMinimized Then
Do
DoEvents
Loop Until Me.WindowState = vbNormal Or Me.WindowState = vbMaximized
End If

lblLoading.Left = ScaleWidth \ 2 - lblLoading.Width \ 2
lblLoading.Top = ScaleHeight \ 2 - lblLoading.Height \ 2

'picMain.Move 0, 0, Me.ScaleWidth, Me.ScaleHeight
DoResize
End Sub


Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If modPlayer.InitializeComplete = True Then
    GlobalEvent "OnMouseDown"
End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
If ZoomFactor = 0 Then Exit Sub

MousePos.x = (x / ZoomFactor)
MousePos.y = (y / ZoomFactor)



MousePosOnMap.x = MousePos.x + CameraX
MousePosOnMap.y = MousePos.y + CameraY

MousePosOnTile.x = (MousePosOnMap.x - (MousePosOnMap.x Mod Const_TileWidth)) / Const_TileWidth
MousePosOnTile.y = (MousePosOnMap.y - (MousePosOnMap.y Mod Const_TileHeight)) / Const_TileHeight

End Sub

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
If modPlayer.InitializeComplete = True Then
    GlobalEvent "OnMouseUp"
End If
End Sub

