VERSION 5.00
Begin VB.Form frmDataElement_Desc 
   Caption         =   "Form1"
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3165
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows-Standard
   Begin VB.PictureBox picInformation 
      AutoSize        =   -1  'True
      Height          =   300
      Left            =   840
      Picture         =   "frmDataElement_Desc.frx":0000
      ScaleHeight     =   240
      ScaleWidth      =   240
      TabIndex        =   1
      Top             =   2040
      Width           =   300
   End
   Begin VB.PictureBox picDataEditor 
      Appearance      =   0  '2D
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000008&
      BorderStyle     =   0  'Kein
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1335
      Left            =   120
      ScaleHeight     =   89
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   297
      TabIndex        =   0
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "frmDataElement_Desc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public MyNode As cGAMRegistryNode
Public MyRenderer As Object

Public Padding As Integer

Public Function GetInstance() As frmDataElement_Desc
    Set GetInstance = New frmDataElement_Desc
End Function

Public Function MainContainer() As PictureBox
    Set MainContainer = picDataEditor
End Function

Private Sub picDataEditor_Resize()
ReRender
End Sub

Public Sub Initialize(iSubCatLevel As Integer)
ReRender
End Sub


Public Sub ReRender()
Dim s As String
s = MyNode.FriendlyName & ": " & MyNode.Description

Dim lastpos As Integer
Dim pos As Integer
Dim sOut As String
Dim sLine As String
Dim sToAdd As String


Do
    pos = InStr(lastpos + 1, s, " ")
    If pos = 0 Then pos = Len(s)

    sToAdd = Mid(s, lastpos + 1, pos - lastpos)

    If Me.picDataEditor.TextWidth(sLine & sToAdd) >= picDataEditor.ScaleWidth - 2 * Padding Then
        sOut = sOut & sLine & vbCrLf
        sLine = ""
    End If
    
    sLine = sLine & sToAdd
    lastpos = pos
    
    If pos >= Len(s) Then Exit Do
Loop

sOut = sOut & sLine

picDataEditor.Height = 2 * Padding + picDataEditor.TextHeight(sOut)

picDataEditor.Cls

picDataEditor.CurrentY = Padding

Dim t() As String
t = Split(sOut, vbCrLf)

Dim tline
For Each tline In t
picDataEditor.CurrentX = Padding
picDataEditor.Print tline
Next

picDataEditor.Line (0, picDataEditor.ScaleHeight - 1)-(picDataEditor.ScaleWidth, picDataEditor.ScaleHeight - 1), &H80000015

End Sub

