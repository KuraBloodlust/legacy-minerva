VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cEdPath"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private CFm_PathName                                     As String
Private CFm_TileSize                                     As Integer
Private CFm_TileSet                                      As String
Private CFm_C1X1                                         As Long
Private CFm_C1Y1                                         As Long
Private CFm_C2X1                                         As Long
Private CFm_C2Y1                                         As Long
Private CFm_DrawX                                        As Long
Private CFm_DrawY                                        As Long

Public Property Get PathName() As String

    PathName = CFm_PathName

End Property

Public Property Let PathName(ByVal PropVal As String)

    CFm_PathName = PropVal

End Property

Public Property Get TileSize() As Integer
  
  TileSize = CFm_TileSize

End Property

Public Property Let TileSize(ByVal PropVal As Integer)

 CFm_TileSize = PropVal

End Property

Public Property Get TileSet() As String

    TileSet = CFm_TileSet

End Property

Public Property Let TileSet(ByVal PropVal As String)

  
    CFm_TileSet = PropVal

End Property

Public Property Get C1X1() As Long

    C1X1 = CFm_C1X1

End Property

Public Property Let C1X1(ByVal PropVal As Long)

    CFm_C1X1 = PropVal

End Property

Public Property Get C1Y1() As Long

    C1Y1 = CFm_C1Y1

End Property

Public Property Let C1Y1(ByVal PropVal As Long)

    CFm_C1Y1 = PropVal

End Property

Public Property Get C2X1() As Long

    C2X1 = CFm_C2X1

End Property

Public Property Let C2X1(ByVal PropVal As Long)

 
    CFm_C2X1 = PropVal

End Property

Public Property Get C2Y1() As Long

    C2Y1 = CFm_C2Y1

End Property

Public Property Let C2Y1(ByVal PropVal As Long)

 
    CFm_C2Y1 = PropVal

End Property

Public Property Get DrawX() As Long

  
    DrawX = CFm_DrawX

End Property

Public Property Let DrawX(ByVal PropVal As Long)

    CFm_DrawX = PropVal

End Property

Public Property Get DrawY() As Long

    DrawY = CFm_DrawY

End Property

Public Property Let DrawY(ByVal PropVal As Long)

      CFm_DrawY = PropVal

End Property

