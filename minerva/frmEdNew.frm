VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEdNew 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Create New Map"
   ClientHeight    =   4455
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6030
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cboTileFormat 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmEdNew.frx":0000
      Left            =   1800
      List            =   "frmEdNew.frx":000A
      TabIndex        =   13
      Text            =   "16*16"
      Top             =   480
      Width           =   1455
   End
   Begin MSComctlLib.ImageList iml 
      Left            =   960
      Top             =   3240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdNew.frx":001C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdNew.frx":05B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdNew.frx":0B50
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdNew.frx":10EA
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView trv 
      Height          =   2655
      Left            =   120
      TabIndex        =   11
      Top             =   1200
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   4683
      _Version        =   393217
      Indentation     =   0
      Style           =   7
      ImageList       =   "iml"
      Appearance      =   1
   End
   Begin VB.PictureBox picPreview 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   2655
      Left            =   2040
      ScaleHeight     =   173
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   253
      TabIndex        =   10
      Top             =   1200
      Width           =   3855
   End
   Begin VB.CheckBox chkZoom 
      Caption         =   "Zoom"
      Height          =   315
      Left            =   600
      TabIndex        =   7
      Top             =   3990
      Width           =   735
   End
   Begin VB.TextBox txtTitle 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1800
      TabIndex        =   5
      Text            =   "Untitled"
      Top             =   120
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4440
      TabIndex        =   4
      Top             =   3960
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   3
      Top             =   3960
      Width           =   1455
   End
   Begin Minerva.Rm2kScroll rmswidth 
      Height          =   300
      Left            =   4440
      TabIndex        =   8
      Top             =   120
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   529
      Max             =   1024
      Value           =   128
   End
   Begin Minerva.Rm2kScroll rmsheight 
      Height          =   300
      Left            =   4440
      TabIndex        =   9
      Top             =   480
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   529
      Max             =   1024
      Value           =   128
   End
   Begin VB.Image Image2 
      Height          =   360
      Left            =   120
      Picture         =   "frmEdNew.frx":18FC
      Top             =   3960
      Width           =   360
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tile Format:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   720
      TabIndex        =   12
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Map title:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   720
      TabIndex        =   6
      Top             =   120
      Width           =   795
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   120
      Picture         =   "frmEdNew.frx":1D9E
      Top             =   120
      Width           =   480
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Startup Tileset:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   1320
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Map Height:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3360
      TabIndex        =   1
      Top             =   480
      Width           =   1020
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Map Width:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3360
      TabIndex        =   0
      Top             =   120
      Width           =   960
   End
End
Attribute VB_Name = "frmEdNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public CurrentFile As String


Private Sub chkZoom_Click()
    GenPreview
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
If trv.SelectedItem Is Nothing Then Exit Sub

Dim p As String, u As String
p = modEdPackageLister.GetPathFromTags(trv.SelectedItem)
u = modEdPackageLister.ObtainResourceURL(p, "tilesets")


Dim FoundMatch As Boolean
Dim myRegExp As RegExp
Set myRegExp = New RegExp
myRegExp.Pattern = "^(\d+)\*(\d+)$"
FoundMatch = myRegExp.Test(cboTileFormat.Text)

If Not FoundMatch Then
    MsgBox "Please insert a valid tile format!", vbCritical, "Error"
    Exit Sub
End If

Dim myMatches As MatchCollection
Set myMatches = myRegExp.Execute(cboTileFormat.Text)
If myMatches.Count >= 1 Then
    Const_TileWidth = myMatches(0).SubMatches(0)
    Const_TileHeight = myMatches(0).SubMatches(1)
End If


  Dim ts   As New cGAMTileset
  'Dim ts2  As New cGAMTileset
  Dim lvl  As New cGAMLevel

    'Dim X    As Integer
    'Dim Y    As Integer
  Dim lyr2 As New cGAMLayer
  'Dim lyr  As New cGAMLayer
  'Dim lvl2 As New cGAMScriptLevel
    Set Map = New cGAMMap
    
    With Map
        Set .MyS2D = GFX
        .CFm_MapFile = CurrentFile
        .MapWidth = rmswidth.Value
        .MapHeight = rmsheight.Value
        .MapTitle = txtTitle.Text
        .newmap
    End With 'MAP
    
    With ts
        Set .MyMap = Map
        .Initialize u
        Map.TileSets.Add ts
    End With 'ts

    With lvl
        Set .MyMap = Map
        .Initialize
        .Desc = "Level 1"
    End With 'lvl
    
    Map.Levels.Add lvl
    Set lyr2.MyLevel = lvl
    
    lyr2.Desc = "Layer 1"
    lyr2.Initialize
    lvl.Layers.Add lyr2

ScrollX = 0
ScrollY = 0

CurrentLayer = 1
CurrentLevel = 1
CurrentTileset = 1
SelNPC = 0

frmEdMap.ListStuff
frmEdMap.ShowView 0
frmEdMap.cboTileset.ListIndex = 0
Mode = 1
UseTools = True
frmEdMap.tlbTools.Visible = UseTools
frmEdMap.UpdateCtls

Unload Me
End Sub

Private Function GenPreview()
    picPreview.Cls
    If trv.SelectedItem Is Nothing Then
      cmdOK.Enabled = False
      Exit Function
    End If
    
    If Not ValidFileExt(trv.SelectedItem.Text, "png|gif|jpg|bmp") Then
      cmdOK.Enabled = False
      Exit Function
    End If
    Dim F As String, tf As Boolean
    
    Dim p As String, u As String
    p = modEdPackageLister.GetPathFromTags(trv.SelectedItem)
    u = modEdPackageLister.ObtainResourceURL(p, "tilesets")
    
    F = ProvideFile(u, tf)
    
    RenderImageToHDC F, picPreview.hdc, 0, 0, 0, 0, IIf(chkZoom.Value, picPreview.ScaleWidth, -1), IIf(chkZoom.Value, picPreview.ScaleHeight, -1)
    
    If tf Then Kill F
    
    picPreview.Refresh
    cmdOK.Enabled = True
End Function

Private Sub filTileset_Click()
    GenPreview
End Sub


Public Sub ListTilesets()
'hallo semi, hier die tilesets aus den rpaks laden.

modEdPackageLister.ListProjectAndFSToTreeView trv, "png|gif|bmp", "tilesets"


End Sub

Private Sub Form_Load()
ListTilesets
End Sub


Private Sub trv_Click()
    GenPreview
End Sub
