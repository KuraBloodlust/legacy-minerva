VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEdSelectProject 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "R-PG Minerva - Choose Project"
   ClientHeight    =   5340
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5145
   Icon            =   "frmEdSplash.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5340
   ScaleWidth      =   5145
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmr 
      Interval        =   500
      Left            =   3480
      Top             =   5040
   End
   Begin MSComctlLib.StatusBar stb 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   10
      Top             =   5085
      Width           =   5145
      _ExtentX        =   9075
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   0
      Picture         =   "frmEdSplash.frx":08CA
      ScaleHeight     =   855
      ScaleWidth      =   5145
      TabIndex        =   8
      Top             =   0
      Width           =   5145
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Select Project"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1920
         TabIndex        =   9
         Top             =   480
         Width           =   1830
      End
   End
   Begin VB.Frame fraDetails 
      Caption         =   "Project Details"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   120
      TabIndex        =   0
      Top             =   3120
      Width           =   4935
      Begin VB.Label lblShortDesc 
         Caption         =   "Short Desc:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   4650
      End
      Begin VB.Label lblLongDesc 
         Caption         =   "Long Desc:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1275
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   4680
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Project Chooser"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   120
      TabIndex        =   3
      Top             =   960
      Width           =   4935
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Delete Project"
         Height          =   375
         Left            =   3600
         TabIndex        =   11
         Top             =   1560
         Width           =   1215
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3600
         TabIndex        =   6
         Top             =   720
         Width           =   1215
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3600
         TabIndex        =   7
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton cmdCreate 
         Caption         =   "Create New ..."
         Height          =   375
         Left            =   3600
         TabIndex        =   5
         Top             =   1200
         Width           =   1215
      End
      Begin MSComctlLib.ListView lvwProjects 
         Height          =   1575
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   2778
         View            =   1
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         Icons           =   "imlProjects"
         SmallIcons      =   "imlProjectsSmall"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ImageList imlProjects 
         Left            =   3840
         Top             =   1080
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEdSplash.frx":6DB2
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ImageList imlProjectsSmall 
         Left            =   4200
         Top             =   1200
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEdSplash.frx":768C
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "frmEdSelectProject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Projs As New CHive

Public Sub ListProjects()
On Error Resume Next
Dim x As String
Dim prj As cGAMProject

lvwProjects.ListItems.Clear
Set Projs = New CHive

x = Dir(App.Path & "\Projects\", vbDirectory)
Do Until x = ""
If Not x = "." And Not x = ".." Then
Set prj = New cGAMProject
    Projs.Add prj, x
End If
x = Dir()
Loop

Dim p As Integer
Do
p = p + 1
If p > Projs.Count Then Exit Do

If Projs(p).LoadHeader(App.Path & "\Projects\" & Projs.Key(p) & "\" & Projs.Key(p) & ".rpak", "", True) Then
    Dim iCount As Integer
    If LoadedDependencies.Count > 0 Then
      For iCount = 1 To LoadedDependencies.Count + 1
        LoadedDependencies.Remove iCount
      Next iCount
    End If
    lvwProjects.ListItems.Add , Projs.Key(p), Projs(p).Package.Title & " [" & Projs.Key(p) & "]", 1, 1
Else
    MsgBox "The project folder '" & Projs.Key(p) & "' could not be loaded." & vbCrLf & "(Reason: '" & Projs(p).ErrDescription & "')" & vbCrLf & vbCrLf & "Make sure it is valid or delete the directory to remove this message.", vbInformation, "Invalid Project Folder"
    
    Projs.Remove p
    p = p - 1
End If
Loop

Set prj = Nothing
End Sub

Private Sub cmdCancel_Click()
Unload Me
End
End Sub

Private Sub cmdCreate_Click()
frmEdNewProject.PackageMode = False
frmEdNewProject.Show vbModal
Form_Load
End Sub

Private Sub cmdDelete_Click()
If lvwProjects.SelectedItem Is Nothing Then Exit Sub

WindowsDelete App.Path & "\Projects\" & lvwProjects.SelectedItem.Key

Form_Load
End Sub

Private Sub cmdOK_Click()
lvwProjects_DblClick
End Sub

Private Sub Form_Load()
ListProjects
lvwProjects_Click
End Sub

Private Sub lvwProjects_Click()
If lvwProjects.SelectedItem Is Nothing Then Exit Sub
Dim prj As cGAMProject

Set prj = Projs(lvwProjects.SelectedItem.Key)

fraDetails.Caption = prj.Package.Title
lblShortDesc.Caption = prj.Package.ShortDesc
lblLongDesc.Caption = prj.Package.LongDesc
End Sub

Private Sub lvwProjects_DblClick()
If lvwProjects.SelectedItem Is Nothing Then Exit Sub

CurProj = lvwProjects.SelectedItem.Key
Unload Me
End Sub

Private Sub tmr_Timer()
Dim UsageTime As Date
UsageTime = GetSetting("R-PG Minerva", "Statistics", "UsageTime", #12:00:00 AM#)

stb.SimpleText = "Total Minerva usage time so far: " & (UsageTime + (Now - StartupTime))


End Sub
