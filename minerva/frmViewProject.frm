VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmViewProject 
   Caption         =   "Project"
   ClientHeight    =   3540
   ClientLeft      =   165
   ClientTop       =   780
   ClientWidth     =   4140
   Icon            =   "frmViewProject.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3540
   ScaleWidth      =   4140
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog cdlFiles 
      Left            =   3360
      Top             =   2280
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "All files (*.*)|*.*"
   End
   Begin MSComctlLib.ImageList iml 
      Left            =   3360
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmViewProject.frx":058A
            Key             =   ":rpak"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmViewProject.frx":0D9C
            Key             =   ":fldrclose"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmViewProject.frx":1336
            Key             =   ":fldropen"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmViewProject.frx":18D0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView trv 
      Height          =   2895
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   5106
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   0
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "iml"
      Appearance      =   0
      OLEDragMode     =   1
      OLEDropMode     =   1
   End
   Begin VB.Menu mnuProject 
      Caption         =   "(ProjectContext)"
      Begin VB.Menu mnuProjectAddFile 
         Caption         =   "Add File"
         Begin VB.Menu mnuProjectAddFileBrowse 
            Caption         =   "From existing file..."
         End
         Begin VB.Menu mnuProjectAddFileCreate 
            Caption         =   "Create new..."
         End
      End
      Begin VB.Menu mnuProjectAddFolder 
         Caption         =   "Create Folder"
      End
      Begin VB.Menu mnuProjectDivider1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuProjectDelete 
         Caption         =   "Delete"
      End
      Begin VB.Menu mnuProjectDivider2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuProjectProperties 
         Caption         =   "Properties"
      End
   End
End
Attribute VB_Name = "frmViewProject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    mnuProject.Visible = False
End Sub

Private Sub Form_Resize()
    trv.Move 0, 0, Me.ScaleWidth, Me.ScaleHeight
End Sub

Public Sub Update()
    trv.Nodes.Clear

    ListPckg Project.Package

    Dim i As Integer

    For i = 1 To LoadedDependencies.Count
        ListPckg LoadedDependencies(i)
    Next i

End Sub

Private Sub ListPckg(Pck As cGAMPackage)
    Dim n As Node

    Dim FN As String
    FN = Pck.RPAK.FileName

    FN = Mid$(FN, InStrRev(FN, "\") + 1)

    Set n = trv.Nodes.Add(, , FN, FN, 1)
    n.Tag = FN

    ListPckgFolder Pck.RPAK.Root, n

End Sub

Private Sub ListPckgFolder(F As cRPakFolder, _
                           ParentNode As Node)

    Dim n As Node
    Dim i As Integer
    Dim FN As String
    Dim ft As String
    Dim Pos As Integer
    Dim ico As Integer

    For i = 0 To F.FileCount - 1
        FN = F.FileName(i)
    
        Pos = InStrRev(FN, ".")
    
        If Pos > 0 Then
            ft = Mid$(FN, Pos + 1)
        
            ico = IMLFindKey(LCase(ft))

            If ico = 0 Then ico = MakeNewTypeIcon(ft)
        
        End If
    
        Set n = trv.Nodes.Add(, , , FN, ico)
        n.Tag = FN
        Set n.Parent = ParentNode
    Next i

    For i = 0 To F.FolderCount - 1
        FN = F.Folders(i).FolderName

        Set n = trv.Nodes.Add(, , , FN, 2)
        Set n.Parent = ParentNode
        n.Tag = FN
        n.ExpandedImage = 3
    
        ListPckgFolder F.Folders(i), n
    
        n.Expanded = False
    Next i

End Sub

Private Function IMLFindKey(Key As String) As Integer
    Dim i As Integer

    For i = 1 To iml.ListImages.Count

        If iml.ListImages(i).Key = "ft_" & Key Then IMLFindKey = i: Exit For
    Next i

End Function

Public Function MakeNewTypeIcon(TypeName As String) As Integer
    Dim tp As String
    tp = Icon_GetTempPath(TypeName)

    Dim ff As Integer
    ff = FreeFile

    Open tp For Output As ff
    Close ff

    iml.ListImages.Add , "ft_" & TypeName, GetDisplayedIcon(tp, shgfiSmall)

    Kill tp

    MakeNewTypeIcon = iml.ListImages.Count
End Function

Private Function Icon_GetTempPath(TypeName As String) As String
    Dim i As Integer
    Dim F As String

    Do
        i = i + 1

        F = App.Path & "\" & TypeName & i & "." & TypeName

        If Dir(F) = "" Then Exit Do
    Loop

    Icon_GetTempPath = F
End Function



Private Sub mnuProjectAddFileBrowse_Click()

    cdlFiles.ShowOpen
    If cdlFiles.FileName = "" Then Exit Sub
    
    Dim F As String
    F = File2Str(cdlFiles.FileName)
    
    
    Dim FN As String
    FN = Mid$(cdlFiles.FileName, InStrRev(cdlFiles.FileName, "\") + 1)
    
    Dim p As String, u As String
    p = GetPathFromTags(trv.SelectedItem)


    If InStr(1, p, "\") = 0 Then
        u = p & "://"
    Else
        u = GetTreeResourceURL(p)
    
        If ProvideDoesExist(u, False) Then
    
            Dim Pos As Integer, pos2 As Integer
            Pos = InStrRev(u, "\")
            pos2 = InStrRev(u, "/")
        
            u = Left$(u, max(CLng(Pos), CLng(pos2)))
        Else
            u = u & "\"
        End If
    End If
    
    StoreFile u & FN, F
    
    
    Update
End Sub

Private Sub mnuProjectAddFileCreate_Click()
    Dim p As String, u As String
    p = GetPathFromTags(trv.SelectedItem)
    
    If InStr(1, p, "\") = 0 Then
        u = p & "://"
    Else
        u = GetTreeResourceURL(p)
    
        If ProvideDoesExist(u, False) Then
    
            Dim Pos As Integer, pos2 As Integer
            Pos = InStrRev(u, "\")
            pos2 = InStrRev(u, "/")
        
            u = Left$(u, max(CLng(Pos), CLng(pos2)))
        Else
            u = u & "\"
        End If
    End If

    frmCreateNewFile.BasePath = u
    frmCreateNewFile.Show vbModal
End Sub

Private Sub mnuProjectAddFolder_Click()
    
    Dim x As String
    x = InputBox("Please enter a name for the new directory:", "Create Directory")
    
    If x = "" Then Exit Sub

    Dim p As String, u As String
    p = GetPathFromTags(trv.SelectedItem)
    
    If InStr(1, p, "\") = 0 Then
        u = p & "://"
    Else
        u = GetTreeResourceURL(p)
    
        If ProvideDoesExist(u, False) Then
    
            Dim Pos As Integer, pos2 As Integer
            Pos = InStrRev(u, "\")
            pos2 = InStrRev(u, "/")
        
            u = Left$(u, max(CLng(Pos), CLng(pos2)))
        Else
            u = u & "\"
        End If
    End If
    
    modFileProvider.MakeDirectory u & x
    
    Update
End Sub

Private Sub mnuProjectDelete_Click()
 Dim p As String, u As String
    p = GetPathFromTags(trv.SelectedItem)


    u = GetTreeResourceURL(p)
        u = GetTreeResourceURL(p)
    
    If ProvideDoesExist(u, False) Then
        If MsgBox("Really delete the file '" & u & "'?" & vbCrLf & "You will not be able to undo the changes!", vbCritical + vbOKCancel, "Delete File") = vbOK Then modFileProvider.DeleteFile u
    Else
        If MsgBox("Really delete the folder '" & u & "'?" & vbCrLf & "You will not be able to undo the changes!", vbCritical + vbOKCancel, "Delete File") = vbOK Then modFileProvider.DeleteFile u, True
    End If
    
    Update
    

End Sub

Private Sub mnuProjectProperties_Click()

    If trv.SelectedItem Is Nothing Then Exit Sub
    If Not trv.SelectedItem.Image = 1 Then Exit Sub
    
    Dim p As String, u As String
    p = GetPathFromTags(trv.SelectedItem)
    
    If InStr(1, p, "\") = 0 Then
        u = p & "://"
    Else
        u = GetTreeResourceURL(p)
    
        If ProvideDoesExist(u, False) Then
    
            Dim Pos As Integer, pos2 As Integer
            Pos = InStrRev(u, "\")
            pos2 = InStrRev(u, "/")
        
            u = Left$(u, max(CLng(Pos), CLng(pos2)))
        Else
            u = u & "\"
        End If
    End If
    
    Dim CurrentFolder As cRPakFolder
    Set CurrentFolder = modFileProvider.GetRPAKFolder(u)
    
    If CurrentFolder.MyArchive Is Project.Package.RPAK Then
         CurrentPackage = -1
    Else
        CurrentPackage = 0
        Dim i As Integer
        For i = 1 To LoadedDependencies.Count
        
            If CurrentFolder.MyArchive Is LoadedDependencies.Item(i).RPAK Then
                CurrentPackage = i
                Exit For
            End If
        Next i
    
    End If
    
    
    If Not CurrentPackage = 0 Then
        frmPackageProperties.Show vbModal
        Update
    End If

End Sub

Private Sub trv_DblClick()

    If trv.SelectedItem Is Nothing Then Exit Sub
    If trv.SelectedItem.Image = 1 Or trv.SelectedItem.Image = 2 Then Exit Sub

    Dim p As String, u As String
    p = GetPathFromTags(trv.SelectedItem)
    u = GetTreeResourceURL(p)

    PluginHost.AttemptEdit u

End Sub

Private Function GetTreeResourceURL(TRVPath As String) As String
    Dim outPath As String
    Dim pakFile As String
    Dim Pos As Integer

    outPath = TRVPath

    Pos = InStr(1, outPath, ".")

    If Pos = 0 Then Exit Function

    Pos = InStr(1, outPath, "\")

    If Pos <= 5 Then
        outPath = "folder://" & outPath

    Else
    
        If LCase(Mid(outPath, Pos - 5, 5)) = ".rpak" Then
            pakFile = Left(outPath, Pos - 1)
            outPath = pakFile & "://" & Mid(outPath, Pos + 1)
        Else
            outPath = "folder://" & outPath
        End If

    End If

    GetTreeResourceURL = outPath
End Function

Private Sub trv_MouseUp(Button As Integer, _
                        Shift As Integer, _
                        x As Single, _
                        y As Single)

    If Button = vbRightButton Then
        PopupMenu mnuProject
    End If

End Sub

Private Sub trv_OLEDragDrop(Data As MSComctlLib.DataObject, _
                            Effect As Long, _
                            Button As Integer, _
                            Shift As Integer, _
                            x As Single, _
                            y As Single)
    Dim i As Integer

    Dim p As String, u As String
    p = GetPathFromTags(trv.SelectedItem)
    
    If InStr(1, p, "\") = 0 Then
        u = p & "://"
    Else
        u = GetTreeResourceURL(p)
    
        If ProvideDoesExist(u, False) Then
    
            Dim Pos As Integer, pos2 As Integer
            Pos = InStrRev(u, "\")
            pos2 = InStrRev(u, "/")
        
            u = Left$(u, max(CLng(Pos), CLng(pos2)))
        Else
            u = u & "\"
        End If
    End If
    
    Dim CurrentFolder As cRPakFolder
    Set CurrentFolder = modFileProvider.GetRPAKFolder(u)

    For i = 1 To Data.Files.Count

        If IsDirectory(Data.Files(i)) Then
            AddFilesRecursive Data.Files(i), CurrentFolder
        Else
            AddFile Data.Files(i), CurrentFolder
        End If

    Next i

    Update

    Exit Sub

End Sub


Public Sub AddFilesRecursive(Path As String, _
                             Folder As cRPakFolder)
    Dim x As String
    Dim nf As cRPakFolder


    Dim i As Integer
    Dim r As VbMsgBoxResult
    Dim s As String
    
    s = Mid(Path, InStrRev(Path, "\") + 1)
    i = Folder.FindFolder(s)
    
    If i >= 0 Then
        Set nf = Folder.Folders(i)
    Else
        Set nf = Folder.AddFolder(s)
    End If
    
    
    x = Dir(Path & "\*.*")

    Do Until x = ""

        i = nf.FindFile(x)
        If i >= 0 Then
            r = MsgBox("File '" & x & "' already exists. Replace it?", vbQuestion + vbOKCancel, "File already exists")
            If r = vbOK Then
                nf.FileContent(i) = File2Str(Path & "\" & x)
            End If
        Else
            nf.AddFile x, File2Str(Path & "\" & x)
        End If

        x = Dir()
    Loop

    Dim filestoadd As New Collection

    x = Dir(Path & "\", vbDirectory)

    Do Until x = ""

        If Not (x = "." Or x = "..") Then
            If IsDirectory(Path & "\" & x) Then filestoadd.Add Path & "\" & x
        End If

        x = Dir()
    Loop


    For i = 1 To filestoadd.Count
        AddFilesRecursive filestoadd(i), nf
    Next i

End Sub

Public Function IsDirectory(p As String)
IsDirectory = ((GetAttr(p) And vbDirectory) = vbDirectory)
End Function

Public Sub AddFile(FileName As String, CurrentFolder As cRPakFolder)
    Dim i As Integer, s As String
    Dim r As VbMsgBoxResult
    
    s = Mid(FileName, InStrRev(FileName, "\") + 1)
    i = CurrentFolder.FindFile(s)

    If i >= 0 Then
        r = MsgBox("File '" & s & "' already exists. Replace it?", vbQuestion + vbOKCancel, "File already exists")

        If r = vbOK Then
            CurrentFolder.FileContent(i) = File2Str(FileName)
        End If

    Else
        CurrentFolder.AddFile s, File2Str(FileName)
    End If

End Sub

Private Sub trv_OLEStartDrag(Data As MSComctlLib.DataObject, _
                             AllowedEffects As Long)

    If trv.SelectedItem Is Nothing Then Exit Sub

    Data.Clear
    Data.Files.Clear


    Dim tp As String
    tp = String(260, Chr(0))

    GetTempPath Len(tp), tp
    tp = Replace(tp, Chr(0), "")

    Dim FN As String
    FN = tp & trv.SelectedItem.Text

    If FileExists(FN) Then Kill FN

Dim i As Integer

    Dim p As String, u As String
    p = GetPathFromTags(trv.SelectedItem)
    u = GetTreeResourceURL(p)

    If modFileProvider.ProvideDoesExist(u, True) Then Exit Sub
    
    
    Data.SetData , vbCFFiles

    Open FN For Binary Access Write As 1
    
    Dim s As String
    Dim tf As Boolean
    
    s = ProvideFile(u, tf)
    
    Put #1, , File2Str(s)
        If tf Then Kill s
    Close 1

    Data.Files.Add FN

    AllowedEffects = vbDropEffectCopy Or vbDropEffectMove

End Sub
