VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmParticle 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Particle Preview"
   ClientHeight    =   6300
   ClientLeft      =   2565
   ClientTop       =   1500
   ClientWidth     =   8505
   Icon            =   "frmParticle.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   420
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   567
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fmeEmpty 
      Height          =   1335
      Left            =   2640
      TabIndex        =   34
      Top             =   4560
      Width           =   2535
      Begin VB.Timer tmrDraw 
         Enabled         =   0   'False
         Interval        =   1
         Left            =   1800
         Top             =   1320
      End
      Begin MSComDlg.CommonDialog cdColor 
         Left            =   2040
         Top             =   1320
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Image imgBack 
         Height          =   1230
         Left            =   -360
         Picture         =   "frmParticle.frx":000C
         Top             =   240
         Width           =   3195
      End
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Height          =   375
      Left            =   5280
      TabIndex        =   3
      Top             =   5520
      Width           =   3015
   End
   Begin VB.Frame fmeOptions 
      Caption         =   "Options"
      Height          =   1455
      Left            =   120
      TabIndex        =   30
      Top             =   4440
      Width           =   2415
      Begin VB.CheckBox chkAdditive 
         Caption         =   "Draw Additive"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   1080
         Width           =   2175
      End
      Begin VB.CheckBox chkOverride 
         Caption         =   "Override AniQ Animations"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   720
         Width           =   2175
      End
      Begin VB.CheckBox FollowM 
         Caption         =   "Follow Mouse"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.Frame fmeSettings 
      Caption         =   "Settings:"
      Height          =   4335
      Left            =   5280
      TabIndex        =   33
      Top             =   120
      Width           =   3015
      Begin VB.CommandButton cmdCol 
         BackColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   3960
         Width           =   1455
      End
      Begin Minerva.Rm2kScroll scrSet 
         Height          =   300
         Index           =   8
         Left            =   120
         TabIndex        =   12
         Top             =   3480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         Max             =   100
         Value           =   10
      End
      Begin Minerva.Rm2kScroll scrSet 
         Height          =   300
         Index           =   7
         Left            =   120
         TabIndex        =   11
         Top             =   3120
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         Max             =   100
      End
      Begin Minerva.Rm2kScroll scrSet 
         Height          =   300
         Index           =   6
         Left            =   120
         TabIndex        =   10
         Top             =   2760
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         Max             =   100
      End
      Begin Minerva.Rm2kScroll scrSet 
         Height          =   300
         Index           =   5
         Left            =   120
         TabIndex        =   9
         Top             =   2280
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         Min             =   -10
         Max             =   10
         Value           =   0
      End
      Begin Minerva.Rm2kScroll scrSet 
         Height          =   300
         Index           =   4
         Left            =   120
         TabIndex        =   8
         Top             =   1920
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         Max             =   200
         Value           =   50
      End
      Begin Minerva.Rm2kScroll scrSet 
         Height          =   300
         Index           =   3
         Left            =   120
         TabIndex        =   7
         Top             =   1560
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         Max             =   999
         Value           =   100
      End
      Begin Minerva.Rm2kScroll scrSet 
         Height          =   300
         Index           =   2
         Left            =   120
         TabIndex        =   6
         Top             =   1080
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         Min             =   -50
         Max             =   50
         Value           =   0
      End
      Begin Minerva.Rm2kScroll scrSet 
         Height          =   300
         Index           =   1
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         Min             =   -50
         Max             =   50
         Value           =   0
      End
      Begin Minerva.Rm2kScroll scrSet 
         Height          =   300
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         Max             =   1000
         Value           =   100
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Duration"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1680
         TabIndex        =   29
         Top             =   1920
         Width           =   735
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Gravitation(Y)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1680
         TabIndex        =   28
         Top             =   1080
         Width           =   1200
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Gravitation(X)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1680
         TabIndex        =   27
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Sprite Count"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1680
         TabIndex        =   26
         Top             =   1560
         Width           =   1080
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Power"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1680
         TabIndex        =   25
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Rotation "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1680
         TabIndex        =   24
         Top             =   2280
         Width           =   765
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Zoom Out"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1680
         TabIndex        =   23
         Top             =   2760
         Width           =   855
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Zoom In"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1680
         TabIndex        =   22
         Top             =   3120
         Width           =   735
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Zoom Speed"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1680
         TabIndex        =   21
         Top             =   3480
         Width           =   1095
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Color"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1680
         TabIndex        =   20
         Top             =   3960
         Width           =   465
      End
   End
   Begin MSComctlLib.StatusBar Status 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   32
      Top             =   6045
      Width           =   8505
      _ExtentX        =   15002
      _ExtentY        =   450
      SimpleText      =   "Ready"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "Ready"
            TextSave        =   "Ready"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12383
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdChoose 
      Caption         =   "Choose &Particle Image"
      Default         =   -1  'True
      Height          =   375
      Left            =   5280
      TabIndex        =   0
      Top             =   4560
      Width           =   3015
   End
   Begin VB.PictureBox picPrev 
      BackColor       =   &H00000000&
      Height          =   4335
      Left            =   120
      ScaleHeight     =   285
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   333
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   120
      Width           =   5055
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   3
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample4 
         Caption         =   "Sample 4"
         Height          =   1785
         Left            =   2100
         TabIndex        =   19
         Top             =   840
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   1
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample2 
         Caption         =   "Sample 2"
         Height          =   1785
         Left            =   645
         TabIndex        =   18
         Top             =   300
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Generate Script"
      Enabled         =   0   'False
      Height          =   375
      Left            =   5280
      TabIndex        =   1
      Top             =   5040
      Width           =   3015
   End
End
Attribute VB_Name = "frmParticle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetSystemDefaultLCID _
    Lib "kernel32" () As Long

Private Declare Function GetLocaleInfo Lib "kernel32" _
    Alias "GetLocaleInfoA" ( _
    ByVal Locale As Long, _
    ByVal LCType As Long, _
    ByVal lpLCData As String, _
    ByVal cchData As Long) As Long
        
Private Const LOCALE_SDECIMAL As Long = &HE
Private Const strName         As String = "Sample"
Private Const strCaption      As String = "Script Export"
Private Const strExport       As String = "Code was copied to Clipboard"
Private Const strImgText      As String = "Image: "

Private Effect      As New cS2DParticle
Dim m_x             As Integer
Dim m_y             As Integer
Dim s_x             As Integer
Dim s_y             As Integer
Dim oldzoom         As Integer
Dim blnScroll       As Boolean
Dim blnEffect       As Boolean
Dim strImage        As String
Dim MouseX          As Double
Dim MouseY          As Double
Dim lngTrans        As Long

Private Sub Form_Load()
  With picPrev
    DoResize picPrev, .ScaleWidth, .ScaleHeight
  End With
  
  oldzoom = ZoomFactor
  ZoomFactor = 1
  blnEffect = False
  s_x = 0
  s_y = 0
  MouseX = 0
  MouseY = 0

  Set Effect = Nothing
  Set Effect.S2D = GFX

  Flatten cmdChoose.hwnd
  Flatten cmdOK.hwnd
  tmrDraw.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
  tmrDraw.Enabled = False
  DoResize
  Effect.Unload
  ZoomFactor = oldzoom
End Sub

Private Sub cmdChoose_Click()
  If PromptForSprite("Set Particle Sprite", "images", False) Then
    strImage = RetSprites(1)
    lngTrans = RetColorKey
    RecreateParticle
    blnEffect = True
    cmdOK.Enabled = True
    Status.Panels(2).Text = strImgText & strImage
  End If
End Sub

Private Function GetEntry(ByRef lngID As Long) As String
Dim lngLCID    As Long
Dim lngResult  As Long
Dim lngLength  As Long
Dim strBuffer  As String

  lngLCID = GetSystemDefaultLCID()
  lngLength = GetLocaleInfo(lngLCID, lngID, strBuffer, 0) - 1
  strBuffer = Space(lngLength + 1)
  lngResult = GetLocaleInfo(lngLCID, lngID, strBuffer, lngLength)
  GetEntry = Left$(strBuffer, lngLength)
End Function

Private Sub scrSet_Change(Index As Integer)
  RecreateParticle
End Sub

Private Sub chkAdditive_Click()
  RecreateParticle
End Sub

Private Sub cmdCol_Click()
  cdColor.ShowColor
  cmdCol.BackColor = cdColor.Color
  RecreateParticle
End Sub

Private Sub cmdClose_Click()
  Unload Me
End Sub

Private Sub tmrDraw_Timer()

  With GFX
  .Resize picPrev.ScaleWidth, picPrev.ScaleHeight
  .Clear
  .BeginScene
  End With
  
  With picPrev
  If Map.MapHeight < .ScaleHeight / Const_TileHeight Then
  s_y = .ScaleHeight / 2 - (Map.MapHeight * Const_TileHeight) / 2
  End If
    
  If Map.MapWidth < .ScaleWidth / Const_TileWidth Then
  s_x = .ScaleWidth / 2 - (Map.MapWidth * Const_TileWidth) / 2
  End If
      
  TranspBack.RenderTiled s_x Mod 32 - 32, s_y Mod 32 - 32, _
                        .ScaleWidth + 128, .ScaleHeight + 128
  End With
    
  Map.NoBuffer = True
  Map.Render s_x, s_y, 0, 1000
    
  If blnEffect Then
    If FollowM.Value = 1 Then
      Effect.SetPositionByID 1, m_x, m_y
      Effect.RenderAll
    Else
      Effect.SetPositionByID 1, m_x, m_y
      Effect.RenderAll 'm_x, m_y
    End If
  End If
    
  GFX.EndScene
  GFX.Flip picPrev.hwnd
End Sub

Private Function RecreateParticle()

    If Right(LCase(strImage), 3) = "png" _
    Or Right(LCase(strImage), 4) = "aniq" Then
          Effect.CreateParticles strName, _
          strImage, lngTrans, MouseX - s_x, MouseY - s_y, _
          scrSet(0).Value / 100, scrSet(1).Value / 100, _
          scrSet(2).Value / 100, scrSet(3).Value, _
          cmdCol.BackColor, chkAdditive.Value, _
          scrSet(4).Value / 100, scrSet(6).Value, _
          scrSet(7).Value, scrSet(8).Value / 100, scrSet(5).Value
    End If
    
End Function

Private Sub picPrev_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  If Button = 2 Or Button = 1 Or FollowM.Value = 1 Then
    m_x = X
    m_y = y
    blnScroll = True
  End If
    
End Sub

Private Sub picPrev_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
  If Button = 2 Then
    blnScroll = False
  End If
End Sub
Private Sub cmdOK_Click()
Dim strScript As String

  strScript$ = _
  "Particles.CreateParticles " & Chr(34) _
  & strName & Chr(34) & ", " & Chr(34) & _
  strImage & Chr(34) & ", " & _
  ConvertCommas(lngTrans) & ", " & _
  ConvertCommas(m_x - s_x) & ", " & _
  ConvertCommas(m_y - s_y) & ", " & _
  ConvertCommas(scrSet(0).Value / 100) & ", " & _
  ConvertCommas(scrSet(1).Value / 100) & ", " & _
  ConvertCommas(scrSet(2).Value / 100) & ", " & _
  ConvertCommas(scrSet(3).Value) & ", " & _
  ConvertCommas(cmdCol.BackColor) & ", " & _
  ConvertCommas(chkAdditive.Value) & ", " & _
  ConvertCommas(scrSet(4).Value / 100) & ", " & _
  ConvertCommas(scrSet(6).Value) & ", " & _
  ConvertCommas(scrSet(7).Value) & ", " & _
  ConvertCommas(scrSet(8).Value / 100) & ", " & _
  ConvertCommas(scrSet(5).Value) & _
  IIf(chkOverride.Value = Checked, _
  vbCrLf & "Particles.OverrideAniq (" & Chr(34) & strName & Chr(34) & ")", "")
  
  Clipboard.Clear
  Clipboard.SetText strScript

  MsgBox strExport, vbInformation, strCaption
End Sub

Private Function ConvertCommas(ByVal strText As String) As String
  ConvertCommas$ = Replace(strText, GetEntry(LOCALE_SDECIMAL), ".")
End Function

Private Sub chkOverride_Click()
  Effect.OverrideAniq strName
End Sub

Private Sub picPrev_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
  If Button = 2 And blnScroll Then
    s_x = s_x - (m_x - X)
    s_y = s_y - (m_y - y)
    m_x = X
    m_y = y
    
    If s_x > 0 Then s_x = 0
    If s_y > 0 Then s_y = 0
    
    If s_x - ScreenWidth < 0 - Map.MapWidth * Const_TileWidth Then
      s_x = 0 - Map.MapWidth * Const_TileWidth + ScreenWidth
    End If
    
    If s_y - ScreenHeight < 0 - Map.MapHeight * Const_TileHeight Then
      s_y = 0 - Map.MapHeight * Const_TileHeight + ScreenHeight
    End If
  End If

  If Button = 1 And blnScroll Or FollowM.Value = 1 Then
    MouseX = MouseX - (m_x - X)
    MouseY = MouseY - (m_y - y)
    m_x = X
    m_y = y
  End If
  
  Status.Panels(1).Text = "X : " & X - s_x & " Y : " & y - s_y
End Sub
