VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEdSprite 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Choose Sprite..."
   ClientHeight    =   4290
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   8025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4290
   ScaleWidth      =   8025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ImageList imlSprites 
      Left            =   120
      Top             =   1800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdSprite.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdSprite.frx":059A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdSprite.frx":0AEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdSprite.frx":1086
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView trvSprites 
      Height          =   4095
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   7223
      _Version        =   393217
      Indentation     =   0
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "imlSprites"
      Appearance      =   1
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6600
      Picture         =   "frmEdSprite.frx":15D8
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   720
      Width           =   1335
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6600
      Picture         =   "frmEdSprite.frx":1962
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   120
      Width           =   1335
   End
   Begin VB.PictureBox picTest 
      Height          =   2655
      Left            =   2760
      ScaleHeight     =   173
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   341
      TabIndex        =   0
      Top             =   1560
      Width           =   5175
      Begin VB.Timer tmrEngine 
         Interval        =   100
         Left            =   2400
         Top             =   720
      End
   End
   Begin VB.PictureBox picInfo 
      BackColor       =   &H00FFFFFF&
      Height          =   1335
      Left            =   2760
      ScaleHeight     =   1275
      ScaleWidth      =   3675
      TabIndex        =   3
      Top             =   120
      Width           =   3735
      Begin VB.PictureBox picColor 
         BackColor       =   &H00FF00FF&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   60
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   5
         Top             =   960
         Width           =   255
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Right Click on Preview to change Color Key"
         Height          =   255
         Left            =   360
         TabIndex        =   6
         Top             =   960
         Width           =   3255
      End
      Begin VB.Label lblInfo 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Please select an image file!"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   390
         TabIndex        =   4
         Top             =   45
         Width           =   2085
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   75
         Picture         =   "frmEdSprite.frx":1CEC
         Top             =   120
         Width           =   240
      End
   End
End
Attribute VB_Name = "frmEdSprite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim colorkey As Long


Dim sx As Integer, sy As Integer, sw As Integer, sh As Integer
Public PromptDir As String

Public MultiSel As Boolean

Private Sub cmdCancel_Click()
RetMsg = False
Unload Me
End Sub

Private Sub cmdOK_Click()
If trvSprites.SelectedItem Is Nothing Then Exit Sub
If Not ValidFileExt(trvSprites.SelectedItem.Text) Then Exit Sub

RetMsg = True
RetColorKey = colorkey

RetSX = sx
RetSY = sy
RetSW = sw
RetSH = sh

Set RetSprites = New Collection

Dim Pos As Integer
Dim ImgPath As String
ImgPath = trvSprites.SelectedItem.FullPath
Pos = InStr(1, ImgPath, "\")


ImgPath = Mid(ImgPath, Pos + 1)


RetSprites.Add ImgPath

Unload Me
End Sub

Private Sub Form_Load()

colorkey = vbMagenta


ListProjectAndFSToTreeView trvSprites, "bmp|png|gif|jpg|jpeg|aniq|rani", PromptDir


End Sub

Public Function ListFSFolder(p As String, Optional pn As Node = Nothing)


Dim X As String
Dim Nod As Node
Dim Folders As New CHive



X = Dir(p & "\", vbDirectory)
Do Until X = ""
If Not X = "." And Not X = ".." Then
If CBool(GetAttr(p & "\" & X) And vbDirectory) Then
    Set Nod = trvSprites.Nodes.Add(, , X, X, 1)
    Nod.ExpandedImage = 2
    If Not pn Is Nothing Then Set Nod.Parent = pn
    
    Folders.Add Nod, X
End If
End If

X = Dir()
Loop

Dim i As Integer
For i = 1 To Folders.Count
    ListFSFolder p & "\" & Folders.Key(i), Folders.Item(i)
Next i



X = Dir(p & "\*.*")
Do Until X = ""
    If ValidFileExt(X) Then
        Set Nod = trvSprites.Nodes.Add(, , X, X, 3, 3)
        If Not pn Is Nothing Then Set Nod.Parent = pn
    End If
X = Dir()
Loop






End Function

Public Function ListPackage(Pkg As Object) As Node



Dim fId As Long
Dim fldr As Object

fId = Pkg.RPAK.Root.FindFolder(PromptDir)

If fId >= 0 Then
Dim n As Node
Dim FN As String, ext As String

FN = Pkg.RPAK.FileName
FN = Mid(FN, InStrRev(FN, "\") + 1)

Set n = trvSprites.Nodes.Add(, , , FN, 4, 4)

    

Set fldr = Pkg.RPAK.Root.GetFolder(PromptDir)

ListFolder n, fldr

End If

Set ListPackage = n
End Function

Public Function ListFolder(pn As Node, pf As Object)
Dim ext As String
Dim i As Integer
Dim n As Node


For i = 0 To pf.FileCount - 1
    
    ext = LCase(Right(pf.FileName(i), 4))

    If ValidFileExt(pf.FileName(i)) Then

        Set n = trvSprites.Nodes.Add(, , , pf.FileName(i), 3, 3)
        Set n.Parent = pn
    End If
    
Next i

For i = 0 To pf.FolderCount - 1
    Set n = trvSprites.Nodes.Add(, , , pf.Folders(i).FolderName, 1, 1)
    Set n.Parent = pn
    n.ExpandedImage = 2
    
    
    
    ListFolder n, pf.Folders(i)
Next i


End Function

Private Sub pic_Resize()
trv.Height = pic.ScaleHeight
End Sub


Public Function ValidFileExt(ByVal FileName As String) As Boolean
Dim ext As String
  
  ext$ = Mid(FileName$, InStrRev(FileName$, ".") + 1)
  ext$ = LCase(ext)
  
  Select Case ext$
  Case "bmp", "rani", "gif", "jpg", "jpeg", "png", "aniq"
  ValidFileExt = True
  End Select

End Function

Function SizeWarning(w As Integer, H As Integer)


Dim exp As Double

exp = Math.Log(w) / Math.Log(2)
If Not Int(exp) = exp Then
SizeWarning = "WARNING: Width should be 2^n!"
Exit Function
End If

exp = Math.Log(H) / Math.Log(2)
If Not Int(exp) = exp Then
SizeWarning = "WARNING: Height should be 2^n!"
Exit Function
End If

If w > MaxTexWidth Then
SizeWarning = "WARNING: Max Width: " & MaxTexWidth & "!"
Exit Function
End If

If w > MaxTexHeight Then
SizeWarning = "WARNING: Max Height: " & MaxTexHeight & "!"
Exit Function
End If

SizeWarning = "Image Size is OK"
End Function



Private Sub picTest_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
If trvSprites.SelectedItem Is Nothing Then Exit Sub

If Not Right(trvSprites.SelectedItem.Text, 4) = "aniq" Then

Dim ARGBColorKey As Long
colorkey = picTest.Point(X, y)

Dim clr As ColorRGB
clr = SplitColor(colorkey)
ARGBColorKey = D3DColorARGB(255, clr.r, clr.g, clr.B)

picColor.BackColor = colorkey

'colorkey = ARGBColorKey
End If
trvSprites_Click
End Sub

Private Sub tmrEngine_Timer()
    GFX.Resize picTest.ScaleWidth, picTest.ScaleHeight
    GFX.Clear
    GFX.BeginScene
    TranspBack.RenderTiled 0, 0, picTest.ScaleWidth, picTest.ScaleHeight
    
    If Not tempspr Is Nothing Then
    If Not tempspr.TexID = 0 Then
    
        tempspr.Render 0, 0, sw, sh, sx, sy, sw, sh
        'Effects.BorderRect 0, 0, CDbl(sw), CDbl(sh), vbRed, 200, 2
    
    End If
    End If
    
    GFX.EndScene
    GFX.Flip picTest.hwnd
End Sub

Private Sub trvSprites_Click()
If trvSprites.SelectedItem Is Nothing Then tempspr.Unload: Exit Sub

Dim Pos As Integer
Dim pakFile As String



Dim ImgPath As String

If Not ValidFileExt(trvSprites.SelectedItem.FullPath) Then tempspr.Unload: Exit Sub
ImgPath = ObtainResourceURL(trvSprites.SelectedItem.FullPath, PromptDir)


tempspr.Unload
tempspr.Initialize ImgPath, colorkey


Dim Info As String

If Right(ImgPath, 4) = "aniq" Then
sw = tempspr.Width
sh = tempspr.Height

Dim aniq As New cS2DAniQ
Set aniq = GFX.TexturePool.Animation(GFX.TexturePool.GetTextureAnimation(tempspr.TexID))

Dim FTotal As Long
Dim i As Integer

For i = 0 To aniq.AnimationCount
FTotal = FTotal + aniq.GetAnimationFrames(i)
Next i

Info = "Format: AniQ, Cell Size: " & sw & "x" & sh & vbCrLf & _
        "Image Size: " & tempspr.Width & "x" & tempspr.Height & vbCrLf & _
        "Animations in File: " & aniq.AnimationCount & ", Frames Total: " & FTotal & vbCrLf & SizeWarning(tempspr.Width, tempspr.Height)
Else
sw = tempspr.Width
sh = tempspr.Height
Info = "Format: " & LCase(Right(ImgPath, 4)) & ", Size: " & sw & "x" & sh & vbCrLf & SizeWarning(sw, sh)

End If


lblInfo.Caption = Info
DoEvents


End Sub
