VERSION 5.00
Begin VB.Form frmDataType_Boolean 
   Caption         =   "Form1"
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3165
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows-Standard
   Begin VB.PictureBox picBoolean 
      AutoSize        =   -1  'True
      Height          =   300
      Left            =   120
      Picture         =   "frmDataType_Boolean.frx":0000
      ScaleHeight     =   240
      ScaleWidth      =   240
      TabIndex        =   2
      Top             =   960
      Width           =   300
   End
   Begin VB.PictureBox picDataEditor 
      Appearance      =   0  '2D
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   120
      ScaleHeight     =   495
      ScaleWidth      =   4455
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      Begin VB.CheckBox chkBoolean 
         BackColor       =   &H80000005&
         Caption         =   "Check1"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   4215
      End
   End
End
Attribute VB_Name = "frmDataType_Boolean"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public MyNode As cGAMRegistryNode
Public MyRenderer As Object

Public Function GetInstance() As frmDataType_Boolean
    Set GetInstance = New frmDataType_Boolean
End Function

Public Function MainContainer() As PictureBox
    Set MainContainer = picDataEditor
End Function

Private Sub chkBoolean_Click()
MyNode.Value = IIf(chkBoolean.Value = 1, "true", "false")
End Sub




Private Sub picDataEditor_Resize()
chkBoolean.Width = picDataEditor.ScaleWidth - 2 * chkBoolean.Left
End Sub

Public Sub Initialize(iSubCatLevel As Integer)
chkBoolean.Value = IIf(MyNode.Value = "true", 1, 0)
chkBoolean.Caption = MyNode.FriendlyName & " [" & MyNode.NodeName & "]"
End Sub

