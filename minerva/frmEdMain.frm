VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{A1F6324F-6D37-4183-8FC4-D4D77A331150}#1.0#0"; "VbEclipse.ocx"
Begin VB.Form frmEdMain 
   Caption         =   "R-PG: Minerva"
   ClientHeight    =   8025
   ClientLeft      =   165
   ClientTop       =   480
   ClientWidth     =   12690
   Icon            =   "frmEdMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8025
   ScaleWidth      =   12690
   StartUpPosition =   2  'CenterScreen
   Begin VbEclipse.ucPerspective ucp 
      Align           =   3  'Align Left
      Height          =   8025
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   14155
   End
   Begin MSComctlLib.ImageList imlSettings 
      Left            =   120
      Top             =   4320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Menu mnuProject 
      Caption         =   "Project"
      Begin VB.Menu mnuProjectChange 
         Caption         =   "Projectmanager"
      End
      Begin VB.Menu mnuProjectDivider1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuProjectQuit 
         Caption         =   "Quit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "Edit"
      Begin VB.Menu mnuEditParticles 
         Caption         =   "Particle Editor"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "View"
      Begin VB.Menu mnuViewMapEditor 
         Caption         =   "Map Rendering"
         Begin VB.Menu mnuViewMapEditorLayerHilight 
            Caption         =   "Layer Highlighting"
            Begin VB.Menu mnuViewMapEditorLayerHilightNone 
               Caption         =   "None"
               Checked         =   -1  'True
            End
            Begin VB.Menu mnuViewMapEditorLayerHilightDivider1 
               Caption         =   "-"
            End
            Begin VB.Menu mnuViewMapEditorLayerHilightAlpha 
               Caption         =   "Alpha Transparency"
            End
            Begin VB.Menu mnuViewMapEditorLayerHilightBrightness 
               Caption         =   "Brightness"
            End
         End
         Begin VB.Menu mnuViewMapEditorDivider1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuViewMapEditorRaster 
            Caption         =   "Render Raster"
         End
      End
      Begin VB.Menu mnuViewDivider1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewLayouts 
         Caption         =   "Layouts"
         Begin VB.Menu mnuLayoutsButtons 
            Caption         =   "Layout 0"
            Checked         =   -1  'True
            Index           =   0
         End
         Begin VB.Menu mnuViewLayoutDivider1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuViewLayoutCustom 
            Caption         =   "Custom"
         End
      End
      Begin VB.Menu mnuViewDivider2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRegistry 
         Caption         =   "Registry"
      End
   End
   Begin VB.Menu Tools 
      Caption         =   "Tools"
      Begin VB.Menu Pluginmanager 
         Caption         =   "Pluginmanager"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About..."
      End
   End
End
Attribute VB_Name = "frmEdMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Log "Initializing GUI...", ltAction
  InitGUI
End Sub

Public Sub InitGUI()
  Log "Initializing Docking engine...", ltAction
  InitEclipse
End Sub

Public Sub InitEclipse()
    pathUserData = GetSpecialFolderLocation(CSIDL_PERSONAL) & "\Minerva"
    If Dir(pathUserData, vbDirectory) = "" Then MkDir pathUserData
    
    If Dir(pathUserData & "\Layout.cfg") <> "" Then
      With ucp
        .MainHwnd = Me.hwnd
        .SetScheme New SchemeDefault
        .AddView "Log", frmScriptLog
        .AddView "Project", frmViewProject
        
        .Load pathUserData & "\Layout.cfg"
        PluginHost.TriggerRegisterViews
        .OpenEditor frmEditorRegistry
      End With
    Else
      With ucp
        .MainHwnd = Me.hwnd
        .SetScheme New SchemeDefault
        .AddView "Log", frmScriptLog
        .AddView "Project", frmViewProject
    
        With .AddPerspective("MinervaDefault")
            With .AddFolder("Bottom", vbRelBottom, 0.75, .ID_EDITOR_AREA)
                .AddView "Log"
                .ActiveViewId = "Log"
            End With
        
            With .AddFolder("Right", vbRelRight, 0.25, .ID_EDITOR_AREA)
                .AddView "Project"
                .ActiveViewId = "Project"
            End With
        
            .AddFolder "Float", vbRelFloating, 1, .ID_EDITOR_AREA
            .EditorAreaVisible = True
            .ActiveViewId = "Log"
        End With
    
        PluginHost.TriggerRegisterViews
        .ShowPerspective "MinervaDefault"
        .OpenEditor frmEditorRegistry
      End With
    End If
End Sub

Private Sub Form_Resize()
  On Error Resume Next
  ucp.Width = Me.ScaleWidth
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Dir(pathUserData, vbDirectory) = "" Then MkDir pathUserData
    If Dir(pathUserData & "\Layout.cfg") <> "" Then Kill pathUserData & "\Layout.cfg"
    ucp.Save pathUserData & "\Layout.cfg"

    ucp.Terminate
    BenchmarkAndEnd
    DoEvents
    Unload Me
End Sub

Private Sub mnuEditParticles_Click()
    frmEdMap.tmrRender.Enabled = False
    frmParticle.Show vbModal
    frmEdMap.tmrRender.Enabled = True
End Sub

Private Sub mnuProjectChange_Click()
    frmEdSelectProject.Show vbModal
End Sub

Private Sub mnuProjectQuit_Click()
    Unload Me
End Sub

Private Sub mnuViewMapEditorLayerHilightAlpha_Click()
    HLMode = 1
    UpdateHLMode
End Sub

Private Sub mnuViewMapEditorLayerHilightBrightness_Click()
    HLMode = 2
    UpdateHLMode
End Sub

Private Sub mnuViewMapEditorLayerHilightNone_Click()
    HLMode = 0
    UpdateHLMode
End Sub

Private Sub mnuViewMapEditorRaster_Click()
  RenderRaster = Not RenderRaster
  mnuViewMapEditorRaster.Checked = RenderRaster
End Sub

Private Sub UpdateHLMode()
    mnuViewMapEditorLayerHilightNone.Checked = False
    mnuViewMapEditorLayerHilightAlpha.Checked = False
    mnuViewMapEditorLayerHilightBrightness.Checked = False
    Select Case HLMode
     Case 0
        mnuViewMapEditorLayerHilightNone.Checked = True
     Case 1
        mnuViewMapEditorLayerHilightAlpha.Checked = True
     Case 2
        mnuViewMapEditorLayerHilightBrightness.Checked = True
    End Select
End Sub

Private Sub mnuViewRegistry_Click()
    Dim iNumEditors As Integer
    If IsEmpty(ucp.Editors) Then
      Log "Reinitializing Registry...", ltAction
      Set SEditor = New cEdSettingsEditor
      SEditor.Initialize
    
      Log "Opening Registry...", ltAction
      ucp.OpenEditor frmEditorRegistry
      
      Log "Populating Registry Tree...", ltAction
      frmEditorRegistry.Update
      
      Exit Sub
    End If
    
    iNumEditors = UBound(ucp.Editors)
    If iNumEditors >= 0 Then
      Dim iCount As Integer
      
      For iCount = 0 To iNumEditors
        If TypeOf ucp.Editors(iCount) Is frmEditorRegistry Then
          Log "Registry is already opened!", ltAlert
          Exit Sub
        End If
      Next iCount
      
      Log "Reinitializing Registry...", ltAction
      Set SEditor = New cEdSettingsEditor
      SEditor.Initialize
      
      Log "Opening Registry...", ltAction
      ucp.OpenEditor frmEditorRegistry
      
      Log "Populating Registry Tree...", ltAction
      frmEditorRegistry.Update
    End If
End Sub

Private Sub Pluginmanager_Click()
    frmPluginManager.Show vbModal
End Sub
