VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGAMRegistryNode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim m_NodeName As String
Dim m_NodeType As String

Dim m_FriendlyName As String
Dim m_Description As String

Public Parameters As CHive
Dim m_SubElements As CHive

Dim m_Value

Dim m_ParentNode As cGAMRegistryNode

Dim m_XMLNode As IXMLDOMNode

Public Property Get ParentNode() As cGAMRegistryNode
    Set ParentNode = m_ParentNode
End Property

Public Property Set ParentNode(vNew As cGAMRegistryNode)
    Set m_ParentNode = vNew
End Property

Public Property Get NodeName() As String
    NodeName = m_NodeName
End Property

Public Property Let NodeName(vNew As String)
    m_NodeName = vNew
End Property

Public Property Get FriendlyName() As String
    FriendlyName = m_FriendlyName
End Property

Public Property Let FriendlyName(vNew As String)
    m_FriendlyName = vNew
End Property

Public Property Get Description() As String
    Description = m_Description
End Property

Public Property Let Description(sNew As String)
    m_Description = sNew
End Property

Public Property Get NodeType() As String
    NodeType = m_NodeType
End Property

Public Property Let NodeType(vNew As String)
    m_NodeType = vNew
End Property

Public Property Get SubElements() As CHive
    Set SubElements = m_SubElements
End Property



Public Property Get Value() As Variant
    Value = m_Value
End Property

Public Property Let Value(vNew As Variant)
    m_Value = vNew
End Property

Public Property Get XMLNode() As IXMLDOMNode
    Set XMLNode = m_XMLNode
End Property

Public Property Set XMLNode(oNew As IXMLDOMNode)
    Set m_XMLNode = oNew
End Property



Public Function RecursiveFind(p As String) As cGAMRegistryNode
Dim s1 As String, s2 As String
Dim Pos As Integer


If p = "" Then Set RecursiveFind = Me: Exit Function

Pos = InStr(1, p, "\")

If Pos = 0 Then
    Set RecursiveFind = m_SubElements(p)
Else

    s1 = Left(p, Pos - 1)
    s2 = Mid(p, Pos + 1)
    
    If Not m_SubElements.Exist(s1) Then Exit Function
    Set RecursiveFind = m_SubElements(s1).RecursiveFind(s2)

End If
End Function

Private Sub Class_Initialize()
Set m_SubElements = New CHive
Set Parameters = New CHive
End Sub

Public Function GetPath() As String
If m_ParentNode Is Nothing Then Exit Function

GetPath = m_ParentNode.GetPath & "\" & m_NodeName
End Function
