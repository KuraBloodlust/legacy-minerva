VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDevIL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Public Sub Blit(ImgFrom As cDevILImage, ImgTo As cDevILImage, DestX As Long, DestY As Long, DestZ As Long, SrcX As Long, SrcY As Long, SrcZ As Long, Width As Long, Height As Long, Depth As Long)
ImgTo.BindMe
ilBlit ImgFrom.Handle, DestX, DestY, DestZ, SrcX, SrcY, SrcZ, Width, Height, Depth
End Sub

Private Sub Class_Initialize()
ilInit
iluInit
ilutInit

ilutRenderer ILUT_WIN32
End Sub

Public Sub BindImg(ID As Long)
ilBindImage ID
End Sub

Public Function GenImg() As Long
Dim ID As Long
ilGenImages 1, ID
GenImg = ID
End Function

Public Function NewImg() As cDevILImage
Dim i As New cDevILImage
Set i.devil = Me
Set NewImg = i
End Function

Public Function CreateImage(Width As Long, Height As Long, Depth As Long, Bpp As Byte, Format As Long, lType As Long) As cDevILImage
Dim i As cDevILImage
Set i = NewImg

i.Handle = GenImg
i.BindMe

ilTexImage0 Width, Height, Depth, Bpp, Format, lType, 0
ilClearImage

Set CreateImage = i
End Function

Public Function LoadImage(sPath As String) As cDevILImage
Dim img As cDevILImage
Set img = NewImg()


Dim ID As Long

'generate new image + bind it
ID = GenImg
BindImg ID

'load to binded image
ilLoadImage sPath

'save handle
img.Handle = ID

'return id
Set LoadImage = img
End Function


Public Sub SaveImage(oImage As cDevILImage, sPath As String)
BindImg oImage.Handle
ilSaveImage sPath
End Sub

Public Property Get Error() As Long
Error = ilGetError
End Property

Public Property Get ErrorString() As String
ErrorString = iluErrorString(Error)
End Property

