Attribute VB_Name = "modGlobal"
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Const MapFormatMajor        As Integer = 0
Public Const MapFormatMinor        As Integer = 0
Public Const MapFormatRevision     As Integer = 5

Public Const PackageFormatMajor    As Integer = 0
Public Const PackageFormatMinor    As Integer = 0
Public Const PackageFormatRevision As Integer = 1

Public Const Const_ColorKey        As Long = &HFFFF00FF

Public Const_TileWidth As Integer
Public Const_TileHeight As Integer
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Declare Sub CopyMemory Lib "kernel32" Alias _
        "RtlMoveMemory" (pDst As Any, pSrc As Any, ByVal _
        ByteLen As Long)

Private Declare Sub FillMemory Lib "kernel32.dll" Alias _
        "RtlFillMemory" (Destination As Any, ByVal Length _
        As Long, ByVal Fill As Byte)
        
Public Declare Function ShowCursor Lib "user32" (ByVal bShow As Boolean) As Long

Public Type tMapHeader
    FileType            As String * 4
    verMajor            As Integer
    verMinor            As Integer
    verRevision         As Integer
    someMoreBytes       As String * 255
End Type

Public Type tMapCell
    TileX               As Integer
    TileY               As Integer
    DrawTile            As Boolean
    Additional          As Integer
    AdditionalID        As Integer
    RM2KF               As Boolean
    RM2KP(1 To 4)       As Point
    Shifted             As Boolean
End Type

Public Type tColCell
    TileColX            As Byte
    TileColY            As Byte
    TileInf             As Integer
End Type

Public Type RM2Ksel
    x                   As Double
    y                   As Double
    TileSet             As Integer
End Type

Public Type tMapNPC
    x                   As Long
    y                   As Long
    SourceX             As Integer
    SourceY             As Integer
    SourceWidth         As Integer
    SourceHeight        As Integer
    Level               As Integer
    ZPlus               As Integer
    Color               As Long
    Alpha               As Byte
    Additional          As Integer
    Depth               As Double
    StairHeight         As Double
    JumpSpeed           As Double
    JumpDuration        As Double
    MaskXStart          As Integer
    MaskYStart          As Integer
    MaskXEnd            As Integer
    MaskYEnd            As Integer
End Type

Public MaxTexWidth              As Long
Public MaxTexHeight             As Long

Public CurProj                  As String
Public RetPassword              As String
Public RootDir                  As String

Public RM2K_Paths()             As RM2Ksel
Public RM2K_AnimatedPaths()     As RM2Ksel
Public RM2K_Animations()        As RM2Ksel
Public RMXP_AnimatedPaths()        As RM2Ksel

Public RM2K_Path_Count          As Integer
Public RM2K_AnimatedPath_Count  As Integer
Public RM2K_Animation_Count     As Integer
Public RMXP_AnimatedPath_Count     As Integer
Public Temp()                   As Integer
Public RetOk                    As Integer
Public RetObject                As Object
Public RetString                As String

Public IgnoreScripts            As Boolean

Public MousePos                 As Point
Public MousePosOnMap            As Point
Public MousePosOnTile           As Point

Public CameraX                  As Double
Public CameraY                  As Double
Public CameraSpeedX             As Double
Public CameraSpeedY             As Double
Public GeneralCameraSpeed       As Double
Public GeneralCameraSpeedFactor As Double
Public GeneralCameraMaxSpeed    As Double
Public GeneralCameraMaxDistance As Double
Public OldCameraX               As Double
Public OldCameraY               As Double
Public Gravity                  As Double

Public FocusNPC                 As cS2DNPC
Public LoadedDependencies       As New Collection

Public pathUserData As String

Public Declare Function Rectangle Lib "gdi32.dll" (ByVal hdc As Long, ByVal X1 As Long, _
  ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long

Public Declare Function SetParentAPI Lib "user32" Alias "SetParent" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long

Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long

Private Type SHFILEOPSTRUCT
    hwnd As Long
    wFunc As Long
    pFrom As String
    pTo As String
    fFlags As Integer
    fAborted As Boolean
    hNameMaps As Long
    sProgress As String
End Type
Private Const FO_DELETE = &H3
Private Const FOF_ALLOWUNDO = &H40
Private Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As Long


Public Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String) As Long


Public Function GetCase(ByVal Text As String) As String
 GetCase$ = UCase(Mid$(Text$, 1, 1)) & LCase(Mid$(Text$, 2))
End Function

Public Function GetPos(sString As String, lLine As Long) As Long
Dim tmp()  As String
Dim i  As Long, Length As Long

  tmp = Split(sString, vbCrLf)
 
    
    lLine& = lLine& - 1
    For i& = 0 To UBound(tmp())
        If i < lLine Then
            Length& = Length& + Len(tmp(i)) + 2
        Else
            Exit For
        End If
    Next i

    GetPos = Length
    Length& = 0
End Function

Public Function GetEnd(ByVal Text As String) As Long
Dim Pos As Long

  Pos& = InStr(Text$, vbCrLf)

  If Pos& <= 0 Then
  GetEnd& = Len(Text$)
  Else
  GetEnd& = Pos& - 1
  End If

End Function

Public Function CheckValidChars(sCheck As String, sCharSpace As String) As Boolean
Dim i As Integer

For i = 1 To Len(sCheck)
    If InStr(1, sCharSpace, Mid(sCheck, i, 1)) = 0 Then Exit Function
Next i

CheckValidChars = True

End Function



Public Function PasswordBox(Prompt As String, _
Optional MaskPassword As Boolean = True) As String

  With frmPassword
  .lblPrompt.Caption = Prompt$
  .chkMask.Value = IIf(MaskPassword, 1, 0)
  .txtPassword.PasswordChar = IIf(MaskPassword, "*", "")
  .Show vbModal
  End With
  
  If RetOk = 1 Then PasswordBox = RetPassword
End Function

Public Function FindLDependency(ByVal FName As String) As Long
Dim i As Long

  For i& = 1 To LoadedDependencies.Count
    If LCase(LoadedDependencies(i).ShortName) = LCase(FName) Then
    FindLDependency& = i&
    Exit Function
    End If
  Next

End Function

Public Function DependencyNeeded(DName As String) As Boolean
  
  If Project.Package.FindDependency(DName) Then
  DependencyNeeded = True
  Exit Function
  End If
  
  Dim i As Integer
  Dim LD As cGAMPackage

  For i% = 1 To LoadedDependencies.Count
  Set LD = LoadedDependencies(i)
    
  If LD.FindDependency(DName) > 0 Then
  DependencyNeeded = True
  Exit Function
  End If
    
  Next

End Function

Public Function min(V1 As Long, V2 As Long) As Long
   min = IIf(V1& <= V2&, V1&, V2&)
End Function

Public Function max(V1 As Long, V2 As Long) As Long
   max = IIf(V1& >= V2&, V1&, V2&)
End Function
 
Public Function Snap(Cordinate As Variant, Dimension As Integer) As Integer
    Snap% = (Cordinate \ Dimension%) * Dimension%
End Function

Public Function TileInfColors(Ti As Integer) As Long

    Select Case Ti
     Case 0:    TileInfColors& = rgb(50, 50, 50)    'Platform
     Case 1:    TileInfColors& = vbWhite            'Hole
     Case 2:    TileInfColors& = vbRed              'Wall
     Case 3:    TileInfColors& = vbBlue             'Water
     Case 4:    TileInfColors& = vbYellow           'Sloww
     Case Else: TileInfColors& = QBColor(Ti)
    End Select

End Function

Public Sub AddRM2KPaths()
RM2K_Path_Count = 12
ReDim RM2K_Paths(1 To RM2K_Path_Count)

RM2K_Paths(1).x = 6 * Const_TileWidth
RM2K_Paths(1).y = 0 * Const_TileHeight
RM2K_Paths(1).TileSet = 1

RM2K_Paths(2).x = 9 * Const_TileWidth
RM2K_Paths(2).y = 0 * Const_TileHeight
RM2K_Paths(2).TileSet = 1

RM2K_Paths(3).x = 6 * Const_TileWidth
RM2K_Paths(3).y = 4 * Const_TileHeight
RM2K_Paths(3).TileSet = 1

RM2K_Paths(4).x = 9 * Const_TileWidth
RM2K_Paths(4).y = 4 * Const_TileHeight
RM2K_Paths(4).TileSet = 1

RM2K_Paths(5).x = 0 * Const_TileWidth
RM2K_Paths(5).y = 8 * Const_TileHeight
RM2K_Paths(5).TileSet = 1

RM2K_Paths(6).x = 3 * Const_TileWidth
RM2K_Paths(6).y = 8 * Const_TileHeight
RM2K_Paths(6).TileSet = 1

RM2K_Paths(7).x = 6 * Const_TileWidth
RM2K_Paths(7).y = 8 * Const_TileHeight
RM2K_Paths(7).TileSet = 1

RM2K_Paths(8).x = 9 * Const_TileWidth
RM2K_Paths(8).y = 8 * Const_TileHeight
RM2K_Paths(8).TileSet = 1

RM2K_Paths(9).x = 0 * Const_TileWidth
RM2K_Paths(9).y = 12 * Const_TileHeight
RM2K_Paths(9).TileSet = 1

RM2K_Paths(10).x = 3 * Const_TileWidth
RM2K_Paths(10).y = 12 * Const_TileHeight
RM2K_Paths(10).TileSet = 1

RM2K_Paths(11).x = 6 * Const_TileWidth
RM2K_Paths(11).y = 12 * Const_TileHeight
RM2K_Paths(11).TileSet = 1

RM2K_Paths(12).x = 9 * Const_TileWidth
RM2K_Paths(12).y = 12 * Const_TileHeight
RM2K_Paths(12).TileSet = 1

RM2K_Animation_Count = 3
ReDim RM2K_Animations(1 To RM2K_Animation_Count)

RM2K_Animations(1).x = 3 * Const_TileWidth
RM2K_Animations(1).y = 4 * Const_TileHeight
RM2K_Animations(1).TileSet = 1

RM2K_Animations(2).x = 4 * Const_TileWidth
RM2K_Animations(2).y = 4 * Const_TileHeight
RM2K_Animations(2).TileSet = 1

RM2K_Animations(3).x = 5 * Const_TileWidth
RM2K_Animations(3).y = 4 * Const_TileHeight
RM2K_Animations(3).TileSet = 1

RM2K_AnimatedPath_Count = 3
ReDim RM2K_AnimatedPaths(1 To RM2K_AnimatedPath_Count)

RM2K_AnimatedPaths(1).x = 0
RM2K_AnimatedPaths(1).y = 0
RM2K_AnimatedPaths(1).TileSet = 1

RM2K_AnimatedPaths(2).x = 3 * Const_TileWidth
RM2K_AnimatedPaths(2).y = 0
RM2K_AnimatedPaths(2).TileSet = 1

RM2K_AnimatedPaths(3).x = 0
RM2K_AnimatedPaths(3).y = 6 * Const_TileHeight
RM2K_AnimatedPaths(3).TileSet = 1

End Sub

'Returns on folder up (C:\test\kat\)->(C:\test)
Public Function UpOneFolder(ByVal sPath As String) As String
Dim s As String
On Error Resume Next
  
  s$ = sPath$
  If Right$(s$, 1) = "\" Then s$ = Left(s$, Len(s$) - 1)
  s$ = Left(s, InStrRev(s$, "\") - 1)
  UpOneFolder$ = s$
End Function

'Returns true if the programm is in the IDE
Public Function InDevelopment() As Boolean
  InDevelopment = (App.LogMode = 0)
End Function

'Looks whether a file exists
Public Function FileExists(ByVal Datei As String) As Boolean
  On Error GoTo fehler

  If Dir(Datei$) <> "" Then
    If FileLen(Datei$) > 0 Then
        FileExists = True
    End If
  End If

fehler:
End Function

'Looks whether a folder exists
Public Function FolderExists(ByVal Folder As String) As Boolean
 On Error GoTo fehler
 
 If Dir(Folder, vbDirectory) <> "" Then
    If GetAttr(Folder) And vbDirectory Then FolderExists = True
 End If
fehler:
End Function

'Its a own Sleep Methode
Public Sub Sleep(ByVal Ticks As Integer)
  Dim cTickCount As Integer

  Do
  If GetInputState() <> 0 Then
  DoEvents
  End If

  cTickCount% = cTickCount% + 1
  RenderGame
  Loop Until cTickCount% = Ticks%
End Sub

Public Sub WindowsDelete(Url As String)
    Dim SHFileOp As SHFILEOPSTRUCT
    With SHFileOp
        .wFunc = FO_DELETE
        .pFrom = Url
        .fFlags = FOF_ALLOWUNDO
    End With
    
    SHFileOperation SHFileOp
End Sub
