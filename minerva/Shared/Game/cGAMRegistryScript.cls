VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGAMRegistryScript"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function GetSetting(SettingPath As String, DefaultValue As Variant) As Variant
    Dim n As cGAMRegistryNode
    Set n = Project.Registry.RecursiveFind(SettingPath)
    
    If n Is Nothing Then
        GetSetting = DefaultValue
    Else
        GetSetting = n.Value
    End If
End Function

Public Function SetSetting(SettingPath As String, NewValue As Variant, Optional SavePackage As Boolean = False)
    Dim n As cGAMRegistryNode
    Set n = Project.Registry.RecursiveFind(SettingPath)
    n.Value = NewValue
    
    If SavePackage Then Project.SaveRegistry
End Function


Public Function GetSubItemCount(SettingPath As String) As Integer
    Dim n As cGAMRegistryNode
    Set n = Project.Registry.RecursiveFind(SettingPath)
    GetSubItemCount = n.SubElements.Count
End Function
