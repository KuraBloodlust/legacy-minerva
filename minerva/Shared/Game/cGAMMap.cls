VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGAMMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private CFm_MapTitle      As String
Public CFm_MapFile       As String 'dokumentieren: NEIN
Private CFm_MapWidth      As Integer
Private CFm_MapHeight     As Integer
Private CFm_MyS2D         As cS2D_i
Public Levels        As CHive 'dokumentieren: JA
Public TileSets      As CHive 'dokumentieren: JA
Public m_NPCs          As New CHive 'dokumentieren: NEIN, �ber NPCs

Public NoBuffer As Boolean

Public Function RedrawAll()
    Dim i As Integer
    Dim a As Integer
    
    For i = 1 To Levels.Count
        For a = 1 To Levels(i).Layers.Count
             Levels(i).Layers(a).Redraw = True
        Next a
    Next i
End Function

'dokumentieren: JA
Public Property Get NPCs(ID As Variant) As Object

If Val(ID) = 0 Then

Dim i As Integer
    For i = 1 To m_NPCs.Count
        If LCase(m_NPCs(i).Name) = LCase(ID) Then Set NPCs = m_NPCs(i): Exit For
    Next i

Else
    Set NPCs = m_NPCs(Val(ID))
End If

End Property

Public Property Set NPCs(ID As Variant, vNew As Object)

If Val(ID) = 0 Then
Dim i As Integer
    For i = 1 To m_NPCs.Count
        If LCase(m_NPCs(i).Name) = LCase(ID) Then Set m_NPCs(i) = vNew: Exit For
    Next i

Else
    Set m_NPCs(Val(ID)) = vNew
End If

End Property

'dokumentieren: JA
Public Sub ChangeLevel(newmap As String)
Log "Mapchange: '" & newmap & "'...", ltAction

Dim s As String
s = CFm_MapFile

s = UpOneFolder(s)

Unload
LoadMap s & "\" & newmap

Log "Mapchange completed", ltSuccess
End Sub

'dokumentieren: JA
Public Function RunScript(SubName As String)
Dim i As Integer

For i = 1 To m_NPCs.Count

m_NPCs(i).RunScript SubName

Next i

If SubName = "Initialize" Then
    For i = 1 To Levels.Count
        Levels(i).RunScript SubName
    Next i
End If

End Function

'dokumentieren: JA
Public Property Get MapTitle() As String

    MapTitle = CFm_MapTitle

End Property

Public Property Let MapTitle(ByVal PropVal As String)

    CFm_MapTitle = PropVal

End Property

'dokumentieren: JA
Public Property Get MapFile() As String

     MapFile = CFm_MapFile

End Property

Public Property Let MapFile(ByVal PropVal As String)

   CFm_MapFile = PropVal

End Property

'dokumentieren: JA
Public Property Get MapWidth() As Integer

   MapWidth = CFm_MapWidth

End Property

Public Property Let MapWidth(ByVal PropVal As Integer)
   CFm_MapWidth = PropVal

End Property

'dokumentieren: JA
Public Property Get MapHeight() As Integer
  
  MapHeight = CFm_MapHeight

End Property

Public Property Let MapHeight(ByVal PropVal As Integer)

    CFm_MapHeight = PropVal

End Property

Public Property Get MyS2D() As cS2D_i

    Set MyS2D = CFm_MyS2D

End Property

Public Property Set MyS2D(PropVal As cS2D_i)
    
    Set CFm_MyS2D = PropVal

End Property

'dokumentieren: VIELLEICHT
Public Function GetLevelByZ(ByVal Z As Double) As Integer

  Dim i As Integer

    For i = 1 To Levels.Count
    
        If Levels(i).Z > Z Then
        
            Dim j As Integer
            For j = i - 1 To 1 Step -1
                If TypeOf Levels(j) Is cGAMLevel Then GetLevelByZ = j: Exit Function
                
            Next j
            
            Exit Function
        End If
        
    Next i
    GetLevelByZ = Levels.Count

End Function

'dokumentieren: NEIN
Public Function LoadMap(ByVal strFilename As String) As Boolean
  Dim TilesetCount As Integer
  Dim LevelCount   As Integer
  Dim Npccount     As Integer
  Dim i            As Integer
  Dim ts           As cGAMTileset
  Dim tsf          As String
  Dim j            As Byte
  Dim lvl          As Object
  Dim t            As Byte
  Dim Npc          As cS2DNPC


    newmap
    
    Dim tf As String
    Dim umtf As Boolean
    CFm_MapFile = strFilename
    tf = ProvideFile(CFm_MapFile, umtf)
    
    Open tf For Binary Access Read As 1
     
    Dim headr As tMapHeader
    Get #1, , headr
    
    Log "Checking Map header...", ltAction
    If Not CheckHeader(headr) Then Close 1: If umtf Then Kill tf: Exit Function
    Log "Map file is valid (ver: " & headr.verMajor & "." & headr.verMinor & "." & headr.verRevision & ")", ltSuccess
    
    CFm_MapTitle = String$(255, Chr$(0))
    Get #1, , CFm_MapTitle
    CFm_MapTitle = Replace(CFm_MapTitle, Chr$(0), vbNullString)
    Get #1, , CFm_MapWidth
    Get #1, , CFm_MapHeight
    Get #1, , Const_TileWidth
    Get #1, , Const_TileHeight
    Get #1, , TilesetCount
    Get #1, , LevelCount
    Get #1, , Npccount
    
    Log "Map Width/Height: " & CFm_MapWidth & "*" & CFm_MapHeight & ", Tilesets: " & TilesetCount & ", Levels: " & LevelCount & ", NPCs: " & Npccount, ltInformation
    
    Get #1, , RM2K_Path_Count
    ReDim RM2K_Paths(0 To RM2K_Path_Count)
    
    For i = 1 To RM2K_Path_Count
    Get #1, , RM2K_Paths(i)
    Next i
    
    Get #1, , RM2K_AnimatedPath_Count
    ReDim RM2K_AnimatedPaths(0 To RM2K_AnimatedPath_Count)
    
    For i = 1 To RM2K_AnimatedPath_Count
    Get #1, , RM2K_AnimatedPaths(i)
    Next i
    
    Get #1, , RM2K_Animation_Count
    ReDim RM2K_Animations(0 To RM2K_Animation_Count)
    
    For i = 1 To RM2K_Animation_Count
    Get #1, , RM2K_Animations(i)
    Next i
    
    Get #1, , RMXP_AnimatedPath_Count
    ReDim RMXP_AnimatedPaths(0 To RMXP_AnimatedPath_Count)
    
    For i = 1 To RMXP_AnimatedPath_Count
    Get #1, , RMXP_AnimatedPaths(i)
    Next i
    
    'tilesets auslesen & laden
    TileSets.Clear
    For j = 0 To TilesetCount - 1
        Set ts = New cGAMTileset
        tsf = String$(255, Chr$(0))
        Dim tcc As Long
        
        Get #1, , tsf
        Dim TColor As Long
        Get #1, , TColor
        
        tsf = Replace(tsf, Chr$(0), vbNullString)
        With ts
            Dim utf As Boolean

            Log "Loading tileset: '" & tsf & "'...", ltAction
            Set .MyMap = Me
            .Initialize "tilesets\" & tsf, TColor
            TileSets.Add ts
            
        End With 'ts
    Next j
    For i = 1 To LevelCount
        Get #1, , t
        If t = 0 Then
            'Terrain
            Set lvl = New cGAMLevel
            Set lvl.MyMap = Me
            lvl.Load
            Levels.Add lvl
         ElseIf t = 1 Then 'NOT T...
            Set lvl = New cGAMScriptLevel
            Set lvl.MyMap = Me
            lvl.Load
            Levels.Add lvl
         Else 'NOT T...
            MsgBox "Unsupported Level Type"
        End If
    Next i
    For i = 1 To Npccount
        Set Npc = New cS2DNPC
        Set Npc.MyMap = Me
        Set Npc.MyS2D = MyS2D
        Npc.Load
        m_NPCs.Add Npc
        Npc.Index = m_NPCs.Count
    Next i
    Close 1
    
    LoadMap = True

    If umtf Then Kill tf
End Function

'dokumentieren: NEIN
Private Function CheckHeader(headr As tMapHeader)
    If Not headr.FileType = "RMAP" Then
        MsgBox "This is not a Minerva mapfile or it was saved in an old version!", vbCritical, "Not a mapfile"
        Exit Function
    End If
    
    If Not ((headr.verMajor = MapFormatMajor) And (headr.verMinor = MapFormatMinor) And (headr.verRevision = MapFormatRevision)) Then
        MsgBox "Invalid Map file version." & vbCr & _
        "Mapfile: " & headr.verMajor & "." & headr.verMinor & "." & headr.verRevision & vbCrLf & _
        "Minerva: " & MapFormatMajor & "." & MapFormatMinor & "." & MapFormatRevision, vbCritical, "Invalid version"
        Exit Function
    End If
    
    CheckHeader = True
End Function

'dokumentieren: NEIN
Public Sub newmap()
  Log "New Map created", ltInformation
    RM2K_Path_Count = 0
    RM2K_AnimatedPath_Count = 0
    RM2K_Animation_Count = 0
    RMXP_AnimatedPath_Count = 0
    
    Set Levels = New CHive
    Set m_NPCs = New CHive

End Sub

Public Sub UpdateDimensions(newW As Integer, newH As Integer)
Dim oldW As Integer, oldH As Integer
oldW = MapWidth
oldH = MapHeight
MapWidth = newW
MapHeight = newH

Dim i As Integer
    For i = 1 To Levels.Count
        Levels(i).UpdateDimensions oldW, oldH
    Next i
End Sub

'dokumentieren: NEIN
Public Sub ReInit()

  Dim i      As Integer
  Dim curlvl As Object

    For i = 1 To Levels.Count
        Set curlvl = Levels(i)
        curlvl.ReInit
    Next i

End Sub

'dokumentieren: VIELLEICHT
Public Sub Render(ByVal XTo As Long, _
                  ByVal YTo As Long, _
                  ByVal AreaZFrom As Double, _
                  ByVal AreaZTo As Double)

 
  Dim n      As cS2DNPC

  Dim i      As Integer
  Dim curlvl As Object
    For i = 1 To Levels.Count
        
        Set curlvl = Levels(i)
        With curlvl
            If .Z >= AreaZFrom Then
                If .Z <= AreaZTo Then
                    .Render XTo, YTo
                End If
            End If
        End With 'curlvl
    Next i
    
    
    
    For i = 1 To m_NPCs.Count
        Set n = m_NPCs(i)
        n.Render n.x + XTo, n.y + YTo ', n.Alpha, n.Additional, n.Color
    Next i

End Sub

'dokumentieren: NEIN
Public Sub SaveMap(ByVal strFilename As String)

  Dim mt           As String
  Dim TilesetCount As Integer
  Dim LevelCount   As Integer
  Dim Npccount     As Integer
  Dim i            As Integer
  Dim j            As Byte
  Dim t            As Byte

    Dim tf As String
    Dim utf As Boolean
    
    
    Open strFilename For Binary Access Write As 1
    
    Dim headr As tMapHeader
    headr.FileType = "RMAP"
    headr.verMajor = MapFormatMajor
    headr.verMinor = MapFormatMinor
    headr.verRevision = MapFormatRevision
    Put #1, , headr
    
    mt = CFm_MapTitle
    mt = mt & String$(255 - Len(mt), Chr$(0))
    Put #1, , mt
    Put #1, , CFm_MapWidth
    Put #1, , CFm_MapHeight
    Put #1, , Const_TileWidth
    Put #1, , Const_TileHeight
    TilesetCount = TileSets.Count
    LevelCount = Levels.Count
    Npccount = m_NPCs.Count
    Put #1, , TilesetCount
    Put #1, , LevelCount
    Put #1, , Npccount
    
    Put #1, , RM2K_Path_Count
    For i = 1 To RM2K_Path_Count
    Put #1, , RM2K_Paths(i)
    Next i
    
    Put #1, , RM2K_AnimatedPath_Count
    For i = 1 To RM2K_AnimatedPath_Count
    Put #1, , RM2K_AnimatedPaths(i)
    Next i
    
    Put #1, , RM2K_Animation_Count
    For i = 1 To RM2K_Animation_Count
    Put #1, , RM2K_Animations(i)
    Next i
    
    Put #1, , RMXP_AnimatedPath_Count
    For i = 1 To RMXP_AnimatedPath_Count
    Put #1, , RMXP_AnimatedPaths(i)
    Next i
    
    'tilesets auslesen & laden
    For j = 1 To TileSets.Count
        TileSets(j).Save
    Next j
    For i = 1 To LevelCount
        If TypeOf Levels(i) Is cGAMLevel Then
            t = 0
         ElseIf TypeOf Levels(i) Is cGAMScriptLevel Then 'NOT TYPEOF...
            t = 1
         Else 'NOT TYPEOF...
            t = 255
            MsgBox "Unknown Level Type."
        End If
        Put #1, , t
        Levels(i).Save
    Next i
    For i = 1 To Npccount
        m_NPCs(i).Save
    Next i
    Close 1

End Sub

'dokumentieren: NEIN
Public Sub Unload()
Dim i As Integer
For i = 1 To m_NPCs.Count
m_NPCs(i).Unload
Next i

For i = 1 To TileSets.Count
TileSets(i).Unload
Next i
End Sub

Private Sub Class_Initialize()
Set Levels = New CHive
Set TileSets = New CHive
End Sub
