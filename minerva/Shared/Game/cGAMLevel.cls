VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGAMLevel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private CFm_XOffset        As Long
Private CFm_YOffset        As Long
Private CFm_Z              As Double
Private CFm_MyMap          As cGAMMap
Public Layers         As CHive
Private CFm_Alpha          As Integer
Private CFm_Desc           As String
Private TileInf()     As tColCell
Public Visible As Boolean

Public Property Get XOffset() As Long

    XOffset = CFm_XOffset

End Property

Public Property Let XOffset(ByVal PropVal As Long)

    CFm_XOffset = PropVal

End Property

Public Property Get YOffset() As Long

    YOffset = CFm_YOffset

End Property

Public Property Let YOffset(ByVal PropVal As Long)

    CFm_YOffset = PropVal

End Property

Public Property Get Z() As Double

    Z = CFm_Z

End Property

Public Property Let Z(ByVal PropVal As Double)

    CFm_Z = PropVal

End Property

Public Property Get MyMap() As cGAMMap

    Set MyMap = CFm_MyMap

End Property

Public Property Set MyMap(PropVal As cGAMMap)

     Set CFm_MyMap = PropVal

End Property

Public Property Get Alpha() As Integer

    Alpha = CFm_Alpha

End Property

Public Property Let Alpha(ByVal PropVal As Integer)

    CFm_Alpha = PropVal

End Property

Public Property Get Desc() As String

    Desc = CFm_Desc

End Property

Public Property Let Desc(ByVal PropVal As String)

    CFm_Desc = PropVal

End Property

Private Sub Class_Initialize()
Visible = True
    CFm_Alpha = 255
Set Layers = New CHive
End Sub

Public Property Get ColInf(x As Integer, _
                           y As Integer) As Integer
On Error Resume Next
    ColInf = TileInf(x, y).TileInf

End Property

Public Property Let ColInf(x As Integer, _
                           y As Integer, _
                           ByVal newval As Integer)

 On Error Resume Next
    TileInf(x, y).TileInf = newval

End Property

Public Property Get ColTileX(x As Integer, _
                             y As Integer) As Byte

On Error Resume Next
    ColTileX = TileInf(x, y).TileColX

End Property

Public Property Let ColTileX(x As Integer, _
                             y As Integer, _
                             ByVal newval As Byte)
On Error Resume Next
    TileInf(x, y).TileColX = newval

End Property

Public Property Get ColTileY(x As Integer, _
                             y As Integer) As Byte
On Error Resume Next
    ColTileY = TileInf(x, y).TileColY

End Property

Public Property Let ColTileY(x As Integer, _
                             y As Integer, _
                             ByVal newval As Byte)
On Error Resume Next
    TileInf(x, y).TileColY = newval

End Property

Public Sub Initialize()
    If Not (MyMap.MapWidth <= 0 Or MyMap.MapHeight <= 0) Then ReDim TileInf(0 To MyMap.MapWidth - 1, 0 To MyMap.MapHeight - 1) As tColCell
    Set Layers = New CHive

End Sub

Public Sub UpdateDimensions(oldW As Integer, oldH As Integer)
    ReDim TileInf(0 To MyMap.MapWidth - 1, 0 To MyMap.MapHeight - 1) As tColCell
    
    Dim i As Integer
    For i = 1 To Layers.Count
        Layers(i).UpdateDimensions oldW, oldH
    Next i
End Sub

Public Sub Load()

    Dim lC       As Integer
    Dim x        As Integer
    Dim y        As Integer
    Dim LyrCount As Integer
    Dim i        As Integer
    Dim lyr      As cGAMLayer

    Initialize
    Get #1, , CFm_XOffset
    Get #1, , CFm_YOffset
    Get #1, , CFm_Z
    Get #1, , lC
    CFm_Desc = String$(lC, Chr$(0))
    
    Get #1, , CFm_Desc

    For x = 0 To CFm_MyMap.MapWidth - 1
        For y = 0 To CFm_MyMap.MapHeight - 1
            Get #1, , TileInf(x, y)
        Next y
    Next x

    Get #1, , LyrCount

    For i = 1 To LyrCount
        Set lyr = New cGAMLayer
        Set lyr.MyLevel = Me
        lyr.Load
        Layers.Add lyr
    Next i

End Sub

Public Sub ReInit()

  Dim i   As Integer
  Dim lyr As cGAMLayer

    For i = 1 To Layers.Count
        Set lyr = Layers(i)
        lyr.ReInit
    Next i

End Sub

Public Function RunScript(SubName As String)
    'dummi
End Function

Public Sub Render(ByVal XTo As Long, _
                  ByVal YTo As Long)
If Not Visible Then Exit Sub

  Dim i   As Integer
  Dim lyr As cGAMLayer

    For i = 1 To Layers.Count
        Set lyr = Layers(i)
        lyr.Render XTo, YTo
    Next i

End Sub

Public Sub Save()

  Dim lC       As Integer
  Dim x        As Integer
  Dim y        As Integer
  Dim LyrCount As Integer
  Dim i        As Integer

    Put #1, , CFm_XOffset
    Put #1, , CFm_YOffset
    Put #1, , CFm_Z
    lC = Len(CFm_Desc)
    Put #1, , lC
    Put #1, , CFm_Desc
    For x = 0 To CFm_MyMap.MapWidth - 1
        For y = 0 To CFm_MyMap.MapHeight - 1
            Put #1, , TileInf(x, y)
        Next y
    Next x
    LyrCount = Layers.Count
    Put #1, , LyrCount
    For i = 1 To LyrCount
        Layers(i).Save
    Next i

End Sub

