VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGAMTileset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private CFm_MyMap        As cGAMMap
Private CFm_FileName     As String
Private CFm_TexID        As Long
Private Cfm_ColorKey As Long

Public Property Get MyMap() As cGAMMap

    Set MyMap = CFm_MyMap

End Property

Public Property Set MyMap(PropVal As cGAMMap)

     Set CFm_MyMap = PropVal

End Property

Public Property Get FileName() As String

     FileName = CFm_FileName

End Property

Public Property Let FileName(ByVal PropVal As String)

     CFm_FileName = PropVal

End Property

Public Property Get TexID() As Long

  
    TexID = CFm_TexID

End Property

Public Property Let TexID(ByVal PropVal As Long)

 
    CFm_TexID = PropVal

End Property

Public Sub Initialize(ByVal TileFile As String, Optional colorkey As Long = Const_ColorKey)
CFm_FileName = TileFile
    CFm_TexID = MyMap.MyS2D.TexturePool.AddTexture(TileFile, colorkey)
    Cfm_ColorKey = colorkey
    
End Sub

Public Sub Save()

  Dim s As String

    s = CFm_FileName
    s = Mid(s, InStrRev(s, "\") + 1)
    s = s & String$(255 - Len(s), Chr$(0))
    Put #1, , s
    Put #1, , Cfm_ColorKey
        
End Sub

Public Sub Unload()
MyMap.MyS2D.TexturePool.RemoveTexture CInt(CFm_TexID)
End Sub
