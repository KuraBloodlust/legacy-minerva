Attribute VB_Name = "modPakSystem"
Option Explicit

Public Const cPakVersion = 1

Public Const cPakPassword = "_m!N3|2\/A-R��><" ' ausgeschrieben: _Minerva Roxx :D

Public Type tPakHeader
    Identifier As String * 4
    Version As Integer
    A_HunkAJunk As String * 123
    
    PasswordLength As Byte
    Password As String * 255
    Another_HunkAJunk As String * 35
End Type

Public Type tPakFile
    FileName As String
    Flags As Long
    FileContent As String
    FileSize As Long
End Type

Public ZLib As New cls_zlib



Public Function GenerateRandomString(Length As Integer) As String
Randomize
Dim o As String, i As Integer
For i = 1 To Length
o = o & Chr(Rnd * 255)
Next i
GenerateRandomString = o
End Function

Public Function CodeString(ByRef strSrcString As String, ByRef strPwString As String) As Boolean

    If strPwString = "" Then CodeString = True: Exit Function
    On Error GoTo errorhandler
    
    Dim lt As Long
    Dim lCodeLen As Long
    
   
    Dim arrCode() As Byte
    
    'arrWork = strSrcString ' so geht das doch viel schneller, MasterK :D
    'arrCode = strPwString
    
    Dim i As Long, pwLen As Integer
    pwLen = Len(strPwString)

    ReDim arrCode(1 To pwLen)
    For lt = 1 To Len(strPwString)
        arrCode(lt) = Asc(Mid$(strPwString, lt, 1))
    Next lt
    
    
    For i = 1 To Len(strSrcString)
    Mid$(strSrcString, i, 1) = Chr$(Asc(Mid$(strSrcString, i, 1)) Xor arrCode((i Mod pwLen) + 1))
    Next i
    
    
    
    CodeString = True
    
    Exit Function
errorhandler:
    Debug.Print "Fehler in CodeString"
    Debug.Print "Fehlercode: " & Err.Number
    Debug.Print Err.Description
    
End Function
