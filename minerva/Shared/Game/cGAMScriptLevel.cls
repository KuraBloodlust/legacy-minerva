VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGAMScriptLevel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private CFm_XOffset        As Long
Private CFm_YOffset        As Long
Private CFm_Z              As Double
Private CFm_MyMap          As cGAMMap
Private CFm_Desc           As String
Private CFm_Script         As String
Public Layers As New CHive
Public ScriptClass As New cGAMScript
Public Visible As Boolean

Public Property Get XOffset() As Long

    XOffset = CFm_XOffset

End Property

Public Property Let XOffset(ByVal PropVal As Long)

    CFm_XOffset = PropVal

End Property

Public Property Get YOffset() As Long

 
    YOffset = CFm_YOffset

End Property

Public Property Let YOffset(ByVal PropVal As Long)

    CFm_YOffset = PropVal

End Property

Public Property Get Z() As Double
  
    Z = CFm_Z

End Property

Public Property Let Z(ByVal PropVal As Double)

    CFm_Z = PropVal

End Property

Public Property Get MyMap() As cGAMMap

    Set MyMap = CFm_MyMap

End Property

Public Property Set MyMap(PropVal As cGAMMap)

    Set CFm_MyMap = PropVal

End Property

Public Property Get Desc() As String

    Desc = CFm_Desc

End Property

Public Property Let Desc(ByVal PropVal As String)

    CFm_Desc = PropVal

End Property

Public Property Get Script() As String

    Script = CFm_Script

End Property

Public Property Let Script(ByVal PropVal As String)

    CFm_Script = PropVal

End Property

Public Sub Initialize()



End Sub

Public Sub Load()

  Dim lC As Integer

    Initialize
    Get #1, , CFm_XOffset
    Get #1, , CFm_YOffset
    Get #1, , CFm_Z
    Get #1, , lC
    CFm_Desc = String$(lC, Chr$(0))
    Get #1, , CFm_Desc
    Get #1, , lC
    CFm_Script = String$(lC, Chr$(0))
    Get #1, , CFm_Script
    
    ScriptClass.LoadText Script
    
End Sub

Public Sub ReInit()



End Sub

Public Sub Render(ByVal XTo As Long, _
                  ByVal YTo As Long)
If Not Visible Then Exit Sub

RunScript "Interval"

End Sub

Public Sub Save()

  Dim lC As Integer

    Put #1, , CFm_XOffset
    Put #1, , CFm_YOffset
    Put #1, , CFm_Z
    lC = Len(CFm_Desc)
    Put #1, , lC
    Put #1, , CFm_Desc
    lC = Len(CFm_Script)
    Put #1, , lC
    Put #1, , CFm_Script

End Sub

Public Function RunScript(SubName As String)
    ScriptClass.CallFunc SubName
End Function

Public Sub InheritScript(sName As String)
    ScriptClass.Load AppPath & "\projects\" & CurProj & "\scripts\" & sName
End Sub

Private Sub Class_Initialize()
    Visible = True
    
    If Not IgnoreScripts Then
        Set ScriptClass.ParentObj = Me
    End If
    
    ScriptClass.AddObjects
    
End Sub
