VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGAMScriptAccess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Brightness As Integer
Public BrightnessColor As Long

Private Type StrParam
    Params() As Byte
End Type

'dokumentieren: JA
Public Sub Sleep(miliseconds As Long)
modGlobal.Sleep miliseconds
End Sub

'dokumentieren: JA
Public Function RunDll(libName As String, funcName As String, ParamArray lngFuncParams()) As Long

Dim StrReal() As StrParam
Dim StrCount As Integer

Dim lngFuncRealParams() As Long
ReDim lngFuncRealParams(UBound(lngFuncParams))

Dim i As Integer

For i = 0 To UBound(lngFuncParams)

'MsgBox lngFuncParams(i)

If TypeName(lngFuncParams(i)) = "String" Then
    
    StrCount = StrCount + 1
    ReDim Preserve StrReal(StrCount)
    
    StrReal(StrCount).Params = StrConv(lngFuncParams(i) & Chr(0), vbFromUnicode)

    lngFuncRealParams(i) = CLng(VarPtr(StrReal(StrCount).Params(0)))
    
ElseIf TypeName(lngFuncParams(i)) = "Long" Then

    lngFuncRealParams(i) = CLng(lngFuncParams(i))
    
End If

'MsgBox lngFuncRealParams(i)

Next i

  Dim x
  x = CallApiByName(libName, funcName, lngFuncRealParams())
  
End Function

'dokumentieren: JA
Public Sub AddDebugText(ByVal s As String)
Dim i As Integer
For i = 4 To 0 Step -1
    DebugText(i + 1) = DebugText(i)
Next i


     DebugText(0) = s

End Sub

'dokumentieren: JA
Public Sub Log(Message As String, Optional logType As eLogTypes = ltInformation)
modLogging.Log Message, logType
End Sub

Private Sub Class_Initialize()
Brightness = 255
BrightnessColor = &H4B000C
End Sub

'dokumentieren: JA
Public Property Get GlobalScripts() As Object
Set GlobalScripts = modPlayer.GlobalScripts
End Property

Public Property Set GlobalScripts(vNew As Object)
Set modPlayer.GlobalScripts = vNew
End Property

'dokumentieren: JA
Public Sub AddGlobalScript(Path As String)
Dim gs As New cGAMScript
gs.Load Path
gs.AddObjects
gs.CallFunc "Initialize"
modPlayer.GlobalScripts.Add gs
End Sub

'dokumentieren: JA
Public Property Get IgnorePlayerControls() As Boolean
IgnorePlayerControls = IgnorePlrControls
End Property

Public Property Let IgnorePlayerControls(vNew As Boolean)
IgnorePlrControls = vNew
End Property
