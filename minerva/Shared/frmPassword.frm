VERSION 5.00
Begin VB.Form frmPassword 
   BorderStyle     =   4  'Festes Werkzeugfenster
   Caption         =   "Enter Password"
   ClientHeight    =   1860
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3960
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   3960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'Bildschirmmitte
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2400
      TabIndex        =   4
      Top             =   1320
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   3
      Top             =   1320
      Width           =   1455
   End
   Begin VB.CheckBox chkMask 
      Caption         =   "Mask password"
      Height          =   255
      Left            =   2280
      TabIndex        =   2
      Top             =   960
      Value           =   1  'Aktiviert
      Width           =   1455
   End
   Begin VB.TextBox txtPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   720
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   600
      Width           =   3015
   End
   Begin VB.Label lblPrompt 
      AutoSize        =   -1  'True
      Caption         =   "(Prompt)"
      Height          =   195
      Left            =   720
      TabIndex        =   0
      Top             =   240
      Width           =   585
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   120
      Picture         =   "frmPassword.frx":0000
      Top             =   120
      Width           =   480
   End
End
Attribute VB_Name = "frmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkMask_Click()
txtPassword.PasswordChar = IIf(chkMask.Value = 1, "*", "")
End Sub

Private Sub cmdCancel_Click()
RetOk = 0
Unload Me
End Sub

Private Sub cmdOK_Click()
RetOk = 1
RetPassword = txtPassword.Text
Unload Me
End Sub

Private Sub txtPassword_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyReturn Then cmdOK_Click
End Sub
