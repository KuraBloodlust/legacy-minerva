Attribute VB_Name = "modFileProvider"
Option Explicit

'
'modFileProvider: Stellt Dateien aus Verzeichnissystem / Packages f�r andere Programmteile bereit.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' Alle normalen Zugriffe durch andere Programmteile auf Packages sollten durch diese Funktionen erfolgen.
'

Public Function ProvideDoesExist(FileName As String, Optional LookingForFolder As Boolean = False) As Boolean
Dim FN As String
Dim source As String
Dim SourceID As Integer
Dim Pos As Integer

'check for specified source
Pos = InStr(1, FileName, "://")

If Pos > 0 Then
    'source was specified
    source = Left(FileName, Pos - 1)
    FN = Mid(FileName, Pos + 3)
Else
    'we will need so search for the source
    FN = FileName
    source = FindSource(FileName)
End If

If LCase(source) = "folder" Then
    SourceID = 0
Else
    SourceID = GetSourceID(source)
End If


If SourceID = 0 Then
    If LookingForFolder Then
        ProvideDoesExist = FolderExists(App.Path & "\projects\" & CurProj & "\" & FN)
    Else
        ProvideDoesExist = FileExists(App.Path & "\projects\" & CurProj & "\" & FN)
    End If
Else
    ProvideDoesExist = DoesExistInPackage(FN, SourceID, LookingForFolder)
End If

End Function

Public Function ProvideFile(FileName As String, ByRef usedTempFile As Boolean) As String


Dim FN As String
Dim source As String
Dim SourceID As Integer
Dim Pos As Integer

'check for specified source
Pos = InStr(1, FileName, "://")

If Pos > 0 Then
    'source was specified
    source = Left(FileName, Pos - 1)
    FN = Mid(FileName, Pos + 3)
Else
    'we will need so search for the source
    FN = FileName
    source = FindSource(FileName)
End If


If LCase(source) = "folder" Then
    SourceID = 0
Else
    SourceID = GetSourceID(source)
End If

If SourceID = 0 Then
    ProvideFile = App.Path & "\projects\" & CurProj & "\" & FN
    usedTempFile = False
Else
    ProvideFile = ExtractAndProvideFile(FN, SourceID)
    usedTempFile = True
End If
End Function

Public Function StoreFile(FileName As String, Content As String) As Boolean
Dim FN As String
Dim source As String
Dim SourceID As Integer
Dim Pos As Integer

'check for specified source
Pos = InStr(1, FileName, "://")

If Pos > 0 Then
    'source was specified
    source = Left(FileName, Pos - 1)
    FN = Mid(FileName, Pos + 3)
Else
    Err.Raise -1, "FileProvider", "StoreFile needs source to be specified. ('" & FileName & "')"
End If


If LCase(source) = "folder" Then
    SourceID = 0
Else
    SourceID = GetSourceID(source)
End If

If SourceID = 0 Then
    Dim F As String
    F = App.Path & "\projects\" & CurProj & "\" & FN
    
    If FileExists(F) Then Kill F
    Open F For Binary Access Write As 1
    Put #1, , Content
    Close 1
    
Else
    
    CompressAndStoreFile FN, SourceID, Content
End If

StoreFile = True
End Function

Public Function DeleteFile(FileName As String, Optional DeleteFolder As Boolean = False)
Dim FN As String
Dim source As String
Dim SourceID As Integer
Dim Pos As Integer

'check for specified source
Pos = InStr(1, FileName, "://")

If Pos > 0 Then
    'source was specified
    source = Left(FileName, Pos - 1)
    FN = Mid(FileName, Pos + 3)
Else
    Err.Raise -1, "FileProvider", "DeleteFile needs source to be specified. ('" & FileName & "')"
End If


If LCase(source) = "folder" Then
    SourceID = 0
Else
    SourceID = GetSourceID(source)
End If

If SourceID = 0 Then
    Dim F As String
    F = App.Path & "\projects\" & CurProj & "\" & FN
    
    If Not DeleteFolder Then
        If FileExists(F) Then Kill F
    Else
        If FolderExists(F) Then WindowsDelete F
    End If
Else
    RemoveFile FN, SourceID, DeleteFolder
End If

DeleteFile = True

End Function

Public Function RenameFileOrFolder(FileName As String, FileNameNew As String, Optional IsFolder As Boolean = False)
Dim FN As String, FN2 As String
Dim source As String
Dim SourceID As Integer
Dim Pos As Integer, pos2 As Integer

'check for specified source
Pos = InStr(1, FileName, "://")
pos2 = InStr(1, FileNameNew, "://")

If (Pos > 0) And (pos2 > 0) Then
    'source was specified
    source = Left(FileName, Pos - 1)
    
    
    FN = Mid(FileName, Pos + 3)
    FN2 = Mid(FileNameNew, pos2 + 3)
Else
    Err.Raise -1, "FileProvider", "RenameFileOrFolder needs source to be specified. ('" & FileName & "')"
End If


If LCase(source) = "folder" Then
    SourceID = 0
Else
    SourceID = GetSourceID(source)
End If

If SourceID = 0 Then
    Dim F As String
    Dim f2 As String
    
    F = App.Path & "\projects\" & CurProj & "\" & FN
    f2 = App.Path & "\projects\" & CurProj & "\" & FN2
    
    MoveFile F, f2
Else
    RenamePFile FN, FN2, SourceID, IsFolder
End If

RenameFileOrFolder = True

End Function

Public Function MakeDirectory(FileName As String)
Dim FN As String
Dim source As String
Dim SourceID As Integer
Dim Pos As Integer

'check for specified source
Pos = InStr(1, FileName, "://")

If Pos > 0 Then
    'source was specified
    source = Left(FileName, Pos - 1)
    FN = Mid(FileName, Pos + 3)
Else
    Err.Raise -1, "FileProvider", "MakeDirectory needs source to be specified. ('" & FileName & "')"
End If


If LCase(source) = "folder" Then
    SourceID = 0
Else
    SourceID = GetSourceID(source)
End If

If SourceID = 0 Then
    MkDir App.Path & "\projects\" & CurProj & "\" & FN
Else
    CreateDirectory FN, SourceID
End If

MakeDirectory = True

End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' helper functions
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Private Function FindSource(FileName As String) As String

If Dir(App.Path & "\" & CurProj & "\" & FileName) <> "" Then FindSource = "folder"

Dim i As Integer, D As cGAMPackage
For i = 1 To LoadedDependencies.Count
    Set D = LoadedDependencies(i)
    If D.RPAK.Root.DoesExist(FileName) Then
        FindSource = D.RPAK.FileName
        FindSource = Mid(FindSource, InStrRev(FindSource, "\") + 1)
        Exit Function
    End If
Next i

If Project.Package.RPAK.Root.DoesExist(FileName) Then
FindSource = Project.Package.RPAK.FileName
FindSource = Mid(FindSource, InStrRev(FindSource, "\") + 1)
End If

End Function

Private Function GetSourceID(SourceName As String) As String
Dim i As Integer, a As String
Dim D As cGAMPackage

If LCase(SourceName) = LCase(CurProj) & ".rpak" Then
    GetSourceID = -2
    Exit Function
End If

For i = 1 To LoadedDependencies.Count
    Set D = LoadedDependencies(i)
    a = LCase(D.RPAK.FileName)
    a = Mid(a, InStrRev(a, "\") + 1)
    If a = LCase(SourceName) Then GetSourceID = i: Exit Function
Next i

GetSourceID = -1
End Function

Private Function RemoveFile(FileName As String, SourceID As Integer, Optional DeleteFolder As Boolean = False)
Dim s As cGAMPackage

If SourceID = -1 Then
    Exit Function
ElseIf SourceID > 0 Then
    Set s = LoadedDependencies(SourceID)
ElseIf SourceID = -2 Then
    Set s = Project.Package
End If

Dim fldr As String
fldr = Left(FileName, InStrRev(FileName, "\"))

Dim F As cRPakFolder
Set F = s.RPAK.Root.GetFolder(fldr)

Dim FN As String
FN = Mid(FileName, InStrRev(FileName, "\") + 1)

Dim fId As Integer

If DeleteFolder Then
    fId = F.FindFolder(FN)
    If fId >= 0 Then F.DeleteFolder fId
Else



    fId = F.FindFile(FN)
    If fId >= 0 Then F.DeleteFile fId
End If

s.FileSave s.RPAK.FileName
End Function

Private Function ExtractAndProvideFile(FileName As String, SourceID As Integer)

Dim s As cGAMPackage

If SourceID = -1 Then
    Exit Function
ElseIf SourceID > 0 Then
    Set s = LoadedDependencies(SourceID)
ElseIf SourceID = -2 Then
    Set s = Project.Package
End If

Dim fContent As String
fContent = s.RPAK.Root.GetContent(FileName)

Dim ext As String
ext = Mid(FileName, InStrRev(FileName, ".") + 1)

Dim sTemp As String
sTemp = ProvideTempFile(ext)

Dim ff As Integer
ff = FreeFile()

Open sTemp For Binary Access Write As #ff
    Put #ff, , fContent
Close ff

ExtractAndProvideFile = sTemp
End Function

Public Function ProvideTempFile(ext As String) As String
Dim sTemp2 As String
Dim sTemp As String

    
    sTemp2 = String(260, 0)
    GetTempPath Len(sTemp2), sTemp2
    sTemp2 = Replace(sTemp2, Chr(0), "")
    
    sTemp = String(260, 0)
    GetTempFileName sTemp2, "Minerva", 0, sTemp
    
    sTemp = Replace(sTemp, Chr(0), "")
    Kill sTemp
    sTemp = sTemp & "." & ext
    
If FileExists(sTemp) Then Kill sTemp
ProvideTempFile = sTemp
End Function


Private Function CompressAndStoreFile(FileName As String, SourceID As Integer, Content As String) As Boolean
Dim s As cGAMPackage

If SourceID = -1 Then
    Exit Function
ElseIf SourceID > 0 Then
    Set s = LoadedDependencies(SourceID)
ElseIf SourceID = -2 Then
    Set s = Project.Package
End If

Dim fldr As String
fldr = Left(FileName, InStrRev(FileName, "\"))

Dim F As cRPakFolder
Set F = s.RPAK.Root.GetFolder(fldr)

Dim FN As String
FN = Mid(FileName, InStrRev(FileName, "\") + 1)

Dim fId As Integer
fId = F.FindFile(FN)

If fId >= 0 Then
    F.FileContent(fId) = Content
Else
    F.AddFile FN, Content
End If

s.FileSave s.RPAK.FileName
End Function

Public Function GetRPAKFolder(FileName As String) As cRPakFolder

    Dim FN As String
    Dim source As String
    Dim SourceID As Integer
    Dim Pos As Integer

    'check for specified source
    Pos = InStr(1, FileName, "://")

    If Pos > 0 Then
        'source was specified
        source = Left(FileName, Pos - 1)
        FN = Mid(FileName, Pos + 3)
    Else
        'we will need so search for the source
        FN = FileName
        source = FindSource(FileName)
    End If

    If LCase(source) = "folder" Then
        Err.Raise 1234, "FileProvider", "Only folders in RPAK Files are supported!"
    Else
        SourceID = GetSourceID(source)
    End If

    If SourceID = 0 Then
    Else
        Dim s As cGAMPackage

        If SourceID = -1 Then
            Exit Function
        ElseIf SourceID > 0 Then
            Set s = LoadedDependencies(SourceID)
        ElseIf SourceID = -2 Then
            Set s = Project.Package
        End If

        Set GetRPAKFolder = s.RPAK.Root.GetFolder(FN)
    End If

End Function

Private Function RenamePFile(ByVal FileName As String, _
ByVal FileNameNew As String, ByVal SourceID As Integer, _
ByVal Folder As Boolean) As Boolean

Dim s As cGAMPackage

If SourceID = -1 Then
    Exit Function
ElseIf SourceID > 0 Then
    Set s = LoadedDependencies(SourceID)
ElseIf SourceID = -2 Then
    Set s = Project.Package
End If

Dim fldr As String
fldr = Left(FileName, InStrRev(FileName, "\"))

Dim F As cRPakFolder
Set F = s.RPAK.Root.GetFolder(fldr)

Dim FN As String, FN2 As String
FN = Mid(FileName, InStrRev(FileName, "\") + 1)
FN2 = Mid(FileNameNew, InStrRev(FileNameNew, "\") + 1)

Dim fId As Integer

'Is the Object a Folder?
If Folder Then

  fId = F.FindFolder(FN)

  If fId >= 0 Then
  F.Folders(fId).FolderName = FN2
  Else
  Err.Raise 11234, "RenamePFile", "Folder not found: " & FN
  End If

Else
  fId = F.FindFile(FN)
  
   If fId >= 0 Then
   F.FileName(fId) = FN2
   Else
   Err.Raise 11234, "RenamePFile", "File not found: " & FN
   End If

End If

s.FileSave s.RPAK.FileName
End Function


Private Function DoesExistInPackage(FileName As String, SourceID As Integer, Optional LookingForFolder As Boolean = False) As Boolean
On Error GoTo fehler

Dim s As cGAMPackage

If SourceID = -1 Then
    Exit Function
ElseIf SourceID > 0 Then
    Set s = LoadedDependencies(SourceID)
ElseIf SourceID = -2 Then
    Set s = Project.Package
End If


Dim fldr As String
fldr = Left(FileName, InStrRev(FileName, "\"))
Dim F As cRPakFolder
Set F = s.RPAK.Root.GetFolder(fldr)
Dim FN As String
FN = Mid(FileName, InStrRev(FileName, "\") + 1)

Dim fId As Integer, fldrId As Integer
fId = F.FindFile(FN)
fldrId = F.FindFolder(FN)

If LookingForFolder Then
    DoesExistInPackage = (fldrId >= 0)
Else
    DoesExistInPackage = (fId >= 0)
End If

Exit Function
fehler:
DoesExistInPackage = False
End Function


Private Function CreateDirectory(FileName As String, SourceID As Integer) As Boolean
Dim s As cGAMPackage

If SourceID = -1 Then
    Exit Function
ElseIf SourceID > 0 Then
    Set s = LoadedDependencies(SourceID)
ElseIf SourceID = -2 Then
    Set s = Project.Package
End If

Dim fldr As String
fldr = Left(FileName, InStrRev(FileName, "\"))

Dim F As cRPakFolder
Set F = s.RPAK.Root.GetFolder(fldr)

Dim FN As String
FN = Mid(FileName, InStrRev(FileName, "\") + 1)

Dim fId As Integer
fId = F.FindFolder(FN)

If fId < 0 Then
    F.AddFolder FN
End If

s.FileSave s.RPAK.FileName
End Function
