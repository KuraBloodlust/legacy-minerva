VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DWaypoint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private Type WayPoint
    Points()                As Point
    PointCount              As Integer
    CurrentPoint            As Integer
End Type
Private WayPoints()       As WayPoint
Private WayPointCount     As Integer

Public Sub ClearPoints()
ReDim Points(0 To 0) As WayPoint
WayPointCount = 0
End Sub

Public Function AddWayPointCollection() As Integer

    WayPointCount = WayPointCount + 1
    AddWayPointCollection = WayPointCount
    ReDim Preserve WayPoints(0 To WayPointCount)

End Function

Public Function Play(ByVal ID As Integer, _
                     ByVal X As Double, _
                     ByVal Y As Double, _
                     Optional Speed As Double = 1) As Point
On Error Resume Next
    Play.X = X
    Play.Y = Y
    If X < WayPoints(ID).Points(WayPoints(ID).CurrentPoint).X Then
        Play.X = X + Speed
    End If
    If X > WayPoints(ID).Points(WayPoints(ID).CurrentPoint).X Then
        Play.X = X - Speed
    End If
    If Y < WayPoints(ID).Points(WayPoints(ID).CurrentPoint).Y Then
        Play.Y = Y + Speed
    End If
    If Y > WayPoints(ID).Points(WayPoints(ID).CurrentPoint).Y Then
        Play.Y = Y - Speed
    End If
    With WayPoints(ID)
        If X = .Points(.CurrentPoint).X Then
            If Y = .Points(.CurrentPoint).Y Then
                .CurrentPoint = .CurrentPoint + 1
                If .CurrentPoint > .PointCount Then
                    .CurrentPoint = 1
                End If
            End If
        End If
    End With 'WayPoints(ID)

End Function


Public Function AddWayPoint(ByVal ID As Integer, ByVal X As Double, ByVal Y As Double) As Integer

With WayPoints(ID)
.PointCount = .PointCount + 1
ReDim Preserve .Points(1 To WayPoints(ID).PointCount)
.Points(.PointCount).X = X
.Points(.PointCount).Y = Y
.CurrentPoint = 1
End With 'WayPoints(ID)

End Function
