VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DOnline"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private CurrentData     As String

Public Function GetData() As String
    GetData = CFm_CurrentData
End Function

Public Sub SendData(ByVal pstrSendData As String)
    frmMain.wsClient.SendData pstrSendData
End Sub

Public Sub ConnectToIp(ByVal Ip As String)

    frmMain.wsClient.RemoteHost = Ip
    frmMain.wsClient.Connect
    Do Until frmMain.wsClient.State = 7
        If frmMain.wsClient.State = 0 Or frmMain.wsClient.State = 9 Then
            MsgBox "Error at connecting!", vbCritical, "Winsock Error"
            Exit Function
        End If
        If GetInputState() <> 0 Then
            DoEvents
        End If
    Loop
    frmMain.tmrDisconnect.Enabled = True
    
End Function
