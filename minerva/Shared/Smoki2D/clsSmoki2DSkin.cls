VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DSkin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function MakeRegion(Source As Form, _
                           Optional lngColor As Long) As Long


  Dim X             As Long
  Dim Y             As Long
  Dim StartLineX    As Long
  Dim FullRegion    As Long
  Dim LineRegion    As Long
  Dim InFirstRegion As Boolean
  Dim InLine        As Boolean
  Dim hDC           As Long
  Dim Width         As Long
  Dim Height        As Long

    With Source
        hDC = .hDC
        Width = .ScaleWidth
        Height = .ScaleHeight
    End With 'Source
    InFirstRegion = True
    InLine = False
    X = Y = StartLineX = 0
    If lngColor = 0 Then
        lngColor = GetPixel(hDC, 0, 0)
    End If
    For Y = 0 To Height - 1
        For X = 0 To Width - 1
            If GetPixel(hDC, X, Y) = lngColor Or X = Width Then
                If InLine Then
                    InLine = False
                    LineRegion = CreateRectRgn(StartLineX, Y, X, Y + 1)
                    If InFirstRegion Then
                        FullRegion = LineRegion
                        InFirstRegion = False
                     Else 'INFIRSTREGION = FALSE/0
                        CombineRgn FullRegion, FullRegion, LineRegion, RGN_OR
                        DeleteObject LineRegion
                    End If
                End If
             Else 'NOT GETPIXEL(HDC,...
                If Not InLine Then
                    InLine = True
                    StartLineX = X
                End If
            End If
        Next '  X
    Next '  Y
    MakeRegion = FullRegion

End Function

Public Sub MakeSkin(Source As Form, ByVal File As String)
Dim WindowRegion As Long
Source.ScaleMode = vbPixels
Source.AutoRedraw = True
Set Source.Picture = LoadPicture(File)
'WindowRegion = Skin.MakeRegion(Source)
SetWindowRgn Source.hWnd, WindowRegion, True
End Sub

