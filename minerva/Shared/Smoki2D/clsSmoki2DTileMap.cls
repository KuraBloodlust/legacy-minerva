VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DTileMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private Type Tile
    SourceX                     As Double
    SourceY                     As Double
    Drawn                       As Boolean
End Type
Private Type Layer
    Tiles()                     As Tile
End Type
Private Layers()              As Layer
Private TileSet               As Integer
Private MapWidth              As Integer
Private MapHeight             As Integer
Private Const TileWidth       As Integer = 32
Private Const TileHeight      As Integer = 32
Private CFm_Zoom                   As Double

Public Property Get Zoom() As Double

    Zoom = CFm_Zoom

End Property

Public Property Let Zoom(ByVal PropVal As Double)

    CFm_Zoom = PropVal

End Property

Public Sub LoadMap(ByVal strFilename As String)

 
  Dim TempX  As Integer
  Dim TempY  As Integer
  Dim TempSX As Integer
  Dim TempSY As Integer

    ReDim Layers(0 To 1)
    CFm_Zoom = 1
    TileSet = engine.TexturePool.AddTexture(App.Path & "/tileset.png", rgb(255, 0, 255))
    Open strFilename For Input As #1
    Input #1, MapWidth, MapHeight
    ReDim Layers(0).Tiles(0 To MapWidth, 0 To MapHeight)
    Do Until EOF(1)
        Input #1, TempX, TempY, TempSX, TempSY
        With Layers(0)
            .Tiles(TempX, TempY).SourceX = TempSX
            .Tiles(TempX, TempY).SourceY = TempSY
            .Tiles(TempX, TempY).Drawn = True
        End With 'Layers(0)
    Loop
    Close #1

End Sub


''Public Sub RenderMap(ByVal X As Double, ByVal Y As Double)
''
''Dim a
''Dim b
''Dim StartX As Integer
''Dim StartY As Integer
''Dim EndX   As Integer
''Dim EndY   As Integer
''StartX = Format$(-1 * X / TileWidth, 0) - 2
''If StartX < 0 Then
''StartX = 0
''End If
''StartY = Format$(-1 * Y / TileHeight, 0) - 2
''If StartY < 0 Then
''StartY = 0
''End If
''EndX = StartX + Format$(ScreenWidth / TileWidth, 0) + 4
''If EndX > MapWidth Then
''EndX = MapWidth
''End If
''EndY = StartY + Format$(ScreenHeight / TileHeight, 0) + 4
''If EndY > MapHeight Then
''EndY = MapHeight
''End If
''For a = StartX To EndX
''For b = StartY To EndY
''If Layers(0).Tiles(a, b).Drawn Then
''TexturePool.RenderTexture TileSet, (a * TileWidth + X - TileWidth) * CFm_Zoom, (b * TileHeight + Y - TileHeight) * CFm_Zoom, TileWidth * CFm_Zoom, TileHeight * CFm_Zoom, TileWidth, TileHeight, Layers(0).Tiles(a, b).SourceX * TileWidth, Layers(0).Tiles(a, b).SourceY * TileHeight, 120
''End If
''Next b
''Next a
''End Function

'''~~~'Public Sub GenerateMap()
'''~~~'
'''~~~''<:-) :WARNING: Unused Sub GenerateMap
'''~~~''<:-) May be a prototype Sub you have not yet implimented or left over from a deleted Control.
'''~~~''<:-) :WARNING: Function changed to Sub as nothing is returned via the Function Name.
'''~~~'Dim m As Long '<:-) Missing Dim Auto-inserted (Auto-Type may not be correct)
'''~~~''<:-) May be Control name using default property or a control(probably Form) Property being used with default assignment (not a good idea; be explicit)
'''~~~'Open App.Path & "\test.rmap" For Output As #1
'''~~~'Print #1, "64", "64"
'''~~~'Randomize Timer
'''~~~'For i = 1 To 64
'''~~~'For m = 1 To 64
'''~~~'Print #1, i, m, Int(3 * Rnd), Int(3 * Rnd)
'''~~~'Next m
'''~~~'Next i
'''~~~'Close #1
'''~~~'End Function
