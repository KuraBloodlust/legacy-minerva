VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DInputbox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private Type IO
    X                                                 As Double
    Y                                                 As Double
    Width                                             As Double
    Height                                            As Double
    Color                                             As Long
    Alpha                                             As Integer
    Text                                              As String
    UnformattedText                                   As String
    CurrentCharacter                                  As Integer
    Visible                                           As Boolean
    OnWait                                            As Integer
End Type
Private Enum eButtons
    OkOnly
    OkCancel
    YesNo
    YesNoCancel
    NoButtons_Input
    Custom
End Enum
#If False Then 'Trick preserves Case of Enums when typing in IDE
Private OkOnly, OkCancel, YesNo, YesNoCancel, NoButtons_Input, Custom
#End If
Private Enum eStyle
    AlignButtonsLeft
    AlignButtonsRight
    AlignButtonsTop
    AlignButtonsBottom
End Enum
#If False Then 'Trick preserves Case of Enums when typing in IDE
Private AlignButtonsLeft, AlignButtonsRight, AlignButtonsTop, AlignButtonsBottom
#End If
