VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DNPC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public TexID As Integer 'dokumentieren: NEIN
Public S2D As cS2D_i 'dokumentieren: NEIN

Public x As Double 'dokumentieren: JA
Public y As Double 'dokumentieren: JA
Public Z As Double 'dokumentieren: JA
Public ZPlus As Double 'dokumentieren: JA
Public imgfile As String 'dokumentieren: JA
Public Name As String 'dokumentieren: JA
Public Color As Long 'dokumentieren: JA
Public Alpha As Byte 'dokumentieren: JA
Public Additional As Integer 'dokumentieren: JA
Public OriginalhDC As Long

Public MyMap As cGAMMap 'dokumentieren: NEIN
Public MyS2D As cS2D_i

Public JumpPos As Double 'dokumentieren: JA
Public StairHeight As Double 'dokumentieren: JA
Public Depth As Double 'dokumentieren: JA

Public Index As Integer 'dokumentieren: NEIN
Public MyColorKey As Long 'dokumentieren: JA

Public Jumping As Boolean 'dokumentieren: JA
Public JumpSpeed As Double 'dokumentieren: JA
Public JumpDuration As Double 'dokumentieren: JA

Public SourceX As Integer 'dokumentieren: JA
Public SourceY As Integer 'dokumentieren: JA
Public SourceWidth As Integer 'dokumentieren: JA
Public SourceHeight As Integer 'dokumentieren: JA

Private devil As New cDevIL
Private Image As cDevILImage
Public MyDC As Long, mBitmap As Long, mBitmap2 As Long 'dokumentieren: NEIN

Public CurrentLevel As Integer 'dokumentieren: JA
Public ZToGround As Long 'dokumentieren: JA
Public Direction As Integer 'dokumentieren: JA

Public Script As New cGAMScript 'dokumentieren: JA

Private PathWay As New cS2DWaypoint
Private GoalX As Integer, GoalY As Integer

Public Moving As Boolean 'dokumentieren: JA
Public Use3dCol As Boolean 'dokumentieren: JA
Public Blocking As Boolean 'dokumentieren: JA
Public Silent As Boolean

Public MaskXStart As Integer
Public MaskYStart As Integer
Public MaskXEnd As Integer
Public MaskYEnd As Integer

'dokumentieren: JA
Public Property Get Visible() As Boolean

    Dim vVis As Boolean
    vVis = True
    
    If x < OldCameraX * -1 Then vVis = False
    If y < OldCameraY * -1 Then vVis = False
    If x > OldCameraX * -1 + ScreenWidth Then vVis = False
    If y > OldCameraY * -1 + ScreenHeight Then vVis = False
    
    Visible = vVis

End Property

'dokumentieren: NEIN
Public Sub Load()

    Dim Npc As tMapNPC
    Dim nlen As Integer
    Dim ScriptText As String

    Get #1, , nlen
    Name = String(nlen, Chr(0))
    Get #1, , Name
        
    Get #1, , nlen
    ScriptText = String(nlen, Chr(0))
    Get #1, , ScriptText
    Script.LoadText (ScriptText)
    
    Get #1, , nlen
    imgfile = String(nlen, Chr(0))
    Get #1, , imgfile
    
    Get #1, , Npc
    
    Dim TColor As Long
    Get #1, , TColor

    x = Npc.x
    y = Npc.y
    SourceX = Npc.SourceX
    SourceY = Npc.SourceY
    SourceWidth = Npc.SourceWidth
    SourceHeight = Npc.SourceHeight

    CurrentLevel = Npc.Level
    If CurrentLevel = 0 Then CurrentLevel = 1
    Z = Map.Levels(CurrentLevel).Z
    
    ZPlus = Npc.ZPlus
    Color = Npc.Color
    Alpha = Npc.Alpha
    Additional = Npc.Additional
    Depth = Npc.Depth
    MaskXStart = Npc.MaskXStart
    MaskXEnd = Npc.MaskXEnd
    MaskYStart = Npc.MaskYStart
    MaskYEnd = Npc.MaskYEnd
    
    If Not IgnoreScripts Then Script.AddObjects
    
    Set S2D = MyMap.MyS2D
    Initialize imgfile, TColor, SourceX, SourceY, SourceWidth, SourceHeight

End Sub

'dokumentieren: NEIN
Public Sub Save()
    
    Dim Npc As tMapNPC
    Dim nlen As Integer
    
    Npc.x = x
    Npc.y = y
    Npc.SourceX = SourceX
    Npc.SourceY = SourceY
    Npc.SourceWidth = SourceWidth
    Npc.SourceHeight = SourceHeight
    Npc.ZPlus = ZPlus
    Npc.Level = CurrentLevel
    Npc.Color = Color
    Npc.Alpha = Alpha
    Npc.Additional = Additional
    Npc.MaskXStart = MaskXStart
    Npc.MaskXEnd = MaskXEnd
    Npc.MaskYStart = MaskYStart
    Npc.MaskYEnd = MaskYEnd
    
    nlen = Len(Name)
    Put #1, , nlen
    Put #1, , Name
    
    nlen = Len(Script.ScriptText)
    Put #1, , nlen
    Put #1, , Script.ScriptText
    
    nlen = Len(imgfile)
    Put #1, , nlen
    Put #1, , imgfile
    
    Put #1, , Npc
    Put #1, , MyColorKey

End Sub

'dokumentieren: JA
Public Sub AdjustCamera()
    
    Dim PlayerCameraX As Double
    Dim PlayerCameraY As Double
    
    PlayerCameraX = (ScreenWidth / 2) - Me.x - (Me.Width / 2)
    PlayerCameraY = (ScreenHeight / 2) - Me.y + Z + ZPlus - (Me.Height / 2)
    
    CameraSpeedX = Round(CameraSpeedX, 1)
    CameraSpeedY = Round(CameraSpeedY, 1)
    
    If Not Ceil(CameraX, -1) = Ceil(PlayerCameraX, -1) Then
        If CameraX < (PlayerCameraX) Then
            If CameraSpeedX < 0 Then
                CameraSpeedX = CameraSpeedX + GeneralCameraSpeed * GeneralCameraSpeedFactor
            Else
                CameraSpeedX = CameraSpeedX + GeneralCameraSpeed
            End If
        End If
    
        If CameraX > (PlayerCameraX) Then
            If CameraSpeedX > 0 Then
                CameraSpeedX = CameraSpeedX - GeneralCameraSpeed * GeneralCameraSpeedFactor
            Else
                CameraSpeedX = CameraSpeedX - GeneralCameraSpeed
            End If
        End If
    Else
        If CameraSpeedX <= GeneralCameraSpeed * GeneralCameraSpeedFactor And CameraSpeedX >= 0 Then CameraSpeedX = 0
        If CameraSpeedX >= -GeneralCameraSpeed * GeneralCameraSpeedFactor And CameraSpeedX <= 0 Then CameraSpeedX = 0
    End If
    
    If CameraSpeedX < 0 Then
        
        If CameraX - PlayerCameraX > GeneralCameraMaxDistance Then
            CameraX = CameraX + (GeneralCameraMaxDistance - (CameraX - PlayerCameraX))
        End If
        
        If CameraSpeedX < -GeneralCameraMaxSpeed Then
            CameraSpeedX = -GeneralCameraMaxSpeed
        End If
    Else
    
        If CameraX - PlayerCameraX < -GeneralCameraMaxDistance Then
            CameraX = CameraX + (-GeneralCameraMaxDistance - (CameraX - PlayerCameraX))
        End If
        
        If CameraSpeedX > GeneralCameraMaxSpeed Then
            CameraSpeedX = GeneralCameraMaxSpeed
        End If
    End If
    
    CameraX = CameraX + CameraSpeedX
    
    If Not Ceil(CameraY, -1) = Ceil(PlayerCameraY, -1) Then
        If CameraY < (PlayerCameraY) Then
            If CameraSpeedY < 0 Then
                CameraSpeedY = CameraSpeedY + GeneralCameraSpeed * GeneralCameraSpeedFactor
            Else
                CameraSpeedY = CameraSpeedY + GeneralCameraSpeed
            End If
        End If
    
        If CameraY > (PlayerCameraY) Then
            If CameraSpeedY > 0 Then
                CameraSpeedY = CameraSpeedY - GeneralCameraSpeed * GeneralCameraSpeedFactor
            Else
                CameraSpeedY = CameraSpeedY - GeneralCameraSpeed
            End If
        End If
    Else
        If CameraSpeedY <= GeneralCameraSpeed * GeneralCameraSpeedFactor And CameraSpeedY >= 0 Then CameraSpeedY = 0
        If CameraSpeedY >= -GeneralCameraSpeed * GeneralCameraSpeedFactor And CameraSpeedY <= 0 Then CameraSpeedY = 0
    End If
    
    If CameraSpeedY < 0 Then
    
        If CameraY - PlayerCameraY > GeneralCameraMaxDistance Then
            CameraY = CameraY + (GeneralCameraMaxDistance - (CameraY - PlayerCameraY))
        End If
        
        If CameraSpeedY < -GeneralCameraMaxSpeed Then
            CameraSpeedY = -GeneralCameraMaxSpeed
        End If
    Else
    
        If CameraY - PlayerCameraY < -GeneralCameraMaxDistance Then
            CameraY = CameraY + (-GeneralCameraMaxDistance - (CameraY - PlayerCameraY))
        End If
        
        If CameraSpeedY > GeneralCameraMaxSpeed Then
            CameraSpeedY = GeneralCameraMaxSpeed
        End If
    End If
    
    CameraY = CameraY + CameraSpeedY
    
    
    'CameraX = (S2D.ScreenWidth / 2) - Me.x - (Me.Width / 2)
    'CameraY = (S2D.ScreenHeight / 2) - Me.y + Z + ZPlus - (Me.Height / 2)

End Sub

'dokumentieren: JA
Public Sub FocusCamera()

    Set FocusNPC = Me
    AdjustCamera
    
End Sub

'dokumentieren: JA
Public Sub Initialize(FileName As String, Optional colorkey As Long = vbMagenta, Optional sx As Integer, Optional sy As Integer, Optional sw As Integer, Optional sh As Integer)

    Dim tmp As String
    Dim ARGBColorKey As Long
    Dim clr As ColorRGB

    If TexID > 0 Then S2D.TexturePool.RemoveTexture TexID

    tmp = FileName
    imgfile = FileName
    MyColorKey = colorkey
    clr = SplitColor(colorkey)
    ARGBColorKey = D3DColorARGB(255, clr.r, clr.g, clr.b)
    TexID = S2D.TexturePool.AddTexture("images\" & imgfile, ARGBColorKey)
    
    Dim MyColor As Long
    MyColor = colorkey
    
    Dim FN As String
    Dim tf As Boolean
    FN = ProvideFile("images\" & FileName, tf)
    
    If Right(FileName, 4) = "rani" Then
        SourceWidth = S2D.TexturePool.TextureWidth(TexID)
        SourceHeight = S2D.TexturePool.TextureHeight(TexID)
        If MaskXEnd = 0 Then MaskXEnd = SourceWidth
        If MaskYEnd = 0 Then MaskYEnd = SourceHeight
        Kill FN
        Exit Sub
    End If
    
    If Right(FileName, 4) = "aniq" Then
    
        Dim l     As Integer

        Dim tani As Boolean
        tani = True
        
        Open FN For Binary Access Read As #2
        Get #2, , l
        imgfile = String(l, Chr(0))
        Get #2, , imgfile
        
        Dim AniC As Integer
        Get #2, , AniC
    
        Get #2, , MyColor
        Dim tWidth As Integer
        Dim tHeight As Integer
        
        Get #2, , tWidth
        Get #2, , tHeight
        
        Close #2
        
    End If
    
    If sw = 0 Then SourceWidth = S2D.TexturePool.TextureWidth(TexID) Else SourceWidth = sw
    If sh = 0 Then SourceHeight = S2D.TexturePool.TextureHeight(TexID) Else SourceHeight = sh
    SourceX = sx
    SourceY = sy
    
    MyDC = CreateCompatibleDC(GetDC(0))
    mBitmap = CreateCompatibleBitmap(GetDC(0), Me.Width, Me.Height)
    SelectObject MyDC, mBitmap
    
    OriginalhDC = CreateCompatibleDC(GetDC(0))
    mBitmap2 = CreateCompatibleBitmap(GetDC(0), Me.Width, Me.Height)
    SelectObject OriginalhDC, mBitmap2

    If tani Then
        Set Image = devil.LoadImage(ProvideFile("images\" & imgfile, tf))
    Else
        Set Image = devil.LoadImage(FN)
    End If
    
    Image.RenderTo OriginalhDC, 0, 0, 0, 0, Me.Width, Me.Height
    

    If MaskXEnd = 0 Then MaskXEnd = Me.Width
    If MaskYEnd = 0 Then MaskYEnd = Me.Height
    
    Dim TX As Integer, Ty As Integer
    
    For TX = 0 To Me.Width
        For Ty = 0 To Me.Height
            SetPixel MyDC, TX, Ty, MyColor
        Next Ty
    Next TX
    
    Dim XMask As Integer, YMask As Integer
    For XMask = MaskXStart To MaskXEnd
        For YMask = MaskYStart To MaskYEnd

            If Not GetPixel(OriginalhDC, XMask, YMask) = MyColor Then
                SetPixel MyDC, XMask, YMask, vbBlack
            End If

        Next YMask
    Next XMask
    
    
    'Set frmdebug.npc = Me
    'frmdebug.Show vbModal
    'imgfile = tmp
    
    If tani Then imgfile = tmp
    If tf And FN <> "" Then Kill FN
End Sub

'dokumentieren: JA
Public Sub Move(xdir As Double, ydir As Double)

    If Not Silent Then
        Dim t
        
        t = modCollision.MoveObjectIfPossible(Me, xdir, ydir)
        If t = 1 Then Moving = False
        
        If xdir = 0 And ydir = 0 Then Direction = 0
        
        'Log Desc & ": " & xdir & " / " & ydir, ltInformation
        
        If ydir < 0 Then Direction = 1 'up
        If xdir > 0 Then Direction = 2 'right
        If ydir > 0 Then Direction = 3 'down
        If xdir < 0 Then Direction = 4 'left
        
        If xdir > 0 And ydir < 0 Then Direction = 5 'right/up
        If xdir > 0 And ydir > 0 Then Direction = 6 'right/down
        If xdir < 0 And ydir > 0 Then Direction = 7 'left/down
        If xdir < 0 And ydir < 0 Then Direction = 8 'left/up
    End If
    
End Sub

'dokumentieren: JA
Public Sub GoToPosition(x As Double, y As Double)

    If Not Moving = True Then
    
        Set PathWay = New cS2DWaypoint
        PathWay.AddWayPointCollection
        
        PathWay.AddWayPoint 1, x, y
        
        Moving = True
        
        GoalX = x
        GoalY = y
        
    End If

End Sub

'dokumentieren: JA
Public Sub FindPath(x As Double, y As Double)
    
    If Not Moving = True Then
        Dim Path As New cS2DPathfinding
        Set Path.S2D = S2D
        
        Path.StartNewSearch x, y, Me
    
        Set PathWay = New cS2DWaypoint
    
        PathWay.AddWayPointCollection
        
        Dim i As Integer
        
        
        For i = 1 To Path.FinalPointsCount - 1
        PathWay.AddWayPoint 1, Path.GetPoint(i).x, Path.GetPoint(i).y
        Next i
        
        GoalX = Path.GetPoint(Path.FinalPointsCount - 1).x
        GoalY = Path.GetPoint(Path.FinalPointsCount - 1).y
        
        Moving = True
    End If

End Sub

'dokumentieren: NEIN
Public Sub GameLogic()

    'Direction = 0
    If Use3dCol Then
    
        If Jumping Then
            If JumpPos < JumpDuration Then
                ZPlus = ZPlus + (1 / ((JumpPos / JumpDuration) + 1) * JumpSpeed)
            End If
            JumpPos = JumpPos + 1
        End If
        ZPlus = ZPlus + Gravity
        
        Dim CurLevelZ As Long
        CurLevelZ = Map.Levels(CurrentLevel).Z
    
        If ZPlus <= 0 Then
            If (CollideWithLevel(Me, x, y - CurLevelZ, CurrentLevel) And 1) = 1 Then
                Jumping = False: ZPlus = 0: JumpPos = 0 ' Auf dem Boden aufprallen
            End If
        End If
    
        Dim old_lvl As Integer
        old_lvl = CurrentLevel
        CurrentLevel = Map.GetLevelByZ(Z + ZPlus)
        
        If CurrentLevel < 1 Then
            CurrentLevel = 1
            ZPlus = 0
        End If
    
        If Z < Map.Levels(1).Z Then
            Z = Map.Levels(1).Z
        End If
    
        If old_lvl = 0 Then Exit Sub
    
        If CurrentLevel <> old_lvl Then
            If (CurrentLevel > old_lvl) And ((CollideWithLevel(Me, x, y - CurLevelZ, CurrentLevel) And 1) = 1) Then
                CurrentLevel = old_lvl
                JumpPos = JumpDuration
            Else
                Z = Z + (Map.Levels(CurrentLevel).Z - Map.Levels(old_lvl).Z)
                ZPlus = ZPlus - (Map.Levels(CurrentLevel).Z - Map.Levels(old_lvl).Z)
            End If
        End If
    
        Dim i As Integer
        Dim curmapz As Long
        
        ZToGround = -Z
        
        For i = CurrentLevel To 1 Step -1
            curmapz = Map.Levels(i).Z
            ZToGround = ZToGround + curmapz
            If (CollideWithLevel(Me, x, y - curmapz, i) And 1) = 1 Then Exit For
        Next i
        
    End If
    
    If Moving Then
        If x = GoalX And y = GoalY Then Moving = False: GoTo t_Path
    
        Dim too As Point
        too = PathWay.Play(1, x, y)
    
        Dim xdir As Double, ydir As Double
        xdir = too.x - x
        ydir = too.y - y
    
        Move xdir, ydir
t_Path:
    End If

End Sub

'dokumentieren: NEIN
Public Sub Render(toX As Double, toY As Double)
    
    If Additional > 0 Then
        GFX.TexturePool.Pool1.SetBlending CInt(Additional)
    End If
    
    S2D.TexturePool.RenderTexture TexID, toX, toY - ZPlus - Z, SourceWidth, SourceHeight, SourceWidth, SourceHeight, SourceX, SourceY, Me.Alpha, , , , Me.ZPlus, Me.Color
    
    If ZPlus > 0 Then S2D.TexturePool.RenderTexture TexID, toX, toY - Height / 4 + ZPlus, SourceWidth, SourceHeight / 1.5, SourceWidth, SourceHeight, SourceX, SourceY, 100, 180, , , ZPlus - 1, vbBlack
   
    If Additional > 0 Then
        GFX.TexturePool.Pool1.SetBlending S2D_Alpha
    End If

End Sub

'dokumentieren: JA
Public Property Get Width() As Long
    Width = S2D.TexturePool.TextureWidth(TexID)
End Property

'dokumentieren: JA
Public Property Get Height() As Long
    Height = S2D.TexturePool.TextureHeight(TexID)
End Property

'dokumentieren: NEIN
Private Sub Class_Initialize()

    CurrentLevel = 1
    Alpha = 255
    Color = vbWhite
    Blocking = True
    
    If Not IgnoreScripts Then
        Set Script.ParentObj = Me
    End If

End Sub

'dokumentieren: NEIN
Private Sub Class_Terminate()
    S2D.TexturePool.RemoveTexture TexID
End Sub

'dokumentieren: NEIN
Public Sub Unload()
    ShowCursor True
    S2D.TexturePool.RemoveTexture TexID
End Sub

'dokumentieren: JA
Public Function RunScript(SubName As String)
    Script.CallFunc SubName
End Function

'dokumentieren: JA
Public Sub InheritScript(sName As String)
    Script.Load AppPath & "\projects\" & CurProj & "\scripts\" & sName
End Sub

'dokumentieren: JA
Public Function ChangeAnimation(Name As String)
    S2D.TexturePool.ChangeAnimation TexID, Name
End Function

'dokumentieren: JA
Public Function StopAnimation(Optional Frame As Integer = 1)
    S2D.TexturePool.StopAnimation TexID, Frame
End Function

'dokumentieren: JA
Public Function PlayAnimation(Optional Frame As Integer = 1)
    S2D.TexturePool.PlayAnimation TexID
End Function
