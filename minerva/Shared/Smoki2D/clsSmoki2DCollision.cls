VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DCollision"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function Distance(pX As Double, _
                         pY As Double, _
                         pX2 As Double, _
                         pY2 As Double) As Double

    Distance = Sqr(((pX - pX2) ^ 2) + ((pY - pY2) ^ 2))

End Function

Public Function IsInRect(ByVal X1 As Long, _
                         ByVal Y1 As Long, _
                         ByVal X2 As Long, _
                         ByVal y2 As Long, _
                         ByVal XPos As Long, _
                         ByVal YPos As Long) As Boolean

    If XPos >= X1 And XPos <= X2 And YPos >= Y1 And YPos <= y2 Then
        IsInRect = True
     Else 'NOT XPOS...
        IsInRect = False
    End If

End Function

Public Function RectCollide(ByVal Obj1Left As Double, _
                            ByVal Obj1Top As Double, _
                            ByVal Obj1Height As Double, _
                            ByVal Obj1Width As Double, _
                            ByVal Obj2Left As Double, _
                            ByVal Obj2Top As Double, _
                            ByVal Obj2Height As Double, _
                            ByVal Obj2Width As Double) As Boolean


    If Obj1Left >= Obj2Left Then
        If Obj1Left <= Obj2Left + Obj2Width Then
            If Obj1Top >= Obj2Top Then
                If Obj1Top <= Obj2Top + Obj2Height Then
                    RectCollide = True
                End If
            End If
        End If
    End If
    If Obj1Left + Obj1Width >= Obj2Left Then
        If Obj1Left + Obj1Width <= Obj2Left + Obj2Width Then
            If Obj1Top >= Obj2Top Then
                If Obj1Top <= Obj2Top + Obj2Height Then
                    RectCollide = True
                End If
            End If
        End If
    End If
    If Obj1Left >= Obj2Left Then
        If Obj1Left <= Obj2Left + Obj2Width Then
            If Obj1Top + Obj1Height >= Obj2Top Then
                If Obj1Top + Obj1Height <= Obj2Top + Obj2Height Then
                    RectCollide = True
                End If
            End If
        End If
    End If
    If Obj1Left + Obj1Width >= Obj2Left Then
        If Obj1Left + Obj1Width <= Obj2Left + Obj2Width Then
            If Obj1Top + Obj1Height >= Obj2Top Then
                If Obj1Top + Obj1Height <= Obj2Top + Obj2Height Then
                    RectCollide = True
                End If
            End If
        End If
    End If

End Function

