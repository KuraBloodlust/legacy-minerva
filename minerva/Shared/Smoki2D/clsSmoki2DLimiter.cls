VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DLimiter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private m_CurFrequency      As Currency
Private m_HasCounter        As Boolean
Private m_FrameStart        As Currency
Private m_FrameEnd          As Currency
Private m_CurTime           As Currency
Private m_Delay             As Currency
Private Declare Function QueryPerformanceCounter Lib "kernel32" (lpPerformanceCount As Currency) As Long
Private Declare Function QueryPerformanceFrequency Lib "kernel32" (lpFrequency As Currency) As Long

Private Sub Class_Initialize()

    m_HasCounter = QueryPerformanceFrequency(m_CurFrequency)
    m_CurFrequency = m_CurFrequency * 10000

End Sub


Public Sub LimitFrames(ByVal nFPS As Integer)

QueryPerformanceCounter m_FrameEnd
m_Delay = ((1000 / nFPS) * m_CurFrequency / 10000000) - (m_FrameEnd - m_FrameStart)
Do
If GetInputState() <> 0 Then
DoEvents
End If
QueryPerformanceCounter m_CurTime
Loop Until (m_CurTime - m_FrameEnd) >= m_Delay
QueryPerformanceCounter m_FrameStart
End Sub
