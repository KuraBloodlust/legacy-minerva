VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DSound3D"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private DS              As DirectSound8
Private DSBListener     As DirectSound3DListener8
Private Type Sound
    Buffer                As DirectSoundSecondaryBuffer8
    Buffer3D              As DirectSound3DBuffer8
End Type
Private Sounds()        As Sound
Private SoundCount      As Integer

Public Function AddSound(ByVal File As String, _
                         ByVal X As Double, _
                         ByVal Y As Double, _
                         Optional Distance As Integer = 5) As Integer

   
  Dim DSBDesc As DSBUFFERDESC

    SoundCount = SoundCount + 1
    ReDim Preserve Sounds(0 To SoundCount)
    AddSound = SoundCount
    DSBDesc.lFlags = DSBCAPS_CTRL3D Or DSBCAPS_CTRLVOLUME
    DSBDesc.guid3DAlgorithm = GUID_DS3DALG_HRTF_FULL
    With Sounds(SoundCount)
        Set .Buffer = Nothing
        Set .Buffer = DS.CreateSoundBufferFromFile(File, DSBDesc)
        Set .Buffer3D = Sounds(SoundCount).Buffer.GetDirectSound3DBuffer()
        .Buffer.SetVolume 0
        .Buffer3D.SetConeAngles 360, 360, DS3D_IMMEDIATE
        .Buffer3D.SetConeOutsideVolume 0, DS3D_IMMEDIATE
        .Buffer3D.SetPosition X, 0, Y, DS3D_IMMEDIATE
        .Buffer3D.SetMinDistance Distance, DS3D_IMMEDIATE
    End With 'Sounds(SoundCount)

End Function

Public Sub StopAll()

     Dim i As Integer
    
    For i = 1 To SoundCount
        StopSound CInt(i)
    Next i

End Sub

Public Sub StopSound(ByVal ID As Integer)

 
    Sounds(ID).Buffer.Stop

End Sub

Public Sub Unload()

  
    Set DS = Nothing
    
    Dim i As Integer
    
    For i = 1 To SoundCount
        Set Sounds(i).Buffer = Nothing
        Set Sounds(i).Buffer3D = Nothing
    Next i

End Sub

Public Sub InitialiseDirectSound()
Dim DSBDesc_2  As DSBUFFERDESC
Dim DSBPrimary As DirectSoundPrimaryBuffer8
Dim WaveFormat As WAVEFORMATEX

'Set DS = S2D.DX.DirectSoundCreate("")
'DS.SetCooperativeLevel engine.Destination, DSSCL_PRIORITY
DSBDesc_2.lFlags = DSBCAPS_CTRL3D Or DSBCAPS_PRIMARYBUFFER
Set DSBPrimary = DS.CreatePrimarySoundBuffer(DSBDesc_2)
Set DSBListener = DSBPrimary.GetDirectSound3DListener
With DSBListener
.SetOrientation 0#, 1#, 1#, 0#, 1#, 0#, DS3D_IMMEDIATE
.SetRolloffFactor 1, DS3D_IMMEDIATE
.SetDopplerFactor 0, DS3D_IMMEDIATE
End With 'DSBListener
End Sub


Public Sub PlaySound(ByVal ID As Integer, Optional Looping As Boolean = False)
If Not Looping Then
Sounds(ID).Buffer.Play DSBPLAY_DEFAULT
Else 'NOT NOT...
Sounds(ID).Buffer.Play DSBPLAY_LOOPING
End If
End Sub


Public Sub SetListener(ByVal X As Double, ByVal Y As Double)
DSBListener.SetPosition X, 0, Y, DS3D_IMMEDIATE
End Sub


Public Sub SetSound(ByVal ID As Integer, ByVal X As Double, ByVal Y As Double)

Sounds(ID).Buffer3D.SetPosition X, 0, Y, DS3D_IMMEDIATE
End Sub

