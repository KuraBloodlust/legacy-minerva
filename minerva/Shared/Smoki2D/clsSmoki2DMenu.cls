VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Type MenuFrame
    
    x As Double
    y As Double
    Width As Integer
    Height As Integer
    Color As Long
    
End Type

Private Type MenuList
    
    x As Double
    y As Double
    Width As Integer
    Height As Integer
    List As New Collection
    Color As Long
    CurrentItem As Integer
    ItemCount As Integer
    ListStart As Integer
    ListEnd As Integer
    
End Type

Private Type MenuButton
    
    x As Double
    y As Double
    Text As String
    
End Type

Private Type MenuImage
    
    x As Double
    y As Double
    Width As Integer
    Height As Integer
    SourceWidth As Integer
    SourceHeight As Integer
    SourceX As Integer
    SourceY As Integer
    textureID As Integer
    
End Type

Private Type MenuText
    
    x As Double
    y As Double
    Text As String
    
End Type

Private Type MenuPage
    
    Frames() As MenuFrame
    FrameCount As Integer
    Buttons() As MenuButton
    ButtonCount As Integer
    Images() As MenuImage
    ImageCount As Integer
    Texts() As MenuText
    TextCount As Integer
    Lists() As MenuList
    ListCount As Integer
    BackgroundColor As Long
    CurrentButton As Integer
    CurrentList As Integer
    Visibility As Integer
    
End Type

Private MenuPages() As MenuPage
Private PageCount As Integer

Private textureID As Integer
Private CurrentPage As Integer

Private Const CornerWidth As Integer = 8
Private Const CornerHeight As Integer = 8

Public Text As cS2DText
Public S2D As cS2D_i

Public Function GetPageCount() As Integer
    GetPageCount = PageCount
End Function

Public Function SetButtonFocus(Page As Integer, ID As Integer)
    MenuPages(Page).CurrentButton = ID
End Function

Public Function GetButtonCount(Page As Integer) As Integer
    GetButtonCount = MenuPages(Page).ButtonCount
End Function

Public Function GetListCount(Page As Integer) As Integer
    GetListCount = MenuPages(Page).ListCount
End Function

Public Function GetFrameCount(Page As Integer) As Integer
    GetFrameCount = MenuPages(Page).FrameCount
End Function

Public Function GetTextCount(Page As Integer) As Integer
    GetTextCount = MenuPages(Page).TextCount
End Function

Public Function GetImageCount(Page As Integer) As Integer
    GetImageCount = MenuPages(Page).ImageCount
End Function

Public Function GetButtonDimension(Page As Integer, ID As Integer) As RECT

    GetButtonDimension.Left = MenuPages(Page).Buttons(ID).x
    GetButtonDimension.Top = MenuPages(Page).Buttons(ID).y
    GetButtonDimension.bottom = Text.GetTextHeight(1)
    GetButtonDimension.Right = Text.GetTextWidth(MenuPages(Page).Buttons(ID).Text, 1)
    
End Function

Public Function GetFrameDimension(Page As Integer, ID As Integer) As RECT

    GetFrameDimension.Left = MenuPages(Page).Frames(ID).x
    GetFrameDimension.Top = MenuPages(Page).Frames(ID).y
    GetFrameDimension.bottom = MenuPages(Page).Frames(ID).Height
    GetFrameDimension.Right = MenuPages(Page).Frames(ID).Width
    
End Function

Public Function GetListDimension(Page As Integer, ID As Integer) As RECT

    GetListDimension.Left = MenuPages(Page).Lists(ID).x
    GetListDimension.Top = MenuPages(Page).Lists(ID).y
    GetListDimension.bottom = MenuPages(Page).Lists(ID).Height
    GetListDimension.Right = MenuPages(Page).Lists(ID).Width
    
End Function

Public Function GetTextDimension(Page As Integer, ID As Integer) As RECT

    GetTextDimension.Left = MenuPages(Page).Texts(ID).x
    GetTextDimension.Top = MenuPages(Page).Texts(ID).y
    GetTextDimension.bottom = Text.GetTextHeight(1)
    GetTextDimension.Right = Text.GetTextWidth(MenuPages(Page).Texts(ID).Text, 1)
    
End Function

Public Function GetImageDimension(Page As Integer, ID As Integer) As RECT

    GetImageDimension.Left = MenuPages(Page).Images(ID).x
    GetImageDimension.Top = MenuPages(Page).Images(ID).y
    GetImageDimension.bottom = MenuPages(Page).Images(ID).Height
    GetImageDimension.Right = MenuPages(Page).Images(ID).Width
    
End Function

Public Function SetButtonDimension(Page As Integer, ID As Integer, x As Double, y As Double)

    MenuPages(Page).Buttons(ID).x = x
    MenuPages(Page).Buttons(ID).y = y
    
End Function

Public Function SetTextDimension(Page As Integer, ID As Integer, x As Double, y As Double)

    MenuPages(Page).Texts(ID).x = x
    MenuPages(Page).Texts(ID).y = y
    
End Function

Public Function SetFrameDimension(Page As Integer, ID As Integer, x As Double, y As Double, Width As Double, Height As Double)

    MenuPages(Page).Frames(ID).x = x
    MenuPages(Page).Frames(ID).y = y
    MenuPages(Page).Frames(ID).Width = Width
    MenuPages(Page).Frames(ID).Height = Height
    
End Function

Public Function SetListDimension(Page As Integer, ID As Integer, x As Double, y As Double, Width As Double, Height As Double)

    MenuPages(Page).Lists(ID).x = x
    MenuPages(Page).Lists(ID).y = y
    MenuPages(Page).Lists(ID).Width = Width
    MenuPages(Page).Lists(ID).Height = Height
    
End Function

Public Function SetImageDimension(Page As Integer, ID As Integer, x As Double, y As Double, Width As Double, Height As Double)

    MenuPages(Page).Images(ID).x = x
    MenuPages(Page).Images(ID).y = y
    MenuPages(Page).Images(ID).Width = Width
    MenuPages(Page).Images(ID).Height = Height
    
End Function

Public Function SetFocus(Page As Integer)
    CurrentPage = Page
End Function

Public Function AddPage() As Integer
    
    ReDim Preserve MenuPages(0 To PageCount + 1)
    PageCount = PageCount + 1
    AddPage = PageCount
    
    textureID = S2D.TexturePool.AddTexture(App.Path & "\system.png", rgb(255, 0, 255))
    
End Function

Public Function ButtonPressed(Page As Integer, cID As Integer) As Boolean

    If CurrentPage = Page And MenuPages(Page).CurrentList = 0 Then
        If MenuPages(Page).CurrentButton = cID Then
            If Controller.CheckKey(vbKeyReturn, 8) Then
                ButtonPressed = True
            End If
        End If
    End If
    
End Function

Public Function ListPressed(Page As Integer, List As Integer, Index As Integer) As Boolean

    If CurrentPage = Page And MenuPages(Page).CurrentButton = 0 Then
        If MenuPages(Page).CurrentList = List Then
            If MenuPages(Page).Lists(List).CurrentItem = Index Then
                If Controller.CheckKey(vbKeyReturn, 8) Then
                    ListPressed = True
                End If
            End If
        End If
    End If
    
End Function

Public Function FocusList(Page As Integer, List As Integer)
    
    MenuPages(Page).CurrentList = List
    MenuPages(Page).Lists(List).CurrentItem = 1
    MenuPages(Page).Lists(List).ListStart = 1

    If MenuPages(Page).Lists(List).ItemCount < MenuPages(Page).Lists(List).Height / Text.GetTextHeight(1) - 1 Then
        MenuPages(Page).Lists(List).ListEnd = MenuPages(Page).Lists(List).ItemCount
    Else
        MenuPages(Page).Lists(List).ListEnd = MenuPages(Page).Lists(List).Height / Text.GetTextHeight(1) - 1
    End If
    
    MenuPages(Page).CurrentButton = 0
    
End Function

Public Function FadeOut(Page As Integer)

    If MenuPages(Page).Visibility > 0 Then MenuPages(Page).Visibility = MenuPages(Page).Visibility - 5
    
End Function

Public Function FadeIn(Page As Integer)

    If MenuPages(Page).Visibility < 100 Then MenuPages(Page).Visibility = MenuPages(Page).Visibility + 5
    
End Function

Public Function RenderPage(Page As Integer)

    If MenuPages(Page).Visibility <= 0 Then Exit Function

    If Page = CurrentPage And MenuPages(Page).CurrentList = 0 Then
        
        If Controller.CheckKey(vbKeyUp, 8) Then
            MenuPages(Page).CurrentButton = MenuPages(Page).CurrentButton - 1
        End If
        
        If Controller.CheckKey(vbKeyDown, 8) Then
            MenuPages(Page).CurrentButton = MenuPages(Page).CurrentButton + 1
        End If
        
        If MenuPages(Page).CurrentButton > MenuPages(Page).ButtonCount Then
            MenuPages(Page).CurrentButton = 1
            GoTo Back
        End If
        
        If Controller.CheckKey(vbKeyTab, 8) Then
            If MenuPages(Page).ListCount > 0 Then
                FocusList Page, 1
            End If
        
            If MenuPages(Page).CurrentButton < 1 Then
                MenuPages(Page).CurrentButton = MenuPages(Page).ButtonCount
                GoTo Back
            End If
        End If
        
    ElseIf Page = CurrentPage And MenuPages(Page).CurrentList > 0 Then
    
        If Controller.CheckKey(vbKeyUp, 8) Then
            If Not MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = 1 Then
                MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem - 1
                If MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart Then
                    If Not MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = 1 Then
                        MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart - 1
                        MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd - 1
                    End If
                End If
            End If
        End If
    
        If Controller.CheckKey(vbKeyDown, 8) Then
            If Not MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ItemCount Then
                MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem + 1
                If MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd Then
                    If Not MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ItemCount Then
                        MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart + 1
                        MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd + 1
                    End If
                End If
            End If
        End If
    
        If Controller.CheckKey(vbKeyTab, 8) Then
            If MenuPages(Page).ListCount > MenuPages(Page).CurrentList Then
                MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = 0
                FocusList Page, MenuPages(Page).CurrentList + 1
                GoTo Back
            End If
            If MenuPages(Page).ButtonCount = 0 Then
                FocusList Page, 1
            Else
                MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = 0
                MenuPages(Page).CurrentList = 0
                MenuPages(Page).CurrentButton = 1
            End If
        End If
    
    End If

Back:

    For b = 1 To MenuPages(Page).FrameCount
    
        Effects.RenderRectangle MenuPages(Page).Frames(b).x + 1, MenuPages(Page).Frames(b).y + 1, MenuPages(Page).Frames(b).Width - 2, MenuPages(Page).Frames(b).Height - 2, CLng(MenuPages(Page).Frames(b).Color), CLng(MenuPages(Page).Frames(b).Color), vbBlack, vbBlack, MenuPages(Page).Visibility + 50
        DrawBorder Page, MenuPages(Page).Frames(b).x, MenuPages(Page).Frames(b).y, MenuPages(Page).Frames(b).Width, MenuPages(Page).Frames(b).Height
    
    Next b
    
    For E = 1 To MenuPages(Page).ListCount
    
        Effects.RenderRectangle MenuPages(Page).Lists(E).x + 1, MenuPages(Page).Lists(E).y + 1, MenuPages(Page).Lists(E).Width - 2, MenuPages(Page).Lists(E).Height - 2, CLng(MenuPages(Page).Lists(E).Color), CLng(MenuPages(Page).Lists(E).Color), vbBlack, vbBlack, MenuPages(Page).Visibility + 50
        DrawBorder Page, MenuPages(Page).Lists(E).x, MenuPages(Page).Lists(E).y, MenuPages(Page).Lists(E).Width, MenuPages(Page).Lists(E).Height
        'S2D.Effects.RECT MenuPages(Page).Lists(e).X + CornerWidth, MenuPages(Page).Lists(e).Y + CornerHeight, MenuPages(Page).Lists(e).Width - 2 * CornerWidth, MenuPages(Page).Lists(e).Height - 2 * CornerHeight, CLng(MenuPages(Page).Lists(e).Color), MenuPages(1).Visibility
    
        If Not MenuPages(Page).Lists(E).List.Count = 0 Then
            For F = MenuPages(Page).Lists(E).ListStart To MenuPages(Page).Lists(E).ListEnd
                If Not F = MenuPages(Page).Lists(E).CurrentItem Then
                    Text.DrawText MenuPages(Page).Lists(E).x + CornerWidth, MenuPages(Page).Lists(E).y + (F - MenuPages(Page).Lists(E).ListStart) * 10 + CornerHeight, MenuPages(Page).Lists(E).List.Item(F), 1, MenuPages(Page).Visibility + 20
                Else
                    Effects.RenderRectangle MenuPages(Page).Lists(E).x + CornerWidth, MenuPages(Page).Lists(E).y + (F - MenuPages(Page).Lists(E).ListStart) * 10 + CornerHeight, MenuPages(Page).Lists(E).Width - 2 * CornerWidth, Text.GetTextHeight(1), CLng(MenuPages(Page).Lists(E).Color), CLng(MenuPages(Page).Lists(E).Color), CLng(MenuPages(Page).Lists(E).Color), CLng(MenuPages(Page).Lists(E).Color), MenuPages(Page).Visibility + 70
                    Text.DrawText MenuPages(Page).Lists(E).x + CornerWidth, MenuPages(Page).Lists(E).y + (F - MenuPages(Page).Lists(E).ListStart) * 10 + CornerHeight, MenuPages(Page).Lists(E).List.Item(F), 1, MenuPages(Page).Visibility + 100
                End If
            Next F
        End If
        
    Next E
    
    
    For a = 1 To MenuPages(Page).ButtonCount
        
        If Not a = MenuPages(Page).CurrentButton Then
            Text.DrawText MenuPages(Page).Buttons(a).x, MenuPages(Page).Buttons(a).y, MenuPages(Page).Buttons(a).Text, 1, MenuPages(Page).Visibility + 20
        Else
            Text.DrawText MenuPages(Page).Buttons(MenuPages(Page).CurrentButton).x, MenuPages(Page).Buttons(MenuPages(Page).CurrentButton).y, MenuPages(Page).Buttons(MenuPages(Page).CurrentButton).Text, 1, MenuPages(Page).Visibility + 100
        End If
        
    Next a
    
    For C = 1 To MenuPages(Page).ImageCount
        
        engine.TexturePool.RenderTexture MenuPages(Page).Images(C).textureID, MenuPages(Page).Images(C).x + 1, MenuPages(Page).Images(C).y + 1, MenuPages(Page).Images(C).Width - 2, MenuPages(Page).Images(C).Height - 2, MenuPages(Page).Images(C).SourceHeight, MenuPages(Page).Images(C).SourceWidth, MenuPages(Page).Images(C).SourceX, MenuPages(Page).Images(C).SourceY, MenuPages(Page).Visibility + 100
        DrawBorder Page, MenuPages(Page).Images(C).x, MenuPages(Page).Images(C).y, MenuPages(Page).Images(C).Width, MenuPages(Page).Images(C).Height
        
    Next C
    
    For D = 1 To MenuPages(Page).TextCount
        Text.DrawText MenuPages(Page).Texts(D).x, MenuPages(Page).Texts(D).y, MenuPages(Page).Texts(D).Text, 1, MenuPages(Page).Visibility + 100
    Next D
    
End Function

Private Function DrawBorder(Page As Integer, x As Double, y As Double, Width As Integer, Height As Integer)

        S2D.TexturePool.RenderTexture textureID, x, y, CornerWidth, CornerHeight, CornerWidth, CornerHeight, , , MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, x, y + Height - CornerHeight, CornerWidth, CornerHeight, CornerWidth, CornerHeight, , CornerHeight * 2, MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, x + Width - CornerWidth, y + Height - CornerHeight, CornerWidth, CornerHeight, CornerWidth, CornerHeight, CornerWidth * 2, CornerHeight * 2, MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, x + Width - CornerWidth, y, CornerWidth, CornerHeight, CornerWidth, CornerHeight, CornerWidth * 2, , MenuPages(Page).Visibility + 50
        
        S2D.TexturePool.RenderTexture textureID, x + CornerWidth, y, Width - CornerWidth * 2, CornerHeight, CornerWidth, CornerHeight, CornerWidth, , MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, x + CornerWidth, y + Height - CornerHeight, Width - CornerWidth * 2, CornerHeight, CornerWidth, CornerHeight, CornerWidth, CornerHeight * 2, MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, x, y + CornerHeight, CornerWidth, Height - CornerHeight * 2, CornerWidth, CornerHeight, , CornerHeight, MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, x + Width - CornerWidth, y + CornerHeight, CornerWidth, Height - CornerHeight * 2, CornerWidth, CornerHeight, CornerWidth * 2, CornerHeight, MenuPages(Page).Visibility + 50

End Function

Public Function AddListItem(Page As Integer, List As Integer, Name As String, Key As String)

    MenuPages(Page).Lists(List).List.Add Name, Key
    MenuPages(Page).Lists(List).ItemCount = MenuPages(Page).Lists(List).ItemCount + 1
    
    If MenuPages(Page).Lists(List).ListEnd < MenuPages(Page).Lists(List).Height / Text.GetTextHeight(1) - 1 Then
        MenuPages(Page).Lists(List).ListEnd = MenuPages(Page).Lists(List).ItemCount
    Else
        MenuPages(Page).Lists(List).ListEnd = MenuPages(Page).Lists(List).Height / Text.GetTextHeight(1) - 1
    End If
    
End Function

Public Function AddList(Page As Integer, x As Double, y As Double, Width As Double, Height As Double, Color As Long) As Integer
    
    ReDim Preserve MenuPages(Page).Lists(1 To MenuPages(Page).ListCount + 1)
    MenuPages(Page).ListCount = MenuPages(Page).ListCount + 1
    
    AddList = MenuPages(Page).ListCount
    MenuPages(Page).Lists(MenuPages(Page).ListCount).x = x
    MenuPages(Page).Lists(MenuPages(Page).ListCount).y = y
    MenuPages(Page).Lists(MenuPages(Page).ListCount).Width = Width
    MenuPages(Page).Lists(MenuPages(Page).ListCount).Height = Height
    MenuPages(Page).Lists(MenuPages(Page).ListCount).Color = Color
    
    MenuPages(Page).Lists(MenuPages(Page).ListCount).ListStart = 1
    MenuPages(Page).Lists(MenuPages(Page).ListCount).ListEnd = 1

End Function

Public Function AddButton(Page As Integer, x As Double, y As Double, Text As String) As Integer

    ReDim Preserve MenuPages(Page).Buttons(1 To MenuPages(Page).ButtonCount + 1)
    MenuPages(Page).ButtonCount = MenuPages(Page).ButtonCount + 1
    
    If MenuPages(Page).ButtonCount = 1 Then MenuPages(Page).CurrentButton = 1
    
    AddButton = MenuPages(Page).ButtonCount
    MenuPages(Page).Buttons(MenuPages(Page).ButtonCount).x = x
    MenuPages(Page).Buttons(MenuPages(Page).ButtonCount).y = y
    MenuPages(Page).Buttons(MenuPages(Page).ButtonCount).Text = Text
    
End Function

Public Function AddFrame(Page As Integer, x As Single, y As Single, Width As Single, Height As Single, Color As Single) As Integer

    ReDim Preserve MenuPages(Page).Frames(1 To MenuPages(Page).FrameCount + 1)
    MenuPages(Page).FrameCount = MenuPages(Page).FrameCount + 1
    AddFrame = MenuPages(Page).FrameCount
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).x = x
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).y = y
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).Width = Width
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).Height = Height
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).Color = Color
    
End Function

Public Function AddImage(Page As Integer, File As String, x As Double, y As Double, Width As Double, Height As Double, Optional SourceWidth As Double = -1, Optional SourceHeight As Double = -1, Optional SourceX As Double = 0, Optional SourceY As Double = 0) As Integer
    
    ReDim Preserve MenuPages(Page).Images(1 To MenuPages(Page).ImageCount + 1)
    MenuPages(Page).ImageCount = MenuPages(Page).ImageCount + 1
    AddImage = MenuPages(Page).ImageCount
    ID = engine.TexturePool.AddTexture(File, rgb(255, 0, 255))
    
    MenuPages(Page).Images(MenuPages(Page).ImageCount).x = x
    MenuPages(Page).Images(MenuPages(Page).ImageCount).y = y
    MenuPages(Page).Images(MenuPages(Page).ImageCount).Width = Width
    MenuPages(Page).Images(MenuPages(Page).ImageCount).Height = Height
    MenuPages(Page).Images(MenuPages(Page).ImageCount).textureID = ID
    
    MenuPages(Page).Images(MenuPages(Page).ImageCount).SourceWidth = SourceWidth
    MenuPages(Page).Images(MenuPages(Page).ImageCount).SourceHeight = SourceHeight
    MenuPages(Page).Images(MenuPages(Page).ImageCount).SourceX = SourceX
    MenuPages(Page).Images(MenuPages(Page).ImageCount).SourceY = SourceY
    
End Function

Public Function AddText(Page As Integer, x As Double, y As Double, Text As String) As Integer

    ReDim Preserve MenuPages(Page).Texts(1 To MenuPages(Page).TextCount + 1)
    MenuPages(Page).TextCount = MenuPages(Page).TextCount + 1
    AddText = MenuPages(Page).TextCount
    MenuPages(Page).Texts(MenuPages(Page).TextCount).x = x
    MenuPages(Page).Texts(MenuPages(Page).TextCount).y = y
    MenuPages(Page).Texts(MenuPages(Page).TextCount).Text = Text
    
End Function
