VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DInput"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private CFm_ButtonDelay As Double
Private CFm_S2D As cS2D_i

'dokumentieren: JA
Public Property Get ButtonDelay() As Integer
    ButtonDelay = CFm_ButtonDelay
End Property

Public Property Let ButtonDelay(ByVal PropVal As Integer)
    CFm_ButtonDelay = PropVal
End Property

'dokumentieren: NEIN
Public Property Get S2D() As cS2D_i
    Set S2D = CFm_S2D
End Property

Public Property Set S2D(PropVal As cS2D_i)
    Set CFm_S2D = PropVal
End Property

'dokumentieren: JA
Public Function CheckKey(Key As Long, Optional Delay As Integer = 1) As Boolean

    'If Key = Asc("a") Then Stop
    If HasFocus Then
        If CFm_ButtonDelay > 0 Then
            CFm_ButtonDelay = CFm_ButtonDelay - 0.1
            Exit Function
        End If
        
        If GetAsyncKeyState(Key) < -5 Then
            CheckKey = True
            CFm_ButtonDelay = Delay
         Else 'NOT GETASYNCKEYSTATE(KEY)...
            CheckKey = False
        End If
    End If

End Function

'dokumentieren: JA
Public Function HasFocus()
    HasFocus = True '(GetFocus = frmMain.hwnd)
End Function

'dokumentieren: JA
Public Function GetMousePosX() As Long
    GetMousePosX = MousePos.x
End Function

'dokumentieren: JA
Public Function GetMousePosOnMapX() As Long
    GetMousePosOnMapX = MousePosOnMap.x
End Function

'dokumentieren: JA
Public Function GetMousePosOnTileX() As Long
    GetMousePosOnTileX = MousePosOnTile.x
End Function

'dokumentieren: JA
Public Function GetMousePosY() As Long
    GetMousePosY = MousePos.y
End Function

'dokumentieren: JA
Public Function GetMousePosOnMapY() As Long
    GetMousePosOnMapY = MousePosOnMap.y
End Function

'dokumentieren: JA
Public Function GetMousePosOnTileY() As Long
    GetMousePosOnTileY = MousePosOnTile.y
End Function
