VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DSprite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private CFm_TexID     As Integer
Private CFm_S2D       As cS2D_i

Public Property Get TexID() As Integer
    TexID = CFm_TexID
End Property

Public Property Let TexID(ByVal PropVal As Integer)
    CFm_TexID = PropVal
End Property

Public Property Get S2D() As cS2D_i
    Set S2D = CFm_S2D
End Property

Public Property Set S2D(PropVal As cS2D_i)
    Set CFm_S2D = PropVal
End Property

Public Property Get Height() As Long
    Height = S2D.TexturePool.TextureHeight(CLng(CFm_TexID))
End Property

Public Sub Initialize(ByVal strFilename As String, ByVal colorkey As Long)

    Dim ARGBColorKey As Long
    Dim clr As ColorRGB
    
    clr = SplitColor(colorkey)
    ARGBColorKey = D3DColorARGB(255, clr.r, clr.g, clr.B)

    If CFm_TexID > 0 Then S2D.TexturePool.RemoveTexture CFm_TexID
    CFm_TexID = S2D.TexturePool.AddTexture(strFilename, ARGBColorKey)

End Sub

Public Sub Unload()

    If CFm_TexID > 0 Then
        CFm_S2D.TexturePool.RemoveTexture CFm_TexID
        CFm_TexID = 0
    End If

End Sub

Public Function Render(x As Double, y As Double, Optional toWidth As Integer = -1, Optional toHeight As Integer = -1, Optional SourceX As Integer = -1, Optional SourceY As Integer = -1, Optional FromWidth As Integer = -1, Optional FromHeight As Integer = -1, Optional Alpha As Integer = 255, Optional Angle As Integer = 0, Optional rotCntrX As Integer = -1, Optional rotCntrY As Integer = -1, Optional toZ As Integer = 0, Optional Color As Long = vbWhite)
    S2D.TexturePool.Pool1.SetBlending S2D_Alpha
    S2D.TexturePool.RenderTexture CFm_TexID, x, y, toWidth, toHeight, FromWidth, FromHeight, SourceX, SourceY, Alpha, Angle, rotCntrX, rotCntrY, toZ, Color
End Function

Public Function RenderTiled(ByVal x As Double, ByVal y As Double, Optional toWidth As Integer = -1, Optional toHeight As Integer = -1, Optional Alpha As Integer = 255, Optional toZ As Integer = 0, Optional Color As Long = vbWhite)
    S2D.TexturePool.RenderTiled CFm_TexID, x, y, toWidth, toHeight, Alpha, toZ, Color
End Function

Public Property Get Width() As Long
    Width = S2D.TexturePool.TextureWidth(CLng(CFm_TexID))
End Property

