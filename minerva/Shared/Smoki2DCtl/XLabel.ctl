VERSION 5.00
Begin VB.UserControl XLabel 
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ScaleHeight     =   240
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   320
   ToolboxBitmap   =   "XLabel.ctx":0000
   Begin VB.Label lbl 
      BackStyle       =   0  'Transparent
      Caption         =   "XLabel1"
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1695
   End
End
Attribute VB_Name = "XLabel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'Standard-Eigenschaftswerte:

Const m_def_ForeColor = 0
'Eigenschaftsvariablen:

Dim m_ForeColor As OLE_COLOR
Dim m_Alpha As Long

Public Engine As cS2DCTLEngine

Public posTop As Double
Public posLeft As Double
Public ctlVisible As Boolean
Public fnt As cS2DText
'Ereignisdeklarationen:

Public Event Click()
Public Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

Public Event KeyDown(KeyCode As Integer, Shift As Integer)
Public Event KeyUp(KeyCode As Integer, Shift As Integer)

Public ID As String







Public Property Get Alpha() As Integer
    Alpha = m_Alpha
End Property

Public Property Let Alpha(newalpha As Integer)
m_Alpha = newalpha
PropertyChanged "Alpha"
End Property

'ACHTUNG! DIE FOLGENDEN KOMMENTIERTEN ZEILEN NICHT ENTFERNEN ODER VER�NDERN!
'MemberInfo=10,0,0,0
Public Property Get ForeColor() As OLE_COLOR
    ForeColor = m_ForeColor
End Property

Public Property Let ForeColor(ByVal New_ForeColor As OLE_COLOR)
    lbl.ForeColor = New_ForeColor
    m_ForeColor = New_ForeColor
    PropertyChanged "ForeColor"
End Property

'ACHTUNG! DIE FOLGENDEN KOMMENTIERTEN ZEILEN NICHT ENTFERNEN ODER VER�NDERN!
'MappingInfo=cmd,cmd,-1,Caption
Public Property Get Caption() As String
Attribute Caption.VB_Description = "Gibt den Text zur�ck, der in der Titelleiste eines Objekts oder unter dem Symbol eines Objekts angezeigt wird, oder legt diesen fest."
    Caption = lbl.Caption
End Property

Public Property Let Caption(ByVal New_Caption As String)
    lbl.Caption() = New_Caption
    PropertyChanged "Caption"
End Property

Public Sub Init()
Set fnt = New cS2DText
Set fnt.S2D = Engine.S2D
End Sub

'Eigenschaften f�r Benutzersteuerelement initialisieren
Private Sub UserControl_InitProperties()
    m_ForeColor = m_def_ForeColor
    m_Alpha = 255
End Sub

'Eigenschaftenwerte vom Speicher laden
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

    m_Alpha = PropBag.ReadProperty("Alpha", 255)
    m_ForeColor = PropBag.ReadProperty("ForeColor", m_def_ForeColor)
    lbl.ForeColor = m_ForeColor
    
    lbl.Caption = PropBag.ReadProperty("Caption", "Command1")
End Sub

Private Sub UserControl_Resize()
lbl.Width = UserControl.ScaleWidth
lbl.Height = UserControl.ScaleHeight
End Sub

'Eigenschaftenwerte in den Speicher schreiben
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Call PropBag.WriteProperty("Alpha", m_Alpha, 255)
    Call PropBag.WriteProperty("ForeColor", m_ForeColor, m_def_ForeColor)
    Call PropBag.WriteProperty("Caption", lbl.Caption, "Label")
End Sub

Public Sub OnMouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
RaiseEvent MouseUp(Button, Shift, X, Y)
RaiseEvent Click
End Sub


Public Sub OnMouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Public Sub OnMouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Public Sub OnKeyDown(KeyCode As Integer, Shift As Integer)
RaiseEvent KeyDown(KeyCode, Shift)
End Sub

Public Sub OnKeyUp(KeyCode As Integer, Shift As Integer)
RaiseEvent KeyUp(KeyCode, Shift)
End Sub


Public Function ToString() As String
ToString = "lbl [" & Caption & "]"
End Function

Public Sub Render()

fnt.DrawTextSimple posLeft, posTop, Caption, 1, Alpha, 0, , ForeColor, UserControl.ScaleWidth, UserControl.ScaleHeight
End Sub
