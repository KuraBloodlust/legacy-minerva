VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DCTLEngine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public S2D As cS2D

Dim m_Controls() As Object
Public ControlsCount As Long

Dim m_BGControls() As Object
Public BGControlsCount As Long


Public CurCtl As Object
Public MouseOverCtl As Object
Public FocusCtl As Object

Public Text As New cS2DText

Public MouseIsDown As Boolean

Public SpritePath As String

Public Sub Init(Obj As Object, sSpritePath As String)




SpritePath = sSpritePath

Dim ctl As Object
For Each ctl In Obj.Controls

If TypeOf ctl Is XCommand Then InitCtl ctl: GoTo nextctl           '
If TypeOf ctl Is XLabel Then InitCtl ctl: GoTo nextctl          '
If TypeOf ctl Is XCheck Then InitCtl ctl: GoTo nextctl            '
If TypeOf ctl Is XText Then InitCtl ctl: GoTo nextctl            '
If TypeOf ctl Is XScroll Then InitCtl ctl: GoTo nextctl
'If TypeOf ctl Is XList Then InitCtl ctl: GoTo nextctl

If TypeOf ctl Is XFrame Then InitCtl ctl, True: GoTo nextctl               '


'addctl ctl


nextctl:
Next

Set Text.S2D = S2D
End Sub

Public Sub InitCtl(ctl As Object, Optional bg As Boolean = False)
If TypeOf ctl.Container Is PictureBox Then
    ctl.posTop = ctl.Top + ctl.Container.Top
    ctl.posLeft = ctl.Left + ctl.Container.Left
Else
    ctl.posTop = ctl.Top
    ctl.posLeft = ctl.Left
End If

Set ctl.Engine = Me
Set ctl.fnt = Text

ctl.ID = ControlsCount

ctl.ctlVisible = ctl.Visible
ctl.Visible = False

ctl.Init

AddControl ctl, bg
End Sub

Public Function AddControl(Obj As Object, Optional bg As Boolean = False) As Object

If bg = False Then

ReDim Preserve m_Controls(0 To ControlsCount) As Object
Set m_Controls(ControlsCount) = Obj
ControlsCount = ControlsCount + 1
Else
ReDim Preserve m_BGControls(0 To BGControlsCount) As Object
Set m_BGControls(BGControlsCount) = Obj
BGControlsCount = BGControlsCount + 1
End If


Set AddControl = Obj




End Function

Public Property Get Controls(index As Long) As Object
Set Controls = m_Controls(index)
End Property

Public Property Set Controls(index As Long, vnewval As Object)
Set m_Controls(index) = vnewval
End Property

Public Property Get BGControls(index As Long) As Object
Set BGControls = m_BGControls(index)
End Property

Public Property Set BGControls(index As Long, vnewval As Object)
Set m_BGControls(index) = vnewval
End Property



Public Sub Render(Optional bDebug As Boolean = False)
Dim I As Long

On Error Resume Next
For I = 0 To BGControlsCount - 1
    If m_BGControls(I).ctlVisible Then m_BGControls(I).Render
Next I

For I = 0 To ControlsCount - 1
    If m_Controls(I).ctlVisible Then m_Controls(I).Render
Next I


If bDebug Then
If CurCtl Is Nothing Then Exit Sub
Text.DrawTextSimple 0, 0, CurCtl.ToString, , , , , vbRed
End If
End Sub


Public Sub OnMouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
On Error Resume Next
Set CurCtl = Nothing

Dim ctl
For Each ctl In m_Controls
If ctl.ctlVisible Then
If ctl.posTop <= y Then
If ctl.posLeft <= x Then

If ctl.posTop + ctl.Height >= y Then
If ctl.posLeft + ctl.Width >= x Then

Set CurCtl = ctl
Exit For
End If
End If
End If
End If
End If

Next ctl
If CurCtl Is Nothing Then
For Each ctl In m_BGControls
If ctl.ctlVisible Then
If ctl.posTop <= y Then
If ctl.posLeft <= x Then

If ctl.posTop + ctl.Height >= y Then
If ctl.posLeft + ctl.Width >= x Then
Set CurCtl = ctl
Exit For
End If
End If
End If
End If
End If

Next ctl
End If

MouseIsDown = True
If CurCtl Is Nothing Then
Set FocusCtl = Nothing
Else
Set FocusCtl = CurCtl
CurCtl.OnMouseDown Button, Shift, x - CurCtl.posLeft, y - CurCtl.posTop
End If

End Sub

Public Sub OnMouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
On Error Resume Next

Set MouseOverCtl = Nothing

Dim ctl
For Each ctl In m_Controls
If ctl.ctlVisible Then
If ctl.posTop <= y Then
If ctl.posLeft <= x Then

If ctl.posTop + ctl.Height >= y Then
If ctl.posLeft + ctl.Width >= x Then
Set MouseOverCtl = ctl
Exit For
End If
End If
End If
End If
End If

Next ctl
If MouseOverCtl Is Nothing Then

For Each ctl In m_BGControls
If ctl.ctlVisible Then
If ctl.posTop <= y Then
If ctl.posLeft <= x Then

If ctl.posTop + ctl.Height >= y Then
If ctl.posLeft + ctl.Width >= x Then
Set MouseOverCtl = ctl
Exit For
End If
End If
End If
End If
End If
Next ctl
End If

blah:
If Not CurCtl Is Nothing Then
If Not TypeOf CurCtl Is XScroll Then
Set CurCtl = MouseOverCtl
End If
End If

If CurCtl Is Nothing Then Exit Sub
CurCtl.OnMouseMove Button, Shift, x - CurCtl.posLeft, y - CurCtl.posTop

End Sub

Public Sub OnMouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
MouseIsDown = False
If CurCtl Is Nothing Then Exit Sub
CurCtl.OnMouseUp Button, Shift, x - CurCtl.posLeft, y - CurCtl.posTop
End Sub


Public Sub OnKeyDown(KeyCode As Integer, Shift As Integer)
If FocusCtl Is Nothing Then Exit Sub
FocusCtl.OnKeyDown KeyCode, Shift
End Sub


Public Sub OnKeyUp(KeyCode As Integer, Shift As Integer)
If FocusCtl Is Nothing Then Exit Sub
FocusCtl.OnKeyUp KeyCode, Shift
End Sub

Public Sub OnKeyPress(KeyAscii As Integer)
If FocusCtl Is Nothing Then Exit Sub
FocusCtl.OnKeyPress KeyAscii
End Sub
