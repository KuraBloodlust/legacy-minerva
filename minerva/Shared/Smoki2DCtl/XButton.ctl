VERSION 5.00
Begin VB.UserControl XCommand 
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ScaleHeight     =   240
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   320
   ToolboxBitmap   =   "XButton.ctx":0000
   Begin VB.CommandButton cmd 
      Caption         =   "Test"
      Height          =   375
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   0
      Width           =   1455
   End
End
Attribute VB_Name = "XCommand"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'Standard-Eigenschaftswerte:
Const m_def_BackColor = 0
Const m_def_ForeColor = 0
Const m_def_BorderColor = vbWhite

'Eigenschaftsvariablen:
Dim m_BackColor As OLE_COLOR
Dim m_ForeColor As OLE_COLOR
Dim m_BorderColor As OLE_COLOR


Dim m_Alpha As Long

Public Engine As cS2DCTLEngine

Public posTop As Double
Public posLeft As Double

Public ctlVisible As Boolean
Public MySpr As New cS2DSprite

Public fnt As cS2DText
'Ereignisdeklarationen:

Public Event Click()
Public Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

Public Event KeyDown(KeyCode As Integer, Shift As Integer)
Public Event KeyUp(KeyCode As Integer, Shift As Integer)
Public Event KeyPress(KeyAscii As Integer)

Public ID As String

'Public Event XClick()




Public Property Get BorderColor() As OLE_COLOR
    BorderColor = m_BorderColor
End Property

Public Property Let BorderColor(vNew As OLE_COLOR)
    m_BorderColor = vNew
    PropertyChanged "BorderColor"
End Property

Public Property Get BackColor() As OLE_COLOR
    BackColor = m_BackColor
End Property

Public Property Let BackColor(ByVal New_BackColor As OLE_COLOR)
    cmd.BackColor = New_BackColor
    m_BackColor = New_BackColor
    PropertyChanged "BackColor"
End Property

Public Property Get Alpha() As Integer
    Alpha = m_Alpha
End Property

Public Property Let Alpha(newalpha As Integer)
m_Alpha = newalpha
PropertyChanged "Alpha"
End Property

'ACHTUNG! DIE FOLGENDEN KOMMENTIERTEN ZEILEN NICHT ENTFERNEN ODER VER�NDERN!
'MemberInfo=10,0,0,0
Public Property Get ForeColor() As OLE_COLOR
    ForeColor = m_ForeColor
End Property

Public Property Let ForeColor(ByVal New_ForeColor As OLE_COLOR)
    m_ForeColor = New_ForeColor
    PropertyChanged "ForeColor"
End Property

'ACHTUNG! DIE FOLGENDEN KOMMENTIERTEN ZEILEN NICHT ENTFERNEN ODER VER�NDERN!
'MappingInfo=cmd,cmd,-1,Caption
Public Property Get Caption() As String
Attribute Caption.VB_Description = "Gibt den Text zur�ck, der in der Titelleiste eines Objekts oder unter dem Symbol eines Objekts angezeigt wird, oder legt diesen fest."
    Caption = cmd.Caption
End Property

Public Property Let Caption(ByVal New_Caption As String)
    cmd.Caption() = New_Caption
    PropertyChanged "Caption"
End Property

Public Sub Init()
Set fnt = New cS2DText
Set fnt.S2D = Engine.S2D

Set MySpr.S2D = Engine.S2D
MySpr.Initialize Engine.SpritePath & "\border.bmp", vbMagenta

End Sub

'Eigenschaften f�r Benutzersteuerelement initialisieren
Private Sub UserControl_InitProperties()
    m_BackColor = m_def_BackColor
    m_ForeColor = m_def_ForeColor
    m_Alpha = 255
    m_BorderColor = m_def_BorderColor
End Sub

'Eigenschaftenwerte vom Speicher laden
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

    m_Alpha = PropBag.ReadProperty("Alpha", 255)
    m_BackColor = PropBag.ReadProperty("BackColor", m_def_BackColor)
    m_ForeColor = PropBag.ReadProperty("ForeColor", m_def_ForeColor)
    m_BorderColor = PropBag.ReadProperty("BorderColor", m_def_BorderColor)
    
    cmd.BackColor = m_BackColor
    
    cmd.Caption = PropBag.ReadProperty("Caption", "Command1")
End Sub

Private Sub UserControl_Resize()
cmd.Width = UserControl.ScaleWidth
cmd.Height = UserControl.ScaleHeight
End Sub

'Eigenschaftenwerte in den Speicher schreiben
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Call PropBag.WriteProperty("Alpha", m_Alpha, 255)
    Call PropBag.WriteProperty("BackColor", m_BackColor, m_def_BackColor)
    Call PropBag.WriteProperty("ForeColor", m_ForeColor, m_def_ForeColor)
    Call PropBag.WriteProperty("BorderColor", m_BorderColor, m_def_BorderColor)
    Call PropBag.WriteProperty("Caption", cmd.Caption, "Command1")
End Sub

Public Sub OnMouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
RaiseEvent MouseUp(Button, Shift, X, Y)
RaiseEvent Click
End Sub


Public Sub OnMouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Public Sub OnMouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Public Sub OnKeyDown(KeyCode As Integer, Shift As Integer)
RaiseEvent KeyDown(KeyCode, Shift)
End Sub

Public Sub OnKeyUp(KeyCode As Integer, Shift As Integer)
RaiseEvent KeyUp(KeyCode, Shift)
End Sub

Public Sub OnKeyPress(KeyAscii As Integer)
RaiseEvent KeyPress(KeyAscii)
End Sub

Public Function ToString() As String
ToString = "cmd [" & Caption & "]"
End Function

Public Sub Render()

'DrawNormalRect

'If Not Engine.MouseOverCtl Is Nothing Then
'If Engine.MouseOverCtl.ID = ID Then
'DrawFreakyRect
'End If
'End If

'If Not Engine.CurCtl Is Nothing Then
'If Engine.CurCtl.ID = ID Then
'DrawBrightRect
'End If
'End If

'MySpr.Render posLeft, posTop, UserControl.ScaleWidth, UserControl.ScaleHeight, 12, 12, 4, 4, 150, False


RenderBorder

fnt.DrawTextSimple posLeft + UserControl.ScaleWidth / 2 - UserControl.TextWidth(Caption) / 2, posTop + UserControl.ScaleHeight / 2 - UserControl.TextHeight(Caption) / 2, Caption, 1, Alpha, 0, , ForeColor



End Sub


Private Sub DrawFreakyRect()
Dim clr As ColorRGB
clr = SplitColor(BackColor)

Dim clr1 As Long, clr2 As Long, clr3 As Long
clr1 = RGB(clr.r, clr.g, clr.b)
clr2 = RGB(clr.r + 30, clr.g + 30, clr.b + 30)
clr3 = RGB(clr.r + 60, clr.g + 60, clr.b + 60)


Dim dest As RECT
dest.Left = posLeft + 1
dest.Top = posTop + 1
dest.bottom = UserControl.ScaleHeight - 2
dest.Right = UserControl.ScaleWidth - 2

Engine.S2D.Effects.GradiantRect dest, clr1, clr2, clr2, clr3, Alpha

End Sub

Private Sub DrawBrightRect()
Dim clr As ColorRGB
clr = SplitColor(BackColor)

Dim clr1 As Long, clr2 As Long, clr3 As Long
clr1 = RGB(clr.r, clr.g, clr.b)
clr2 = RGB(clr.r + 40, clr.g + 40, clr.b + 40)
clr3 = RGB(clr.r + 80, clr.g + 80, clr.b + 80)


Dim dest As RECT
dest.Left = posLeft + 1
dest.Top = posTop + 1
dest.bottom = UserControl.ScaleHeight - 2
dest.Right = UserControl.ScaleWidth - 2

Engine.S2D.Effects.GradiantRect dest, clr1, clr2, clr2, clr3

End Sub

Private Sub DrawNormalRect()
Dim clr As ColorRGB
clr = SplitColor(BackColor)

Dim clr1 As Long, clr2 As Long, clr3 As Long
clr1 = RGB(clr.r, clr.g, clr.b)
clr2 = RGB(clr.r + 25, clr.g + 25, clr.b + 25)
clr3 = RGB(clr.r + 50, clr.g + 50, clr.b + 50)

Dim dest As RECT
dest.Left = posLeft + 1
dest.Top = posTop + 1
dest.bottom = UserControl.ScaleHeight - 2
dest.Right = UserControl.ScaleWidth - 2

Engine.S2D.Effects.GradiantRect dest, clr1, clr2, clr2, clr3, Alpha
End Sub


Public Sub RenderBorder()

With MySpr
.Render posLeft, posTop, 4, 4, 0, 0, 4, 4, Alpha, , m_BorderColor, m_BorderColor, m_BorderColor, m_BorderColor
.Render posLeft + 4, posTop, UserControl.ScaleWidth - 8, 4, 4, 0, 4, 4, Alpha, , m_BorderColor, m_BorderColor, m_BorderColor, m_BorderColor
.Render posLeft + UserControl.ScaleWidth - 4, posTop, 4, 4, 8, 0, 4, 4, Alpha, , m_BorderColor, m_BorderColor, m_BorderColor, m_BorderColor
.Render posLeft, posTop + 4, 4, UserControl.ScaleHeight - 8, 0, 4, 4, 4, Alpha, , m_BorderColor, m_BorderColor, m_BorderColor, m_BorderColor
.Render posLeft + UserControl.ScaleWidth - 4, posTop + 4, 4, UserControl.ScaleHeight - 8, 8, 4, 4, 4, Alpha, , m_BorderColor, m_BorderColor, m_BorderColor, m_BorderColor
.Render posLeft, posTop + UserControl.ScaleHeight - 4, 4, 4, 0, 8, 4, 4, Alpha, , m_BorderColor, m_BorderColor, m_BorderColor, m_BorderColor
.Render posLeft + 4, posTop + UserControl.ScaleHeight - 4, UserControl.ScaleWidth - 8, 4, 4, 8, 4, 4, Alpha, , m_BorderColor, m_BorderColor, m_BorderColor, m_BorderColor
.Render posLeft + UserControl.ScaleWidth - 4, posTop + UserControl.ScaleHeight - 4, 4, 4, 8, 8, 4, 4, Alpha, , m_BorderColor, m_BorderColor, m_BorderColor, m_BorderColor
End With

End Sub

