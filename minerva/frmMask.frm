VERSION 5.00
Begin VB.Form frmMask 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Collision Mask Editor"
   ClientHeight    =   2835
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   3345
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   189
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   223
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.VScrollBar VScroll1 
      Height          =   2535
      Left            =   2400
      TabIndex        =   2
      Top             =   0
      Width           =   255
   End
   Begin VB.HScrollBar HScroll1 
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   2280
      Width           =   2415
   End
   Begin VB.PictureBox picMask 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   2295
      Left            =   0
      ScaleHeight     =   153
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   161
      TabIndex        =   0
      Top             =   0
      Width           =   2415
      Begin VB.Label lblXEnd 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X2"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Palatino Linotype"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00CA882F&
         Height          =   240
         Left            =   480
         TabIndex        =   6
         Top             =   1800
         Width           =   195
      End
      Begin VB.Label lblYStart 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y1"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Palatino Linotype"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00CA882F&
         Height          =   240
         Left            =   1080
         TabIndex        =   5
         Top             =   480
         Width           =   195
      End
      Begin VB.Label lblYEnd 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y2"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Palatino Linotype"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00CA882F&
         Height          =   240
         Left            =   1920
         TabIndex        =   4
         Top             =   480
         Width           =   195
      End
      Begin VB.Label lblXStart 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X1"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Palatino Linotype"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00CA882F&
         Height          =   240
         Left            =   600
         TabIndex        =   3
         Top             =   840
         Width           =   195
      End
      Begin VB.Line LineYEnd 
         BorderColor     =   &H00404040&
         BorderStyle     =   5  'Dash-Dot-Dot
         X1              =   32
         X2              =   176
         Y1              =   136
         Y2              =   136
      End
      Begin VB.Line LineYStart 
         BorderColor     =   &H00404040&
         BorderStyle     =   5  'Dash-Dot-Dot
         X1              =   40
         X2              =   184
         Y1              =   72
         Y2              =   72
      End
      Begin VB.Line LineXEnd 
         BorderColor     =   &H00404040&
         BorderStyle     =   5  'Dash-Dot-Dot
         X1              =   128
         X2              =   128
         Y1              =   32
         Y2              =   160
      End
      Begin VB.Line LineXStart 
         BorderColor     =   &H00404040&
         BorderStyle     =   5  'Dash-Dot-Dot
         X1              =   72
         X2              =   72
         Y1              =   32
         Y2              =   160
      End
   End
End
Attribute VB_Name = "frmMask"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public MaskWidth As Integer
Public MaskHeight As Integer
Public MaskhDC As Long

Private PicFile As String

Private devil As New cDevIL
Private picImage As cDevILImage
Private MyDC As Long, mBitmap As Long

Private colorkey As Long

Private Declare Function CreateBrushIndirect Lib "gdi32" (lpLogBrush As LOGBRUSH) As Long
Private Declare Function FillRect Lib "user32" (ByVal hDC As Long, lpRect As RECT, ByVal hBrush As Long) As Long
Private Declare Function TransparentBlt Lib "msimg32.dll" (ByVal hDC As Long, ByVal X As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal crTransparent As Long) As Boolean
Private Declare Function GetSysColorBrush Lib "user32" (ByVal nIndex As Long) As Long

Public MaskXStart As Integer
Public MaskXEnd As Integer
Public MaskYStart As Integer
Public MaskYEnd As Integer

Private LineToMove As Integer

Private RenderX As Integer, RenderY As Integer

Private Type LOGBRUSH
    lbStyle As Long
    lbColor As Long
    lbHatch As Long
End Type

Private ohDC As Long

Public Function LoadMask(hDC As Long, Width As Integer, Height As Integer, OriginalhDC As Long, TransparencyColor As Long)

MaskWidth = Width
MaskHeight = Height
MaskhDC = hDC
MyDC = OriginalhDC
colorkey = TransparencyColor
ohDC = OriginalhDC

RefreshMask
Form_Resize

End Function

Private Sub Form_Resize()
picMask.Move 0, 0, Me.ScaleWidth - VScroll1.Width, Me.ScaleHeight - HScroll1.Height
VScroll1.Move picMask.Left + picMask.ScaleWidth, picMask.Top, VScroll1.Width, picMask.ScaleHeight
HScroll1.Move picMask.Left, picMask.Top + picMask.ScaleHeight, picMask.ScaleWidth, HScroll1.Height

HScroll1.max = (MaskWidth - picMask.ScaleWidth) / 10
VScroll1.max = (MaskHeight - picMask.ScaleHeight) / 10

If HScroll1.max < 0 Then HScroll1.max = 0: HScroll1.Enabled = False Else HScroll1.Enabled = True
If VScroll1.max < 0 Then VScroll1.max = 0: VScroll1.Enabled = False Else VScroll1.Enabled = True

RefreshMask

LineXStart.Y1 = 0
LineXStart.Y2 = picMask.ScaleHeight
LineYStart.X1 = 0
LineYStart.X2 = picMask.ScaleWidth
LineXEnd.Y1 = 0
LineXEnd.Y2 = picMask.ScaleHeight
LineYEnd.X1 = 0
LineYEnd.X2 = picMask.ScaleWidth

LineXStart.X1 = RenderX + MaskXStart
LineXStart.X2 = LineXStart.X1

LineYStart.Y1 = RenderY + MaskYStart
LineYStart.Y2 = LineYStart.Y1

LineXEnd.X1 = RenderX + MaskXEnd
LineXEnd.X2 = LineXEnd.X1

LineYEnd.Y1 = RenderY + MaskYEnd
LineYEnd.Y2 = LineYEnd.Y1

RefreshLabels

End Sub

Private Sub RefreshLabels()

lblXStart.Move LineXStart.X1 + 2, LineXStart.Y1
lblXEnd.Move LineXEnd.X1 + 2, LineXEnd.Y1
lblYStart.Move LineYStart.X1, LineYStart.Y1 + 2
lblYEnd.Move LineYEnd.X1, LineYEnd.Y1 + 2

lblXStart.Caption = "X1:" & MaskXStart
lblXEnd.Caption = "X2:" & MaskXEnd
lblYStart.Caption = "Y1:" & MaskYStart
lblYEnd.Caption = "Y2:" & MaskXEnd

End Sub

Private Sub RefreshMask()

picMask.Cls

Dim fRe As RECT
Dim fLB As LOGBRUSH
Dim fBr As Long

fLB.lbColor = vbWhite '&HE0E0E0
fLB.lbStyle = 2
fLB.lbHatch = 5

fBr = CreateBrushIndirect(fLB)
fRe.Right = picMask.ScaleWidth
fRe.Bottom = picMask.ScaleHeight

FillRect picMask.hDC, fRe, fBr

If picMask.ScaleWidth < MaskWidth Then
    If HScroll1.Value * 10 > MaskWidth - picMask.ScaleWidth Then RenderX = (MaskWidth - picMask.ScaleWidth) * -1 Else RenderX = (HScroll1.Value * 10) * -1
Else
    RenderX = picMask.ScaleWidth / 2 - MaskWidth / 2
End If

If picMask.ScaleHeight < MaskHeight Then
    If VScroll1.Value * 10 > MaskHeight - picMask.ScaleHeight Then RenderY = (MaskHeight - picMask.ScaleHeight) * -1 Else RenderY = (VScroll1.Value * 10) * -1
Else
    RenderY = picMask.ScaleHeight / 2 - MaskHeight / 2
End If

BitBlt picMask.hDC, RenderX, RenderY, MaskWidth, MaskHeight, MyDC, 0, 0, vbSrcCopy
TransparentBlt picMask.hDC, RenderX, RenderY, MaskWidth, MaskHeight, MaskhDC, 0, 0, MaskWidth, MaskHeight, colorkey

picMask.Refresh

End Sub

Private Sub HScroll1_Change()
Form_Resize
End Sub

Private Sub picMask_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

If y < LineYStart.Y1 + 2 And y > LineYStart.Y1 - 2 Then
    If Button = 1 Then
        LineYStart.BorderColor = &H404040
        LineToMove = 1
    End If
End If

If y < LineYEnd.Y1 + 2 And y > LineYEnd.Y1 - 2 Then
    If Button = 1 Then
        LineYEnd.BorderColor = &H404040
        LineToMove = 2
    End If
End If

If X < LineXStart.X1 + 2 And X > LineXStart.X1 - 2 Then
    If Button = 1 Then
        LineXStart.BorderColor = &H404040
        LineToMove = 3
    End If
End If

If X < LineXEnd.X1 + 2 And X > LineXEnd.X1 - 2 Then
    If Button = 1 Then
        LineXEnd.BorderColor = &H404040
        LineToMove = 4
    End If
End If

End Sub

Private Sub picMask_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)

Select Case LineToMove

Case 1

If y - RenderY >= 0 Then
If y - RenderY <= MaskHeight Then
LineYStart.Y1 = y
LineYStart.Y2 = y
End If
End If

Case 2

If y - RenderY >= 0 Then
If y - RenderY <= MaskHeight Then
LineYEnd.Y1 = y
LineYEnd.Y2 = y
End If
End If

Case 3

If X - RenderX >= 0 Then
If X - RenderX <= MaskWidth Then
LineXStart.X1 = X
LineXStart.X2 = X
End If
End If

Case 4

If X - RenderX >= 0 Then
If X - RenderX <= MaskWidth Then
LineXEnd.X1 = X
LineXEnd.X2 = X
End If
End If

End Select

RefreshLabels
End Sub

Private Sub picMask_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)

Select Case LineToMove
Case 1
MaskYStart = y - RenderY
If MaskYStart < 0 Then MaskYStart = 0
If MaskYStart > MaskHeight Then MaskYStart = MaskHeight
Case 2
MaskYEnd = y - RenderY
If MaskYEnd < 0 Then MaskYEnd = 0
If MaskYEnd > MaskHeight Then MaskYEnd = MaskHeight
Case 3
MaskXStart = X - RenderX
If MaskXStart < 0 Then MaskXStart = 0
If MaskXStart > MaskWidth Then MaskXStart = MaskWidth
Case 4
MaskXEnd = X - RenderX
If MaskXEnd < 0 Then MaskXEnd = 0
If MaskXEnd > MaskWidth Then MaskXEnd = MaskWidth

End Select

CreateMask MaskXStart, MaskXEnd, MaskYStart, MaskYEnd
RefreshMask

LineToMove = 0

LineYStart.BorderColor = &H808080
LineXStart.BorderColor = &H808080
LineYEnd.BorderColor = &H808080
LineXEnd.BorderColor = &H808080

End Sub

Private Sub VScroll1_Change()
Form_Resize
End Sub

Private Function CreateMask(X1 As Integer, X2 As Integer, Y1 As Integer, Y2 As Integer)
Dim X As Integer
Dim y As Integer

Dim fRe As RECT
Dim fLB As LOGBRUSH
Dim fBr As Long

fLB.lbColor = colorkey  '&HE0E0E0
fLB.lbStyle = 0

fBr = CreateBrushIndirect(fLB)
fRe.Right = MaskWidth
fRe.Bottom = MaskHeight

FillRect MaskhDC, fRe, fBr

For X = X1 To X2
For y = Y1 To Y2

    If Not GetPixel(ohDC, X, y) = colorkey Then
        SetPixel MaskhDC, X, y, vbBlack
    End If
    
Next y
Next X

End Function
