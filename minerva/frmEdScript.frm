VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmEdScript 
   Caption         =   "Script Editor"
   ClientHeight    =   5790
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   8640
   Icon            =   "frmEdScript.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   386
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   576
   StartUpPosition =   1  'CenterOwner
   Tag             =   "Script Editor"
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   2880
      Top             =   4560
   End
   Begin VB.PictureBox lineNumPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00E1A822&
      BorderStyle     =   0  'None
      ForeColor       =   &H00FFFFFF&
      Height          =   4200
      Left            =   240
      ScaleHeight     =   280
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   33
      TabIndex        =   6
      Top             =   480
      Width           =   495
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      ItemData        =   "frmEdScript.frx":08E1
      Left            =   720
      List            =   "frmEdScript.frx":08E3
      Sorted          =   -1  'True
      TabIndex        =   4
      Top             =   480
      Visible         =   0   'False
      Width           =   2895
   End
   Begin RichTextLib.RichTextBox cs 
      Height          =   3480
      Left            =   840
      TabIndex        =   5
      Top             =   840
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   6138
      _Version        =   393217
      BorderStyle     =   0
      Enabled         =   -1  'True
      ScrollBars      =   3
      MaxLength       =   65000
      Appearance      =   0
      TextRTF         =   $"frmEdScript.frx":08E5
   End
   Begin MSComctlLib.StatusBar stb 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   5535
      Width           =   8640
      _ExtentX        =   15240
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList iml 
      Left            =   5280
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":0967
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlb 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8640
      _ExtentX        =   15240
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      ImageList       =   "imlDriveFileList2"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   11
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "save"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   4
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   5
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   1e-4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   8
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   10
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   11
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   12
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "end"
            ImageIndex      =   14
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.ImageList imlDriveFileList2 
      Left            =   5280
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":0EB9
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":1253
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":15ED
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":1987
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":1D21
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":20BB
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":2455
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":27EF
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":2B89
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":2F23
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":32BD
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":3657
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":39F1
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdScript.frx":3D8B
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox rtbGuide 
      Height          =   2655
      Left            =   6360
      TabIndex        =   3
      Top             =   1800
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   4683
      _Version        =   393217
      BorderStyle     =   0
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   3
      RightMargin     =   1e9
      TextRTF         =   $"frmEdScript.frx":4125
   End
   Begin MSComctlLib.TabStrip ts 
      Height          =   5055
      Left            =   0
      TabIndex        =   2
      Top             =   360
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   8916
      Placement       =   1
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Script"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Reference"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileImport 
         Caption         =   "Import..."
      End
      Begin VB.Menu mnuFileExport 
         Caption         =   "Export..."
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "Edit"
      Begin VB.Menu mnuEditUndo 
         Caption         =   "Undo"
      End
      Begin VB.Menu mnuEditRedo 
         Caption         =   "Redo"
      End
      Begin VB.Menu mnuEditDivider2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditCut 
         Caption         =   "Cut"
      End
      Begin VB.Menu mnuEditCopy 
         Caption         =   "Copy"
      End
      Begin VB.Menu mnuEditPaste 
         Caption         =   "Paste"
      End
      Begin VB.Menu mnuEditDivider1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditFind 
         Caption         =   "Find..."
      End
      Begin VB.Menu mnuEditFindNext 
         Caption         =   "Find Next"
      End
      Begin VB.Menu mnuEditReplace 
         Caption         =   "Replace..."
      End
      Begin VB.Menu mnuEditGoto 
         Caption         =   "Go To..."
      End
   End
   Begin VB.Menu mnuEX 
      Caption         =   "Extras"
      Begin VB.Menu mnuPart 
         Caption         =   "Particle Script Generator"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
   End
End
Attribute VB_Name = "frmEdScript"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim s_x As Long, s_y As Long
Dim clr As New cColor

Dim firstLine As Long
Dim lastLine As Long

Dim newPos As Long
Dim lastpos As Long

Dim blacked As Boolean

Dim scrly As Long
Dim scrlyold As Long

Dim ISense As New cIntelliSense
Dim Schalter As Boolean
Dim Strg As Boolean
Dim StrgL As Boolean
Dim Pos As Long
Dim edited As Boolean

Private Sub Form_GotFocus()
    RefreshMenu
End Sub

Private Sub Form_Load()
    cs.RightMargin = cs.Width + 100000
    
    rtbGuide.LoadFile App.Path & "\script.rtf"
    
End Sub

Private Sub Form_Resize()

    On Error Resume Next
    ts.Width = ScaleWidth
    ts.Height = ScaleHeight - ts.Top - stb.Height
    
    lineNumPanel.Left = ts.ClientLeft
    lineNumPanel.Top = ts.ClientTop
    lineNumPanel.Height = ts.ClientHeight
    
    cs.Top = ts.ClientTop
    cs.Left = ts.ClientLeft + lineNumPanel.Width
    cs.Width = ts.ClientWidth - lineNumPanel.Width
    cs.Height = ts.ClientHeight
    
    rtbGuide.Left = ts.ClientLeft
    rtbGuide.Top = ts.ClientTop
    rtbGuide.Width = ts.ClientWidth
    rtbGuide.Height = ts.ClientHeight

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Dim x  As Integer
    If SelNPC <= 0 Then
        If Not Map.Levels(CurrentLevel).Script = cs.Text Then
            x = MsgBox("Save Changes in Script Layer Script?", vbYesNoCancel + vbQuestion, "Script editor")
            If x = vbCancel Then Cancel = 1
            If x = vbYes Then
                Map.Levels(CurrentLevel).Script = cs.Text
            End If
        End If
    Else
        If Not Map.m_NPCs(SelNPC).Script.ScriptText = cs.Text Then
            x = MsgBox("Save Changes in NPC Script?", vbYesNoCancel + vbQuestion, "Script editor")
            If x = vbCancel Then Cancel = 1
            If x = vbYes Then
                Map.m_NPCs(SelNPC).Script.ScriptText = cs.Text
            End If
        End If
    End If

End Sub

Private Sub picSizer_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    s_x = x
    s_y = y
End Sub

Private Sub picSizer_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error Resume Next
    rtbGuide.Width = rtbGuide.Width - (x - s_x)
    s_x = x
    Form_Resize
End Sub

Public Sub RefreshMenu()
    Me.Caption = "Script Editor" & IIf(edited, "*", "")
End Sub

Private Sub mnuPart_Click()
    frmParticle.Show vbModal
End Sub

Private Sub tlb_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
Case "save"
    If SelNPC <= 0 Then
        x = MsgBox("Save Changes in Script Layer Script?", vbYesNoCancel + vbQuestion, "Script editor")
        If x = vbCancel Then Cancel = 1
        If x = vbYes Then
            Map.Levels(CurrentLevel).Script = cs.Text
        End If
    Else
        x = MsgBox("Save Changes in NPC Script?", vbYesNoCancel + vbQuestion, "Script editor")
        If x = vbCancel Then Cancel = 1
        If x = vbYes Then
            Map.m_NPCs(SelNPC).Script.ScriptText = cs.Text
        End If
    End If
Case Else
    MsgBox Button.Key

End Select
End Sub

Private Sub ts_Click()
cs.Visible = (ts.SelectedItem.Index = 1)
lineNumPanel.Visible = (ts.SelectedItem.Index = 1)
rtbGuide.Visible = (ts.SelectedItem.Index = 2)
End Sub
