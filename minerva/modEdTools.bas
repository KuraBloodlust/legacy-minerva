Attribute VB_Name = "modEdTools"
Option Explicit

Public Paths           As New Collection
Public DispPaths       As New Collection
Private m_DrawOrErase  As Boolean
Public UseTools        As Boolean
Public IsShift         As Boolean
Public CurrentTool     As Integer
Public CurrentPath     As Integer
Private Drawing        As Integer
Public CurPath         As cEdPath
Private rct_x          As Long
Private rct_y          As Long

Public Sub DrawMap(ByVal toX As Integer, _
                   ByVal toY As Integer, _
                   XOffset As Long, _
                   YOffset As Long)

  If CurrentLevel = 0 Then
      Exit Sub
  ElseIf Map.Levels(CurrentLevel) Is Nothing Then
      Exit Sub
  ElseIf toX < 0 Then
      Exit Sub
  ElseIf toY < 0 Then
      Exit Sub
  ElseIf toX >= Map.MapWidth Then
      Exit Sub
  ElseIf toY >= Map.MapHeight Then
      Exit Sub
  End If

  Dim lvl As cGAMLevel
  Dim lyr As cGAMLayer
    
  If Mode = 1 Then
  Set lyr = Map.Levels(CurrentLevel).Layers(CurrentLayer)
  
  If lyr Is Nothing Then Exit Sub
      
  If DrawOrErase Then
     lyr.MapDrawing(toX, toY) = False
     lyr.MapTileX(toX, toY) = -1
     lyr.MapTileY(toX, toY) = -1
     lyr.AdditionalInfo toX, toY
                 
  Else 'DRAWORERASE = FALSE/0
  
     lyr.MapDrawing(toX, toY) = True
     lyr.MapTileX(toX, toY) = SelTileX + XOffset&
     lyr.MapTileY(toX, toY) = SelTileY + YOffset&
     lyr.AdditionalInfo toX, toY
   
  End If
  
  ElseIf Mode = 2 Then 'NOT MODE...
      Set lvl = Map.Levels(CurrentLevel)
      lvl.ColTileX(toX, toY) = SelColTileX + XOffset&
      lvl.ColTileY(toX, toY) = SelColTileY + YOffset&
      lvl.ColInf(toX, toY) = SelColInf
  End If

End Sub

Public Sub DrawMapPath(xOfs As Long, yOfs As Long, _
                       Optional OtherBorders As Boolean = False, _
                       Optional TX As Long = 0, _
                       Optional Ty As Long = 0)

  If Not Mode = 1 Then
    Exit Sub
  ElseIf IsBlock(xOfs, yOfs) Then
    Exit Sub
  End If
  
  Dim CurLyr As cGAMLayer
  Dim x      As Integer
  Dim y      As Integer
  
    
  For x% = 0 To (CurPath.TileSize / 2)
    For y% = 0 To (CurPath.TileSize / 2)
      Set CurLyr = Map.Levels(CurrentLevel).Layers(CurrentLayer)
            
       If OtherBorders Then
       CurLyr.MapTileX(MouseX + x + (xOfs * CurPath.TileSize), _
                       MouseY + y + (yOfs * CurPath.TileSize)) _
                     = CurPath.C2X1 + (TX * CurPath.TileSize) + x
                       
       CurLyr.MapTileY(MouseX + x + (xOfs * CurPath.TileSize), _
                       MouseY + y + (yOfs * CurPath.TileSize)) _
                    = CurPath.C2Y1 + (Ty * CurPath.TileSize) + y
       Else
       CurLyr.MapTileX(MouseX + x + (xOfs * CurPath.TileSize), _
                       MouseY + y + (yOfs * CurPath.TileSize)) _
                     = CurPath.DrawX + (xOfs * CurPath.TileSize) + x
       
       CurLyr.MapTileY(MouseX + x + (xOfs * CurPath.TileSize), _
                       MouseY + y + (yOfs * CurPath.TileSize)) _
                     = CurPath.DrawY + (yOfs * CurPath.TileSize) + y
       End If
       CurLyr.MapDrawing(MouseX + x + (xOfs * CurPath.TileSize), _
                         MouseY + y + (yOfs * CurPath.TileSize)) = True
     
     Next
   Next

End Sub

Public Function IsBlock(x As Long, y As Long) As Boolean
  IsBlock = (Map.Levels(CurrentLevel).Layers(CurrentLayer).MapTileX(MouseX + x * CurPath.TileSize, MouseY + y * CurPath.TileSize) = CurPath.DrawX)
  IsBlock = IsBlock And (Map.Levels(CurrentLevel).Layers(CurrentLayer).MapTileY(MouseX + x * CurPath.TileSize, MouseY + y * CurPath.TileSize) = CurPath.DrawY)
End Function

Public Sub Path_Draw()

    If Mode <> 1 Then
        Exit Sub
    ElseIf CurrentPath = 0 Then
        Exit Sub
    End If
    
    Set CurPath = Paths(CurrentPath)
    DrawMapPath 0, 0
    If Not IsBlock(-1, 0) Then
        DrawMapPath -1, 0
    End If
    If Not IsBlock(0, -1) Then
        DrawMapPath 0, -1
    End If
    If Not IsBlock(1, 0) Then
        DrawMapPath 1, 0
    End If
    If Not IsBlock(0, 1) Then
        DrawMapPath 0, 1
    End If
    If Not IsBlock(1, 0) And Not IsBlock(0, -1) Then
        DrawMapPath 1, -1
    End If
    If Not IsBlock(1, 0) And Not IsBlock(0, 1) Then
        DrawMapPath 1, 1
    End If
    If Not IsBlock(-1, 0) And Not IsBlock(0, -1) Then
        DrawMapPath -1, -1
    End If
    If Not IsBlock(-1, 0) And Not IsBlock(0, 1) Then
        DrawMapPath -1, 1
    End If
    If IsBlock(1, -1) Then
        DrawMapPath 1, 0, True, 0, 0
        DrawMapPath 0, -1, True, 1, 1
    End If
    If IsBlock(1, 1) Then
        DrawMapPath 1, 0, True, 0, 1
        DrawMapPath 0, 1, True, 1, 0
    End If
    If IsBlock(-1, -1) Then
        DrawMapPath -1, 0, True, 1, 0
        DrawMapPath 0, -1, True, 0, 1
    End If
    If IsBlock(-1, 1) Then
        DrawMapPath -1, 0, True, 1, 1
        DrawMapPath 0, 1, True, 0, 0
    End If
    

End Sub

Public Sub Path_Init()

  Dim l       As String
  Dim x       As String

    'Dim X       As String
  Dim newpath As cEdPath
    'o------------------------------------------------------------------------------------------------o
    x = Dir(App.Path & "\data\pathed\*.pth")
    Do Until LenB(x) = 0
        Set newpath = New cEdPath
        Open App.Path & "\data\pathed\" & x For Input As 1
        Input #1, l
        newpath.PathName = l
        Input #1, l
        newpath.TileSet = l
        Input #1, l
        newpath.TileSize = l
        Input #1, l
        newpath.C1X1 = Left$(l, InStr(1, l, "/") - 1)
        newpath.C1Y1 = Mid$(l, InStr(1, l, "/") + 1)
        Input #1, l
        With newpath
            .C2X1 = Left$(l, InStr(1, l, "/") - 1)
            .C2Y1 = Mid$(l, InStr(1, l, "/") + 1)
            .DrawX = .C1X1 + newpath.TileSize
            .DrawY = .C1Y1 + newpath.TileSize
        End With 'newpath
        Close 1
        Paths.Add newpath
        x = Dir()
    Loop

End Sub

Public Sub Path_List()

If CurrentTileset = 0 Then Exit Sub
  Dim i   As Integer
  Dim tsn As String

    frmEdMap.cboPath.Clear
    Set DispPaths = New Collection
    For i = 1 To Paths.Count
        tsn = LCase$(Map.TileSets(CurrentTileset).FileName)
        tsn = Mid$(tsn, InStrRev(tsn, "\") + 1)
        If LCase$(Paths(i).TileSet) = tsn Then
            frmEdMap.cboPath.AddItem Paths(i).PathName
            DispPaths.Add i
        End If
    Next i
    CurrentPath = 0
    With frmEdMap
        If .cboPath.ListCount > 0 Then
            .cboPath.ListIndex = 0
            .cboPath_Click
        End If
    End With 'frmEdMain

End Sub

Public Sub Path_MouseDown(Button As Integer, _
                          Shift As Integer, _
                          x As Single, _
                          y As Single)

    If Button = 1 Then
        Path_Draw
    End If

End Sub

Public Sub Path_MouseMove(Button As Integer, _
                          Shift As Integer, _
                          x As Single, _
                          y As Single)

    If Button = 1 Then
        Path_Draw
    End If

End Sub

Public Sub Path_MouseUp(Button As Integer, _
                        Shift As Integer, _
                        x As Single, _
                        y As Single)

    If Button = 1 Then
        Path_Draw
    End If

End Sub

Public Sub Path_Render()

    If Not Mode = 1 Then
        Exit Sub
    End If
    If CurrentPath = 0 Then
        Exit Sub
    End If
    modEdGlobal.EdFont.DrawText MouseXScreen + 16, MouseYScreen + 16, "<color(0,0,255)>" & Paths(CurrentPath).PathName

End Sub

Public Sub Pen_Draw()

  Dim x As Integer, y As Integer, w As Integer, H As Integer
  If Mode = 1 Then
    w = SelTileWidth
    H = SelTileHeight
  Else
    w = SelColTileWidth
    H = SelColTileHeight
  End If
  

    For x = 1 To w
        For y = 1 To H
            DrawMap MouseX + (x - 1), MouseY + (y - 1), (x - 1), (y - 1)
        Next y
    Next x



End Sub

Public Sub Pen_Init()


End Sub

'o------------------------------------------------------------------------------------------------o
Public Sub Pen_MouseDown(Button As Integer, _
                         Shift As Integer, _
                         x As Single, _
                         y As Single)

    If Button = 1 Then
        Pen_Draw
    End If

End Sub

Public Sub Pen_MouseMove(Button As Integer, _
                         Shift As Integer, _
                         x As Single, _
                         y As Single)

    If Button = 1 Then
        Pen_Draw
    End If

End Sub

Public Sub Pen_MouseUp(Button As Integer, _
                       Shift As Integer, _
                       x As Single, _
                       y As Single)

    If Button = 1 Then
        Pen_Draw
    End If

End Sub

Public Sub Pen_Render()

  Dim clr As Long

    If Mode = 1 Then
        If Not DrawOrErase Then
            GFX.TexturePool.RenderTexture Map.TileSets(CurrentTileset).TexID, MouseX * Const_TileWidth + ScrollX, MouseY * Const_TileHeight + ScrollY, SelTileWidth * Const_TileWidth, SelTileHeight * Const_TileHeight, SelTileWidth * Const_TileWidth, SelTileHeight * Const_TileHeight, SelTileX * Const_TileWidth, SelTileY * Const_TileHeight, 180, , , , 9999
        Else
            Eraser.Render MouseXScreen + 12, MouseYScreen + 12
        End If
        Effects.RenderBorderRect MouseX * Const_TileWidth + ScrollX, MouseY * Const_TileHeight + ScrollY, SelTileWidth * Const_TileWidth, SelTileHeight * Const_TileHeight, vbWhite, 200, 2, 9999
        Effects.RenderRectangle MouseX * Const_TileWidth + ScrollX, MouseY * Const_TileHeight + ScrollY, SelTileWidth * Const_TileWidth, SelTileHeight * Const_TileHeight, vbWhite, vbWhite, vbWhite, vbBlack, 60, 9999
        
     ElseIf Mode = 2 Then 'NOT MODE...
        Effects.RenderBorderRect ScrollX + MouseX * Const_TileWidth, ScrollY + MouseY * Const_TileHeight, SelColTileWidth * Const_TileWidth - 1, SelColTileHeight * Const_TileHeight - 1, TileInfColors(SelColInf), 100, 1, 9999
        'Effects.RenderRectangle ScrollX + MouseX * Const_TileWidth, ScrollY + MouseY * Const_TileHeight, SelTileWidth * Const_TileWidth - 1, SelTileHeight * Const_TileHeight - 1, TileInfColors(SelColInf), TileInfColors(SelColInf), TileInfColors(SelColInf), TileInfColors(SelColInf), 50
        clr = TileInfColors(SelColInf)
        ColTS.Render MouseX * Const_TileWidth + ScrollX, MouseY * Const_TileHeight + ScrollY, SelColTileWidth * Const_TileWidth, SelColTileHeight * Const_TileHeight, SelColTileX * Const_TileWidth, SelColTileY * Const_TileHeight, SelColTileWidth * Const_TileWidth, SelColTileWidth * Const_TileHeight, 100, , , , 9999
    End If

End Sub

Public Sub Pipette_Init()



End Sub

'o------------------------------------------------------------------------------------------------o
Public Sub Pipette_MouseDown(Button As Integer, _
                             Shift As Integer, _
                             x As Single, _
                             y As Single)



End Sub

Public Sub Pipette_MouseMove(Button As Integer, _
                             Shift As Integer, _
                             x As Single, _
                             y As Single)



End Sub

Public Sub Pipette_MouseUp(Button As Integer, _
                           Shift As Integer, _
                           x As Single, _
                           y As Single)


End Sub

Public Sub Pipette_Render()

    EdFont.DrawText MouseXScreen + 16, MouseYScreen + 16, "<color ( 255 , 0 , 0 )> <dance> Coming soon!"

End Sub

Public Sub RandomPen_Draw()

  Dim rx As Integer
  Dim ry As Integer

    Dim x  As Integer
    Dim y  As Integer
    If Mode = 1 Then
        Randomize
        rx = Int(Rnd * (SelTileWidth))
        ry = Int(Rnd * (SelTileHeight))
        rx = rx - rx Mod (DrawModeWidth / Const_TileWidth)
        ry = ry - ry Mod (DrawModeHeight / Const_TileHeight)
        For x = 0 To (DrawModeWidth / Const_TileWidth) - 1
            For y = 0 To (DrawModeHeight / Const_TileHeight) - 1
                DrawMap MouseX + x, MouseY + y, rx + x, ry + y
            Next y
        Next x
    End If


End Sub

Public Sub RandomPen_Init()



End Sub

'o------------------------------------------------------------------------------------------------o
Public Sub RandomPen_MouseDown(Button As Integer, _
                               Shift As Integer, _
                               x As Single, _
                               y As Single)

    If Button = 1 Then
        RandomPen_Draw
    End If

End Sub

Public Sub RandomPen_MouseMove(Button As Integer, _
                               Shift As Integer, _
                               x As Single, _
                               y As Single)

    If Button = 1 Then
        RandomPen_Draw
    End If

End Sub

Public Sub RandomPen_MouseUp(Button As Integer, _
                             Shift As Integer, _
                             x As Single, _
                             y As Single)

    If Button = 1 Then
        RandomPen_Draw
    End If

End Sub

Public Sub RandomPen_Render()

  Dim clr As Long

    
    If Mode = 1 Then
    If DrawOrErase Then
    Eraser.Render MouseXScreen + 12, MouseYScreen + 12, , , , , , , , , , , 9999
    End If
        clr = vbGreen
        Effects.RenderBorderRect MouseX * Const_TileWidth + ScrollX, MouseY * Const_TileHeight + ScrollY, CDbl(DrawModeWidth), CDbl(DrawModeHeight), clr, 200, , 9999
        Effects.RenderRectangle MouseX * Const_TileWidth + ScrollX, MouseY * Const_TileHeight + ScrollY, CDbl(DrawModeWidth), CDbl(DrawModeHeight), clr, clr, clr, clr, 60, 9999
    End If

End Sub

Public Sub Rect_Init()


End Sub

'o------------------------------------------------------------------------------------------------o
Public Sub Rect_MouseDown(Button As Integer, _
                          Shift As Integer, _
                          x As Single, _
                          y As Single)

    If Button = 1 Then
        Drawing = 1
        rct_x = MouseX
        rct_y = MouseY
    End If

End Sub

Public Sub Rect_MouseMove(Button As Integer, _
                          Shift As Integer, _
                          x As Single, _
                          y As Single)


End Sub

Public Sub Rect_MouseUp(Button As Integer, _
                        Shift As Integer, _
                        x As Single, _
                        y As Single)
                        
On Error Resume Next

  Dim mx     As Integer
  Dim my     As Integer
  Dim rToX   As Long
  Dim rToY   As Long
  Dim rFromX As Long
  Dim rFromY As Long
  Dim rX1    As Long
  Dim rX2    As Long
  Dim rY1    As Long
  Dim rY2    As Long

    If Button = 1 Then
        Drawing = False
        rX1 = rct_x
        rY1 = rct_y
        rX2 = MouseX
        rY2 = MouseY
        rFromX = IIf(rX1 < rX2, rX1, rX2)
        rFromY = IIf(rY1 < rY2, rY1, rY2)
        rToX = IIf(rX1 > rX2, rX1, rX2) + (DrawModeWidth / Const_TileWidth) - 1
        rToY = IIf(rY1 > rY2, rY1, rY2) + (DrawModeHeight / Const_TileHeight) - 1
        For mx = rFromX To rToX
            For my = rFromY To rToY
                DrawMap mx, my, mx Mod IIf(Mode = 1, SelTileWidth, SelColTileWidth), my Mod IIf(Mode = 1, SelTileHeight, SelColTileHeight)
            Next my
        Next mx
    End If
    
    

End Sub

Public Sub Rect_Render()

  Dim rx As Long
  Dim ry As Integer
  Dim rw As Integer
  Dim rh As Integer
  



    If Drawing = 1 Then
        rx = IIf(rct_x < MouseX, rct_x, MouseX)
        ry = IIf(rct_y < MouseY, rct_y, MouseY)
        rw = IIf(rct_x < MouseX, MouseX - rct_x, rct_x - MouseX) + (DrawModeWidth / Const_TileWidth)
        rh = IIf(rct_y < MouseY, MouseY - rct_y, rct_y - MouseY) + (DrawModeHeight / Const_TileHeight)
     Else 'NOT DRAWING...
        rx = MouseX
        ry = MouseY
        rw = (DrawModeWidth / Const_TileWidth)
        rh = (DrawModeHeight / Const_TileHeight)
    End If
    If Mode = 1 Then
        If DrawOrErase Then Eraser.Render MouseXScreen + 12, MouseYScreen + 12, , , , , , , , , , , 9999
        
        Effects.RenderBorderRect ScrollX + rx * Const_TileWidth, ScrollY + ry * Const_TileHeight, rw * Const_TileWidth - 1, rh * Const_TileHeight - 1, vbYellow, 200, 1, 9999
        Effects.RenderRectangle ScrollX + rx * Const_TileWidth, ScrollY + ry * Const_TileHeight, rw * Const_TileWidth - 1, rh * Const_TileHeight - 1, vbYellow, vbYellow, vbYellow, vbYellow, 50, 9999
     ElseIf Mode = 2 Then 'NOT MODE...
        Effects.RenderBorderRect ScrollX + rx * Const_TileWidth, ScrollY + ry * Const_TileHeight, rw * Const_TileWidth - 1, rh * Const_TileHeight - 1, TileInfColors(SelColInf), 200, 1, 9999
        Effects.RenderRectangle ScrollX + rx * Const_TileWidth, ScrollY + ry * Const_TileHeight, rw * Const_TileWidth - 1, rh * Const_TileHeight - 1, TileInfColors(SelColInf), TileInfColors(SelColInf), TileInfColors(SelColInf), TileInfColors(SelColInf), 50, 9999
    End If

End Sub

Public Sub Tools_Init()

    CurrentTool = 1
    'o------------------------------------------------------------------------------------------------o
    Pen_Init
    RandomPen_Init
    Rect_Init
    Path_Init
    Pipette_Init

End Sub

Public Sub Tools_MouseDown(Button As Integer, _
                           Shift As Integer, _
                           x As Single, _
                           y As Single)

    If UseTools = False Then
        Exit Sub
    End If
    Select Case CurrentTool
     Case 1 'Pen
        Pen_MouseDown Button, Shift, x, y
     Case 2 'RandomPen
        RandomPen_MouseDown Button, Shift, x, y
     Case 3 'Rect
        Rect_MouseDown Button, Shift, x, y
     Case 4 'Path
        Path_MouseDown Button, Shift, x, y
     Case 5 'Pipette
        Pipette_MouseDown Button, Shift, x, y
    End Select

End Sub

Public Sub Tools_MouseMove(Button As Integer, _
                           Shift As Integer, _
                           x As Single, _
                           y As Single)

    If UseTools = False Then
        Exit Sub
    End If
    Select Case CurrentTool
     Case 1 'Pen
        Pen_MouseMove Button, Shift, x, y
     Case 2 'RandomPen
        RandomPen_MouseMove Button, Shift, x, y
     Case 3 'Rect
        Rect_MouseMove Button, Shift, x, y
     Case 4 'Path
        Path_MouseMove Button, Shift, x, y
     Case 5 'Pipette
        Pipette_MouseMove Button, Shift, x, y
    End Select

End Sub

Public Sub Tools_MouseUp(Button As Integer, _
                         Shift As Integer, _
                         x As Single, _
                         y As Single)

    If UseTools = False Then
        Exit Sub
    End If
    Select Case CurrentTool
     Case 1 'Pen
        Pen_MouseUp Button, Shift, x, y
     Case 2 'RandomPen
        RandomPen_MouseUp Button, Shift, x, y
     Case 3 'rect
        Rect_MouseUp Button, Shift, x, y
     Case 4 'Path
        Path_MouseUp Button, Shift, x, y
     Case 5 'Pipette
        Pipette_MouseUp Button, Shift, x, y
    End Select

End Sub

Public Sub Tools_Render()
   
   If Map Is Nothing Then Exit Sub
   If Map.TileSets.Count = 0 Then Exit Sub

  If UseTools = False Then
   Exit Sub
  End If
    
  Select Case CurrentTool
   Case 1: Pen_Render       'Pen
   Case 2: RandomPen_Render 'RandomPen
   Case 3: Rect_Render      'Rect
   Case 4: Path_Render      'Path
   Case 5: Pipette_Render   'Pipette
  End Select

End Sub

Public Property Get DrawOrErase() As Variant
  DrawOrErase = m_DrawOrErase Or (GetKeyState(VK_SHIFT) < -50)
End Property

Public Property Let DrawOrErase(ByVal vNewValue As Variant)
  m_DrawOrErase = vNewValue
End Property
