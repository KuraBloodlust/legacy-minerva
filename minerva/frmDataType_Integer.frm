VERSION 5.00
Begin VB.Form frmDataType_Integer 
   Caption         =   "Form1"
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3165
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows-Standard
   Begin VB.PictureBox picInteger 
      AutoSize        =   -1  'True
      Height          =   300
      Left            =   600
      Picture         =   "frmDataType_Integer.frx":0000
      ScaleHeight     =   240
      ScaleWidth      =   240
      TabIndex        =   4
      Top             =   2040
      Width           =   300
   End
   Begin VB.PictureBox picDataEditor 
      Appearance      =   0  '2D
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000008&
      Height          =   975
      Left            =   120
      ScaleHeight     =   975
      ScaleWidth      =   4455
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      Begin Minerva.Rm2kScroll rm2k 
         Height          =   300
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   529
      End
      Begin VB.Label lblMax 
         Alignment       =   1  'Rechts
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "9999"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3915
         TabIndex        =   3
         Top             =   675
         Width           =   420
      End
      Begin VB.Label lblMin 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   2
         Top             =   660
         Width           =   105
      End
      Begin VB.Label lblCaption 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Feldname:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   900
      End
   End
End
Attribute VB_Name = "frmDataType_Integer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Public MyNode As cGAMRegistryNode
Public MyRenderer As Object

Public Function GetInstance() As frmDataType_Integer
    Set GetInstance = New frmDataType_Integer
End Function

Public Function MainContainer() As PictureBox
    Set MainContainer = picDataEditor
End Function

Private Sub picDataEditor_Resize()
rm2k.Width = picDataEditor.ScaleWidth - 2 * rm2k.Left
lblMax.Left = rm2k.Width + rm2k.Left - lblMax.Width
End Sub

Public Sub Initialize(iSubCatLevel As Integer)





rm2k.min = 0
rm2k.max = 9999
rm2k.Value = CInt(MyNode.Value)
lblCaption.Caption = MyNode.FriendlyName & " [" & MyNode.NodeName & "]"

On Error Resume Next
Dim max As String, min As String

max = MyNode.Parameters("max")
min = MyNode.Parameters("min")

If CInt(min) < CInt(max) Then
    rm2k.min = CInt(min)
    rm2k.max = CInt(max)
End If

If rm2k.Value > rm2k.max Then rm2k.Value = rm2k.max
If rm2k.Value < rm2k.min Then rm2k.Value = rm2k.min

rm2k.UpdateScroller

lblMax.Caption = rm2k.max
lblMin.Caption = rm2k.min
End Sub



Private Sub rm2k_Change()
MyNode.Value = rm2k.Value
End Sub
