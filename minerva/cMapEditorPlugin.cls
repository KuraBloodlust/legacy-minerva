VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cMapEditorPlugin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_PlugHost As Object
Private m_Instances As Collection

Public IsMapEditorOpen As Boolean

Public Sub IEditorPlugin_AddViews()

End Sub

Private Property Get IEditorPlugin_Instances() As Collection
    Set IEditorPlugin_Instances = m_Instances
End Property

Public Property Get IEditorPlugin_MyPlugHost() As Object
    Set IEditorPlugin_MyPlugHost = m_PlugHost
End Property

Public Function IEditorPlugin_Initialize(PlugHost As Object) As Boolean
    Set m_PlugHost = PlugHost
    Set m_Instances = New Collection
    
   m_PlugHost.Log "Map Editor plugin initialized."
End Function


Public Property Get IEditorPlugin_PluginName() As String
    IEditorPlugin_PluginName = "Map Editor"
End Property

Public Property Get IEditorPlugin_FileTypes() As Collection
    Dim C As New Collection
    
    C.Add "rmap"

    Set IEditorPlugin_FileTypes = C
End Property

Public Function IEditorPlugin_OpenFile(FileName As String) As Object
    

    
        frmEdMap.CurrentFile = FileName
        frmEdMap.OpenFile FileName
        frmEdMain.mnuEditParticles.Enabled = True

    If Not IsMapEditorOpen Then
        Set IEditorPlugin_OpenFile = frmEdMap
    Else
        Set IEditorPlugin_OpenFile = Nothing
    End If
    
        IsMapEditorOpen = True

End Function


Public Function IEditorPlugin_NewFile(FileName As String) As Object


    
        frmEdMap.CurrentFile = FileName
        
        frmEdMap.NewFile
        frmEdMain.mnuEditParticles.Enabled = True
        
    If Not IsMapEditorOpen Then
        Set IEditorPlugin_NewFile = frmEdMap
    Else
        Set IEditorPlugin_NewFile = Nothing
    End If
End Function
