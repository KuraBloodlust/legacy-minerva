VERSION 5.00
Begin VB.Form frmPluginforType 
   BorderStyle     =   4  'Festes Werkzeugfenster
   Caption         =   "Multiple Plugins for "".%ext"""
   ClientHeight    =   3525
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3525
   ScaleWidth      =   4575
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'Bildschirmmitte
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   3
      Top             =   3000
      Width           =   1575
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   3000
      Width           =   1695
   End
   Begin VB.ListBox lstPlugins 
      Height          =   2010
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   4335
   End
   Begin VB.Label lblPrompt 
      BackStyle       =   0  'Transparent
      Caption         =   "You have multiple plugins installed that are able to edit the %ext file type. Please select which one you would like to use."
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4335
   End
End
Attribute VB_Name = "frmPluginforType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Plugins As Collection
Public FileType As String


Public Sub Init()
    Me.Caption = Replace$(Me.Caption, "%ext", FileType)
    lblPrompt.Caption = Replace$(lblPrompt.Caption, "%ext", FileType)
    
    
    lstPlugins.Clear
    
    Dim i As Integer
    For i = 1 To Plugins.Count
        lstPlugins.AddItem Plugins(i).IEditorPlugin_PluginName
    Next i
    
    
    
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    If lstPlugins.ListIndex = -1 Then Exit Sub
    
    RetOk = 1
    Set RetObject = Plugins(lstPlugins.ListIndex + 1)
    Unload Me
End Sub


Private Sub lstPlugins_DblClick()
cmdOK_Click
End Sub
