VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCreateNewFile 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "New File"
   ClientHeight    =   3480
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3480
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList iml 
      Left            =   3480
      Top             =   1920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCreateNewFile.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCreateNewFile.frx":059A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCreateNewFile.frx":0B34
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2880
      TabIndex        =   5
      Top             =   600
      Width           =   1695
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   2880
      TabIndex        =   4
      Top             =   120
      Width           =   1695
   End
   Begin VB.TextBox txtFileName 
      Height          =   285
      Left            =   600
      TabIndex        =   1
      Top             =   3120
      Width           =   2175
   End
   Begin MSComctlLib.TreeView trv 
      Height          =   2895
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   5106
      _Version        =   393217
      Indentation     =   0
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "iml"
      Appearance      =   1
   End
   Begin VB.Label Label1 
      Caption         =   "File:"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   3120
      Width           =   495
   End
   Begin VB.Label lblExt 
      Caption         =   ".ext"
      Height          =   255
      Left            =   2880
      TabIndex        =   2
      Top             =   3135
      Width           =   495
   End
End
Attribute VB_Name = "frmCreateNewFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public BasePath As String
Public SelExt As String


Public Sub ListPlugins()

    trv.Nodes.Clear
    
    Dim i As Integer, j As Integer
    
    
    Dim ext As New Collection
    
    Dim n As Node, m As Node
    
    
    For i = 1 To PluginHost.Plugins.Count
        Set n = trv.Nodes.Add(, , Chr(i), PluginHost.Plugins(i).IEditorPlugin_PluginName, 1, 1)
        n.ExpandedImage = 2
        
        Set ext = PluginHost.Plugins(i).IEditorPlugin_FileTypes()
        
        For j = 1 To ext.Count
            Set m = trv.Nodes.Add(, , Chr(i) & Chr(j), ext(j), 3)
            Set m.Parent = n
        Next j
    Next i



End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()

    If txtFileName.Text = "" Then
        MsgBox "Please enter a filename for the new file!", vbCritical, "Error"
        Exit Sub
    End If

    If SelExt = "" Then
       MsgBox "Please select a file extension!", vbCritical, "Error"
    End If
    
    Me.Hide
    Dim plugin As Object
    
    Set plugin = PluginHost.Plugins(Asc(Left(trv.SelectedItem.Key, 1)))
    PluginHost.NewFile BasePath & txtFileName.Text & "." & SelExt, plugin
    Unload Me
End Sub

Private Sub Form_Load()
    ListPlugins
    trv_Click
End Sub

Private Sub trv_Click()
    If trv.SelectedItem Is Nothing Then Exit Sub
    If trv.SelectedItem.Parent Is Nothing Then Exit Sub
    
    SelExt = trv.SelectedItem.Text
    
    lblExt.Caption = "." & SelExt
    
End Sub
