VERSION 5.00
Begin VB.Form frmDataElement_Title 
   Caption         =   "Form1"
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3165
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows-Standard
   Begin VB.PictureBox picCustom 
      AutoSize        =   -1  'True
      Height          =   300
      Left            =   360
      Picture         =   "frmDataType_Title.frx":0000
      ScaleHeight     =   240
      ScaleWidth      =   240
      TabIndex        =   2
      Top             =   1320
      Width           =   300
   End
   Begin VB.PictureBox picDataEditor 
      Appearance      =   0  '2D
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   120
      ScaleHeight     =   18
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   297
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      Begin VB.Image imgIcon 
         Height          =   240
         Left            =   30
         Top             =   15
         Width           =   240
      End
      Begin VB.Label lblTitle 
         AutoSize        =   -1  'True
         BackColor       =   &H80000009&
         BackStyle       =   0  'Transparent
         Caption         =   "Custom Type Title"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   360
         TabIndex        =   1
         Top             =   30
         Width           =   1770
      End
      Begin VB.Image imgBG 
         Height          =   270
         Left            =   0
         Picture         =   "frmDataType_Title.frx":058A
         Stretch         =   -1  'True
         Top             =   0
         Width           =   345
      End
   End
End
Attribute VB_Name = "frmDataElement_Title"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public MyNode As cGAMRegistryNode
Public MyRenderer As Object

Public Function GetInstance() As frmDataElement_Title
    Set GetInstance = New frmDataElement_Title
End Function

Public Function MainContainer() As PictureBox
    Set MainContainer = picDataEditor
End Function





Public Sub Initialize(iSubCatLevel As Integer)


lblTitle.Caption = MyNode.FriendlyName

If (MyNode.NodeType = "list") And (iSubCatLevel > 0) Then
    lblTitle.Caption = lblTitle.Caption & " (" & MyNode.SubElements.Count & IIf(MyNode.SubElements.Count = 1, " element", " elements") & ")"
End If


Set imgIcon.Picture = frmEditorRegistry.imlSettings.ListImages(MyRenderer.MySEditor.IconForType(frmEditorRegistry.imlSettings, MyNode)).Picture


End Sub


Private Sub imgIcon_Click()
picDataEditor_Click
End Sub

Private Sub imgBG_Click()
picDataEditor_Click
End Sub

Private Sub lblTitle_Click()
picDataEditor_Click
End Sub

Private Sub picDataEditor_Click()
MyRenderer.MySEditor.SelectNode MyNode
End Sub

Private Sub picDataEditor_Resize()
imgBG.Width = picDataEditor.ScaleWidth - imgBG.Left
End Sub
