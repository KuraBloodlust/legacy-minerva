VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSettingsRenderer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Renderer-Klasse f�r allgemeine Registry-Kategorieelemente

Option Explicit

Public MyForm As Form
Public MySEditor As cEdSettingsEditor

Public Sub PreInitialize()
Load MyForm
End Sub

Public Sub Initialize()
Set MyForm = MyForm.GetInstance
Set MyForm.MyRenderer = Me
MyForm.Visible = False
End Sub

Public Function Render(ByRef snode As cGAMRegistryNode, ByRef picSEditor As PictureBox, ByRef x As Long, ByRef y As Long, ByRef SubCatLevel As Integer, ByRef DisplayCollection As CHive)
Dim pb As PictureBox

Dim frm As Object
Set frm = GetSettingsEditor("sys:title")
frm.Initialize
frm.Render snode, picSEditor, x, y, SubCatLevel, DisplayCollection


If Not snode.Description = "" Then

Set frm = GetSettingsEditor("sys:desc")
frm.Initialize
frm.Render snode, picSEditor, x, y, SubCatLevel, DisplayCollection
End If


Set MyForm.MyNode = snode
Set pb = MyForm.MainContainer

SetParentAPI pb.hwnd, picSEditor.hwnd
pb.Left = x
pb.Top = y
pb.Width = picSEditor.ScaleWidth - pb.Left

y = y + pb.Height

MyForm.Initialize SubCatLevel

DisplayCollection.Add pb
End Function

Public Function DataToXML(snode As cGAMRegistryNode, XMLNode As IXMLDOMElement, Dom As DOMDocument, Project As cGAMProject) As Boolean
XMLNode.Text = snode.Value
End Function

Public Function XMLToData(snode As cGAMRegistryNode, XMLNode As IXMLDOMNode, Dom As DOMDocument, Project As cGAMProject)
snode.Value = XMLNode.Text
End Function

