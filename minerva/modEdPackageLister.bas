Attribute VB_Name = "modEdPackageLister"
Option Explicit


Public Function ObtainResourceURL(TRVPath As String, SpecificFolder) As String
Dim outPath As String
Dim pakFile As String
Dim Pos As Integer

outPath = TRVPath

Pos = InStr(1, outPath, ".")
If Pos = 0 Then Exit Function

Pos = InStr(1, outPath, "\")
If Pos <= 5 Then
    outPath = "folder://" & SpecificFolder & "\" & outPath

Else
    
    If LCase(Mid(outPath, Pos - 5, 5)) = ".rpak" Then
    pakFile = Left(outPath, Pos - 1)
    outPath = pakFile & "://" & SpecificFolder & "\" & Mid(outPath, Pos + 1)
    Else
        outPath = "folder://" & SpecificFolder & "\" & outPath
    End If
    

End If
ObtainResourceURL = outPath
End Function

Public Function ListProjectAndFSToTreeView(ctlTrv As TreeView, ValidExts As String, SpecificFolder As String)
ctlTrv.Nodes.Clear

ListPackage(Project.Package, ctlTrv, ValidExts, SpecificFolder).Bold = True

Dim i As Integer
For i = 1 To LoadedDependencies.Count
ListPackage LoadedDependencies(i), ctlTrv, ValidExts, SpecificFolder
Next i

ListFSFolder App.Path & "\projects\" & CurProj & "\" & SpecificFolder, ctlTrv, ValidExts

End Function



Public Function ListPackage(Pkg As Object, ctlTrv As TreeView, ValidExts As String, SpecificFolder As String) As Node

Dim fId As Long
Dim fldr As Object

fId = Pkg.RPAK.Root.FindFolder(SpecificFolder)

If fId >= 0 Then
Dim n As Node
Dim FN As String, ext As String

FN = Pkg.RPAK.FileName
FN = Mid(FN, InStrRev(FN, "\") + 1)

Set n = ctlTrv.Nodes.Add(, , , FN, 4, 4)
n.Tag = FN
    

Set fldr = Pkg.RPAK.Root.GetFolder(SpecificFolder)

ListFolder n, fldr, ctlTrv, ValidExts

End If

Set ListPackage = n
End Function

Public Function ListFolder(pn As Node, pf As Object, ctlTrv As TreeView, ValidExts As String)
Dim ext As String
Dim i As Integer
Dim n As Node


For i = 0 To pf.FileCount - 1
    
    ext = LCase(Right(pf.FileName(i), 4))

    If ValidFileExt(pf.FileName(i), ValidExts) Then

        Set n = ctlTrv.Nodes.Add(, , , pf.FileName(i), 3, 3)
        n.Tag = pf.FileName(i)
        Set n.Parent = pn
    End If
    
Next i

For i = 0 To pf.FolderCount - 1
    Set n = ctlTrv.Nodes.Add(, , , pf.Folders(i).FolderName, 1, 1)
    Set n.Parent = pn
    n.Tag = pf.Folders(i).FolderName
    n.ExpandedImage = 2
    
    
    
    ListFolder n, pf.Folders(i), ctlTrv, ValidExts
Next i


End Function

Public Function ValidFileExt(FileName As String, ValidExts As String)
Dim ext As String
Dim exts() As String

ext = Mid(FileName, InStrRev(FileName, ".") + 1)
ext = LCase(ext)

exts = Split(ValidExts, "|")

Dim i As Integer
For i = 0 To UBound(exts)
If LCase(ext) = LCase(exts(i)) Then ValidFileExt = True: Exit Function
Next i

ValidFileExt = False
End Function

Public Function ListFSFolder(p As String, ctlTrv As TreeView, ValidExts As String, Optional pn As Node = Nothing)


Dim x As String
Dim Nod As Node
Dim Folders As New CHive



x = Dir(p & "\", vbDirectory)
Do Until x = ""
If Not x = "." And Not x = ".." Then
If CBool(GetAttr(p & "\" & x) And vbDirectory) Then
    Set Nod = ctlTrv.Nodes.Add(, , x, x, 1)
    Nod.ExpandedImage = 2
    Nod.Tag = x
    If Not pn Is Nothing Then Set Nod.Parent = pn
    
    Folders.Add Nod, x
End If
End If

x = Dir()
Loop

Dim i As Integer
For i = 1 To Folders.Count
    ListFSFolder p & "\" & Folders.Key(i), ctlTrv, ValidExts, Folders.Item(i)
Next i



x = Dir(p & "\*.*")
Do Until x = ""
    If ValidFileExt(x, ValidExts) Then
        Set Nod = ctlTrv.Nodes.Add(, , x, x, 3, 3)
        Nod.Tag = x
        If Not pn Is Nothing Then Set Nod.Parent = pn
    End If
x = Dir()
Loop






End Function

Public Function GetPathFromTags(n As Node) As String
On Error Resume Next
Dim s As String

Dim cn As Node
Set cn = n


Do

'If cn.Index = n.Root.Index Then Exit Do
If cn Is Nothing Then Exit Do

s = cn.Tag & "\" & s

Set cn = cn.Parent

Loop

If Not s = "" Then s = Left$(s, Len(s) - 1)

GetPathFromTags = s
End Function
