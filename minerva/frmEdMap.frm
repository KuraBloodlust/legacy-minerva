VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmEdMap 
   BorderStyle     =   1  'Fest Einfach
   Caption         =   "Map Editor"
   ClientHeight    =   8370
   ClientLeft      =   150
   ClientTop       =   750
   ClientWidth     =   10965
   Icon            =   "frmEdMap.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   10965
   StartUpPosition =   3  'Windows-Standard
   Begin MSComctlLib.ImageList imlTrv 
      Left            =   3960
      Top             =   6360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":0B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":10BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":19AF
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":1F01
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":2453
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":29A5
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":2EF7
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   375
      Left            =   3600
      TabIndex        =   36
      Top             =   120
      Width           =   1215
   End
   Begin VB.Timer tmrRender 
      Interval        =   1
      Left            =   6840
      Top             =   5640
   End
   Begin MSComctlLib.ImageList imlTlbTools 
      Left            =   5160
      Top             =   6360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":3491
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":39E3
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":3D35
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":4087
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":43D9
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":44EB
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":483D
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":4B8F
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":4EE1
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlTlbLevelTools 
      Left            =   4560
      Top             =   6360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":5233
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":57CD
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":5D67
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":6301
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":689B
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdMap.frx":6C35
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.HScrollBar hsbMap 
      Height          =   255
      LargeChange     =   1000
      Left            =   3960
      SmallChange     =   16
      TabIndex        =   30
      Top             =   4680
      Width           =   3855
   End
   Begin VB.VScrollBar vsbMap 
      Height          =   2655
      LargeChange     =   1000
      Left            =   7800
      SmallChange     =   16
      TabIndex        =   29
      Top             =   2040
      Width           =   255
   End
   Begin VB.PictureBox picStandby 
      AutoRedraw      =   -1  'True
      Height          =   1380
      Left            =   6360
      ScaleHeight     =   1320
      ScaleWidth      =   1620
      TabIndex        =   27
      Top             =   360
      Visible         =   0   'False
      Width           =   1680
   End
   Begin VB.PictureBox picRender 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'Kein
      Height          =   4455
      Left            =   3960
      ScaleHeight     =   297
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   257
      TabIndex        =   28
      Top             =   240
      Width           =   3855
   End
   Begin VB.PictureBox picSizer 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'Kein
      Height          =   495
      Left            =   3480
      MousePointer    =   9  'Gr��en�nderung W O
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   2
      TabIndex        =   26
      Top             =   0
      Width           =   30
   End
   Begin VB.PictureBox picLeftBarMap 
      BorderStyle     =   0  'Kein
      Height          =   6645
      Left            =   0
      ScaleHeight     =   443
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   227
      TabIndex        =   1
      Top             =   0
      Width           =   3405
      Begin VB.PictureBox picLevelList 
         BorderStyle     =   0  'Kein
         Height          =   1935
         Left            =   0
         ScaleHeight     =   1935
         ScaleWidth      =   3375
         TabIndex        =   23
         Top             =   4680
         Width           =   3375
         Begin MSComctlLib.Toolbar tlbLevelTools 
            Height          =   330
            Left            =   0
            TabIndex        =   24
            Top             =   0
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   582
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            ImageList       =   "imlTlbLevelTools"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   8
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Up"
                  Object.ToolTipText     =   "Move Up"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Down"
                  Object.ToolTipText     =   "Move Down"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Add New"
                  Object.ToolTipText     =   "Create New"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Delete"
                  Object.ToolTipText     =   "Delete"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Zoom in"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Zoom out"
                  ImageIndex      =   6
               EndProperty
            EndProperty
            Begin Minerva.Rm2kScroll rmsZoom 
               Height          =   300
               Left            =   2400
               TabIndex        =   35
               Top             =   0
               Width           =   975
               _ExtentX        =   1720
               _ExtentY        =   529
               Min             =   25
               Max             =   200
               Value           =   100
            End
         End
         Begin MSComctlLib.TreeView trvMap 
            Height          =   1575
            Left            =   0
            TabIndex        =   34
            ToolTipText     =   "Displays Items on the map"
            Top             =   360
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   2778
            _Version        =   393217
            HideSelection   =   0   'False
            Indentation     =   0
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            Appearance      =   0
         End
      End
      Begin VB.PictureBox picLeftContainer 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'Kein
         Height          =   4095
         Left            =   0
         ScaleHeight     =   4095
         ScaleWidth      =   3360
         TabIndex        =   3
         Top             =   360
         Width           =   3360
         Begin VB.PictureBox picLB 
            Appearance      =   0  '2D
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3855
            Index           =   1
            Left            =   720
            ScaleHeight     =   255
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   135
            TabIndex        =   4
            Top             =   480
            Width           =   2055
            Begin Minerva.XEdTileSet picColTS 
               Height          =   1095
               Left            =   0
               TabIndex        =   32
               Top             =   0
               Width           =   975
               _ExtentX        =   1720
               _ExtentY        =   1931
            End
            Begin VB.PictureBox picLevelProps 
               BorderStyle     =   0  'Kein
               Height          =   1575
               Left            =   0
               ScaleHeight     =   1575
               ScaleWidth      =   2055
               TabIndex        =   5
               Top             =   2400
               Width           =   2055
               Begin VB.Frame Frame2 
                  Caption         =   "Z-Value"
                  Height          =   615
                  Left            =   120
                  TabIndex        =   39
                  Top             =   720
                  Width           =   1815
                  Begin Minerva.Rm2kScroll ZValue 
                     CausesValidation=   0   'False
                     Height          =   300
                     Left            =   120
                     TabIndex        =   40
                     Top             =   240
                     Width           =   1575
                     _ExtentX        =   2778
                     _ExtentY        =   529
                     Min             =   0
                     Max             =   2048
                     Step            =   16
                     Value           =   0
                  End
               End
               Begin VB.Frame Frame1 
                  Caption         =   "Mode"
                  Height          =   615
                  Left            =   120
                  TabIndex        =   37
                  Top             =   120
                  Width           =   1815
                  Begin VB.ComboBox cboColMode 
                     Height          =   315
                     ItemData        =   "frmEdMap.frx":6FCF
                     Left            =   120
                     List            =   "frmEdMap.frx":6FE2
                     Style           =   2  'Dropdown-Liste
                     TabIndex        =   38
                     Top             =   240
                     Width           =   1575
                  End
               End
               Begin VB.PictureBox picLevPropsDivider 
                  BackColor       =   &H80000010&
                  BorderStyle     =   0  'Kein
                  Height          =   30
                  Left            =   0
                  MousePointer    =   7  'Gr��en�nderung N S
                  ScaleHeight     =   2
                  ScaleMode       =   3  'Pixel
                  ScaleWidth      =   99
                  TabIndex        =   6
                  Top             =   0
                  Width           =   1485
               End
            End
         End
         Begin VB.PictureBox picLB 
            Appearance      =   0  '2D
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3855
            Index           =   4
            Left            =   600
            ScaleHeight     =   3825
            ScaleWidth      =   2025
            TabIndex        =   7
            Top             =   600
            Width           =   2055
            Begin VB.TextBox txtMFG 
               BorderStyle     =   0  'Kein
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   600
               TabIndex        =   10
               Text            =   "(txtMFG)"
               Top             =   840
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.ComboBox cboMFG 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Left            =   600
               Style           =   2  'Dropdown-Liste
               TabIndex        =   9
               Top             =   1200
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.CommandButton cmdMFG 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   1200
               TabIndex        =   8
               Top             =   1680
               Visible         =   0   'False
               Width           =   255
            End
            Begin MSFlexGridLib.MSFlexGrid mfgNPC 
               Height          =   2655
               Left            =   0
               TabIndex        =   11
               Top             =   0
               Width           =   1695
               _ExtentX        =   2990
               _ExtentY        =   4683
               _Version        =   393216
               FixedRows       =   0
               FixedCols       =   0
               BackColorBkg    =   -2147483633
               GridColor       =   -2147483632
               GridColorFixed  =   -2147483632
               ScrollBars      =   2
               BorderStyle     =   0
               Appearance      =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.PictureBox picLB 
            Appearance      =   0  '2D
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3855
            Index           =   2
            Left            =   360
            ScaleHeight     =   3825
            ScaleWidth      =   2025
            TabIndex        =   16
            Top             =   360
            Width           =   2055
            Begin Minerva.Rm2kScroll ZValue2 
               Height          =   300
               Left            =   120
               TabIndex        =   33
               Top             =   1320
               Width           =   1695
               _ExtentX        =   2990
               _ExtentY        =   529
               Min             =   0
               Max             =   2048
               Step            =   16
               Value           =   0
            End
            Begin VB.CommandButton cmdScriptLvl 
               Caption         =   "Edit Script"
               Height          =   375
               Left            =   120
               TabIndex        =   18
               Top             =   600
               Width           =   1695
            End
            Begin VB.CheckBox chkScriptLvl 
               Caption         =   "Visible"
               Height          =   255
               Left            =   120
               TabIndex        =   17
               Top             =   1800
               Width           =   1695
            End
            Begin VB.Image Image1 
               Height          =   480
               Left            =   0
               Top             =   0
               Width           =   480
            End
            Begin VB.Label Label2 
               BackStyle       =   0  'Transparent
               Caption         =   "Script Level"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   480
               TabIndex        =   20
               Top             =   0
               Width           =   1335
            End
            Begin VB.Label Label5 
               BackStyle       =   0  'Transparent
               Caption         =   "Z Position"
               Height          =   255
               Left            =   120
               TabIndex        =   19
               Top             =   1080
               Width           =   1695
            End
         End
         Begin VB.PictureBox picLB 
            Appearance      =   0  '2D
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3855
            Index           =   3
            Left            =   240
            ScaleHeight     =   3825
            ScaleWidth      =   2025
            TabIndex        =   21
            Top             =   240
            Width           =   2055
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "welcome"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   15.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00800000&
               Height          =   375
               Left            =   120
               TabIndex        =   22
               Top             =   120
               Width           =   1395
            End
         End
         Begin VB.PictureBox picLB 
            Appearance      =   0  '2D
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3855
            Index           =   0
            Left            =   120
            ScaleHeight     =   255
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   135
            TabIndex        =   12
            Top             =   120
            Width           =   2055
            Begin Minerva.XEdTileSet picTileSet 
               Height          =   1455
               Left            =   0
               TabIndex        =   31
               Top             =   0
               Width           =   1095
               _ExtentX        =   1931
               _ExtentY        =   2566
            End
            Begin VB.CommandButton cmdTileset 
               Caption         =   "..."
               Height          =   315
               Left            =   1680
               TabIndex        =   15
               Top             =   3000
               Width           =   375
            End
            Begin VB.ComboBox cboPath 
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   0
               Style           =   2  'Dropdown-Liste
               TabIndex        =   14
               ToolTipText     =   "Path selector"
               Top             =   3360
               Visible         =   0   'False
               Width           =   2055
            End
            Begin VB.ComboBox cboTileset 
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frmEdMap.frx":700A
               Left            =   0
               List            =   "frmEdMap.frx":7011
               Style           =   2  'Dropdown-Liste
               TabIndex        =   13
               ToolTipText     =   "Tileset selector"
               Top             =   3000
               Width           =   1695
            End
         End
      End
      Begin VB.PictureBox picSizer2 
         BackColor       =   &H80000010&
         BorderStyle     =   0  'Kein
         Height          =   30
         Left            =   0
         MousePointer    =   7  'Gr��en�nderung N S
         ScaleHeight     =   2
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   219
         TabIndex        =   2
         Top             =   4605
         Width           =   3285
      End
      Begin MSComctlLib.Toolbar tlbTools 
         Height          =   330
         Left            =   0
         TabIndex        =   25
         Top             =   0
         Visible         =   0   'False
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   582
         ButtonWidth     =   609
         ButtonHeight    =   582
         Style           =   1
         ImageList       =   "imlTlbTools"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   11
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "Pen"
               ImageIndex      =   1
               Style           =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "Random Pen"
               ImageIndex      =   8
               Style           =   1
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "Rectangle"
               ImageIndex      =   2
               Style           =   1
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "Path Tool"
               ImageIndex      =   5
               Style           =   1
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "Tile Picker"
               ImageIndex      =   3
               Style           =   1
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "Erazor"
               ImageIndex      =   4
               Style           =   1
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "16*16 editing mode"
               ImageIndex      =   6
               Style           =   1
               Value           =   1
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "32*32 editing mode"
               ImageIndex      =   7
               Style           =   1
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "FlexPen(tm)"
               ImageIndex      =   9
               Style           =   1
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.StatusBar stb 
      Align           =   2  'Unten ausrichten
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   8115
      Width           =   10965
      _ExtentX        =   19341
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Level: 0"
            TextSave        =   "Level: 0"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Layer: 0"
            TextSave        =   "Layer: 0"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "Mouse: 0,0"
            TextSave        =   "Mouse: 0,0"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "Fps: 0"
            TextSave        =   "Fps: 0"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Text            =   "(Message)"
            TextSave        =   "(Message)"
            Key             =   "Message"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog cdl 
      Left            =   5760
      Top             =   6360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "Map Files (*.rmap)|*.rmap"
   End
   Begin VB.Menu mnuPop 
      Caption         =   "(Popups)"
      Begin VB.Menu mnuPopMapContext 
         Caption         =   "(MapContext)"
         Begin VB.Menu mnuPopMapContextAdd 
            Caption         =   "Add"
            Begin VB.Menu mnuPopMapContextAddLayer 
               Caption         =   "Layer"
            End
            Begin VB.Menu mnuPopMapContextAddDivider1 
               Caption         =   "-"
            End
            Begin VB.Menu mnuPopMapContextAddLevel 
               Caption         =   "Terrain Level"
            End
            Begin VB.Menu mnuPopMapContextAddScriptLevel 
               Caption         =   "Script Level"
            End
            Begin VB.Menu mnuPopMapContextAddDivider2 
               Caption         =   "-"
            End
            Begin VB.Menu mnuPopMapContextAddNPC 
               Caption         =   "NPC"
            End
         End
         Begin VB.Menu mnuPopMapContextDivider1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopMapContextVisible 
            Caption         =   "Visible"
            Checked         =   -1  'True
         End
         Begin VB.Menu mnuPopMapContextDivider3 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopMapContextRename 
            Caption         =   "Rename"
         End
         Begin VB.Menu mnuPopMapContextDel 
            Caption         =   "Delete"
         End
      End
      Begin VB.Menu mnuPopTileContext 
         Caption         =   "(TileContext)"
         Begin VB.Menu mnuPopTileContextAdd 
            Caption         =   "Add Tileset..."
         End
         Begin VB.Menu mnuPopTileContextDel 
            Caption         =   "Remove Tileset"
         End
      End
      Begin VB.Menu mnuPopRM2K 
         Caption         =   "(RM2K)"
         Begin VB.Menu mnuAddAll 
            Caption         =   "Add All Rm2K Blocks (Original Rm2K Sets ONLY)"
         End
         Begin VB.Menu mnuRM2KAnimatedPath 
            Caption         =   "Add RM2K Animated Path Block"
         End
         Begin VB.Menu mnuRM2KAnimation 
            Caption         =   "Add RM2K Animation"
         End
         Begin VB.Menu br3 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRM2KPath 
            Caption         =   "Add RM2K/RMXP Path Block"
         End
         Begin VB.Menu br4 
            Caption         =   "-"
         End
         Begin VB.Menu mnuRMXPAnimation 
            Caption         =   "Add RMXP Animated Path"
         End
      End
   End
End
Attribute VB_Name = "frmEdMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private m_x        As Long
Private m_y        As Long
Private a_x        As Long
Private a_y        As Long

Private s_x        As Single
Private s_y        As Single

Public scroll     As Boolean

Dim TempNPCX As Long
Dim TempNPCY As Long

Private n_x        As Long
Private n_y        As Long
Private npcmove As Boolean

Dim GridEdCtl As Control
Dim DontStackOverrun As Boolean
Dim IgnoreNextClick As Boolean

Private WithEvents Msg As MsgHook 'SubClassing-Objekt
Attribute Msg.VB_VarHelpID = -1
Dim ymax As Long

Public CurrentFile As String

Public Sub OpenFile(FileName As String)

    Set Map = New cGAMMap
    Set Map.MyS2D = GFX

    If Not Map.LoadMap(FileName) Then
        Set Map = Nothing
        Unload Me
        Exit Sub
    End If
    
    ListStuff

End Sub

Public Sub NewFile()


frmEdNew.CurrentFile = CurrentFile
frmEdNew.Show vbModal
    

End Sub

Private Sub cmdMFG_Click()
    Select Case mfgNPC.TextMatrix(mfgNPC.Row, 0)

        Case "Color"
            RefreshStandby
            cdl.ShowColor
            GridUpdated cdl.Color

        Case "Script"

            If SelNPC = 0 Then Exit Sub
            frmEdScript.cs.Text = Map.m_NPCs(SelNPC).Script.ScriptText
            RefreshStandby
            frmEdScript.Show vbModal
            GridUpdated 123

        Case "Sprite"
            RefreshStandby

            ChangeNPCImage
            GridUpdated 123

        Case "Collision Mask"
            RefreshStandby
            
            frmMask.MaskXStart = Map.m_NPCs(SelNPC).MaskXStart
            frmMask.MaskYStart = Map.m_NPCs(SelNPC).MaskYStart
            frmMask.MaskXEnd = Map.m_NPCs(SelNPC).MaskXEnd
            frmMask.MaskYEnd = Map.m_NPCs(SelNPC).MaskYEnd
            
            frmMask.LoadMask Map.m_NPCs(SelNPC).MyDC, Map.m_NPCs(SelNPC).Width, Map.m_NPCs(SelNPC).Height, Map.m_NPCs(SelNPC).OriginalhDC, Map.m_NPCs(SelNPC).MyColorKey
            frmMask.Show vbModal
            
            BitBlt Map.m_NPCs(SelNPC).MyDC, 0, 0, frmMask.MaskWidth, frmMask.MaskHeight, frmMask.MaskhDC, 0, 0, vbSrcCopy
            Map.m_NPCs(SelNPC).MaskXStart = frmMask.MaskXStart
            Map.m_NPCs(SelNPC).MaskYStart = frmMask.MaskYStart
            Map.m_NPCs(SelNPC).MaskXEnd = frmMask.MaskXEnd
            Map.m_NPCs(SelNPC).MaskYEnd = frmMask.MaskYEnd
            
            GridUpdated 123

    End Select

End Sub


Private Sub cmdSave_Click()
    Dim tp As String
    tp = ProvideTempFile("rmap")
    
    Map.SaveMap tp
    
    Dim fc As String
    fc = File2Str(tp)
    
    StoreFile CurrentFile, fc
    
    frmViewProject.Update
    'MsgBox "Saved.", vbInformation, "Done."
End Sub

Private Sub cmdScriptLvl_Click()
    frmEdScript.cs.Text = Map.Levels(CurrentLevel).Script
    RefreshStandby
    frmEdScript.Show vbModal
End Sub

Private Sub cmdTileset_Click()
    PopupMenu mnuPopTileContext
End Sub

Private Sub Form_Load()
    mnuPop.Visible = False
    
    Set Msg = New MsgHook
    If Not InDevelopment Then Msg.Hook hwnd, WM_MOUSEWHEEL

    Set trvMap.ImageList = imlTrv

    mnuPop.Visible = False
    picColTS.TintColor = vbRed
    picColTS.IgnoreRM2K = True
    cboColMode.ListIndex = 0
    ShowView 3
    InitNPCGrid
End Sub

Private Sub Form_Resize()
    On Error Resume Next

    picLeftBarMap.Move 0, 0, picLeftBarMap.Width, Me.ScaleHeight - stb.Height
    picSizer.Move picLeftBarMap.Width, 0, picSizer.Width, picLeftBarMap.Height
    picRender.Move picSizer.Left + picSizer.Width, 0, Me.ScaleWidth - (picSizer.Left + picSizer.Width + vsbMap.Width), Me.ScaleHeight - (stb.Height + hsbMap.Height)

    hsbMap.Move picRender.Left, picRender.Height, picRender.Width, hsbMap.Height
    vsbMap.Move picRender.Left + picRender.Width, 0, vsbMap.Width, picRender.Height
End Sub

Private Sub hsbMap_Change()
    ScrollX = 0 - hsbMap.Value
    Map.RedrawAll
End Sub

Private Sub mfgNPC_Click()
If SelNPC = 0 Then Exit Sub

    Dim curnpc As cS2DNPC
    Set curnpc = Map.m_NPCs(SelNPC)

    If curnpc Is Nothing Then Exit Sub

    If mfgNPC.Col = 1 Then

        Select Case mfgNPC.TextMatrix(mfgNPC.Row, 0)

            Case "Name"
                txtMFG.Text = curnpc.Name
                PlaceGridEditorCtl txtMFG

            Case "Render Mode"
                
                'txtMFG.Text = curnpc.Additional
                cboMFG.Clear
                cboMFG.AddItem "S2D_None"
                cboMFG.AddItem "S2D_Masked"
                cboMFG.AddItem "S2D_Alpha"
                cboMFG.AddItem "S2D_Alpha_Color"
                cboMFG.AddItem "S2D_Additive"
                cboMFG.AddItem "S2D_Additive_Source"
                cboMFG.AddItem "S2D_Additive_Destination"
                cboMFG.AddItem "S2D_Exclude"
                cboMFG.AddItem "S2D_Subtract"
                cboMFG.AddItem "S2D_Minimize"
                cboMFG.AddItem "S2D_Maximize"
                cboMFG.AddItem "S2D_Subtract"
                cboMFG.AddItem "S2D_Subtract"
                
                cboMFG.ListIndex = curnpc.Additional

                PlaceGridEditorCtl cboMFG

            Case "Alpha"
                txtMFG.Text = curnpc.Alpha
                PlaceGridEditorCtl txtMFG

            Case "Color"
                PlaceGridEditorCtl cmdMFG

            Case "Depth"
                txtMFG.Text = curnpc.Depth
                PlaceGridEditorCtl txtMFG

            Case "JumpDuration"
                txtMFG.Text = curnpc.JumpDuration
                PlaceGridEditorCtl txtMFG

            Case "JumpSpeed"
                txtMFG.Text = curnpc.JumpSpeed
                PlaceGridEditorCtl txtMFG

            Case "Script"
                PlaceGridEditorCtl cmdMFG

            Case "SourceX"
                txtMFG.Text = curnpc.SourceX
                PlaceGridEditorCtl txtMFG

            Case "SourceY"
                txtMFG.Text = curnpc.SourceY
                PlaceGridEditorCtl txtMFG

            Case "SourceWidth"
                txtMFG.Text = curnpc.SourceWidth
                PlaceGridEditorCtl txtMFG

            Case "SourceHeight"
                txtMFG.Text = curnpc.SourceHeight
                PlaceGridEditorCtl txtMFG

            Case "Sprite"
                PlaceGridEditorCtl cmdMFG

            Case "StairHeight"
                txtMFG.Text = curnpc.StairHeight
                PlaceGridEditorCtl txtMFG

            Case "X"
                txtMFG.Text = curnpc.x
                PlaceGridEditorCtl txtMFG

            Case "Y"
                txtMFG.Text = curnpc.y
                PlaceGridEditorCtl txtMFG

            Case "ZPlus"
                txtMFG.Text = curnpc.ZPlus
                PlaceGridEditorCtl txtMFG

            Case "Collision Mask"
                txtMFG.Text = "(Mask)"
                PlaceGridEditorCtl cmdMFG

            Case Else
                MsgBox "TODO: Add handler for '" & mfgNPC.TextMatrix(mfgNPC.Row, 0) & "'!", vbExclamation

        End Select

    End If
End Sub

Private Sub mfgNPC_Scroll()
    RePlaceGridEditorCtl
End Sub

Private Sub mnuAddAll_Click()
    AddRM2KPaths
End Sub

Private Sub mnuRM2KAnimatedPath_Click()
    MsgBox "Due to limitation of Rm2k only the 3 Standart Animated paths can be used for now, sry"
    
    Dim i As Integer
    
    'If picTileSet.SelTileHeight > 16 Then Exit Sub
    'If picTileSet.SelTileWidth > 16 Then Exit Sub
    
    For i = 1 To RM2K_AnimatedPath_Count
        If RM2K_AnimatedPaths(i).x = picTileSet.SelTileX * Const_TileWidth Then
        If RM2K_AnimatedPaths(i).y = picTileSet.SelTileY * Const_TileHeight Then
            Exit Sub
        End If
        End If
    Next i
    
    RM2K_AnimatedPath_Count = RM2K_AnimatedPath_Count + 1
    ReDim Preserve RM2K_AnimatedPaths(0 To 3)
    
    RM2K_AnimatedPaths(1).x = 0
    RM2K_AnimatedPaths(1).y = 0
    RM2K_AnimatedPaths(1).TileSet = CurrentTileset
    
    RM2K_AnimatedPaths(2).x = 3 * Const_TileWidth
    RM2K_AnimatedPaths(2).y = 0
    RM2K_AnimatedPaths(2).TileSet = CurrentTileset
    
    RM2K_AnimatedPaths(3).x = 0
    RM2K_AnimatedPaths(3).y = 6 * Const_TileHeight
    RM2K_AnimatedPaths(3).TileSet = CurrentTileset
End Sub

Private Sub mnuRM2KAnimation_Click()

    Dim i As Integer, ii As Integer
    
    If picTileSet.SelTileHeight > Const_TileHeight Then Exit Sub
    If picTileSet.SelTileWidth > Const_TileWidth Then Exit Sub
    
    For i = 1 To RM2K_Animation_Count
        If RM2K_Animations(i).TileSet = CurrentTileset Then                  'picTileSet.TileSet Then
        If RM2K_Animations(i).x = picTileSet.SelTileX * Const_TileWidth Then
        If RM2K_Animations(i).y = picTileSet.SelTileY * Const_TileHeight Then
            For ii = i To RM2K_Animation_Count
                RM2K_Animations(i).TileSet = RM2K_Animations(ii).TileSet
                RM2K_Animations(i).x = RM2K_Animations(ii).x
                RM2K_Animations(i).y = RM2K_Animations(ii).y
            Next ii
            
            RM2K_Animation_Count = RM2K_Animation_Count - 1
         
            Exit Sub
        End If
        End If
        End If
    Next i
    
    RM2K_Animation_Count = RM2K_Animation_Count + 1
    ReDim Preserve RM2K_Animations(0 To RM2K_Animation_Count)
    
    RM2K_Animations(RM2K_Animation_Count).x = picTileSet.SelTileX * Const_TileWidth
    RM2K_Animations(RM2K_Animation_Count).y = picTileSet.SelTileY * Const_TileHeight
    RM2K_Animations(RM2K_Animation_Count).TileSet = CurrentTileset

End Sub

Private Sub mnuRM2KPath_Click()

    Dim i As Integer
    
    If picTileSet.SelTileHeight > Const_TileHeight Then Exit Sub
    If picTileSet.SelTileWidth > Const_TileWidth Then Exit Sub
    
    For i = 1 To RM2K_Path_Count
        If RM2K_Paths(i).TileSet = picTileSet.TileSet Then
        If RM2K_Paths(i).x = picTileSet.SelTileX * Const_TileWidth Then
        If RM2K_Paths(i).y = picTileSet.SelTileY * Const_TileHeight Then
            Exit Sub
        End If
        End If
        End If
    Next i
    
    RM2K_Path_Count = RM2K_Path_Count + 1
    ReDim Preserve RM2K_Paths(0 To RM2K_Path_Count)
    
    RM2K_Paths(RM2K_Path_Count).x = picTileSet.SelTileX * Const_TileWidth
    RM2K_Paths(RM2K_Path_Count).y = picTileSet.SelTileY * Const_TileHeight
    RM2K_Paths(RM2K_Path_Count).TileSet = CurrentTileset
    
End Sub

Private Sub mnuRMXPAnimation_Click()
Dim i As Integer

If picTileSet.SelTileHeight > Const_TileHeight Then Exit Sub
If picTileSet.SelTileWidth > Const_TileWidth Then Exit Sub

For i = 1 To RMXP_AnimatedPath_Count
    If RMXP_AnimatedPaths(i).TileSet = picTileSet.TileSet Then
    If RMXP_AnimatedPaths(i).x = picTileSet.SelTileX * Const_TileWidth Then
    If RMXP_AnimatedPaths(i).y = picTileSet.SelTileY * Const_TileHeight Then
        Exit Sub
    End If
    End If
    End If
Next i

RMXP_AnimatedPath_Count = RMXP_AnimatedPath_Count + 1
ReDim Preserve RMXP_AnimatedPaths(0 To RMXP_AnimatedPath_Count)

RMXP_AnimatedPaths(RMXP_AnimatedPath_Count).x = picTileSet.SelTileX * Const_TileWidth
RMXP_AnimatedPaths(RMXP_AnimatedPath_Count).y = picTileSet.SelTileY * Const_TileHeight
RMXP_AnimatedPaths(RMXP_AnimatedPath_Count).TileSet = CurrentTileset

End Sub

Private Sub picLB_Resize(Index As Integer)

    If Index = 0 Then ' tileset-fenster
    
        picTileSet.Width = picLB(0).ScaleWidth
        picTileSet.Height = picLB(0).ScaleHeight - cboTileset.Height - IIf(cboPath.Visible, cboPath.Height, 0)
        
        cmdTileset.Top = picTileSet.Height
        cmdTileset.Left = picLB(0).ScaleWidth - cmdTileset.Width

        cboTileset.Left = 0
        cboTileset.Top = picTileSet.Height
        cboTileset.Width = picLB(0).ScaleWidth - cmdTileset.Width

        cboPath.Top = picTileSet.Height + cmdTileset.Height
        cboPath.Width = picLB(0).ScaleWidth
        
        Flatten cmdTileset.hwnd
        
     ElseIf Index = 1 Then ' coltileset-fenster'NOT INDEX...
        picColTS.Width = picLB(1).ScaleWidth
        picColTS.Height = picLB(1).ScaleHeight - cmdTileset.Height
        picLevelProps.Width = picLB(1).ScaleWidth
        picLevelProps.Top = picLB(1).ScaleHeight - picLevelProps.Height
    ElseIf Index = 4 Then
        mfgNPC.Width = picLB(4).ScaleWidth
        
        If (mfgNPC.Rows + 1) * mfgNPC.RowHeight(1) > picLB(4).ScaleHeight Then
        mfgNPC.Height = picLB(4).ScaleHeight
        Else
        mfgNPC.Height = (mfgNPC.Rows + 1) * mfgNPC.RowHeight(1)
        End If
        
        mfgNPC.ColWidth(0) = mfgNPC.Width / 2
        mfgNPC.ColWidth(1) = mfgNPC.ColWidth(0)
        
        RePlaceGridEditorCtl
    End If

End Sub

Private Sub picLeftBarMap_Resize()
On Error Resume Next



    tlbTools.Width = picLeftBarMap.ScaleWidth
    
    Dim plcTop As Long
    plcTop = IIf(tlbTools.Visible, tlbTools.Height + tlbTools.Top, tlbTools.Top)
    
    picLeftContainer.Move 0, _
                            plcTop, _
                            picLeftBarMap.ScaleWidth, _
                            picLevelList.Top - picSizer2.Height - plcTop
    
    picSizer2.Move 0, _
                    picLeftContainer.Top + picLeftContainer.Height, _
                    picLeftBarMap.ScaleWidth, _
                    picSizer2.Height

    picLevelList.Move 0, _
                        picLevelList.Top, _
                        picLeftBarMap.ScaleWidth, _
                        picLeftBarMap.ScaleHeight - picLevelList.Top

End Sub

Private Sub picLeftContainer_Resize()
On Error Resume Next
    Dim i As Integer
    
    For i = 0 To picLB.Count - 1
        With picLB(i)
            .Left = 0
            .Top = 0
            .Height = picLeftContainer.ScaleHeight
            .Width = picLeftContainer.ScaleWidth
        End With 'picLB(i)
    Next i

End Sub

Private Sub picLevelList_Resize()
    On Error Resume Next
    tlbLevelTools.Move 0, 0, picLevelList.ScaleWidth, tlbLevelTools.Height
    trvMap.Move 0, tlbLevelTools.Height, picLevelList.ScaleWidth, picLevelList.ScaleHeight - tlbLevelTools.Height
End Sub

Private Sub picLevelProps_Resize()
  ' TODOOOOOO
  picLevPropsDivider.Width = picLevelProps.ScaleWidth
  Frame2.Width = picLevelProps.ScaleWidth - 240
  Frame1.Width = picLevelProps.ScaleWidth - 240
  cboColMode.Width = Frame2.Width - 240
  ZValue.Width = Frame1.Width - 240
End Sub

Private Sub picRender_Resize()
    DoResize picRender, picRender.ScaleWidth, picRender.ScaleHeight
End Sub

Private Sub tlbTools_ButtonClick(ByVal Button As MSComctlLib.Button)

    If Button.Index <= 6 Then
        CurrentTool = Button.Index
        cboPath.Visible = (CurrentTool = 4)
        picLB_Resize 0
        If cboPath.Visible Then
            Path_List
        End If
        
        Dim i As Integer
        For i = 1 To 5
            tlbTools.Buttons(i).Value = tbrUnpressed
        Next i
        tlbTools.Buttons(CurrentTool).Value = tbrPressed
    End If
    If Button.Index = 7 Then
        DrawOrErase = (tlbTools.Buttons(7).Value = tbrPressed)
     ElseIf Button.Index = 9 Then 'NOT BUTTON.INDEX...
        tlbTools.Buttons(9).Value = tbrPressed
        tlbTools.Buttons(10).Value = tbrUnpressed
        DrawModeWidth = Const_TileWidth
        DrawModeHeight = Const_TileWidth
        UpdateDrawModeSize
     ElseIf Button.Index = 10 Then 'NOT BUTTON.INDEX...
        tlbTools.Buttons(9).Value = tbrUnpressed
        tlbTools.Buttons(10).Value = tbrPressed
        DrawModeWidth = Const_TileWidth * 2
        DrawModeHeight = Const_TileHeight * 2
        UpdateDrawModeSize
    
     ElseIf Button.Index = 11 Then
        FlexPen = (tlbTools.Buttons(11).Value = tbrPressed)
    End If
    
    

End Sub

Private Sub tmrRender_Timer()
    Render
    RenderTS
End Sub

Private Sub picRender_KeyDown(KeyCode As Integer, _
                              Shift As Integer)

    If Map Is Nothing Then Exit Sub
    If Map.CFm_MapFile = "" Then Beep: Exit Sub

    Dim old_cl As Integer

    Select Case KeyCode
        Case Asc("A")
            old_cl = CurrentLayer
            CurrentLayer = CurrentLayer - 1

            If Map.Levels(CurrentLevel).Layers(CurrentLayer) Is Nothing Then CurrentLayer = old_cl

            TrvSelectPath Map.Levels(CurrentLevel).Desc & "\" & Map.Levels(CurrentLevel).Layers(CurrentLayer).Desc

        Case Asc("Q")
            old_cl = CurrentLayer
            CurrentLayer = CurrentLayer + 1

            If Map.Levels(CurrentLevel).Layers(CurrentLayer) Is Nothing Then CurrentLayer = old_cl

            TrvSelectPath Map.Levels(CurrentLevel).Desc & "\" & Map.Levels(CurrentLevel).Layers(CurrentLayer).Desc

        Case Asc("S")
            old_cl = CurrentLevel
            CurrentLevel = CurrentLevel - 1

            If Map.Levels(CurrentLevel) Is Nothing Then CurrentLevel = old_cl
            If Map.Levels(CurrentLevel).Layers(CurrentLayer) Is Nothing Then CurrentLayer = Map.Levels(CurrentLevel).Layer.Count

            TrvSelectPath Map.Levels(CurrentLevel).Desc

        Case Asc("W")
            old_cl = CurrentLevel
            CurrentLevel = CurrentLevel + 1

            If Map.Levels(CurrentLevel) Is Nothing Then CurrentLevel = old_cl
            If Map.Levels(CurrentLevel).Layers(CurrentLayer) Is Nothing Then CurrentLayer = Map.Levels(CurrentLevel).Layer.Count

            TrvSelectPath Map.Levels(CurrentLevel).Desc

    End Select

End Sub

Private Sub picRender_MouseDown(Button As Integer, _
                                Shift As Integer, _
                                x As Single, _
                                y As Single)

    If Map Is Nothing Then Exit Sub
    If Map.CFm_MapFile = "" Then MsgBox "Please select a map file from the right hand list or create a new one to continue.", vbInformation, "Need a file to work on": Exit Sub

    If IgnoreNextClick Then Exit Sub

    x = x / ZoomFactor
    y = y / ZoomFactor

    On Error Resume Next

    If Button = 2 Then
        
        If (Map.MapWidth * Const_TileWidth) * ZoomFactor > ScreenWidth Then
            m_x = x
            Me.scroll = True
        End If
        
        If (Map.MapHeight * Const_TileHeight) * ZoomFactor > ScreenHeight Then
            m_y = y
            Me.scroll = True
        End If
        
    End If
    
    If Mode = 3 Then ' npcs bearbeiten
        If Button = 1 Then
    
            Dim oldselnpc As Integer
            oldselnpc = SelNPC
            SelNPC = NPCAtPos(x - ScrollX, y - ScrollY)
    
            UpdateCtls
    
            If SelNPC > 0 Then
                TrvSelectPath "NPCs\" & Map.m_NPCs(SelNPC).Name
        
                If SnapToGrid Then
        
                    TempNPCX = Map.m_NPCs(SelNPC).x
                    TempNPCY = Map.m_NPCs(SelNPC).y
                    Map.m_NPCs(SelNPC).x = Map.m_NPCs(SelNPC).x - (Map.m_NPCs(SelNPC).x Mod Const_TileWidth)
                    Map.m_NPCs(SelNPC).y = Map.m_NPCs(SelNPC).y - (Map.m_NPCs(SelNPC).y Mod Const_TileHeight)
                End If
        
                ShowView 4
        
            ElseIf SelNPC = 0 Then
                ShowView 3
            End If
    
            If SelNPC > 0 Then
                n_x = x
                n_y = y
                npcmove = True
    
            End If
    
        End If
    End If
    
    Tools_MouseDown Button, Shift, x, y

End Sub

Private Sub picRender_MouseMove(Button As Integer, _
                                Shift As Integer, _
                                x As Single, _
                                y As Single)
    s_x = x
    s_y = y

    If Map Is Nothing Then Exit Sub
    If Button <> 0 And Map.CFm_MapFile = "" Then Beep: Exit Sub

    If IgnoreNextClick Then Exit Sub

    x = Int(x / ZoomFactor)
    y = Int(y / ZoomFactor)

    Dim t_x As Long
    Dim t_Y As Long
    
    MouseXScreen = x
    MouseYScreen = y
    t_x = x - ScrollX
    t_Y = y - ScrollY
    MouseX = (t_x - (t_x Mod IIf(FlexPen, SelTileWidth * Const_TileWidth, DrawModeWidth))) / Const_TileWidth
    MouseY = (t_Y - (t_Y Mod IIf(FlexPen, SelTileHeight * Const_TileHeight, DrawModeHeight))) / Const_TileHeight
    
    stb.Panels(3).Text = "Mouse: " & MouseX & "," & MouseY
    
    If Button = 2 And scroll Then
        ScrollX = ScrollX - (m_x - x)
        ScrollY = ScrollY - (m_y - y)
        m_x = x
        m_y = y
        Map.RedrawAll
        UpdateScroll
    End If
    
    If Mode = 3 Then
        If Button = 1 Then
            If npcmove Then

                'If x <> n_x And Y <> n_y Then
                If SelNPC = 0 Then npcmove = False: Exit Sub
                Dim curnpc As cS2DNPC
                Set curnpc = Map.m_NPCs(SelNPC)
                
                If SnapToGrid Then
                
                    TempNPCX = TempNPCX - (n_x - x)
                    TempNPCY = TempNPCY - (n_y - y)
                
                    curnpc.x = Int((TempNPCX - (TempNPCX Mod Const_TileWidth)))
                    curnpc.y = Int((TempNPCY - (TempNPCY Mod Const_TileHeight)))
                
                Else
                    curnpc.x = Int(curnpc.x - (n_x - x))
                    curnpc.y = Int(curnpc.y - (n_y - y))
                    'curnpc.x = x - ScrollX
                    'curnpc.Y = Y - ScrollY
                End If
                
                n_x = x
                n_y = y

                DoEvents
                UpdateNPCDetails
                'End If
            End If
        End If
    End If
    
    If Mode = 1 Then StatusMessage "Use mouse to draw / Hold 'shift' for eraser. | Q: LayUp A: LayDown W: LevUp S: LevDown"
    If Mode = 2 Then StatusMessage "Use mouse to draw | Q: LayUp A: LayDown W: LevUp S: LevDown"
    If Mode = 3 Then StatusMessage "Click an NPC to select it or click and drag to move it."
    Tools_MouseMove Button, Shift, x, y

End Sub

Private Sub picRender_MouseUp(Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)

    If Map Is Nothing Then Exit Sub
    If Map.CFm_MapFile = "" Then Beep: Exit Sub

    If IgnoreNextClick Then IgnoreNextClick = False: Exit Sub

    x = x / ZoomFactor
    y = y / ZoomFactor

    If Button = 2 And scroll Then
        Me.scroll = False
        Map.RedrawAll
    End If

    Tools_MouseUp Button, Shift, x, y

    If Button = 4 Then

        On Error Resume Next
        mnuPopMapContextAddLayer.Enabled = (TypeOf Map.Levels(CurrentLevel) Is cGAMLevel)
        mnuPopMapContextAddLevel.Enabled = True
        mnuPopMapContextAddScriptLevel.Enabled = True
        mnuPopMapContextAddNPC.Enabled = True
        mnuPopMapContextVisible.Checked = True
        mnuPopMapContextVisible.Enabled = True
        mnuPopMapContextRename.Enabled = True
        mnuPopMapContextDel.Enabled = True

        PopupMenu mnuPopMapContext
        
        IgnoreNextClick = True
    End If

    'UpdateCtls
End Sub

Public Sub TrvSelectPath(p As String, _
                         Optional trv As TreeView = Nothing)

    If trv Is Nothing Then Set trv = trvMap
    Dim i As Integer

    For i = 1 To trv.Nodes.Count

        If trv.Nodes(i).FullPath = p Then
            Set trv.SelectedItem = trv.Nodes(i)
            Exit For
        End If

    Next i

End Sub

Public Sub UpdateScroll()

    On Error Resume Next
    hsbMap.Value = 0 - ScrollX
    vsbMap.Value = 0 - ScrollY
    
End Sub

Public Sub UpdateNPCDetails()
    If Not SelNPC = 0 Then
    Dim Npc As cS2DNPC
    Set Npc = Map.m_NPCs(SelNPC)
    
    With mfgNPC
    .TextMatrix(0, 1) = Npc.Name
    .TextMatrix(1, 1) = "Mode: " & Npc.Additional
    .TextMatrix(2, 1) = Npc.Alpha
    .TextMatrix(3, 1) = Npc.Color
    .TextMatrix(4, 1) = Npc.Depth
    .TextMatrix(5, 1) = Npc.JumpDuration
    .TextMatrix(6, 1) = Npc.JumpSpeed
    .TextMatrix(7, 1) = "(script)" 'hier vielleicht sp�ter einen zeilen-counter einf�gen, etc.
    .TextMatrix(8, 1) = Npc.SourceX
    .TextMatrix(9, 1) = Npc.SourceY
    .TextMatrix(10, 1) = Npc.SourceWidth
    .TextMatrix(11, 1) = Npc.SourceHeight
    .TextMatrix(12, 1) = Npc.imgfile
    .TextMatrix(13, 1) = "(mask)"
    .TextMatrix(14, 1) = Npc.StairHeight
    .TextMatrix(15, 1) = Int(Npc.x)
    .TextMatrix(16, 1) = Int(Npc.y)
    .TextMatrix(17, 1) = Npc.ZPlus
    End With


    End If
End Sub

Public Sub UpdateCtls()
If Map Is Nothing Then Exit Sub

    stb.Panels(1).Text = "Level: " & CurrentLevel
    stb.Panels(2).Text = "Layer: " & IIf(CurrentLayer = 0, "-", CurrentLayer)
    Path_List
    
    UpdateNPCDetails
    
    If CurrentLevel > 0 Then
    
        ZValue.Value = Map.Levels(CurrentLevel).Z
        ZValue2.Value = Map.Levels(CurrentLevel).Z
        
        chkScriptLvl.Value = IIf(Map.Levels(CurrentLevel).Visible, 1, 0)
        
        If CurrentLayer > 0 Then
            If cboTileset.ListCount > 0 Then cboTileset.ListIndex = Map.Levels(CurrentLevel).Layers(CurrentLayer).TileSetID - 1
        End If
        
    End If
    
    If Map.MapWidth * Const_TileWidth > ScreenWidth Then
        hsbMap.max = (Map.MapWidth * Const_TileWidth) - ScreenWidth
    End If
    
    If Map.MapHeight * Const_TileHeight > ScreenHeight Then
        vsbMap.max = (Map.MapHeight * Const_TileHeight) - ScreenHeight
    End If
    
    UpdateScroll
End Sub

Public Sub ShowView(ByVal tabid As Integer)
    Dim i As Integer
    
    For i = 0 To picLB.Count - 1
        picLB(i).Visible = False
    Next i

    picLB(tabid).Visible = True
    picLB(tabid).BorderStyle = 0

End Sub

Public Sub StatusMessage(s As String)
    stb.Panels(5).Text = s
End Sub

Private Sub cboColMode_Click()
    SelColInf = cboColMode.ListIndex
End Sub

Private Sub cboMFG_Click()
    GridUpdated cboMFG.ListIndex
End Sub

Private Sub cboMFG_LostFocus()
    cboMFG.Visible = False
    Set GridEdCtl = Nothing
End Sub

Public Sub cboPath_Click()

    CurrentPath = DispPaths(cboPath.ListIndex + 1)
    Set CurPath = modEdTools.Paths(CurrentPath)
    If CurPath.TileSize = 2 Then
        tlbTools_ButtonClick tlbTools.Buttons(10)
     ElseIf CurPath.TileSize = 1 Then 'NOT CURPATH.TILESIZE...
        tlbTools_ButtonClick tlbTools.Buttons(9)
    End If

End Sub


Private Sub cboTileset_Click()

    If DontStackOverrun Then Exit Sub
    If cboTileset.ListIndex = -1 Then Exit Sub
    CurrentTileset = cboTileset.ListIndex + 1
    
    Map.Levels(CurrentLevel).Layers(CurrentLayer).TileSetID = CurrentTileset
    
    UpdateCtls
    UpdateTileSetCombo
    
End Sub

Private Sub chkScriptLvl_Click()
    
    Map.Levels(CurrentLevel).Visible = chkScriptLvl.Value
    ListStuff
    
End Sub

Public Sub UpdateTileSetCombo()
    If Map Is Nothing Then Exit Sub
  Dim t As String
    Dim i As Integer
    
    cboTileset.Clear
    For i = 1 To Map.TileSets.Count
        t = Map.TileSets(i).FileName
    t = Mid$(t, InStrRev(t, "\") + 1)
    cboTileset.AddItem t
    Next i
    
    DontStackOverrun = True
    cboTileset.ListIndex = CurrentTileset - 1
    DontStackOverrun = False
End Sub

Public Sub ListStuff()
    Map2Trv
    UpdateTileSetCombo
    UpdateCtls
End Sub

Public Sub Map2Trv()
    trvMap.Nodes.Clear
    Dim i As Integer
    Dim NPCs As Node
    Set NPCs = trvMap.Nodes.Add(, , "npclist", "NPCs", 5, 0)
    NPCs.ExpandedImage = 6
    
    Dim n As Node
    For i = 1 To Map.m_NPCs.Count
        Set n = trvMap.Nodes.Add(, , "npc:" & i, Map.m_NPCs(i).Name, 3, 3)
        Set n.Parent = NPCs
    Next i

    For i = Map.Levels.Count To 1 Step -1
        TrvItem Map.Levels(i), i
    Next i
End Sub

Public Sub TrvItem(lvl As Object, _
                   LvlID As Integer)

  Dim n As Object
  Dim ico As Integer
  
    If TypeOf lvl Is cGAMLevel Then
        ico = IIf(lvl.Visible, 1, 8)
        Set n = trvMap.Nodes.Add(, , "lvl:" & LvlID, lvl.Desc, ico, ico)
        SubItemsForGamLevel n, lvl, LvlID
     ElseIf TypeOf lvl Is cGAMScriptLevel Then 'NOT TYPEOF...
        Set n = trvMap.Nodes.Add(, , "scrlvl:" & LvlID, lvl.Desc & IIf(lvl.Visible, "", " (i)"), 2, 2)
     Else 'NOT TYPEOF...
        MsgBox "Unknown Level Type!", vbCritical, "Error"
    End If

End Sub

Public Sub SubItemsForGamLevel(n As Node, _
                               l As cGAMLevel, _
                               ByVal LvlID As Integer)

  
  Dim i As Integer
  Dim m As Node
  

  
  Dim ico As Integer

    For i = 1 To l.Layers.Count
        ico = IIf(l.Layers(i).Visible, 4, 7)
        Set m = trvMap.Nodes.Add(, , "lay:" & LvlID & ":" & i, l.Layers(i).Desc, ico, ico)
        Set m.Parent = n
    Next i
    


End Sub

Public Sub GridUpdated(retVal As Variant)
    On Error Resume Next ' damit ung�ltige werte ignoriert werden

    With mfgNPC

        If SelNPC = 0 Then Exit Sub

        Dim curnpc As cS2DNPC
        Set curnpc = Map.m_NPCs(SelNPC)

        If curnpc Is Nothing Then Exit Sub

        Select Case .TextMatrix(.Row, 0)

            Case "Name"
                curnpc.Name = retVal

            Case "Render Mode"
                curnpc.Additional = retVal

            Case "Alpha"
                curnpc.Alpha = Val(retVal)

            Case "Color"
                curnpc.Color = Val(retVal)

            Case "Depth"
                curnpc.Depth = Val(retVal)

            Case "JumpDuration"
                curnpc.JumpDuration = Val(retVal)

            Case "JumpSpeed"
                curnpc.JumpSpeed = Val(retVal)

            Case "SourceX"
                curnpc.SourceX = Val(retVal)

            Case "SourceY"
                curnpc.SourceY = Val(retVal)

            Case "SourceWidth"
                curnpc.SourceWidth = Val(retVal)

            Case "SourceHeight"
                curnpc.SourceHeight = Val(retVal)

            Case "Sprite"
                'das �bernimmt alles die engine

            Case "StairHeight"
                curnpc.StairHeight = Val(retVal)

            Case "X"
                curnpc.x = Val(retVal)

            Case "Y"
                curnpc.y = Val(retVal)

            Case "ZPlus"
                curnpc.ZPlus = Val(retVal)

        End Select

        ListStuff

    End With

End Sub

Public Sub RePlaceGridEditorCtl()

If GridEdCtl Is Nothing Then Exit Sub

PlaceGridEditorCtl GridEdCtl
End Sub

Public Sub PlaceGridEditorCtl(ctl As Control)
    On Error Resume Next
    'wenn man eine editier-textbox innerhalb eines grids braucht, muss man 'tricksen' und sie an die richtige x/y position setzen
    
    Dim addit As Integer
    If mfgNPC.Height < (mfgNPC.Rows + 1) * mfgNPC.RowHeight(1) Then
        addit = vsbMap.Width
    End If
    
    Set GridEdCtl = ctl

    'je nach typ des eingabefelds andere ausrichtung
    If ctl Is txtMFG Then
        ctl.Left = mfgNPC.Left + mfgNPC.ColPos(1) + (3 - addit) * Screen.TwipsPerPixelX 'sieht mit kleinem abstand besser aus
        ctl.Top = mfgNPC.Top + mfgNPC.RowPos(mfgNPC.Row) + 2 * Screen.TwipsPerPixelX
        ctl.Visible = True
        ctl.Width = mfgNPC.ColWidth(1) - Screen.TwipsPerPixelX * 4
        ctl.Height = mfgNPC.RowHeight(mfgNPC.Row) - Screen.TwipsPerPixelY * 4
        txtMFG.SelStart = 0
        txtMFG.SelLength = Len(txtMFG.Text)

    ElseIf ctl Is cboMFG Then
        ctl.Left = mfgNPC.Left + mfgNPC.ColPos(1) - addit
        ctl.Top = mfgNPC.Top + mfgNPC.RowPos(mfgNPC.Row)
        ctl.Visible = True
        ctl.Width = mfgNPC.ColWidth(1) - Screen.TwipsPerPixelX

    ElseIf ctl Is cmdMFG Then
        ctl.Top = mfgNPC.Top + mfgNPC.RowPos(mfgNPC.Row)
        ctl.Visible = True
        ctl.Height = mfgNPC.RowHeight(mfgNPC.Row) - Screen.TwipsPerPixelY
        ctl.Left = mfgNPC.Left + mfgNPC.ColPos(1) + mfgNPC.ColWidth(1) - ctl.Width - addit
    End If

    ctl.SetFocus

    'todo: Textmarker auf geklickte position setzen. (Position des klicks umrechnen)

End Sub

Private Sub UpdateDrawModeSize()

  Dim old_SeltileX As Integer
  Dim old_Seltiley As Integer

    If Not DrawModeWidth = Const_TileWidth Then
        old_SeltileX = SelTileX
        old_Seltiley = SelTileY
        SelTileX = SelTileX * Const_TileWidth
        SelTileY = SelTileY * Const_TileHeight
        SelTileX = (SelTileX - (SelTileX Mod DrawModeWidth)) / Const_TileWidth
        SelTileY = (SelTileY - (SelTileY Mod DrawModeHeight)) / Const_TileHeight
        SelTileWidth = SelTileWidth + (old_SeltileX - SelTileX)
        SelTileHeight = SelTileHeight + (old_Seltiley - SelTileY)
        SelTileWidth = SelTileWidth * Const_TileWidth
        SelTileWidth = (SelTileWidth + (SelTileWidth Mod DrawModeWidth)) / Const_TileWidth
        SelTileHeight = SelTileHeight * Const_TileHeight
        SelTileHeight = (SelTileHeight + (SelTileHeight Mod DrawModeHeight)) / Const_TileHeight
        If SelTileWidth < (DrawModeWidth / Const_TileWidth) Then
            SelTileWidth = (DrawModeWidth / Const_TileWidth)
        End If
        If SelTileHeight < (DrawModeHeight / Const_TileHeight) Then
            SelTileHeight = (DrawModeHeight / Const_TileHeight)
        End If
        With picTileSet
            .SelTileX = SelTileX
            .SelTileY = SelTileY
            .SelTileWidth = SelTileWidth
            .SelTileHeight = SelTileHeight
        End With 'PICTILESET
    End If

End Sub


Private Sub Msg_Before(uMsg As Long, wParam As Long, lParam As Long, retVal As Long)
  Dim Delta As Integer
  Dim Value As Long
  
With rmsZoom
  
    'Mausrad-Drehung bestimmen:
    Delta = (wParam And &HFFFF0000) \ &H10000
    
    'Zielwert bestimmen:
    If Delta < 0 Then
      'nach unten:
      Value = .Value - 10
    Else
      'nach oben:
      Value = .Value + 10
    End If
    
    'Min/Max beachten:
    Select Case Value
    Case Is < .min: Value = .min
    Case Is > .max: Value = .max
    End Select
    
    'ScrollBar-Anzeige aktualisieren:
    .Value = Value
  
    picRender_MouseMove 0, 0, s_x, s_y
  End With
End Sub

Public Sub InitNPCGrid()

    With mfgNPC
    .Rows = 18
    
    Dim y As Integer
    Dim OnOff As Boolean

    For y = 0 To .Rows - 1
        .Col = 0
        .Row = y
        .CellFontBold = True
    
        ''OnOff = Not OnOff
        'If OnOff Then .CellBackColor = &HEFBD81 Else .CellBackColor = &HFEF0DE
        
        .Col = 1
        .CellAlignment = 2
    Next y

    .TextMatrix(0, 0) = "Name"
    .TextMatrix(1, 0) = "Render Mode"
    .TextMatrix(2, 0) = "Alpha"
    .TextMatrix(3, 0) = "Color"
    .TextMatrix(4, 0) = "Depth"
    .TextMatrix(5, 0) = "JumpDuration"
    .TextMatrix(6, 0) = "JumpSpeed"
    .TextMatrix(7, 0) = "Script"
    .TextMatrix(8, 0) = "SourceX"
    .TextMatrix(9, 0) = "SourceY"
    .TextMatrix(10, 0) = "SourceWidth"
    .TextMatrix(11, 0) = "SourceHeight"
    .TextMatrix(12, 0) = "Sprite"
    .TextMatrix(13, 0) = "Collision Mask"
    .TextMatrix(14, 0) = "StairHeight"
    .TextMatrix(15, 0) = "X"
    .TextMatrix(16, 0) = "Y"
    .TextMatrix(17, 0) = "ZPlus"
End With

End Sub

Private Sub rmsZoom_Change()
    ZoomFactor = (rmsZoom.Value / 100)
    GFX.Resize picRender.ScaleWidth, picRender.ScaleHeight
    Map.RedrawAll
    UpdateCtls
    UpdateScroll
End Sub

Private Sub trvMap_Click()

    If Map Is Nothing Then Exit Sub
    If Map.CFm_MapFile = "" Then Beep: Exit Sub

    Dim sel()   As String
    Dim selnode As Node

    If trvMap.SelectedItem Is Nothing Then
        Exit Sub
    End If

    Set selnode = trvMap.SelectedItem
    sel = Split(selnode.Key, ":")
    UseTools = False
    
    If selnode.Text = "NPCs" Then
        ShowView 3
    End If
    
    Select Case sel(0)

        Case "npclist"
            Mode = 3
            mnuPopMapContextAddLayer.Enabled = False
            mnuPopMapContextAddLevel.Enabled = True
            mnuPopMapContextAddScriptLevel.Enabled = True
            mnuPopMapContextAddNPC.Enabled = True
            mnuPopMapContextVisible.Checked = True
            mnuPopMapContextVisible.Enabled = False
            mnuPopMapContextRename.Enabled = False
            mnuPopMapContextDel.Enabled = False

        Case "npc"
            Mode = 3
            ShowView 4
            SelNPC = sel(1)
        
            mnuPopMapContextAddLayer.Enabled = False
            mnuPopMapContextAddLevel.Enabled = True
            mnuPopMapContextAddScriptLevel.Enabled = True
            mnuPopMapContextAddNPC.Enabled = True
            mnuPopMapContextVisible.Checked = True
            mnuPopMapContextVisible.Enabled = False
            mnuPopMapContextRename.Enabled = True
            mnuPopMapContextDel.Enabled = True

        Case "scrlvl"
            CurrentLevel = sel(1)
            CurrentLayer = 0
            Mode = 0
            ShowView 2
            SelNPC = 0
            mnuPopMapContextAddLayer.Enabled = False
            mnuPopMapContextAddLevel.Enabled = True
            mnuPopMapContextAddScriptLevel.Enabled = True
            mnuPopMapContextAddNPC.Enabled = False
            mnuPopMapContextVisible.Checked = True
            mnuPopMapContextVisible.Enabled = False
            mnuPopMapContextRename.Enabled = True
            mnuPopMapContextDel.Enabled = True

        Case "lvl"
            CurrentLevel = sel(1)
            CurrentLayer = 0
            Mode = 2
            ShowView 1
            UseTools = True
            mnuPopMapContextAddLayer.Enabled = True
            mnuPopMapContextAddLevel.Enabled = True
            mnuPopMapContextAddScriptLevel.Enabled = True
            mnuPopMapContextAddNPC.Enabled = False
            mnuPopMapContextVisible.Checked = Map.Levels(CurrentLevel).Visible
            mnuPopMapContextVisible.Enabled = True
            mnuPopMapContextRename.Enabled = True
            mnuPopMapContextDel.Enabled = True

        Case "lay"
            CurrentLevel = sel(1)
            CurrentLayer = sel(2)
            Mode = 1
            ShowView 0
            UseTools = True
            mnuPopMapContextAddLayer.Enabled = True
            mnuPopMapContextAddLevel.Enabled = True
            mnuPopMapContextAddScriptLevel.Enabled = True
            mnuPopMapContextAddNPC.Enabled = False
            mnuPopMapContextVisible.Checked = Map.Levels(CurrentLevel).Layers(CurrentLayer).Visible
            mnuPopMapContextVisible.Enabled = True
            mnuPopMapContextRename.Enabled = True
            mnuPopMapContextDel.Enabled = True
    End Select

    tlbTools.Visible = UseTools
    picLeftBarMap_Resize
    UpdateCtls
    UpdateTileSetCombo

End Sub

Private Sub trvMap_KeyUp(KeyCode As Integer, _
                         Shift As Integer)

    If KeyCode = vbKeyDelete Then
        KeyCode = 0
        mnuPopMapContextDel_Click

    End If

End Sub

Private Sub trvMap_MouseUp(Button As Integer, _
                           Shift As Integer, _
                           x As Single, _
                           y As Single)

    If Map Is Nothing Then Exit Sub
    If Map.CFm_MapFile = "" Then Beep: Exit Sub

    If Button = 2 Then
        trvMap_Click
        mnuPopMapContextAdd.Visible = True
        PopupMenu mnuPopMapContext
    End If

End Sub

Private Sub picSizer2_MouseDown(Button As Integer, _
                                Shift As Integer, _
                                x As Single, _
                                y As Single)

    a_x = x
    a_y = y

End Sub

Private Sub picSizer2_MouseUp(Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)

    'On Error Resume Next

    
    picLevelList.Top = picLevelList.Top - (a_y - y)
    picLeftBarMap_Resize
    ListStuff
    On Error GoTo 0

End Sub

Private Sub picSizer_MouseDown(Button As Integer, _
                               Shift As Integer, _
                               x As Single, _
                               y As Single)

    a_x = x
    a_y = y

End Sub

Private Sub picLB_MouseDown(Index As Integer, _
                            Button As Integer, _
                            Shift As Integer, _
                            x As Single, _
                            y As Single)

    If Index = 0 Then
        picTileSet.pic_MouseDown Button, Shift, x, y
    End If
    If Index = 1 Then
        picColTS.pic_MouseDown Button, Shift, x, y
    End If


End Sub

Private Sub picLB_MouseMove(Index As Integer, _
                            Button As Integer, _
                            Shift As Integer, _
                            x As Single, _
                            y As Single)

    If Index = 0 Then
        picTileSet.pic_MouseMove Button, Shift, x, y
    End If
    If Index = 1 Then
        picColTS.pic_MouseMove Button, Shift, x, y
    End If

End Sub

Private Sub picLB_MouseUp(Index As Integer, _
                          Button As Integer, _
                          Shift As Integer, _
                          x As Single, _
                          y As Single)

    If Index = 0 Then
        picTileSet.pic_MouseUp Button, Shift, x, y
    End If
    If Index = 1 Then
        picColTS.pic_MouseUp Button, Shift, x, y
    End If

End Sub

Private Sub picTileSet_Change()

    With picTileSet
        SelTileX = .SelTileX
        SelTileY = .SelTileY
        SelTileWidth = .SelTileWidth
        SelTileHeight = .SelTileHeight
    End With 'PICTILESET

End Sub

Private Sub picTileSet_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

x = x * Screen.TwipsPerPixelX
y = y * Screen.TwipsPerPixelY

If Button = 4 Then
Me.PopupMenu mnuPopRM2K, , x, y
End If

If Button = 2 And Shift Then
Me.PopupMenu mnuPopRM2K, , x, y
End If

End Sub

Private Sub picColTS_Click()

    With picColTS
        SelColTileX = .SelTileX
        SelColTileY = .SelTileY
        SelColTileWidth = .SelTileWidth
        SelColTileHeight = .SelTileHeight
    End With 'PICCOLTS

End Sub

Private Sub RefreshStandby()
    Set picRender.Picture = picStandby.Picture
    Set picLB(0).Picture = picStandby.Picture
    Set picLB(1).Picture = picStandby.Picture

    picRender.Cls
    picRender.Refresh
    picLB(0).Cls
    picLB(0).Refresh
    picLB(1).Cls
    picLB(1).Refresh
End Sub

Private Sub txtMFG_Change()
    GridUpdated txtMFG.Text
End Sub

Private Sub vsbMap_Change()
    ScrollY = 0 - vsbMap.Value
    Map.RedrawAll
End Sub

Public Sub ChangeNPCImage()

    If Not PromptForSprite("Set NPC Sprite", "images", False) Then Exit Sub
    
    Map.m_NPCs(SelNPC).MaskXStart = 0
    Map.m_NPCs(SelNPC).MaskXEnd = 0
    Map.m_NPCs(SelNPC).MaskYStart = 0
    Map.m_NPCs(SelNPC).MaskYEnd = 0
    
    If Not Right(RetSprites(1), 4) = "aniq" Then
        Map.m_NPCs(SelNPC).Initialize RetSprites(1), RetColorKey, RetSX, RetSY, RetSW, RetSH
    Else
        Map.m_NPCs(SelNPC).Initialize RetSprites(1), RetColorKey
    End If

    ListStuff

End Sub


Private Sub mnuPopMapContextAddLayer_Click()

  Dim lyr      As New cGAMLayer
  Dim selLevel As cGAMLevel

    If CurrentLevel = 0 Then
        Exit Sub
    End If
    If Not (TypeOf Map.Levels(CurrentLevel) Is cGAMLevel) Then
        Exit Sub
    End If
    Set selLevel = Map.Levels(CurrentLevel)
    Set lyr.MyLevel = selLevel
    lyr.Desc = "Layer " & (selLevel.Layers.Count + 1)
    lyr.Initialize
    lyr.TileSetID = CurrentTileset
    selLevel.Layers.Add lyr
    ListStuff


CurrentLayer = Map.Levels(CurrentLevel).Layers.Count

'<suboptimaler code> <<<<< bessermachen :D

        Mode = 1
        ShowView 0
        UseTools = True
        mnuPopMapContextAddLayer.Enabled = True
        mnuPopMapContextAddLevel.Enabled = True
        mnuPopMapContextAddScriptLevel.Enabled = True
        mnuPopMapContextAddNPC.Enabled = False
        mnuPopMapContextVisible.Checked = Map.Levels(CurrentLevel).Layers(CurrentLayer).Visible
        mnuPopMapContextVisible.Enabled = True

'</suboptimaler code>


End Sub

Private Sub mnuPopMapContextAddLevel_Click()

  Dim lvl As New cGAMLevel

    With lvl
        Set .MyMap = Map
        .Initialize
        .Desc = "Level " & (Map.Levels.Count + 1)
    End With 'lvl
    Map.Levels.Add lvl
    ListStuff
    
    CurrentLevel = Map.Levels.Count

'<todo: optimieren>

        Mode = 2
        ShowView 1
        UseTools = True
        mnuPopMapContextAddLayer.Enabled = True
        mnuPopMapContextAddLevel.Enabled = True
        mnuPopMapContextAddScriptLevel.Enabled = True
        mnuPopMapContextAddNPC.Enabled = False
        mnuPopMapContextVisible.Checked = Map.Levels(CurrentLevel).Visible
        mnuPopMapContextVisible.Enabled = True

'</todo>

End Sub




Private Sub mnuPopMapContextAddNPC_Click()
Dim newNPC As New cS2DNPC
Set newNPC.MyMap = Map
Set newNPC.S2D = GFX
Set newNPC.MyS2D = GFX


If ScrollX < 0 And ScrollY < 0 Then 'die if abfrage wird f�r in der mitte der ansicht zentrierte maps ben�tigt
newNPC.x = 0 - ScrollX
newNPC.y = 0 - ScrollY

End If

newNPC.Initialize "blanknpc.png", rgb(255, 0, 255)
newNPC.Name = "NPC " & (Map.m_NPCs.Count + 1)
newNPC.CurrentLevel = 1
newNPC.Script.LoadText File2Str(App.Path & "\scripttemplate.rscr")


Map.m_NPCs.Add newNPC

SelNPC = Map.m_NPCs.Count



'das ist zwar nicht gerade ideal, aber was besseres f�llt mir momentan auch net ein :D
        Mode = 3
        ShowView 4

        
        mnuPopMapContextAddLayer.Enabled = False
        mnuPopMapContextAddLevel.Enabled = True
        mnuPopMapContextAddScriptLevel.Enabled = True
        mnuPopMapContextAddNPC.Enabled = True
        mnuPopMapContextVisible.Checked = True
        mnuPopMapContextVisible.Enabled = False
'</suboptimaler code>

ListStuff
TrvSelectPath "NPCs\" & newNPC.Name
End Sub

Private Sub mnuPopMapContextAddScriptLevel_Click()

  Dim lvl As New cGAMScriptLevel

    With lvl
        Set .MyMap = Map
        .Initialize
        .Desc = "ScriptLevel " & (Map.Levels.Count + 1)
    End With 'lvl
    Map.Levels.Add lvl
    ListStuff

End Sub

Private Sub mnuPopMapContextDel_Click()

    DelLayOrLev

End Sub

Private Sub mnuPopMapContextRename_Click()

    RenameLayOrLev

End Sub

Private Sub mnuPopMapContextVisible_Click()
    If CurrentLevel = 0 Then
        Exit Sub
    End If
    If CurrentLayer = 0 Then
        Map.Levels(CurrentLevel).Visible = Not Map.Levels(CurrentLevel).Visible
     Else 'NOT CURRENTLAYER...
        Map.Levels(CurrentLevel).Layers(CurrentLayer).Visible = Not Map.Levels(CurrentLevel).Layers(CurrentLayer).Visible
    End If
    
    
    ListStuff
End Sub

Private Sub DelLayOrLev()
    Dim x As String
    If trvMap.SelectedItem Is Nothing Then
        Exit Sub
    End If
    
    If Left(trvMap.SelectedItem.Key, 4) = "npc:" Then
        x = MsgBox("Are you sure you want to delete" & vbCrLf & "'" & trvMap.SelectedItem.Text & "'?", vbExclamation + vbOKCancel, "Delete?")
    If x = vbCancel Then Exit Sub
        Map.m_NPCs.Remove SelNPC
        SelNPC = 0
        ShowView 3
        ListStuff
    ElseIf Left(trvMap.SelectedItem.Key, 7) = "npclist" Then
        'MsgBox "You can't delete this :)", vbInformation, "Cannot delete"
       x = MsgBox("Are you sure you want to delete" & vbCrLf & "'" & Map.m_NPCs(SelNPC).Name & "'?", vbExclamation + vbOKCancel, "Delete?")
    If x = vbCancel Then Exit Sub
        Map.m_NPCs.Remove SelNPC
        SelNPC = 0
        ShowView 3
        ListStuff
    Else
    x = MsgBox("Are you sure you want to delete" & vbCrLf & "'" & trvMap.SelectedItem.Text & "'?", vbExclamation + vbOKCancel, "Delete?")
    If x = vbCancel Then
        Exit Sub
    End If
    If CurrentLevel = 0 Then
        Exit Sub
    End If
    If CurrentLayer = 0 Then
        Map.Levels.Remove CurrentLevel
        
        CurrentLevel = CurrentLevel - 1
        If Map.Levels(CurrentLevel) Is Nothing Then
        CurrentLevel = CurrentLevel + 1
        If Map.Levels(CurrentLevel) Is Nothing Then CurrentLevel = 0
        End If
        
        If CurrentLevel = 0 Then
            ShowView 3
        Else
            TrvSelectPath Map.Levels(CurrentLevel).Desc
            Mode = 2
            UseTools = True
            Me.tlbTools.Visible = UseTools
            ShowView 1
            picLeftBarMap_Resize
        End If
        ListStuff
        
     Else 'NOT CURRENTLAYER...
        Map.Levels(CurrentLevel).Layers.Remove CurrentLayer
        
        CurrentLayer = CurrentLayer - 1
        
        If Map.Levels(CurrentLevel).Layers(CurrentLayer) Is Nothing Then
        CurrentLayer = CurrentLayer + 1
        If Map.Levels(CurrentLevel).Layers(CurrentLayer) Is Nothing Then CurrentLayer = 0
        End If
        
        
        ListStuff
          
        If CurrentLayer > 0 Then
          TrvSelectPath Map.Levels(CurrentLevel).Desc & "\" & Map.Levels(CurrentLevel).Layers(CurrentLayer).Desc
        End If
        
        
    End If
    End If
    
  
    
End Sub

Private Sub RenameLayOrLev()

  Dim ln As String

    If trvMap.SelectedItem Is Nothing Then
        Exit Sub
    End If
    
    If Left(trvMap.SelectedItem.Key, 4) = "npc:" Then
        ln = Map.m_NPCs(SelNPC).Name
        ln = InputBox("Please enter new NPC name:", "Rename NPC", ln)
        If LenB(ln) = 0 Then
            Exit Sub
        End If
        Map.m_NPCs(SelNPC).Name = ln
        
        
    Else
    If CurrentLevel = 0 Then
        Exit Sub
    End If
    If CurrentLayer = 0 Then
        ln = Map.Levels(CurrentLevel).Desc
        ln = InputBox("Please enter new level name:", "Rename Level", ln)
        If LenB(ln) = 0 Then
            Exit Sub
        End If
        Map.Levels(CurrentLevel).Desc = ln
     Else 'NOT CURRENTLAYER...
        ln = Map.Levels(CurrentLevel).Layers(CurrentLayer).Desc
        ln = InputBox("Please enter new layer name:", "Rename Layer", ln)
        If LenB(ln) = 0 Then
            Exit Sub
        End If
        Map.Levels(CurrentLevel).Layers(CurrentLayer).Desc = ln
    End If
    End If
    
    ListStuff

End Sub

Private Sub mnuPopTileContextAdd_Click()

  If PromptForSprite("Add a new tileset", "tilesets", True) Then
    
        Dim i As Integer
        For i = 1 To RetSprites.Count
        
            Dim ts As New cGAMTileset
            Set ts.MyMap = Map
            ts.Initialize "tilesets\" & RetSprites(i)

            Map.TileSets.Add ts
            
        
        Next i
        
        ListStuff
    End If
End Sub

Private Sub mnuPopTileContextDel_Click()
    If Map.TileSets.Count <= 1 Then
        MsgBox "You cannot remove all tilesets in a map file!" & vbCrLf & "If you would like to change the Tileset used, please add the new tileset first and remove the old one afterwards.", vbCritical, "Not enough tilesets"
        Exit Sub
    End If
    
    Dim i As Integer, j As Integer
    
    For i = 1 To Map.Levels.Count
        For j = 1 To Map.Levels(i).Layers.Count
            If (Map.Levels(i).Layers(j).TileSetID = CurrentTileset) Then
                Map.Levels(i).Layers(j).TileSetID = 1
                Map.Levels(i).Layers(j).Redraw = True
            End If
        Next j
    Next i
    

    Map.TileSets.Remove CurrentTileset
    CurrentTileset = 1
    ListStuff
    
    cboTileset_Click
    
End Sub

Private Sub tlbLevelTools_ButtonClick(ByVal Button As MSComctlLib.Button)

    Select Case Button.Key
     Case "Up"
        MoveUp
     Case "Down"
        MoveDown
     Case "Add New"
        PopupMenu mnuPopMapContextAdd
     Case "Delete"
        DelLayOrLev
         Case "Zoom in"
                rmsZoom.Value = min(rmsZoom.max, rmsZoom.Value + 10)
         Case "Zoom out"
                rmsZoom.Value = max(rmsZoom.min, rmsZoom.Value - 10)
     Case Else
        MsgBox "TODO: Add Handler for button '" & Button.Key & "' :D"
    End Select

End Sub

Private Sub MoveDown()

  Dim lay As cGAMLayer
  Dim lvl As Object

    Set lvl = Map.Levels(CurrentLevel)
    If CurrentLevel = 0 Then
        Exit Sub
    End If
    If CurrentLayer = 0 Then
        If CurrentLevel = 1 Then
            Exit Sub
        End If
        Map.Levels.Remove CurrentLevel
        Map.Levels.Add lvl, , CurrentLevel - 1
        CurrentLevel = CurrentLevel - 1
     Else 'NOT CURRENTLAYER...
        If CurrentLayer = 1 Then
            Exit Sub
        End If
        With lvl
            Set lay = .Layers(CurrentLayer)
            .Layers.Remove CurrentLayer
            .Layers.Add lay, , CurrentLayer - 1
        End With 'lvl
        CurrentLayer = CurrentLayer - 1
    End If
    ListStuff

End Sub

Private Sub MoveUp()

  Dim lay As cGAMLayer
  Dim lvl As Object

    Set lvl = Map.Levels(CurrentLevel)
    If CurrentLevel = 0 Then
        Exit Sub
    End If
    If CurrentLayer = 0 Then
        If CurrentLevel = Map.Levels.Count Then
            Exit Sub
        End If
        Map.Levels.Remove CurrentLevel
        Map.Levels.Add lvl, , , CurrentLevel
        CurrentLevel = CurrentLevel + 1
     Else 'NOT CURRENTLAYER...
        If CurrentLayer = lvl.Layers.Count Then
            Exit Sub
        End If
        With lvl
            Set lay = .Layers(CurrentLayer)
            .Layers.Remove CurrentLayer
            .Layers.Add lay, , , CurrentLayer
        End With 'lvl
        CurrentLayer = CurrentLayer + 1
    End If
    ListStuff

End Sub

Private Sub ZValue_Change()
    Map.Levels(CurrentLevel).Z = ZValue.Value
End Sub
