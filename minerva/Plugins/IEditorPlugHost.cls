VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IEditorPlugHost"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Property Get VBEclipseCtl() As Object: End Property

Public Property Get Plugins() As Collection: End Property

Public Function Initialize(sPluginPath As String, oVBEclipseCtl As Object) As Boolean: End Function

Public Function LoadPlugins(sPath As String) As Boolean: End Function

Public Function AddPluginByPath(sPath As String, ClassName As String) As Boolean: End Function

Public Function AddPluginByInstance(Instance As Object) As Boolean: End Function

Public Function PluginForFileType(FileType As String) As IEditorPlugin: End Function

Public Sub Log(Message As String): End Sub

Public Function ProvideFile(FileName As String, ByRef usedTempFile As Boolean) As String: End Function

Public Function AttemptEdit(ResourceURL As String) As Boolean: End Function

