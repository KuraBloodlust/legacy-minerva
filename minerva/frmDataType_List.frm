VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDataType_List 
   Caption         =   "Form1"
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3165
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows-Standard
   Begin MSComctlLib.ImageList imlLst 
      Left            =   1440
      Top             =   2280
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlTlb 
      Left            =   2640
      Top             =   2160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16777215
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDataType_List.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDataType_List.frx":059A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDataType_List.frx":0AEC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picList 
      AutoSize        =   -1  'True
      Height          =   300
      Left            =   480
      Picture         =   "frmDataType_List.frx":0BFE
      ScaleHeight     =   240
      ScaleWidth      =   240
      TabIndex        =   2
      Top             =   2160
      Width           =   300
   End
   Begin VB.PictureBox picDataEditor 
      Appearance      =   0  '2D
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000008&
      Height          =   1815
      Left            =   120
      ScaleHeight     =   1815
      ScaleWidth      =   4455
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      Begin MSComctlLib.ListView lstData 
         Height          =   975
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   1720
         View            =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         Icons           =   "imlLst"
         SmallIcons      =   "imlLst"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.Toolbar tlbEditList 
         Height          =   330
         Left            =   120
         TabIndex        =   3
         Top             =   1365
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   582
         ButtonWidth     =   609
         ButtonHeight    =   582
         Appearance      =   1
         Style           =   1
         ImageList       =   "imlTlb"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "New"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Delete"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Edit"
               ImageIndex      =   3
            EndProperty
         EndProperty
      End
      Begin VB.Label lblCaption 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Feldname:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   900
      End
   End
End
Attribute VB_Name = "frmDataType_List"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public MyNode As cGAMRegistryNode
Public MyRenderer As Object

Public Function GetInstance() As frmDataType_List
    Set GetInstance = New frmDataType_List
End Function

Public Function MainContainer() As PictureBox
    Set MainContainer = picDataEditor
End Function

Private Sub lstData_AfterLabelEdit(Cancel As Integer, NewString As String)
Dim n As cGAMRegistryNode

Set n = MyNode.SubElements(lstData.SelectedItem.Key)
n.FriendlyName = NewString

CauseTreeUpdate
End Sub



Private Sub picDataEditor_Resize()
lstData.Width = picDataEditor.ScaleWidth - 2 * lstData.Left
tlbEditList.Width = lstData.Width
tlbEditList.Left = lstData.Left

End Sub

Public Sub Initialize(iSubCatLevel As Integer)

lblCaption.Caption = MyNode.FriendlyName & " [" & MyNode.NodeName & "]" & " - List of type '" & MyNode.Parameters("listedtype") & "'"


lstData.ListItems.Clear


Dim icn As IPictureDisp, tn As New cGAMRegistryNode
tn.NodeType = MyNode.Parameters("listedtype")
Set icn = frmEditorRegistry.imlSettings.ListImages(MyRenderer.MySEditor.IconForType(frmEditorRegistry.imlSettings, tn)).Picture

imlLst.ListImages.Add , , icn

Dim i As Integer
Dim si As cGAMRegistryNode
Dim li As ListItem
For i = 1 To MyNode.SubElements.Count
    Set si = MyNode.SubElements(i)
    Set li = lstData.ListItems.Add(, si.NodeName, si.FriendlyName, , 1)
Next i
    
End Sub

Private Sub tlbEditList_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
    Case "New"
        NewItem

    Case "Delete"
        DeleteItem
        
    Case "Edit"
        lstData.StartLabelEdit
End Select
End Sub

Private Sub NewItem()
        Dim ni As New cGAMRegistryNode
        Dim nn As Integer
        
        Set ni.ParentNode = MyNode
        ni.NodeType = MyNode.Parameters("listedtype")
        
        nn = FindFreeNodeNum()
        
        ni.NodeName = "item" & nn
        ni.FriendlyName = ni.NodeName
        
        
        If Left(LCase(ni.NodeType), 7) = "custom:" Then Project.Package.BuildRegistryStructure_CustomTypeNode ni, Project.RegistryTypes

        
        MyNode.SubElements.Add ni, ni.NodeName
        
        CauseTreeUpdate
        
End Sub

Private Sub DeleteItem()
If lstData.SelectedItem Is Nothing Then Exit Sub

Dim n As cGAMRegistryNode

Set n = MyNode.SubElements(lstData.SelectedItem.Key)

Dim x As VbMsgBoxResult
x = MsgBox("Really delete: '" & n.FriendlyName & "'?", vbYesNo + vbQuestion, "Delete List item")

If x = vbYes Then
    MyNode.SubElements.Remove lstData.SelectedItem.Key
    
    CauseTreeUpdate

End If

End Sub

Public Sub CauseTreeUpdate()
MyRenderer.MySEditor.RegistryToTree frmEditorRegistry.trvSettings, frmEditorRegistry.imlSettings
MyRenderer.MySEditor.SelectNode MyRenderer.MySEditor.CurrentSelection
End Sub

Public Function FindFreeNodeNum() As Integer
Dim i As Integer
Dim j As Integer
Dim NumIsOk As Boolean
Do

i = i + 1
NumIsOk = True

For j = 1 To MyNode.SubElements.Count
    If MyNode.SubElements(j).NodeName = "item" & i Then NumIsOk = False: Exit For
Next j

Loop Until NumIsOk

FindFreeNodeNum = i

End Function
