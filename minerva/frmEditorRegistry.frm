VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEditorRegistry 
   Caption         =   "Registry"
   ClientHeight    =   3435
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   5340
   Icon            =   "frmEditorRegistry.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3435
   ScaleWidth      =   5340
   StartUpPosition =   3  'Windows Default
   Begin VB.VScrollBar vsbSEditors 
      Height          =   3375
      Left            =   5040
      TabIndex        =   5
      Top             =   360
      Width           =   255
   End
   Begin VB.PictureBox picSEditorToolbar 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   330
      Left            =   0
      ScaleHeight     =   330
      ScaleWidth      =   5340
      TabIndex        =   2
      Top             =   0
      Width           =   5340
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   375
         Left            =   0
         TabIndex        =   6
         Top             =   0
         Width           =   615
      End
      Begin VB.TextBox txtRegNodePath 
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000012&
         Height          =   285
         Left            =   2055
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   45
         Width           =   1935
      End
      Begin VB.Label lblNodePathLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Current Path:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   720
         TabIndex        =   4
         Top             =   60
         Width           =   1290
      End
   End
   Begin MSComctlLib.TreeView trvSettings 
      Height          =   2775
      Left            =   0
      TabIndex        =   1
      Top             =   360
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   4895
      _Version        =   393217
      Indentation     =   0
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "imlSettings"
      Appearance      =   1
   End
   Begin VB.PictureBox picSettingsEditor 
      Height          =   855
      Left            =   2760
      ScaleHeight     =   53
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   149
      TabIndex        =   0
      Top             =   360
      Width           =   2295
   End
   Begin MSComctlLib.ImageList imlSettings 
      Left            =   3240
      Top             =   1920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEditorRegistry.frx":014A
            Key             =   "unknown"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmEditorRegistry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ymax As Long

Public Sub Update()
    SEditor.RegistryToTree trvSettings, imlSettings
    SEditor.SelectNode Project.Registry, 0, ymax
    
    picSettingsEditor_Resize
End Sub

Private Sub cmdSave_Click()
    Project.SaveRegistry
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    
    trvSettings.Move 0, picSEditorToolbar.Height, trvSettings.Width, Me.ScaleHeight - picSEditorToolbar.Height
    picSettingsEditor.Move trvSettings.Width, trvSettings.Top, Me.ScaleWidth - (trvSettings.Width + vsbSEditors.Width), trvSettings.Height
    vsbSEditors.Move picSettingsEditor.Left + picSettingsEditor.Width, picSettingsEditor.Top, vsbSEditors.Width, picSettingsEditor.Height
End Sub

Private Sub picSettingsEditor_Resize()
    Dim scrollmax As Long
    scrollmax = ymax - picSettingsEditor.ScaleHeight

    If scrollmax > 0 Then
        vsbSEditors.max = scrollmax
        vsbSEditors.Enabled = True
    Else
        vsbSEditors.Value = 0
        vsbSEditors.Enabled = False
    End If

    SEditor.UpdateSEditors vsbSEditors.Value, picSettingsEditor
End Sub

Private Sub trvSettings_Click()
    SEditor.CleanupSEditor

    If trvSettings.SelectedItem Is Nothing Then Exit Sub
    
    Dim s As cGAMRegistryNode
    Dim p As String
    Dim n As Node
    Set n = trvSettings.SelectedItem
    
    Do Until n.Parent Is Nothing
        p = "\" & n.Tag & p
        Set n = n.Parent
    Loop
    
    p = Mid(p, 2)
    
    txtRegNodePath.Text = p
    
    Set s = Project.Registry.RecursiveFind(p)
    
    ymax = 0
    SEditor.SelectNode s, 0, ymax
    'SEditor.DisplaySEditor s, picSettingsEditor, 0, ymax
    picSettingsEditor_Resize

End Sub
