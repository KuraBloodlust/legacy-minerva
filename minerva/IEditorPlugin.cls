VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IEditorPlugin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Public Property Get Instances() As Collection: End Property

Public Property Get MyPlugHost() As IEditorPlugHost: End Property

Public Property Get PluginName() As String: End Property

Public Property Get FileTypes() As Collection: End Property

Public Function Initialize(PlugHost As IEditorPlugHost) As Boolean: End Function

Public Function OpenFile(FileName As String) As Form: End Function
