VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cEditorPlugHost"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Private m_PluginPath As String

Private m_VBEclipseCtl As ucPerspective

Private m_Plugins As CHive


Public Sub Log(Message As String)
    modLogging.Log Message, ltNone
End Sub

Public Function ProvideFile(FileName As String, usedTempFile As Boolean) As String
    ProvideFile = modFileProvider.ProvideFile(FileName, usedTempFile)
End Function

Public Function StoreFile(FileName As String, Content As String) As Boolean
    StoreFile = modFileProvider.StoreFile(FileName, Content)
End Function



Public Property Get VBEclipseCtl() As Object
    Set VBEclipseCtl = m_VBEclipseCtl
End Property

Public Property Get Plugins() As CHive
    Set Plugins = m_Plugins
End Property




Public Function Initialize(sPluginPath As String, oVBEclipseCtl As Object) As Boolean
    modLogging.Log "Initializing PlugHost...", ltAction
    modLogging.Log "Path: " & sPluginPath, ltInformation
    
    m_PluginPath = sPluginPath & "\"
    
    If Not TypeOf oVBEclipseCtl Is ucPerspective Then
        modLogging.Log "Error initializing PlugHost: No VBEclipse Control referenced!", ltError
        Exit Function
    End If
    
    
    Set m_VBEclipseCtl = oVBEclipseCtl
    
    modLogging.Log "Loading Plugins...", ltAction
    LoadPlugins m_PluginPath
    
    Initialize = True
End Function

Public Function LoadPlugins(sPath As String) As Boolean
    Set m_Plugins = New CHive
    
    Dim x As String
    x = Dir(sPath, vbDirectory)

    Do Until x = ""
        If ((GetAttr(sPath & x) And vbDirectory) = vbDirectory) Then
        
            If Not (x = "." Or x = "..") Then
                modLogging.Log "Plugin '" & x & "'...", ltAction
                AddPluginByPath sPath & x & "\MPlug_" & x & ".dll", x
            End If

        End If
        x = Dir()
    Loop
    
    LoadPlugins = True
End Function

Public Function AddPluginByPath(sPath As String, ClassName As String) As Boolean
    Dim s As String
    
    If Not FileExist(sPath) Then
        modLogging.Log "Could not find Plugin '" & sPath & "'!", ltError
        Exit Function
    End If
    
    Dim o As Object
    
    Set o = TryCreateObject("MPlug_" & ClassName & ".Plugin")
    
    If o Is Nothing Then
        RegisterDLL (sPath)
        Set o = TryCreateObject("MPlug_" & ClassName & ".Plugin")
    End If
    
    
    If o Is Nothing Then
        modLogging.Log "Could not create Plugin Object 'MPlug_" & ClassName & ".Plugin'!", ltError
        Exit Function
    End If
    
    If Not TypeName(o) = "Plugin" Then
        modLogging.Log "Warning: Plugin Type name mismatch.", ltAlert
    End If
    
    AddPluginByInstance o
End Function

Private Function FileExist(sPath As String) As Boolean
On Error GoTo notfound

FileExist = (FileLen(sPath) > 0)

Exit Function
notfound:
FileExist = False
End Function


Private Function TryCreateObject(ClassName As String) As Object
    On Error GoTo errcreate
    
        Set TryCreateObject = CreateObject(ClassName)
    
    Exit Function
    
errcreate:
    Set TryCreateObject = Nothing
End Function


Public Function AddPluginByInstance(Instance As Object) As Boolean
    modLogging.Log "Plugin '" & Instance.IEditorPlugin_PluginName & "' created successfully!", ltSuccess
    
    Instance.IEditorPlugin_Initialize Me
    
    m_Plugins.Add Instance
    
    Dim C As Collection
    Set C = Instance.IEditorPlugin_FileTypes()
    
    Dim s As String
    
    Dim i As Integer
    For i = 1 To C.Count
        s = s & C(i) & " "
    Next i
    
    modLogging.Log "Plugin supports filetypes: " & s, ltInformation
    
    
End Function

Public Function PluginForFileType(FileType As String) As Object
    Dim i As Integer
    
    Dim matches As New Collection
    
    For i = 1 To m_Plugins.Count
        If CanPluginEditFileType(m_Plugins(i), FileType) Then
            matches.Add m_Plugins(i)
        End If
    Next i
    
    If matches.Count = 0 Then
        MsgBox "Unfortunately, there is no editor registered for the file type '" & FileType & "'." & vbCrLf & "Please make sure that the plugin editing this file type is properly installed.", vbInformation, "No Editor avaliable"
        Exit Function
    End If
    
    If matches.Count = 1 Then
        Set PluginForFileType = matches(1)
        Exit Function
    End If
    
    Dim frm As New frmPluginforType
    Set frm.Plugins = matches
    frm.FileType = FileType
    frm.Init
    
    RetOk = 0
    frm.Show vbModal
    
    If RetOk = 1 Then Set PluginForFileType = RetObject
End Function

Private Function CanPluginEditFileType(plugin As Object, FileType As String) As Boolean
    Dim i As Integer
    Dim ft As Collection
    
    Set ft = plugin.IEditorPlugin_FileTypes()
    
    For i = 1 To ft.Count
        If LCase$(ft(i)) = LCase$(FileType) Then
            CanPluginEditFileType = True
            Exit Function
        End If
    Next i
    
    CanPluginEditFileType = False
End Function

Private Function RegisterDLL(isPluginPath As String) As Boolean
On Error GoTo HandleError:

    modLogging.Log "Registering '" & isPluginPath & "'...", ltAction

    Dim lnResult As Double
    lnResult = Shell("regsvr32 """ & isPluginPath & """ /s")

HandleError:

End Function

Public Function AttemptEdit(ResourceURL As String) As Boolean
Dim ft As String
ft = GetFileType(ResourceURL)
Dim o As Object
Set o = PluginHost.PluginForFileType(ft)

If o Is Nothing Then

Else
    Dim F As Object
    Set F = o.IEditorPlugin_OpenFile(ResourceURL)
    
    If (Not (F Is Nothing)) Then
        m_VBEclipseCtl.OpenEditor F
    End If
    
    AttemptEdit = True
End If
End Function

Private Function GetFileType(sPath As String) As String
Dim p As Integer

p = InStrRev(sPath, ".")

If p = 0 Then Exit Function

GetFileType = Mid$(sPath, p + 1)
End Function

Public Function RegisterView(ViewId As String, _
                             Form As Object, _
                             Optional DesiredFolderID As String = "Plugins", _
                             Optional DesiredRel As vbRelationship = vbRelFloating, _
                             Optional DesiredRatio As Double = 0.5, _
                             Optional DesiredRefId As String = "", _
                             Optional DesiredFloatX As Long = 100, _
                             Optional DesiredFloatY As Long = 100) As Boolean
    
    Dim vbe As ucPerspective
    Set vbe = VBEclipseCtl

    Dim p As VbEclipse.Perspective
    Set p = vbe.Perspectives.Item(0)

    Dim F As VbEclipse.Folder
    
    Dim i As Integer

    For i = 0 To p.Folders.Count

        If p.Folders.Item(i).FolderID = DesiredFolderID Then
            Set F = p.Folders.Item(i)
            Exit For
        End If

    Next i
    
    If DesiredRefId = "" Then DesiredRefId = p.ID_EDITOR_AREA
    If F Is Nothing Then Set F = p.AddFolder(DesiredFolderID, DesiredRel, DesiredRatio, DesiredRefId)
    
    If DesiredRel = vbRelFloating Then
        With F.Position
            .Left = DesiredFloatX
            .Top = DesiredFloatY
            .Right = Form.Width / Screen.TwipsPerPixelX
            .Bottom = Form.Height / Screen.TwipsPerPixelY
        End With
    End If
    
    vbe.AddView ViewId, Form
    F.AddView ViewId
End Function

Public Sub NewFile(FileURL As String, plugin As Object)
    On Error GoTo ErrHandler
    
    Dim F As Form
    Set F = plugin.IEditorPlugin_NewFile(FileURL)

    If Not F Is Nothing Then m_VBEclipseCtl.OpenEditor F
    
    Exit Sub
ErrHandler:
    MsgBox "There is no support for creating a file of this extension in the registered Plugin!", vbCritical, "Can't create the file"
    Err.Clear
End Sub

Public Sub TriggerRegisterViews()
Dim i As Integer
For i = 1 To m_Plugins.Count
    m_Plugins(i).IEditorPlugin_AddViews
Next i
End Sub


Public Sub RefreshProjectView()
    frmViewProject.Update
End Sub
