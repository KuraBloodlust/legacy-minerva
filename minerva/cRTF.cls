VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cRTF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Declare Function GetScrollInfo Lib "user32" (ByVal hwnd As Long, ByVal n As Long, lpScrollInfo As SCROLLINFO) As Long

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type
    
Private Type SCROLLINFO
    cbSize As Long
    fMask As Long
    nMin As Long
    nMax As Long
    nPage As Long
    nPos As Long
    nTrackPos As Long
End Type
    
Private Const SIF_RANGE = &H1
Private Const SIF_PAGE = &H2
Private Const SIF_POS = &H4
Private Const SIF_DISABLENOSCROLL = &H8
Private Const SIF_ALL = SIF_RANGE Or SIF_PAGE Or SIF_POS

Private Const SB_VERT = 1
Private Const SB_HOR = 0

Private Const EM_GETRECT = &HB2
Private Const EM_GETSEL = &HB0
Private Const EM_LINEINDEX = &HBB
Private Const EM_LINELENGTH = &HC1
Private Const EM_GETFIRSTVISIBLELINE = &HCE
Private Const EM_LINEFROMCHAR = &HC9
Private Const EM_GETLINECOUNT = &HBA
Private Const EM_SCROLLCARET = &HB7
Private Const EM_UNDO = &HC7
Private Const EM_GETLINE = &HC4

Private Const WM_USER = &H400
Private Const EM_GETSCROLLPOS = WM_USER + 221
Private Const EM_SETSCROLLPOS = WM_USER + 222

Private Const EM_EXGETSEL = WM_USER + 52
Private Const EM_EXSETSEL = WM_USER + 55

Private Const EM_SETSEL = &HB1

Private Const EM_LIMITTEXT = &HC5

Dim Buffer() As Byte

Public Function SetPos(Rich As RichTextBox, Value As Long)

'If Value > 32000 Then
'    SendMessage Rich.hwnd, EM_SETSEL, 0, Value
'Else
    Rich.SelStart = Value
'End If

End Function

Public Function GetScrollPositionX(Rich As RichTextBox) As Long
    
    Dim stcScrollPoint As Point
    SendMessage Rich.hwnd, EM_GETSCROLLPOS, 0, stcScrollPoint
    GetScrollPositionX = stcScrollPoint.x
    
End Function

Public Function GetScrollPositionY(Rich As RichTextBox) As Long
    
    Dim stcScrollPoint As Point
    SendMessage Rich.hwnd, EM_GETSCROLLPOS, 0, ByVal VarPtr(stcScrollPoint)
    GetScrollPositionY = stcScrollPoint.y
   
End Function

Public Function SetScrollPositionX(Rich As RichTextBox, Value As Long)
    
    Dim topoint As Point
    topoint.x = Value
    topoint.y = GetScrollPositionY(Rich)
    
    SendMessage Rich.hwnd, EM_SETSCROLLPOS, 0, topoint
    
End Function

Public Function SetScrollPositionY(Rich As RichTextBox, Value As Long)
    
    Dim topoint As Point
    topoint.y = Value
    topoint.x = GetScrollPositionX(Rich)
    
    SendMessage Rich.hwnd, EM_SETSCROLLPOS, 0, VarPtr(topoint)
    
End Function

Public Function VisibleLinesCount(incontrol As RichTextBox) As Long
    Dim FirstVisibleLine As Long
    Dim r As RECT
    Dim numberOfLines As Long
    Dim numberOfVisibleLines As Long
    Dim rectHeight As Long
    Dim lineHeight As Long


    FirstVisibleLine = SendMessage(incontrol.hwnd, EM_GETFIRSTVISIBLELINE, 0, 0)
    
        numberOfLines = SendMessage(incontrol.hwnd, EM_GETLINECOUNT, 0, 0)
        SendMessage incontrol.hwnd, EM_GETRECT, 0, r
        rectHeight = r.Bottom - r.Top
        lineHeight = incontrol.Parent.TextHeight("W")
        numberOfVisibleLines = rectHeight / lineHeight
        If numberOfVisibleLines > numberOfLines Then
        numberOfVisibleLines = numberOfLines
        End If

    VisibleLinesCount = numberOfVisibleLines - 1
End Function

Public Function FirstVisibleLine(Rich As RichTextBox) As Long
    FirstVisibleLine = SendMessage(Rich.hwnd, EM_GETFIRSTVISIBLELINE, 0, 0)
End Function

Public Function CursorPos(Rich As RichTextBox) As Long
    CursorPos = Rich.SelStart 'SendMessage(Rich.hwnd, EM_GETSEL, 0, ByVal 0&) \ 65536
End Function

Public Function CurrentLine(Rich As RichTextBox, Position As Long) As Long
    CurrentLine = SendMessage(Rich.hwnd, EM_LINEFROMCHAR, Position, ByVal 0&)
End Function

Public Function LinePosition(Rich As RichTextBox, Line As Long) As Long
    LinePosition = SendMessage(Rich.hwnd, EM_LINEINDEX, Line, ByVal 0&)
End Function

Public Function LineLength(Rich As RichTextBox, Line As Long) As Long
    Dim LinePos As Long
    LinePos = LinePosition(Rich, Line)
    LineLength = SendMessage(Rich.hwnd, EM_LINELENGTH, LinePos, ByVal 0&)
End Function

Public Function LineCount(Rich As RichTextBox) As Long
    LineCount = SendMessage(Rich.hwnd, EM_GETLINECOUNT, 0, 0)
End Function

Public Function LineText(Rich As RichTextBox, Line As Long) As String
On Error Resume Next
    RowLength = LineLength(Rich, Line)
    ReDim Buffer(RowLength + 1)
    Buffer(0) = RowLength + 1
    SendMessage Rich.hwnd, EM_GETLINE, Line, Buffer(0)
    LineText = Left$(StrConv(Buffer, vbUnicode), RowLength)

End Function
