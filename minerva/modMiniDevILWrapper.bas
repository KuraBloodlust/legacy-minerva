Attribute VB_Name = "modMiniDevILWrapper"
Private Declare Sub ilInit Lib "devil" ()
Private Declare Sub iluInit Lib "ilu" ()
Private Declare Sub ilutInit Lib "ilut" ()
Private Declare Function ilutRenderer Lib "ilut" (ByVal Renderer As Long) As Byte
Private Declare Sub ilGenImages Lib "devil" (ByVal Num As Long, ByRef Images As Long)
Private Declare Sub ilBindImage Lib "devil" (ByVal Image As Long)
Private Declare Function ilLoadImage Lib "devil" (ByVal FileName As String) As Byte
Private Declare Sub iluGetImageInfo Lib "ilu" (ByRef Info As ILinfo)
Private Declare Function ilutConvertToHBitmap Lib "ilut" (ByVal hdc As Integer) As Integer

Private Declare Function CreateCompatibleDC Lib "gdi32" (ByVal hdc As Long) As Long
Private Declare Function SelectObject Lib "gdi32" (ByVal hdc As Long, ByVal hObject As Long) As Long
Private Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Declare Function StretchBlt Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long

Private Type ILinfo
  ID As Long          ' the image's id
  DataPointer As Long ' pointer to the image's data (useless in VB)
  Width As Long       ' the image's width
  Height As Long      ' the image's height
  Depth As Long       ' the image's depth
  Bpp As Byte         ' bytes per pixel (not bits) of the image
  SizeOfData As Long  ' the total size of the data (in bytes)
  Format As Long      ' image format (in IL enum style)
  IType As Long       ' image type (in IL enum style)
  Origin As Long      ' origin of the image
  PalettePointer As Long ' pointer to the image's palette (useless in VB)
  PalType As Long     ' palette type
  PalSize As Long     ' palette size
  NumNext As Long     ' number of images following
  NumMips As Long     ' number of mipmaps
  NumLayers  As Long  ' number of layers
End Type

Private Const SRCCOPY = &HCC0020
Private Const ILUT_WIN32 = 2

Public Function RenderImageToHDC(Path As String, hdc As Long, x As Integer, y As Integer, SourceX As Integer, SourceY As Integer, Width As Integer, Height As Integer)
    
    ilInit
    iluInit
    ilutInit
    
    ilutRenderer ILUT_WIN32
    
    Dim ID As Long
    ilGenImages 1, ID
    ilBindImage ID
    
    ilLoadImage Path
    
    Dim inf As ILinfo
    iluGetImageInfo inf
    
    If Width = -1 Then
        Width = inf.Width
    End If
    
    If Height = -1 Then
        Height = inf.Height
    End If
    
    hjpgdc = CreateCompatibleDC(0)
    hJpgBmp = ilutConvertToHBitmap(0)
    SelectObject hjpgdc, hJpgBmp
    
    If Not Width = inf.Width Or Not Height = inf.Height Then
        StretchBlt hdc, x, y, Width, Height, hjpgdc, SourceX, SourceY, inf.Width, inf.Height, vbSrcCopy
    Else
        BitBlt hdc, x, y, Width, Height, hjpgdc, SourceX, SourceY, SRCCOPY
    End If

End Function
