VERSION 5.00
Begin VB.Form frmDataType_Category 
   Caption         =   "Form1"
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3165
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picCategory 
      AutoSize        =   -1  'True
      Height          =   300
      Left            =   360
      Picture         =   "frmDataType_Category.frx":0000
      ScaleHeight     =   240
      ScaleWidth      =   240
      TabIndex        =   2
      Top             =   1320
      Width           =   300
   End
   Begin VB.PictureBox picDataEditor 
      Appearance      =   0  'Flat
      BackColor       =   &H80000002&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      ScaleHeight     =   25
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   297
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      Begin VB.Image img 
         Height          =   240
         Left            =   60
         Picture         =   "frmDataType_Category.frx":058A
         Top             =   75
         Width           =   240
      End
      Begin VB.Label lblTitle 
         AutoSize        =   -1  'True
         BackColor       =   &H80000009&
         BackStyle       =   0  'Transparent
         Caption         =   "Category Title"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   345
         TabIndex        =   1
         Top             =   45
         Width           =   1725
      End
      Begin VB.Image imgBG 
         Height          =   375
         Left            =   0
         Picture         =   "frmDataType_Category.frx":0B14
         Stretch         =   -1  'True
         Top             =   0
         Width           =   615
      End
   End
   Begin VB.Image imgOpen 
      Height          =   240
      Left            =   240
      Picture         =   "frmDataType_Category.frx":0E76
      Top             =   720
      Visible         =   0   'False
      Width           =   240
   End
End
Attribute VB_Name = "frmDataType_Category"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public MyNode As cGAMRegistryNode
Public MyRenderer As Object

Public Function GetInstance() As frmDataType_Category
    Set GetInstance = New frmDataType_Category
End Function

Public Function MainContainer() As PictureBox
    Set MainContainer = picDataEditor
End Function





Public Sub Initialize(iSubCatLevel As Integer)

If iSubCatLevel = 0 Then Set img.Picture = imgOpen.Picture

lblTitle.Caption = MyNode.FriendlyName
End Sub


Private Sub img_Click()
picDataEditor_Click
End Sub

Private Sub imgBG_Click()
picDataEditor_Click
End Sub

Private Sub lblTitle_Click()
picDataEditor_Click
End Sub

Private Sub picDataEditor_Click()
MyRenderer.MySEditor.SelectNode MyNode
End Sub
Private Sub picDataEditor_Resize()
imgBG.Width = picDataEditor.ScaleWidth - imgBG.Left
End Sub
