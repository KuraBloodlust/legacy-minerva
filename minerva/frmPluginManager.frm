VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPluginManager 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pluginmanager"
   ClientHeight    =   3975
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   7455
   Icon            =   "frmPluginManager.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3975
   ScaleWidth      =   7455
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Caption         =   "No plugin selected"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   120
      TabIndex        =   2
      Top             =   1920
      Width           =   7215
      Begin VB.ListBox List1 
         Height          =   1140
         IntegralHeight  =   0   'False
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   6975
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   3240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPluginManager.frx":058A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Close"
      Height          =   375
      Left            =   6120
      TabIndex        =   1
      Top             =   3480
      Width           =   1215
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   2990
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Name"
         Object.Width           =   12550
      EndProperty
   End
End
Attribute VB_Name = "frmPluginManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  On Error Resume Next
  
  Dim iCount As Integer
  
  If PluginHost.Plugins.Count > 0 Then
    For iCount = 1 To PluginHost.Plugins.Count
      Dim plug As Object
      Dim strName As String
      Set plug = PluginHost.Plugins(iCount)
      
      If Not plug Is Nothing Then
        strName = plug.IEditorPlugin_PluginName
        ListView1.ListItems.Add , , strName, , 1
      End If
    Next iCount
  End If
  
  If ListView1.ListItems.Count > 0 Then
    ListView1_ItemClick ListView1.ListItems(1)
  End If
End Sub

Private Sub ListView1_ItemClick(ByVal Item As MSComctlLib.ListItem)
  On Error Resume Next
  
  If Item Is Nothing Then Exit Sub
  
  Dim strName As String
  strName = Item.Text
  
  Dim plug As Object
  Set plug = PluginByName(strName)
  List1.Clear
  
  If Not plug Is Nothing Then
    Frame1.Caption = strName & " Filetypes"
  
    Dim i As Integer
    Dim ft As Collection
    
    Set ft = plug.IEditorPlugin_FileTypes()
    
    If ft.Count > 0 Then
      For i = 1 To ft.Count
        List1.AddItem LCase$(ft(i))
      Next i
    End If
  Else
    Frame1.Caption = "No plugin selected"
  End If
End Sub

Private Function PluginByName(ByVal Name As String) As Object
  On Error Resume Next
  
  Dim iCount As Integer
  
  If PluginHost.Plugins.Count > 0 Then
    For iCount = 1 To PluginHost.Plugins.Count
      Dim plug As Object
      Dim strName As String
      Set plug = PluginHost.Plugins(iCount)
      
      If Not plug Is Nothing Then
        strName = plug.IEditorPlugin_PluginName
        If strName = Name Then
          Set PluginByName = plug
          Exit Function
        End If
      End If
    Next iCount
  End If
  
  Set PluginByName = Nothing
  Exit Function
End Function
