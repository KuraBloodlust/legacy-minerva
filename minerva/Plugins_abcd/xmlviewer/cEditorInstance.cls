VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cEditorInstance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public MyPlugin As Plugin
Public MyForm As Object

Public EditingFile As String

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)



Private Sub Class_Initialize()
    Set MyForm = New frmInstance
    Load MyForm
End Sub

Public Function StartInstance(EditPath As String) As Object
    EditingFile = EditPath
    
    MyForm.Caption = EditPath
    DoEvents
    
    Dim F As String
    Dim TF As Boolean
    F = MyPlugin.IEditorPlugin_MyPlugHost.ProvideFile(EditPath, TF)
    
    
    MyForm.wb.Navigate F
    DoEvents
    Sleep 1000
    
    If TF Then Kill F

    Set StartInstance = MyForm
End Function


