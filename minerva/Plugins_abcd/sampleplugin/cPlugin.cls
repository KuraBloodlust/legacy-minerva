VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Plugin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IEditorPlugin

Private m_PlugHost As Object
Private m_Instances As Collection

Public Sub IEditorPlugin_AddViews()

End Sub

Private Property Get IEditorPlugin_Instances() As Collection
    Set IEditorPlugin_Instances = m_Instances
End Property

Public Property Get IEditorPlugin_MyPlugHost() As Object
    Set IEditorPlugin_MyPlugHost = m_PlugHost
End Property

Public Function IEditorPlugin_Initialize(PlugHost As Object) As Boolean
    Set m_PlugHost = PlugHost
    Set m_Instances = New Collection
    
   m_PlugHost.Log "Welcome to the Minerva Plugin Engine Sample Plugin!"
End Function


Public Property Get IEditorPlugin_PluginName() As String
    IEditorPlugin_PluginName = "Minerva Sample Plugin"
End Property

Public Property Get IEditorPlugin_FileTypes() As Collection
    Dim c As New Collection
    
    c.Add "txt"
    c.Add "vbs"
    c.Add "rscr"

    Set IEditorPlugin_FileTypes = c
End Property

Public Function IEditorPlugin_OpenFile(FileName As String) As Object
    Dim i As New cEditorInstance
    Set i.MyPlugin = Me
    i.StartInstance FileName
    
    m_Instances.Add i
    
    Set IEditorPlugin_OpenFile = i.MyForm
End Function

