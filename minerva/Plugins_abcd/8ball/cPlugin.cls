VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Plugin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IEditorPlugin

Private m_PlugHost As Object
Private m_Instances As Collection

Public Sub IEditorPlugin_AddViews()
   Load frmInstance
   
   'Public Function RegisterView(ViewID As String, Form As Object, Optional DesiredFolderID As String = "Plugins", Optional DesiredRel As vbRelationship = vbRelFloating, Optional DesiredRatio As Double = 0.5, Optional DesiredRefId As String = "") As Boolean
   m_PlugHost.RegisterView "Eightball", frmInstance, "Plugin_Eightball", 4, 0.75, "Right"
End Sub

Private Property Get IEditorPlugin_Instances() As Collection
    Set IEditorPlugin_Instances = m_Instances
End Property

Public Property Get IEditorPlugin_MyPlugHost() As Object
    Set IEditorPlugin_MyPlugHost = m_PlugHost
End Property

Public Function IEditorPlugin_Initialize(PlugHost As Object) As Boolean
    Set m_PlugHost = PlugHost
    Set m_Instances = New Collection
    

    
   m_PlugHost.Log "The magic 8-ball is ready to reply to your questions, o mortal!"
   
   
End Function


Public Property Get IEditorPlugin_PluginName() As String
    IEditorPlugin_PluginName = "The magic 8-Ball"
End Property

Public Property Get IEditorPlugin_FileTypes() As Collection
    Dim c As New Collection

    Set IEditorPlugin_FileTypes = c
End Property

Public Function IEditorPlugin_OpenFile(FileName As String) As Object
    Set IEditorPlugin_OpenFile = Nothing
End Function

