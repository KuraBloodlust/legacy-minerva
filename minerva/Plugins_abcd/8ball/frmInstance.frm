VERSION 5.00
Begin VB.Form frmInstance 
   Caption         =   "The Magic 8-Ball"
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   4680
   Icon            =   "frmInstance.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3165
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows-Standard
   Begin VB.CommandButton cmd8Ball 
      Caption         =   "The Magic 8-Ball"
      Height          =   1815
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2775
   End
End
Attribute VB_Name = "frmInstance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim eightball(0 To 19) As String

Private Sub cmd8Ball_Click()
MsgBox "Close your eyes and think of a question. Then click OK.", vbExclamation, "The magic 8-Ball"
MsgBox eightball(Int(Rnd * 19)), vbExclamation, "The magic 8-Ball"
End Sub

Private Sub Form_Load()
Randomize




eightball(0) = "Signs point to yes."
eightball(1) = "Yes."
eightball(2) = "Reply hazy, try again."
eightball(3) = "Without a doubt."
eightball(4) = "My sources say no."
eightball(5) = "As I see it, yes."
eightball(6) = "You may rely on it."
eightball(7) = "Concentrate and ask again."
eightball(8) = "Outlook not so good."
eightball(9) = "It is decidedly so."
eightball(10) = "Better not tell you now."
eightball(11) = "Very doubtful."
eightball(12) = "Yes - definitely."
eightball(13) = "It is certain."
eightball(14) = "Cannot predict now."
eightball(15) = "Most likely."
eightball(16) = "Ask again later."
eightball(17) = "My reply is no."
eightball(18) = "Outlook good."
eightball(19) = "Don 't count on it."



End Sub

Private Sub Form_Resize()
cmd8Ball.Move 0, 0, ScaleWidth, ScaleHeight
End Sub
