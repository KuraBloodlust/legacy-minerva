VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cEditorInstance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public MyPlugin As Plugin
Public MyForm As Object

Public EditingFile As String

Private Sub Class_Initialize()
    Set MyForm = New frmInstance
    Set MyForm.MyInstance = Me
    Load MyForm
End Sub

Public Function StartInstance(EditPath As String) As Object
    EditingFile = EditPath
    
    MyForm.Caption = EditPath
    DoEvents
    
    Dim F As String
    Dim TF As Boolean
    F = MyPlugin.IEditorPlugin_MyPlugHost.ProvideFile(EditPath, TF)
    
    MyForm.txt.Text = File2Str(F)
    
    If TF Then Kill F

    Set StartInstance = MyForm
End Function


Private Function File2Str(FileName As String)
Dim i As Integer
Dim s As String
i = FreeFile()
Open FileName For Binary Access Read As i
s = String$(LOF(i), Chr(0))
Get #i, , s
Close i

File2Str = s
End Function
