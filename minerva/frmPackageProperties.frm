VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPackageProperties 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Package Properties"
   ClientHeight    =   4890
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4710
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4890
   ScaleWidth      =   4710
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   3120
      TabIndex        =   3
      Top             =   4440
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   1560
      TabIndex        =   2
      Top             =   4440
      Width           =   1455
   End
   Begin VB.PictureBox picTabs 
      Height          =   3735
      Index           =   0
      Left            =   240
      ScaleHeight     =   3675
      ScaleWidth      =   4155
      TabIndex        =   1
      Top             =   480
      Width           =   4215
      Begin VB.ComboBox cboAuthorType 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmPackageProperties.frx":0000
         Left            =   3120
         List            =   "frmPackageProperties.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   2520
         Width           =   975
      End
      Begin VB.TextBox txtAuthorName 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   11
         Top             =   2520
         Width           =   1335
      End
      Begin VB.TextBox txtLongDesc 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1605
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   9
         Top             =   840
         Width           =   3015
      End
      Begin VB.TextBox txtShortDesc 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   7
         Top             =   480
         Width           =   3015
      End
      Begin VB.TextBox txtTitle 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   5
         Top             =   120
         Width           =   3015
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Make sure the author information matches the r-pg.net account name you want to upload it from!"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   735
         Left            =   1080
         TabIndex        =   14
         Top             =   2880
         Width           =   3015
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2520
         TabIndex        =   13
         Top             =   2565
         Width           =   495
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Author ID:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   0
         TabIndex        =   10
         Top             =   2580
         Width           =   915
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Long Desc:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   0
         TabIndex        =   8
         Top             =   840
         Width           =   960
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Short Desc:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   0
         TabIndex        =   6
         Top             =   480
         Width           =   1020
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Title:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   0
         TabIndex        =   4
         Top             =   120
         Width           =   435
      End
   End
   Begin VB.PictureBox picTabs 
      Height          =   3735
      Index           =   2
      Left            =   240
      ScaleHeight     =   3675
      ScaleWidth      =   4155
      TabIndex        =   16
      Top             =   480
      Visible         =   0   'False
      Width           =   4215
      Begin MSComctlLib.TreeView trv 
         Height          =   1695
         Left            =   0
         TabIndex        =   21
         Top             =   0
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   2990
         _Version        =   393217
         Indentation     =   0
         Style           =   7
         Appearance      =   1
      End
   End
   Begin VB.PictureBox picTabs 
      Height          =   3735
      Index           =   1
      Left            =   240
      ScaleHeight     =   3675
      ScaleWidth      =   4155
      TabIndex        =   15
      Top             =   480
      Visible         =   0   'False
      Width           =   4215
      Begin VB.CommandButton cmdRemoveDependency 
         Caption         =   "Remove"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3000
         TabIndex        =   20
         Top             =   1680
         Width           =   1095
      End
      Begin VB.CommandButton cmdAddDependency 
         Caption         =   "Add"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   19
         Top             =   1680
         Width           =   1335
      End
      Begin VB.ListBox lstDependencies 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1230
         Left            =   0
         TabIndex        =   17
         Top             =   360
         Width           =   4095
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Set Dependencies:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   0
         TabIndex        =   18
         Top             =   120
         Width           =   1800
      End
   End
   Begin MSComctlLib.TabStrip ts 
      Height          =   4215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   7435
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   3
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "General"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Dependencies"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Properties"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPackageProperties"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mypackage As cGAMPackage

Private Sub cmdAddDependency_Click()
  Dim x As String
  x = InputBox("Enter new dependency filename:", "Add Dependency")

  If mypackage.AddDependency(x) Then
    lstDependencies.AddItem x
  Else
    MsgBox mypackage.ErrDesc, vbCritical, mypackage.ErrSource
  End If
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdOK_Click()
  mypackage.Title = txtTitle.Text
  mypackage.ShortDesc = txtShortDesc.Text
  mypackage.LongDesc = txtLongDesc.Text
  mypackage.AuthorName = txtAuthorName.Text
  mypackage.AuthorType = cboAuthorType.Text

  mypackage.FileSave mypackage.RPAK.FileName
  Unload Me
End Sub

Private Sub cmdRemoveDependency_Click()
  If lstDependencies.ListIndex = -1 Then Exit Sub

  mypackage.Dependencies.Remove lstDependencies.ListIndex + 1
  If Not DependencyNeeded(lstDependencies.Text) Then LoadedDependencies.Remove (FindLDependency(lstDependencies.Text))
  lstDependencies.RemoveItem lstDependencies.ListIndex
End Sub

Private Sub Form_Load()
  If CurrentPackage > 0 Then
    Set mypackage = LoadedDependencies(CurrentPackage)
  Else
    Set mypackage = Project.Package
  End If

  txtTitle.Text = mypackage.Title
  txtShortDesc.Text = mypackage.ShortDesc
  txtLongDesc.Text = mypackage.LongDesc
  txtAuthorName.Text = mypackage.AuthorName
  cboAuthorType.ListIndex = IIf(LCase(mypackage.AuthorType) = "team", 1, 0)

  Dim n As Node
  Set n = trv.Nodes.Add(, , , "Settings")
  ListSettings n, mypackage.snode.selectSingleNode("settingsdefinition")
  Dim i As Integer

  For i = 1 To mypackage.Dependencies.Count
    lstDependencies.AddItem mypackage.Dependencies(i).ShortName
  Next i

  For i = 0 To picTabs.Count - 1
    picTabs(i).BorderStyle = 0
  Next i
End Sub

Public Sub ShowTab(tid As Integer)
  Dim i As Integer
  
  For i = 0 To picTabs.Count - 1
    picTabs(i).Visible = False
  Next i

  picTabs(tid).Visible = True
End Sub

Private Sub picTabs_Resize(Index As Integer)
  trv.Width = picTabs(2).ScaleWidth
  trv.Height = picTabs(2).ScaleHeight
End Sub

Private Sub ts_Click()
  ShowTab ts.SelectedItem.Index - 1
End Sub

Public Function AddNode(Text As String, Parent As Node) As Node
  Set AddNode = trv.Nodes.Add(, , , Text)
  Set AddNode.Parent = Parent
End Function

Public Sub ListSettings(ParentNode As Node, settingsNode As IXMLDOMNode)
  Dim i As Integer

  For i = 0 To settingsNode.childNodes.Length - 1
    Dim n As Node
    Set n = AddNode(settingsNode.childNodes(i).Attributes.getNamedItem("name").Text, ParentNode)
    ListSettings n, settingsNode.childNodes(i)
  Next i
End Sub
