VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEdNewProject 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Create New Project"
   ClientHeight    =   3240
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   5025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3240
   ScaleWidth      =   5025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3960
      Picture         =   "frmEdNewProject.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2640
      Width           =   975
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2640
      Picture         =   "frmEdNewProject.frx":038A
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2640
      Width           =   1335
   End
   Begin MSComctlLib.ImageList imlProjects 
      Left            =   2160
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEdNewProject.frx":0714
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txtProjName 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   120
      TabIndex        =   3
      Top             =   2280
      Width           =   4815
   End
   Begin MSComctlLib.ListView lvwTemplates 
      Height          =   1575
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   2778
      View            =   1
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "imlProjects"
      SmallIcons      =   "imlProjects"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Template File:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1395
   End
   Begin VB.Label lblProjectName 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Project Name:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   2040
      Width           =   1380
   End
End
Attribute VB_Name = "frmEdNewProject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public PackageMode As Boolean


Private Sub cmdCancel_Click()
Unload Me
End Sub

Private Sub cmdOK_Click()
If lvwTemplates.SelectedItem Is Nothing Then
    MsgBox "Please select a " & IIf(PackageMode, "package", "project") & " template.", vbInformation, "Create Project"
    Exit Sub
End If
    
If txtProjName.Text = "" Then
    MsgBox "Please enter a " & IIf(PackageMode, "package", "project") & "  name.", vbExclamation, "Create Project"
    Exit Sub
End If

If Not CheckValidChars(txtProjName.Text, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!_-") Then
    MsgBox "Your " & IIf(PackageMode, "package", "project") & " name contains invalid characters." & vbCrLf & "Please use alphanumeric and -_! only.", vbExclamation, "Create Project"
    Exit Sub
End If

If PackageMode Then
    If FileExists(App.Path & "\Projects\" & CurProj & "\" & txtProjName.Text & ".rpak") Then MsgBox "A Package file with this name already exists." & vbCrLf & "Please choose a different name.", vbCritical, "Create Package": Exit Sub
Else
    If FolderExists(App.Path & "\Projects\" & txtProjName.Text) Then MsgBox "A Project folder with this name already exists." & vbCrLf & "Please choose a different one.", vbExclamation, "Create Project": Exit Sub
End If

Dim DestFN As String
Dim SourceFN As String
SourceFN = App.Path & "\Templates\" & IIf(PackageMode, "packages", "projects") & "\" & lvwTemplates.SelectedItem.Key
DestFN = App.Path & "\Projects\" & IIf(PackageMode, CurProj, txtProjName.Text) & "\" & txtProjName.Text & ".rpak"

Dim p As New cRPakArchive
If Not p.FileOpen(SourceFN) Then
    If p.ErrDesc = "Could not Load RPak file: Invalid password." Then
        If Not p.FileOpen(SourceFN, PasswordBox("Enter Password for '" & lvwTemplates.SelectedItem.Key & "':")) Then
            MsgBox "Error when loading " & IIf(PackageMode, "package", "project") & " file: " & p.ErrDesc, vbCritical
            Exit Sub
        End If
    Else
        MsgBox "Error when loading " & IIf(PackageMode, "package", "project") & " file: " & p.ErrDesc, vbCritical
        Exit Sub
    End If
  End If

  p.Password = ""

  If Not PackageMode Then
    MkDir App.Path & "\Projects\" & txtProjName.Text
  End If

  p.FileSave DestFN
  RetOk = True
  RetString = txtProjName.Text
  Unload Me
End Sub

Private Sub Form_Load()
  Me.Caption = IIf(PackageMode, "Create New Package/Dependency", "Create New Project")
  Me.lblProjectName = IIf(PackageMode, "Package Name:", "Project Name:")

  ListTemplates
  If lvwTemplates.ListItems.Count > 0 Then Set lvwTemplates.SelectedItem = lvwTemplates.ListItems(1)
End Sub

Public Sub ListTemplates()
  Dim x As String

  lvwTemplates.ListItems.Clear
  x = Dir(App.Path & "\Templates\" & IIf(PackageMode, "Packages", "Projects") & "\*.rpak")
  
  Do Until x = ""
    lvwTemplates.ListItems.Add , x, x, 1, 1
    x = Dir()
  Loop
End Sub

Private Sub txtProjName_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyReturn Then cmdOK_Click
End Sub
