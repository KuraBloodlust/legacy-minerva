VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cIntelliSense"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Declare Function SendMessage Lib "user32" Alias _
        "SendMessageA" (ByVal hwnd As Long, ByVal wMsg _
        As Long, ByVal wParam As Long, ByVal lParam As _
        String) As Long
        
Private Declare Function GetCaretPos Lib "user32" _
        (lpPoint As POINTAPI) As Long
        
Private Declare Function DrawText Lib "user32" Alias _
        "DrawTextA" (ByVal hDC As Long, ByVal lpStr As _
        String, ByVal nCount As Long, lpRect As RECT, _
        ByVal wFormat As Long) As Long
        
Private Type POINTAPI
  X As Long
  Y As Long
End Type

Private Type RECT
  Left As Long
  Top As Long
  Right As Long
  Bottom As Long
End Type

Private Const mRect = &H400
Private Const ToFind = &H18F
Private Const SP As Long = 3
Private Const Z As Long = -1


Private Const SeFile As String = "\Data.txt"
Private Class1 As String

Private Position As POINTAPI
Private Zone     As RECT
Private pHDC     As Long
Private Lx       As Long
Private Ly       As Long
Private Word     As String
Private Break    As String
Private IsEmpty  As Boolean
Private Buffer   As String

Private Type cMObt
    Name As String
    Desc As String
End Type

Private Type cObt
 Name As String
 Data() As cMObt
End Type



Private SData() As cObt

Private Sub Class_Initialize()
  Break = vbCr & vbLf & vbTab & Chr$(32) & Chr$(160)
  
End Sub

Public Sub CheckText(ByVal TextBox As Control, _
ByVal ListBox As Control, Optional Typ As Long)
Dim X        As Long
Dim Y        As Long
  Dim i As Long
    

  With TextBox
  'If IsEmpty Then Exit Sub
  Call DrawText(.Parent.hDC, "x", Z, Zone, mRect)
  Call GetCaretPos(Position)
  
  Lx& = .Left + (Position.X + SP)
  
  If Lx + ListBox.Width > .Width Then
  Lx = .Width - ListBox.Width
  End If
    
  Ly& = .Top + (Position.Y + Zone.Bottom + SP)
         
  If Ly& + ListBox.Height > .Height Then
  Ly& = .Top + Position.Y - ListBox.Height
  End If
    
  ListBox.Left = Lx&
  ListBox.Top = Ly&
  
  Word = FindPos(TextBox)
  ListBox.Visible = False

If Typ = 2 Then
    
    ListBox.Clear
    For i = 1 To UBound(SData)
    ListBox.AddItem SData(i).Name
    Next
    
   Class1 = vbNullString
   ListBox.Visible = True

Else

  If Word$ <> vbNullString Then
    Buffer = IsType(Word$)
 
    
    If Buffer > 0 Then
    
    If ListBox.Visible = False Then ListBox.Clear
    For i = 1 To UBound(SData(Buffer).Data)
    ListBox.AddItem SData(Buffer).Data(i).Name
    Next
    
    Class1 = SData(Buffer).Name & "."
    ListBox.Tag = Class1
   
    
    If UBound(SData(Buffer).Data) > 0 Then
    
    ListBox.Visible = True
        
    End If
    End If
  End If
  
End If

  
  Word$ = Mid$(Word$, Len(Class1) + 1)
  
  X = SendMessage(ListBox.hwnd, ToFind, Z, Word)
  
  If X <> Z Then ListBox.TopIndex = X
  ListBox.ListIndex = X
  End With
End Sub

Private Function IsType(ByVal Text As String) As Long
Dim i As Long

IsType = 0
For i = 1 To UBound(SData)
If LCase(SData(i).Name) & "." = LCase(Left$(Word$, Len(SData(i).Name) + 1)) Then
IsType = i
Exit For
End If
Next

End Function

Public Sub ReadList(ByVal ListBox As Control)
Dim FN As Integer
Dim Data As String
Dim Brack(2) As String
Dim i As Long
  Dim s As Long
  Dim B As Long
 If FileExists(App.Path & SeFile) Then
  FN = FreeFile

  Open App.Path & SeFile For Input As #FN
    Do While Not EOF(FN)
     Input #FN, Data
     
      If InStr(Data, ".") > 0 Then
      Brack(1) = Split(Data, ".")(0)
      Brack(2) = Split(Data, ".")(1)
            
      Dim Desc As String
      If InStr(1, Data, "$") <> 0 Then
        Desc = Split(Data, "$")(1)
      End If
      
      If i > 0 Then
      s = IsData(Brack(1))
      End If
      
      If s <> 0 Then
      
      B = UBound(SData(s).Data) + 1
       ReDim Preserve SData(s).Data(B) As cMObt
       SData(s).Data(B).Name = Brack(2)
      Else
      i = i + 1
      ReDim Preserve SData(i) As cObt
      ReDim Preserve SData(i).Data(1) As cMObt
      
       SData(i).Name = Brack(1)
       SData(i).Data(1).Name = Brack(2)
       SData(i).Data(1).Desc = Desc
       
      End If
      
      End If
      
    Loop
  Close FN
  
  End If
   

  
End Sub

Private Function IsData(ByVal Text As String) As Long
Dim i As Long

 IsData = 0
 
 For i = 1 To UBound(SData)
 If SData(i).Name = Text Then
 IsData = i
 Exit For
 End If
 Next
 
End Function

Private Function FindPos(TextBox As Control, _
Optional ByRef X1 As Long, Optional ByRef X2 As Long) As String
Dim X     As Long
Dim Y     As Long
Dim PosS  As String
  
  X = TextBox.SelStart + 1
  X2 = Len(TextBox.Text)
  X1 = 1

  For Y& = X& - 1 To 1 Step -1
    PosS = Mid$(TextBox.Text, Y, 1)
    If InStr(1, Break, PosS) <> 0 Then
      X1 = Y + 1
       Exit For
      End If
  Next
  
  For Y& = X To X2
    PosS = Mid$(TextBox.Text, Y, 1)
    If InStr(1, Break, PosS) <> 0 Then
      X2 = Y - 1
      Exit For
    End If
  Next
  
  If X2& - X1& >= 0 Then
  FindPos = Mid$(TextBox.Text, X1, X2 - X1 + 1)
  End If
End Function

Public Sub CheckCur(ListBox As Control, ByVal KeyCode As Long)
Dim Pos As Long

  Pos = ListBox.ListIndex
  
    If Pos = Z Then
        ListBox.ListIndex = ListBox.TopIndex
    Else
        If KeyCode = vbKeyRight Then
    
            If Pos + 1 < ListBox.ListCount Then
                ListBox.ListIndex = Pos + 1
            End If
    
        ElseIf KeyCode = vbKeyLeft Then
            If Pos - 1 > Z Then ListBox.ListIndex = Pos - 1
        End If
    
        If KeyCode = vbKeyDown Then
    
            If Pos + 1 < ListBox.ListCount Then
                ListBox.ListIndex = Pos + 1
            End If
    
        ElseIf KeyCode = vbKeyUp Then
            If Pos - 1 > Z Then ListBox.ListIndex = Pos - 1
        End If
        
    End If
  
End Sub

Public Sub SelectWord(ByVal ListBox As Control, _
ByVal TextBox As Control, ByVal KeyAscii As Long)

Dim X1  As Long
Dim X2  As Long
Dim X   As Long
Dim Cut As String
Dim Cut1 As String
Dim Cut2 As String
Dim Cut3 As String
Dim ae As Long

  X = ListBox.ListIndex
  If X <> Z Then
  Word = FindPos(TextBox, X1, X2)
  Word = Mid(Word, Len(Class1) + 1)
      
  ae = Len(Left$(TextBox.Text, X1 - 1)) + _
       Len(Class1) + Len(ListBox.List(X))
      
  Dim curline As String
  curline = rtf.LineText(TextBox, rtf.CurrentLine(TextBox, X1))
  Dim firstl As Long
  firstl = rtf.LinePosition(TextBox, rtf.CurrentLine(TextBox, X1))
  
  'Cut = Left$(TextBox.Text, X1 - (firstl + 1)) & _
        Class1 & ListBox.List(X) & _
        Mid$(TextBox.Text, X2 + 1)

  Cut1 = Mid(curline, 1, Len(curline) - Len(Class1) - Len(Word))
  Cut2 = Class1 & ListBox.List(X)
  If Len(curline) - (Len(Cut1) + Len(Class1) + Len(Word)) > 0 Then
    Cut3 = Mid(curline, Len(Cut1) + Len(Class1) + Len(Word), Len(curline) - (Len(Cut1) + Len(Class1) + Len(Word)))
  End If
                 
  Cut = Cut1 & Cut2 & Cut3
                 
  TextBox.SelStart = firstl
  TextBox.SelLength = Len(curline)
  TextBox.SelText = ""
          
  TextBox.SelStart = rtf.CursorPos(TextBox)
  'TextBox.SelLength = Len(Cut)
  TextBox.SelText = Cut
  
  TextBox.SelStart = ae
        
    ListBox.Visible = False
  End If
  
End Sub


