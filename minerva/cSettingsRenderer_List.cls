VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSettingsRenderer_List"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Renderer-Klasse f�r Liten-Registry-Kategorieelemente

Option Explicit

Public MyForm As New frmDataType_List
Public MySEditor As cEdSettingsEditor

Public Sub PreInitialize()
Load MyForm
End Sub

Public Sub Initialize()
Set MyForm = MyForm.GetInstance
Set MyForm.MyRenderer = Me
MyForm.Visible = False
End Sub

Public Function Render(ByRef snode As cGAMRegistryNode, _
                       ByRef picSEditor As PictureBox, _
                       ByRef x As Long, _
                       ByRef y As Long, _
                       ByRef SubCatLevel As Integer, _
                       ByRef DisplayCollection As CHive)
    Dim pb As PictureBox

    Dim frm As Object
    Set frm = GetSettingsEditor("sys:title")
    frm.Initialize
    frm.Render snode, picSEditor, x, y, SubCatLevel, DisplayCollection

    If SubCatLevel = 0 Then

        If Not snode.Description = "" Then

            Set frm = GetSettingsEditor("sys:desc")
            frm.Initialize
            frm.Render snode, picSEditor, x, y, SubCatLevel, DisplayCollection
        End If

        Set MyForm.MyNode = snode
        Set pb = MyForm.MainContainer

        SetParentAPI pb.hwnd, picSEditor.hwnd
        pb.Left = x
        pb.Top = y
        pb.Width = picSEditor.ScaleWidth - pb.Left

        y = y + pb.Height

        MyForm.Initialize SubCatLevel

        DisplayCollection.Add pb

        Dim i As Integer

        For i = 1 To snode.SubElements.Count
            MySEditor.DisplaySEditor snode.SubElements(i), picSEditor, x + 10, y, True, SubCatLevel + 1
        Next i

    Else
    
    End If

End Function

Public Function DataToXML(snode As cGAMRegistryNode, XMLNode As IXMLDOMElement, Dom As DOMDocument, Project As cGAMProject) As Boolean
Dim i As Integer
Dim sn As cGAMRegistryNode

Dim n As IXMLDOMNode
Dim a As IXMLDOMAttribute

For i = 1 To snode.SubElements.Count
    Set sn = snode.SubElements(i)
    Set n = Dom.createElement("listitem")
    
    Set a = Dom.createAttribute("name")
    a.Text = sn.NodeName
    n.Attributes.setNamedItem a
    
    Set a = Dom.createAttribute("friendlyname")
    a.Text = sn.FriendlyName
    n.Attributes.setNamedItem a
    
    Set a = Dom.createAttribute("type")
    a.Text = sn.NodeType
    n.Attributes.setNamedItem a
    
    Project.AddXMLChildren Dom, n, sn
    
    
    XMLNode.appendChild n
    
    
    
Next i

DataToXML = True
End Function

Public Function XMLToData(snode As cGAMRegistryNode, XMLNode As IXMLDOMNode, Dom As DOMDocument, Project As cGAMProject) As Boolean
    XMLToData = True
    
 
    Dim i As Integer
    Dim n As IXMLDOMNode
    
    Dim sn As cGAMRegistryNode
    
    For i = 0 To XMLNode.childNodes.Length - 1
        Set n = XMLNode.childNodes(i)
        
        Set sn = New cGAMRegistryNode
            
            sn.NodeName = n.Attributes.getNamedItem("name").Text
            sn.FriendlyName = n.Attributes.getNamedItem("friendlyname").Text
            sn.NodeType = n.Attributes.getNamedItem("type").Text
            
            If Left(sn.NodeType, 7) = "custom:" Then Project.Package.BuildRegistryStructure_CustomTypeNode sn, Project.RegistryTypes
            
            Set sn.ParentNode = snode
            snode.SubElements.Add sn, sn.NodeName
            
            Project.XMLElementToRegistry n, Dom, True
    Next i
End Function

