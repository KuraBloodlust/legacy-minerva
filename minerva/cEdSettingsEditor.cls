VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cEdSettingsEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Public CurrentDisplay As CHive

Private m_YScroll As Long
Private m_picSEditor As PictureBox

Public CurrentSelection As cGAMRegistryNode

Public Sub Initialize()
Set SettingsEditors = New CHive
Set CurrentDisplay = New CHive



'Out of the box-Renderer f�r Einstellungen erstellen
'''''''''''''''''''''''''''''''''''''''''''''''''''''

'Kategorien-Renderer
Dim CatRenderer As New cSettingsRenderer_Category
AddSettingsEditor CatRenderer, "category", frmDataType_Category.picCategory.Picture

'Custom Type-Renderer
Dim CustomRenderer As New cSettingsRenderer_Custom
AddSettingsEditor CustomRenderer, "custom", frmDataType_Custom.picCustom.Picture


'Support-Renderer f�r Beschreibungen
Dim DescRenderer As New cSettingsRenderer_Desc
AddSettingsEditor DescRenderer, "sys:desc", frmDataElement_Desc.picInformation.Picture

'Support-Renderer f�r Titelzeilen
Dim TitleRenderer As New cSettingsRenderer_Title
AddSettingsEditor TitleRenderer, "sys:title", frmDataElement_Title.picCustom.Picture




'Eindimensionale Datentypen
Dim tempRenderer As cSettingsRenderer

Set tempRenderer = New cSettingsRenderer
Set tempRenderer.MyForm = frmDataType_String
AddSettingsEditor tempRenderer, "string", frmDataType_String.picString.Picture

Set tempRenderer = New cSettingsRenderer
Set tempRenderer.MyForm = frmDataType_Boolean
AddSettingsEditor tempRenderer, "boolean", frmDataType_Boolean.picBoolean.Picture

Set tempRenderer = New cSettingsRenderer
Set tempRenderer.MyForm = frmDataType_Integer
AddSettingsEditor tempRenderer, "integer", frmDataType_Integer.picInteger.Picture

Set tempRenderer = New cSettingsRenderer
Set tempRenderer.MyForm = frmDataType_ImageResource
AddSettingsEditor tempRenderer, "imageresource", frmDataType_ImageResource.picImage.Picture

'Listen-Renderer
Dim ListRenderer As New cSettingsRenderer_List
AddSettingsEditor ListRenderer, "list", frmDataType_List.picList.Picture

End Sub


Private Sub AddSettingsEditor(SEditor As Object, dataType As String, Icon As Object)
Set SEditor.MySEditor = Me
SEditor.PreInitialize
SettingsEditors.Add SEditor, dataType
If Not Icon Is Nothing Then frmEditorRegistry.imlSettings.ListImages.Add , dataType, Icon
End Sub


Public Sub RegistryToTree(trvSettings As TreeView, imlIcons As ImageList)
trvSettings.Nodes.Clear
RegistryNodeToTree(trvSettings, imlIcons, Project.Registry).Expanded = True
End Sub

Public Function RegistryNodeToTree(trvSettings As TreeView, imlIcons As ImageList, snode As cGAMRegistryNode, Optional ParentNode As Node = Nothing) As Node
Dim n As Node
Dim i As Integer

Dim ico As Integer
ico = IconForType(imlIcons, snode)

Set n = trvSettings.Nodes.Add(, , , snode.FriendlyName, ico, ico)
n.Tag = snode.NodeName
If Not ParentNode Is Nothing Then Set n.Parent = ParentNode

For i = snode.SubElements.Count To 1 Step -1
    RegistryNodeToTree trvSettings, imlIcons, snode.SubElements(i), n
Next i

n.Expanded = False

Set RegistryNodeToTree = n
End Function

Public Function IconForType(imlIcons As ImageList, Node As cGAMRegistryNode) As Integer
On Error GoTo noelement:
Dim tname As String
tname = LCase(Node.NodeType)

If Left(tname, 7) = "custom:" Then
    IconForType = imlIcons.ListImages("custom").Index
Else
    IconForType = imlIcons.ListImages(tname).Index
End If

Exit Function
noelement:
IconForType = imlIcons.ListImages("unknown").Index
End Function

Public Sub CleanupSEditor()
On Error Resume Next

Dim i As Integer
For i = 1 To CurrentDisplay.Count
    DestroyWindow CurrentDisplay(i).hwnd
    CurrentDisplay(i).Visible = False
    Set CurrentDisplay(i) = Nothing
Next i

Set CurrentDisplay = New CHive
End Sub

Public Sub DisplaySEditor(sn As cGAMRegistryNode, picSEditor As PictureBox, Optional ByRef xStart As Long = 0, Optional ByRef yStart As Long = 0, Optional HasParent As Boolean = False, Optional SubCatLevel As Integer = 0)



If sn Is Nothing Then Exit Sub

 
Dim frm As Object, pb As PictureBox
Set frm = GetSettingsEditor(LCase(sn.NodeType))

If frm Is Nothing Then Exit Sub
frm.Initialize

frm.Render sn, picSEditor, xStart, yStart, SubCatLevel, CurrentDisplay


End Sub

Public Sub UpdateSEditors(Optional yScroll As Long = 0, Optional picSEditor As PictureBox = Nothing)
On Error Resume Next

If picSEditor Is Nothing Then
    Set picSEditor = m_picSEditor
    yScroll = m_YScroll
End If

Dim y As Long
Dim i As Integer
Dim cd As PictureBox
For i = 1 To CurrentDisplay.Count
    Set cd = CurrentDisplay(i)
    With cd
        .Top = y - yScroll
        .Width = picSEditor.ScaleWidth - .Left
        y = y + .Height
    End With
Next i

m_YScroll = yScroll
Set m_picSEditor = picSEditor
End Sub

Public Sub SelectNode(RNode As cGAMRegistryNode, Optional ByRef xStart As Long = 0, Optional ByRef yStart As Long = 0)
Set CurrentSelection = RNode

CleanupSEditor
DisplaySEditor RNode, frmEditorRegistry.picSettingsEditor, xStart, yStart


        
        Dim p As String
        p = RNode.GetPath
        p = Mid$(p, 2)
        
        TrvSelectPath p, frmEditorRegistry.trvSettings
        
End Sub

Public Sub TrvSelectPath(p As String, trv As TreeView)

Dim i As Integer
For i = 1 To trv.Nodes.Count
If GetPathFromTags(trv.Nodes(i)) = p Then
    Set trv.SelectedItem = trv.Nodes(i)

    Exit For
End If
Next i
End Sub
