Attribute VB_Name = "Desk"
Option Explicit

Public Const EM_CANUNDO = &HC6
Public Const EM_UNDO = &HC7
Public Const EM_EMPTYUNDOBUFFER = &HCD

Public Enum SpecialFolderIDs
  sfidDESKTOP = &H0
  sfidPROGRAMS = &H2
  sfidPERSONAL = &H5
  sfidFAVORITES = &H6
  sfidSTARTUP = &H7
  sfidRECENT = &H8
  sfidSENDTO = &H9
  sfidSTARTMENU = &HB
  sfidDESKTOPDIRECTORY = &H10
  sfidNETHOOD = &H13
  sfidFONTS = &H14
  sfidTEMPLATES = &H15
  sfidCOMMON_STARTMENU = &H16
  sfidCOMMON_PROGRAMS = &H17
  sfidCOMMON_STARTUP = &H18
  sfidCOMMON_DESKTOPDIRECTORY = &H19
  sfidAPPDATA = &H1A
  sfidPRINTHOOD = &H1B
  sfidProgramFiles = &H10000
  sfidCommonFiles = &H10001
End Enum

Private Type SHITEMID
  cb As Long
  abID As Byte
End Type

Private Type ITEMIDLIST
  mkid As SHITEMID
End Type

Public Declare Function FindWindow Lib "user32" Alias _
"FindWindowA" (ByVal lpClassName As String, _
ByVal lpWindowName As String) As Long

Public Declare Function SetWindowPos Lib "user32" _
  (ByVal hWnd As Long, _
  ByVal hWndInsertAfter As Long, ByVal x As Long, _
  ByVal y As Long, ByVal cx As Long, _
  ByVal cy As Long, ByVal wFlags As Long) As Long
  
Private Declare Function SHGetSpecialFolderLocation Lib _
  "shell32.dll" (ByVal hwndOwner As Long, _
  ByVal nFolder As Long, pidl As ITEMIDLIST) As Long

Private Declare Function SHGetPathFromIDList Lib _
  "shell32.dll" Alias "SHGetPathFromIDListA" _
  (ByVal pidl As Long, ByVal pszPath As String) As Long

' Standard Systemordner ermitteln
Public Function GetSpecialFolder(CSIDL As _
  SpecialFolderIDs) As String

  Dim lResult As Long
  Dim IDL As ITEMIDLIST
  Dim sPath As String
  
  lResult = SHGetSpecialFolderLocation(100, CSIDL, IDL)
  If lResult = 0 Then
    sPath = Space$(512)
    lResult = SHGetPathFromIDList(ByVal IDL.mkid.cb, _
      ByVal sPath)
    GetSpecialFolder = Left$(sPath, InStr(sPath, _
      Chr$(0)) - 1)
  End If
End Function


