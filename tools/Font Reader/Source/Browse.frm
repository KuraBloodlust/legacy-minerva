VERSION 5.00
Begin VB.Form Browse 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Output Ordner w�hlen"
   ClientHeight    =   3495
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4050
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3495
   ScaleWidth      =   4050
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.DirListBox Dir1 
      Height          =   2565
      Left            =   120
      TabIndex        =   4
      ToolTipText     =   "W�hlen sie ihren Project-Ordner"
      Top             =   480
      Width           =   3855
   End
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   3855
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Abbrechen"
      Height          =   300
      Left            =   2790
      TabIndex        =   2
      Top             =   3120
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&OK"
      Height          =   300
      Left            =   1470
      TabIndex        =   1
      Top             =   3120
      Width           =   1215
   End
   Begin VB.CommandButton Command4 
      Caption         =   "&OK"
      Enabled         =   0   'False
      Height          =   300
      Left            =   1470
      TabIndex        =   0
      Top             =   3120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Lab 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Desktop"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Index           =   71
      Left            =   480
      TabIndex        =   5
      Top             =   3240
      Width           =   735
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      Height          =   195
      Left            =   120
      Picture         =   "Browse.frx":0000
      ToolTipText     =   "Desktop"
      Top             =   3240
      Width           =   225
   End
End
Attribute VB_Name = "Browse"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Const F1 As String = "Keine Diskette im Laufwerk!"
Private Const F2 As String = "Auf das Laufwerk kann nicht zugegriffen werden!"
Private Const F3 As String = "Lesefehler"

Private Sub Command1_Click()
Temp = Dir1.Path
Me.Hide
Call Main.Files
Main.Text2.Text = Temp
End Sub

Private Sub Command2_Click()
  Me.Hide
End Sub

Private Sub Drive1_Change()
On Error GoTo Fehler

  Temp = Dir1.Path
  Dir1.Path = Drive1.Drive
  DoEvents
  Exit Sub

Fehler:
  If Drive1.Drive = "a:" Then
  MsgBox F1, vbExclamation, F3
  Else
  MsgBox F2, vbExclamation, F3
  End If
  
End Sub



Private Sub Image1_Click()
Dir1.Path = GetSpecialFolder(sfidDESKTOP)
End Sub

