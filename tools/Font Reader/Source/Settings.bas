Attribute VB_Name = "Settings"
Public SavePfad As String
Public Temp As String
Public Fixt As Long
Public Comp As Long

Private InD As SavedSettings

Private Type SavedSettings
  Pixel As Boolean
  SPfad As String
  DText As String
  Comp As Long
  Fixing As Long
End Type

Public Sub SaveSettings()
Dim Tmp As Integer
Dim Pfad As String

On Error Resume Next

  Pfad = App.Path & "\" & App.EXEName & ".ini"

  With Main
   InD.SPfad = SavePfad
   InD.Pixel = .be(6).Checked
   InD.Fixing = Fixt
   InD.Comp = Comp
   
   If .Text1.Text <> "Demonstration" Then
   InD.DText = .Text1.Text
   Else
   InD.DText = ""
   End If

  End With

  Tmp = FreeFile
  If File(Pfad) Then Kill Pfad

  Open Pfad For Binary As FreeFile
  Put Tmp, 1, InD:  Close

End Sub

Public Sub LoadSettings()
Dim Tmp As Integer
Dim Pfad As String
  
  On Error Resume Next
  Pfad = App.Path & "\" & App.EXEName & ".ini"
  Fixt = 2:  Comp = 0
   
If File(Pfad) Then
  Tmp = FreeFile
  Open Pfad For Binary As FreeFile
  Get Tmp, 1, InD
  Close

  With Main

 
  If InD.SPfad <> "" Then
    .be(1).Enabled = True
    .Command4.Enabled = True
     SavePfad = InD.SPfad
    .Text2.SelStart = 1
    .Text2.ToolTipText = SavePfad
    .Text2.Text = CompactPath(Main, SavePfad, .Text2)
    Call Fontf(SavePfad)
    SavePfad = InD.SPfad
    Temp = SavePfad
  End If
  
  If InD.Pixel = True Then Call Main.be_Click(6)
  If InD.DText <> "" Then .Text1.Text = InD.DText
  Comp = InD.Comp
  Fixt = InD.Fixing
 End With
 
 
End If
End Sub
