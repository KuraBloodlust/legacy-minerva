VERSION 5.00
Begin VB.Form config 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Einstellung"
   ClientHeight    =   1785
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   2955
   ClipControls    =   0   'False
   Icon            =   "config.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1785
   ScaleWidth      =   2955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Abbrechen"
      Height          =   300
      Left            =   720
      TabIndex        =   4
      Top             =   1440
      Width           =   1020
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   300
      Left            =   1800
      TabIndex        =   3
      Top             =   1440
      Width           =   1020
   End
   Begin VB.Frame Frame1 
      Caption         =   "Darstellung:"
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2655
      Begin VB.CheckBox Check2 
         Caption         =   "Zeichenfolge vergleichen"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Width           =   2295
      End
      Begin VB.CheckBox Check1 
         Caption         =   "H�he korrigieren"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Value           =   1  'Checked
         Width           =   2055
      End
   End
End
Attribute VB_Name = "config"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOK_Click()
 If Check1.Value = 1 Then
  Fixt = 2
 Else
  Fixt = 0
 End If

Comp = CLng(Check2.Value)
 
With Main.ListView1
  If .ListItems.Count <> 0 Then
   If .SelectedItem.Selected Then
    If Comp = 0 Then
    Call Main.Draw
    Else
    Call Main.DrawXtra
    End If
   End If
  End If
End With

Unload Me
End Sub

Private Sub Command1_Click()
Unload Me
End Sub

Private Sub Form_Load()
 
 If Fixt <> 2 Then _
 Check1.Value = 0 _
 
 If Comp = 1 Then _
 Check2.Value = 1
End Sub
