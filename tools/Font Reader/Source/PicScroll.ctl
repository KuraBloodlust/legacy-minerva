VERSION 5.00
Begin VB.UserControl PicScroll 
   Alignable       =   -1  'True
   ClientHeight    =   2010
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2010
   ControlContainer=   -1  'True
   ScaleHeight     =   2010
   ScaleWidth      =   2010
   Begin VB.PictureBox pic_Background 
      Height          =   1590
      Left            =   0
      ScaleHeight     =   1530
      ScaleWidth      =   1530
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   0
      Width           =   1590
   End
   Begin VB.HScrollBar hsc_Scroll 
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   1440
      Width           =   1590
   End
   Begin VB.VScrollBar vsc_Scroll 
      Height          =   1590
      Left            =   1560
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   255
   End
End
Attribute VB_Name = "PicScroll"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

' ******************************************************
' *                                                    *
' *  PicScroll 1.0 - UserControl - by KillSwitch 2004  *
' *                                                    *
' ******************************************************

' mailto: TheKillSwitch@gmx.de


Option Explicit

Private Declare Function SetParent Lib "user32" (ByVal _
        hWndChild As Long, ByVal hWndNewParent As Long) _
        As Long
        
Private Type CtlPosXY
    X As Long
    Y As Long
End Type

Dim Twip As CtlPosXY

Public Enum CtlBorderStyle
    ctlKein = 0
    ctl3D = 1
End Enum

Dim picScroll As PictureBox

'+++ Public +++
Public Property Let innerBorderStyle(Style As CtlBorderStyle)
    pic_Background.BorderStyle = Style
    
End Property

Public Property Get innerBorderStyle() As CtlBorderStyle
    innerBorderStyle = pic_Background.BorderStyle
    
End Property

Public Property Let BorderStyle(Style As CtlBorderStyle)
    UserControl.BorderStyle = Style
    
End Property

Public Property Get BorderStyle() As CtlBorderStyle
    BorderStyle = UserControl.BorderStyle
    
End Property

Public Property Let Scrollbars_TabStop(bOnOff As Boolean)
    vsc_Scroll.TabStop = bOnOff
    hsc_Scroll.TabStop = bOnOff
    
End Property

Public Property Get Scrollbars_TabStop() As Boolean
    Scrollbars_TabStop = vsc_Scroll.TabStop
    
End Property

Public Property Let Scrollbars_showVertical(bOnOff As Boolean)
    vsc_Scroll.Visible = bOnOff
    
    Resize
    
End Property

Public Property Get Scrollbars_showVertical() As Boolean
    Scrollbars_showVertical = vsc_Scroll.Visible
    
End Property

Public Property Let Scrollbars_showHorizontal(bOnOff As Boolean)
    hsc_Scroll.Visible = bOnOff
    
    Resize
    
End Property

Public Property Get Scrollbars_showHorizontal() As Boolean
    Scrollbars_showHorizontal = hsc_Scroll.Visible
    
End Property

'+++ Kidnap +++
Public Sub kidnap_PicBox(PicBox As PictureBox)

    Call SetParent(PicBox.hWnd, pic_Background.hWnd)
    Set picScroll = PicBox
    
    picScroll.Top = 0
    picScroll.Left = 0
    picScroll.Visible = True
    
    vsc_Scroll.LargeChange = picScroll.Height / 4
    vsc_Scroll.SmallChange = 8 * Twip.Y
    
    hsc_Scroll.LargeChange = picScroll.Width / 4
    hsc_Scroll.SmallChange = 8 * Twip.Y
    
    hsc_Scroll.Value = 0
    vsc_Scroll.Value = 0
    
    Resize
    
End Sub

'+++ Private +++
Private Sub Resize()
On Error Resume Next

    With UserControl
        .vsc_Scroll.Left = .ScaleWidth - .vsc_Scroll.Width
        .hsc_Scroll.Top = .ScaleHeight - .hsc_Scroll.Height
        
        If .vsc_Scroll.Visible = True Then
            .pic_Background.Width = .ScaleWidth - .vsc_Scroll.Width - (2 * Twip.X)
            .hsc_Scroll.Width = .ScaleWidth - .vsc_Scroll.Width
        Else
            .pic_Background.Width = .ScaleWidth
            .hsc_Scroll.Width = .ScaleWidth
        End If
        
        If .hsc_Scroll.Visible = True Then
            .pic_Background.Height = .ScaleHeight - .hsc_Scroll.Height - (2 * Twip.Y)
            .vsc_Scroll.Height = .ScaleHeight - .hsc_Scroll.Height
        Else
            .pic_Background.Height = .ScaleHeight
            .vsc_Scroll.Height = .ScaleHeight
        End If
        
        'vertikale Scrollleiste
        If picScroll.Height <= pic_Background.Height Then
            If vsc_Scroll.Enabled = True Then
                vsc_Scroll.Enabled = False
            
                picScroll.Top = 0
                vsc_Scroll.Value = 0
            End If
            
        Else
            vsc_Scroll.Max = picScroll.Height - pic_Background.Height
            If vsc_Scroll.Enabled = False Then vsc_Scroll.Enabled = True
            
        End If
    
        'Horrizonatle Scrollleiste
        If picScroll.Width <= pic_Background.Width Then
            If hsc_Scroll.Enabled = True Then
                hsc_Scroll.Enabled = False
            
                picScroll.Left = 0
                hsc_Scroll.Value = 0
            End If
            
        Else
            hsc_Scroll.Max = picScroll.Width - pic_Background.Width
            If hsc_Scroll.Enabled = False Then hsc_Scroll.Enabled = True
            
        End If
        
    End With
    
End Sub

'+++ Control +++
Private Sub UserControl_Initialize()
    Twip.X = Screen.TwipsPerPixelX
    Twip.Y = Screen.TwipsPerPixelY
    
End Sub

Private Sub UserControl_Resize()
    Resize
    
End Sub

Private Sub UserControl_Show()
    Resize
    
End Sub

Private Sub vsc_Scroll_Change()
On Error Resume Next

    picScroll.Top = -vsc_Scroll.Value

End Sub

Private Sub vsc_Scroll_Scroll()
On Error Resume Next

    picScroll.Top = -vsc_Scroll.Value

End Sub

Private Sub hsc_Scroll_Change()
On Error Resume Next

    picScroll.Left = -hsc_Scroll.Value

End Sub

Private Sub hsc_Scroll_Scroll()
On Error Resume Next

    picScroll.Left = -hsc_Scroll.Value

End Sub
