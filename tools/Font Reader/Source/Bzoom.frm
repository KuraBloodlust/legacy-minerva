VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form Bzoom 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Zoom"
   ClientHeight    =   3360
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4785
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3360
   ScaleWidth      =   4785
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture3 
      Height          =   255
      Left            =   3960
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   5
      Top             =   2280
      Width           =   255
   End
   Begin VB.HScrollBar HScroll1 
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   2280
      Width           =   3855
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   2175
      Left            =   3960
      TabIndex        =   3
      Top             =   120
      Width           =   255
   End
   Begin VB.PictureBox Picture2 
      Height          =   2175
      Left            =   120
      ScaleHeight     =   2115
      ScaleWidth      =   3795
      TabIndex        =   1
      Top             =   120
      Width           =   3855
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         FillColor       =   &H00FFFFFF&
         ForeColor       =   &H00FFFFFF&
         Height          =   2175
         Left            =   0
         MouseIcon       =   "Bzoom.frx":0000
         ScaleHeight     =   2175
         ScaleWidth      =   3855
         TabIndex        =   2
         Top             =   0
         Width           =   3855
      End
   End
   Begin ComctlLib.Slider Slider1 
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   2640
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   873
      _Version        =   327682
      LargeChange     =   200
      Min             =   100
      Max             =   1000
      SelStart        =   100
      TickFrequency   =   100
      Value           =   100
   End
End
Attribute VB_Name = "Bzoom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Picture1.Picture = Main.Picture3.Image
Call Redit
End Sub

Private Sub Slider1_Change()
  Picture1.Cls
  Call ZoomSub(Slider1.Value, Picture1, Main.Picture3)
  Picture1.Width = 3855 * (Slider1.Value / 100)
  
  Picture1.Height = 2175 * (Slider1.Value / 100)
  Call Redit
End Sub


Private Sub VScroll1_Change()
 VScroll1_Scroll
 Picture1.Top = -VScroll1.Value
End Sub

Private Sub VScroll1_Scroll()
 Picture1.Top = -VScroll1.Value
End Sub

Private Sub hScroll1_Change()
 hScroll1_Scroll
 Picture1.Left = -HScroll1.Value
End Sub

Private Sub hScroll1_Scroll()
 Picture1.Left = -HScroll1.Value
End Sub
 
Sub Redit()
On Error Resume Next
 VScroll1.LargeChange = Picture2.Height / 4
 VScroll1.SmallChange = 120
 VScroll1.Max = Picture1.Height - Picture2.Height
 
 HScroll1.Max = Picture1.Width - Picture2.Width
 HScroll1.LargeChange = Picture2.Height / 4
 HScroll1.SmallChange = 120

 
End Sub
