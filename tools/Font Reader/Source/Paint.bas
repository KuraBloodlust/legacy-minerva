Attribute VB_Name = "Paint"
Option Explicit

Private Declare Function DrawText Lib "user32" _
  Alias "DrawTextA" ( _
  ByVal hDC As Long, _
  ByVal lpStr As String, _
  ByVal nCount As Long, _
  lpRect As cRECT, _
  ByVal wFormat As Long) As Long
  
Private Type cRECT
  Left As Long
  Top As Long
  Right As Long
  Bottom As Long
End Type


Private Const DT_PATH_ELLIPSIS = &H4000
Private Const DT_MODIFYSTRING = &H10000
Private Const DT_SINGLELINE = &H20&

Public Function CompactPath(oForm As Form, _
  ByVal sPath As String, _
  oControl As Control) As String

  Dim nWidth As Long
  Dim R As cRECT


  R.Right = oControl.Width / Screen.TwipsPerPixelX
  
  DrawText oForm.hDC, sPath, -1, R, _
    DT_PATH_ELLIPSIS Or DT_SINGLELINE Or DT_MODIFYSTRING
  
  CompactPath = sPath
End Function


