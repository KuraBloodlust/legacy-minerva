Attribute VB_Name = "modPakEditor"
Option Explicit


Public Declare Sub CopyMemory Lib "kernel32" Alias _
        "RtlMoveMemory" (pDst As Any, pSrc As Any, ByVal _
        ByteLen As Long)


Public RetPassword As String
Public RetOk As Integer

Public PakForms As New CHive
Public PFCounter As Long


Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Public Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function SetFocusAPI Lib "user32" Alias "SetFocus" (ByVal hwnd As Long) As Long



'Looks whether a file exists
Public Function FileExists(ByVal Datei As String) As Boolean
  On Error GoTo fehler

  If Dir(Datei$) <> "" Then
    If FileLen(Datei$) > 0 Then
        FileExists = True
    End If
  End If

fehler:
End Function

Public Function PasswordBox(Prompt As String, Optional MaskPassword As Boolean = True) As String
frmPassword.lblPrompt.Caption = Prompt
frmPassword.chkMask.Value = IIf(MaskPassword, 1, 0)
frmPassword.txtPassword.PasswordChar = IIf(MaskPassword, "*", "")

frmPassword.Show vbModal

If RetOk = 1 Then PasswordBox = RetPassword
End Function


Public Function File2Str(FileName As String)
Dim f As String, fnum As Integer
fnum = FreeFile
Open FileName For Binary Access Read As fnum
f = String(LOF(fnum), Chr(0))
Get #fnum, , f
Close fnum
File2Str = f
End Function


'Returns true if the programm is in the IDE
Public Function InDevelopment() As Boolean
  InDevelopment = (App.LogMode = 0)
End Function

