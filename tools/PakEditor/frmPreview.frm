VERSION 5.00
Begin VB.Form frmPreview 
   Caption         =   "Preview"
   ClientHeight    =   3525
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   4710
   LinkTopic       =   "Form1"
   ScaleHeight     =   3525
   ScaleWidth      =   4710
   StartUpPosition =   3  'Windows-Standard
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Height          =   375
      Left            =   1920
      TabIndex        =   1
      Top             =   3120
      Width           =   975
   End
   Begin VB.TextBox txtPreview 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Beides
      TabIndex        =   0
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "frmPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOK_Click()
    Unload Me
End Sub

Private Sub Form_Resize()
txtPreview.Width = ScaleWidth - txtPreview.Left * 2
txtPreview.Height = ScaleHeight - txtPreview.Top * 2 - cmdOK.Height

cmdOK.Top = txtPreview.Top + txtPreview.Height
cmdOK.Left = ScaleWidth / 2 - cmdOK.Width / 2
End Sub
