VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.MDIForm mdiMain 
   BackColor       =   &H8000000C&
   Caption         =   "R-PG Pak Editor"
   ClientHeight    =   3420
   ClientLeft      =   60
   ClientTop       =   660
   ClientWidth     =   5805
   Icon            =   "mdiMain.frx":0000
   LinkTopic       =   "MDIForm1"
   OLEDropMode     =   1  'Manuell
   StartUpPosition =   2  'Bildschirmmitte
   Begin MSComctlLib.ImageList imlTlb 
      Left            =   1200
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16711935
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiMain.frx":1B16A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiMain.frx":1B51F
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlb 
      Align           =   1  'Oben ausrichten
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "New"
            Object.ToolTipText     =   "New"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Open"
            Object.ToolTipText     =   "Open"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog cdlFile 
      Left            =   3000
      Top             =   1560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "R-PG Pak Files (*.rpak)|*.rpak"
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "New..."
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "Open..."
      End
      Begin VB.Menu mnuFileDivider2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "Quit"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "?"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About..."
      End
   End
End
Attribute VB_Name = "mdiMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Sub MDIForm_Load()

Set tlb.ImageList = imlTlb

tlb.Buttons(1).Image = 1
tlb.Buttons(2).Image = 2

Me.Show
DoEvents



If Not Command = "" Then LoadFile Replace(Command, """", "")


End Sub

Private Sub MDIForm_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
Dim i As Integer
For i = 1 To Data.Files.Count
    LoadFile Data.Files(i)
Next i
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
End
End Sub

Public Sub mnuFileNew_Click()

cdlFile.DialogTitle = "Create new RPAK Archive"
cdlFile.ShowSave

If cdlFile.FileName = "" Then Exit Sub

Dim pak As New cRPakArchive
Dim pwd As String

pwd = PasswordBox("Please specify a password:", False)
If RetOk = 0 Then Exit Sub

pak.FileNew cdlFile.FileName, pwd





Dim PF As New frmPakFile
Set PF.MyPak = pak
PF.PakFileID = PFCounter

PakForms.Add PF, "pf" & PFCounter
PFCounter = PFCounter + 1

PF.Show
PF.UpdateCtls
End Sub

Public Sub mnuFileOpen_Click()
cdlFile.DialogTitle = "Open an RPAK Archive"
cdlFile.ShowOpen

If cdlFile.FileName = "" Then Exit Sub

LoadFile cdlFile.FileName
End Sub

Public Sub LoadFile(fn As String)
Dim pak As New cRPakArchive
Dim pwd As String


If Not pak.FileOpen(fn, "", InDevelopment) Then
    If pak.ErrDesc <> "Invalid password." Then MsgBox pak.ErrDesc, vbCritical, pak.ErrSource: Exit Sub
    
    pwd = PasswordBox("Enter RPAK password:", True)
    
    If Not pak.FileOpen(fn, pwd) Then MsgBox pak.ErrDesc, vbCritical, pak.ErrSource: Exit Sub
End If

Dim PF As New frmPakFile
Set PF.MyPak = pak
PF.PakFileID = PFCounter

PakForms.Add PF, "pf" & PFCounter
PFCounter = PFCounter + 1

PF.Show

PF.Visible = True


End Sub

Public Sub mnuFileQuit_Click()
Unload Me
End Sub

Public Sub mnuHelpAbout_Click()
frmAbout.Show vbModal
End Sub


Private Sub tlb_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
Case "New"
mnuFileNew_Click
Case "Open"
mnuFileOpen_Click
End Select
End Sub




