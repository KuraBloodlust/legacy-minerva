Attribute VB_Name = "Regedit"
'zunächst alle benötigten API-Deklarationen
Private Declare Function RegOpenKey Lib "advapi32.dll" _
  Alias "RegOpenKeyA" (ByVal hKey As Long, _
  ByVal lpSubKey As String, phkResult As Long) As Long

Private Declare Function RegQueryValueEx Lib "advapi32.dll" _
  Alias "RegQueryValueExA" (ByVal hKey As Long, _
  ByVal lpValueName As String, ByVal lpReserved As Long, _
  lpType As Long, lpData As Any, lpcbData As Long) As Long

Private Declare Function RegSetValueEx Lib "advapi32.dll" _
  Alias "RegSetValueExA" (ByVal hKey As Long, _
  ByVal lpValueName As String, ByVal Reserved As Long, _
  ByVal dwType As Long, lpData As Any, ByVal cbData As Long) _
  As Long

Private Declare Function RegCloseKey Lib "advapi32.dll" _
  (ByVal hKey As Long) As Long

Private Declare Function RegCreateKey Lib "advapi32.dll" _
  Alias "RegCreateKeyA" (ByVal hKey As Long, _
  ByVal lpSubKey As String, phkResult As Long) As Long

Private Declare Function RegDeleteValue Lib "advapi32.dll" _
  Alias "RegDeleteValueA" (ByVal hKey As Long, _
  ByVal lpValueName As String) As Long

Const HKEY_CURRENT_USER = &H80000001
Const REG_SZ = 1
Const REG_BINARY = 3

'Wert (String/Text) für einen bestimmten
'Schlüsselnamen speichern. Sollte der Schlüssel nicht
'existieren, wird dieser autom. erstellt.
'
'Parameterbeschreibung
'---------------------
'hKey (Hauptschlüssel) : z.B. HKEY_CURRENT_USER
'sPath (Schlüsselpfad) : z.B. MeineAnwendung
'sValue (Schlüsselname): z.B. Path
'iData (Schlüsselwert) : z.B. c:\programme\MeineAnwendung
           
Sub fStringSpeichern(hKey As Long, sPath As String, _
  sValue As String, iData As String)

  Dim vRet As Variant

  RegCreateKey hKey, sPath, vRet
  RegSetValueEx vRet, sValue, 0, REG_SZ, ByVal iData, _
    Len(iData)
  RegCloseKey vRet
End Sub


'Wert (Binär 0-255) für einen bestimmten
'Schlüsselnamen speichern. Sollte der Schlüssel nicht
'existieren, wird dieser autom. erstellt.
'
'Parameterbeschreibung
'---------------------
'hKey (Hauptschlüssel) : z.B. HKEY_CURRENT_USER
'sPath (Schlüsselpfad) : z.B. MeineAnwendung
'sValue (Schlüsselname): z.B. Code
'iData (Schlüsselwert) : z.B. 220

Sub fStringSpeichernLong(hKey As Long, sPath As String, _
  sValue As String, iData As String)

  Dim vRet As Variant
    
  RegCreateKey hKey, sPath, vRet
  RegSetValueEx vRet, sValue, 0, REG_BINARY, _
    CByte(iData), 4
  RegCloseKey vRet
End Sub


'Wert für einen bestimmten
'Schlüsselnamen auslesen.
'
'Parameterbeschreibung
'---------------------
'hKey (Hauptschlüssel) : z.B. HKEY_CURRENT_USER
'sPath (Schlüsselpfad) : z.B. MeineAnwendung
'sValue (Schlüsselname): z.B. Path
'Rückgabewert          : z.B. c:\programme\MeineAnwendung

Function fWertLesen(hKey As Long, sPath As String, _
  sValue As String)

  Dim vRet As Variant
    
  RegOpenKey hKey, sPath, vRet
  fWertLesen = fRegAbfrageWert(vRet, sValue)
  RegCloseKey vRet
End Function

'Wird von "fWertLesen" aufgerufen und gibt den Wert
'eines Schlüsselnamens zurück. Hierbei wird autom.
'ermittelt, ob es sich um einen String oder Binärwert
'handelt.
Function fRegAbfrageWert(ByVal hKey As Long, _
  ByVal sValueName As String) As String
    
  Dim sBuffer As String
  Dim lRes As Long
  Dim lTypeValue As Long
  Dim lBufferSizeData As Long
  Dim iData As Integer

  lRes = RegQueryValueEx(hKey, sValueName, 0, _
    lTypeValue, ByVal 0, lBufferSizeData)
  If lRes = 0 Then
    If lTypeValue = REG_SZ Then
      sBuffer = String(lBufferSizeData, Chr$(0))
      lRes = RegQueryValueEx(hKey, sValueName, 0, _
        0, ByVal sBuffer, lBufferSizeData)
      If lRes = 0 Then
        fRegAbfrageWert = Left$(sBuffer, _
          InStr(1, sBuffer, Chr$(0)) - 1)
      End If
    ElseIf lTypeValue = REG_BINARY Then
      lRes = RegQueryValueEx(hKey, sValueName, 0, _
        0, iData, lBufferSizeData)
      If lRes = 0 Then
        fRegAbfrageWert = iData
      End If
    End If
  End If
End Function


'Löschen eines Schlüsselnamens
'
'Parameterbeschreibung
'---------------------
'hKey (Hauptschlüssel) : z.B. HKEY_CURRENT_USER
'sPath (Schlüsselpfad) : z.B. MeineAnwendung
'sValue (Schlüsselname): z.B. Path

Sub fWerteLoeschen(hKey As Long, sPath As String, _
  sValue As String)

  Dim vRet As Variant
    
  RegCreateKey hKey, sPath, vRet
  RegDeleteValue vRet, sValue
  RegCloseKey vRet
End Sub

