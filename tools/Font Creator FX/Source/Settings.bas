Attribute VB_Name = "Settings"
Private InD As SavedSettings

Private Type SavedSettings
  'Font Einstellung
  Font As String
  Color As Long
  Size As Long
  Bold As Byte
  
  '----------
  Border As Long
  Text As String
  
  Farbe As Long
  
  TPfad As String
  OutPfad As String
End Type

Public CheckDraw

'--- Wilkommen in dem "Einstellungs" -Modul ---

'    Hier werden einige Angaben in eine INI
'    gespeichert, die beim Starten wieder
'    zugeordnet werden.

'1. Einstellungen speichern
'2. Einstellungen laden


'### Einstellungen speichern ######################################
Public Sub SaveSettings()
Dim Tmp As Integer
Dim Pfad As String
Dim t As Long

  On Error Resume Next
  Pfad = App.Path & "\" & App.EXEName & ".ini"

  With Main
    InD.Font = .Combo1.Text
    InD.Border = Fcol
    InD.Size = .FSize.Text
    InD.Color = .Shape1.FillColor
    InD.Bold = .Style(1).Value
    InD.Text = .Text1.Text
    InD.Farbe = Colft
    If tPath = App.Path & "\Texturen\" Then
    InD.TPfad = ""
    Else
    InD.TPfad = tPath
    End If
    If sPath = App.Path & "\Output\" Then
    InD.OutPfad = ""
    Else
    InD.OutPfad = sPath
    End If
        
   'For t = 0 To .List1.ListCount
    'InD.Effekte(t) = CLng(.List1.Selected(t))
    'Next t
  End With

  Tmp = FreeFile
  'If File(Pfad) Then Kill Pfad
  Open Pfad For Binary As FreeFile
  Put Tmp, 1, InD:  Close

End Sub

'### Einstellungen laden ###########################################
Public Sub LoadSettings()
Dim Tmp As Integer
Dim Pfad As String
Dim t As Long

  On Error Resume Next
  Pfad = App.Path & "\" & App.EXEName & ".ini"

  If File(Pfad) Then
  Tmp = FreeFile
  Open Pfad For Binary As FreeFile
  Get Tmp, 1, InD

  Close

  With Main
   .FSize.Text = InD.Size
   .Style(1).Value = InD.Bold
   .Text1.Text = InD.Text
   .Shape1.FillColor = InD.Color
   Colft = InD.Farbe
   Fcol = InD.Border
   
   If Not InD.Font = "" Then
     Dim b As Long
       For b = 0 To .Combo1.ListCount
         If .Combo1.List(b) = InD.Font Then
       .Combo1.ListIndex = b
         Exit For
         End If
       Next b
   End If

  ' For t = 0 To .List1.ListCount
  '   .List1.Selected(t) = InD.Effekte(t)
  ' Next t
    
   If InD.TPfad <> "" Then
   tPath = InD.TPfad
   Else
   tPath = App.Path & "\Texturen\"
   End If
   
   If InD.OutPfad <> "" Then
   sPath = InD.OutPfad
   Else
   sPath = App.Path & "\Output\"
   End If
  End With
  
  Else
  
    Fcol = vbBlack
    Colft = vbRed
    tPath = App.Path & "\Texturen\"
    sPath = App.Path & "\Output\"
  End If
  
 If DirExists(tPath) = False Then _
 tPath = App.Path
 
If DirExists(sPath) = False Then _
 sPath = App.Path

CheckDraw = True
Call PreInit(Main.Combo1, Main.Prev)
End Sub

Public Function File(strPath As String) As Boolean
    On Error Resume Next
    File = ((GetAttr(strPath) And (vbDirectory Or vbVolume)) = 0)
End Function
