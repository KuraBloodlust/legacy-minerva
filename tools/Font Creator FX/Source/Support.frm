VERSION 5.00
Begin VB.Form Vars 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Einstellungen"
   ClientHeight    =   2085
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5430
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2085
   ScaleWidth      =   5430
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "Abbrechen"
      Height          =   300
      Left            =   4200
      TabIndex        =   5
      Top             =   1680
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Speichern"
      Height          =   300
      Left            =   3000
      TabIndex        =   2
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Colorieren:"
      Height          =   1455
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   5175
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   1095
         Left            =   120
         ScaleHeight     =   1065
         ScaleWidth      =   4905
         TabIndex        =   7
         Top             =   240
         Width           =   4935
         Begin VB.Image Image3 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   720
            Top             =   240
            Width           =   615
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Farbe:"
            Height          =   195
            Left            =   120
            TabIndex        =   8
            Top             =   285
            Width           =   450
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H80000008&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H000000FF&
            FillStyle       =   0  'Solid
            Height          =   300
            Left            =   720
            Top             =   240
            Width           =   615
         End
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Transparenz:"
      Height          =   1455
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5175
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   1095
         Left            =   120
         ScaleHeight     =   1065
         ScaleWidth      =   4905
         TabIndex        =   1
         Top             =   240
         Width           =   4935
         Begin VB.Label Label2 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "[ 120 ]"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   120
            TabIndex        =   4
            Top             =   480
            Width           =   450
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "St�rke 1-250"
            Height          =   255
            Left            =   120
            TabIndex        =   3
            Top             =   120
            Width           =   1455
         End
      End
   End
End
Attribute VB_Name = "Vars"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Farbe = Shape1.FillColor
Tol = HScroll1.Value
If Option1.Value = True Then Art = 1 Else Art = 2
F1 = Text5.Text
F2 = Text6.Text
R = Text1.Text
G = Text2.Text
B = Text3.Text
Tiefe = Text4.Text
Call PreInit(Main.Combo1, Main.Prev)
Me.Visible = False
Refresh
End Sub

Private Sub Command2_Click()
Me.Visible = False
End Sub

Private Sub Form_Load()
gHW = Me.hwnd
Hook
End Sub

Private Sub Image3_Click()
Dim Col As Long
Col = ColorDialog(Me.hwnd, Shape1.FillColor)
If Col <> -1 Then
Shape1.FillColor = Col
End If
End Sub
Sub Weg()
Frame1.Visible = False
Frame2.Visible = False
Frame3.Visible = False
Frame4.Visible = False
HScroll1.Visible = False
End Sub

Public Sub Senda()
Call Weg

If Main.List1.ListIndex = 5 Then
Frame2.Visible = True
ElseIf Main.List1.ListIndex = 0 Then
Frame1.Visible = True
HScroll1.Visible = True
End If
End Sub

Public Sub Senda2()
Call Weg

If Main.Option2 = True Then
Frame4.Visible = True
ElseIf Option3 = 0 Then
Frame3.Visible = True
End If
End Sub


Private Sub HScroll1_Change()
Label2.Caption = "[ " & HScroll1.Value & "]"
End Sub

Private Sub HScroll1_Scroll()
Call HScroll1_Change
End Sub


