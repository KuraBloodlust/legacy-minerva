Attribute VB_Name = "Bugfix"
Option Explicit

'Modul nur f�r das Scrollbalkenproblem da
'Keine �nderungen n�tig!

Private Declare Function CallWindowProc Lib "user32" Alias _
      "CallWindowProcA" (ByVal lpPrevWndFunc As Long, _
      ByVal hwnd As Long, ByVal Msg As Long, _
      ByVal wParam As Long, ByVal lParam As Long) As Long

Private Declare Function SetWindowLong Lib "user32" Alias _
      "SetWindowLongA" (ByVal hwnd As Long, _
      ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

Private Const GWL_WNDPROC = -4
Private lpPrevWndProc As Long
Private IsHooked As Boolean
Public gHW As Long

Public Sub Hook()
  lpPrevWndProc = SetWindowLong(gHW, GWL_WNDPROC, _
  AddressOf WindowProc)
End Sub

Public Sub UnHook()
Dim temp As Long
  
  If IsHooked Then
  temp = SetWindowLong(gHW, GWL_WNDPROC, lpPrevWndProc)
  IsHooked = False
  End If
    
End Sub

Private Function WindowProc(ByVal hw As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    
    If &H137 <> uMsg Then
    WindowProc = CallWindowProc(lpPrevWndProc, hw, uMsg, wParam, lParam)
    End If
End Function
