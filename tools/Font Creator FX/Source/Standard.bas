Attribute VB_Name = "Standard"
Private Declare Function DrawText Lib "user32" _
  Alias "DrawTextA" ( _
  ByVal hDC As Long, _
  ByVal lpStr As String, _
  ByVal nCount As Long, _
  lpRect As cRECT, _
  ByVal wFormat As Long) As Long

Private Type cRECT
  Left As Long
  Top As Long
  Right As Long
  Bottom As Long
End Type

Private Const DT_PATH_ELLIPSIS = &H4000
Private Const DT_MODIFYSTRING = &H10000
Private Const DT_SINGLELINE = &H20&


Public nPic(0) As New StdPicture, iPic(0) As IPicture
Public PB%, Dauer%
Public T1 As Long, T2 As Long, CT1 As Long, CT2 As Long
Public Colft As Long
Public Typ&, Wat&, Fcol  As Long, tPath As String
Public sPath As String


Public Function PreInit(Typ2 As Control, Typ As PictureBox)
Typ.Visible = False
On Error Resume Next
If Main.List1.Selected(5) = True Then
Typ.ForeColor = &HFFFFFF
Else
Typ.ForeColor = Main.Shape1.FillColor
End If

Typ.BackColor = &HFEFEFE
Typ.FontSize = Main.FSize.Text
Typ.FontBold = Main.Style(1).Value
Typ.FontItalic = Main.Style(2).Value
Typ.CurrentX = 2
Typ.CurrentY = 2

Typ.FontUnderline = Main.Style(3).Value
Typ.FontUnderline = Main.Style(3).Value
Typ.ScaleMode = 3
Typ.FontName = Typ2.List(Typ2.ListIndex)
Typ.Cls

Typ.Picture = LoadPicture("")
Typ.Print Main.Text1.Text
Typ.Picture = Typ.Image

Set nPic(0) = Typ.Picture
Set iPic(0) = nPic(0)
Call Check(Typ, Main.Picture6)
Bon = True

End Function

Sub Check(Typ As PictureBox, Typs2 As PictureBox, Optional t As Long, Optional b As Long)
    
With Main
 Typ.Visible = False

If b <> 2 Then
Typs2.BackColor = vbWhite
.Image7.Visible = True
Dauer = 100
End If

If t <> 1 Then
If b <> 2 Then
Set Main.Prev = iPic(0)
End If
End If

'Border
If Main.List1.Selected(5) = True Then Call Border(Typ, Typ.BackColor)
If b <> 2 Then PB = 20: Call Balken(Typs2)
If b = 2 Then PB = 30: Call Balken(Typs2)
'Textur/Verlauf
If .List1.Selected(6) = True Then
  If b = 2 Then
    If Stil1.Check2.Value = vbUnchecked Then
    Call Textur(Typ)
    End If
  Else
  Call Textur(Typ)
  End If
End If
If b <> 2 Then PB = 30: Call Balken(Typs2)
If b = 2 Then PB = 35: Call Balken(Typs2)
'Diffuesing
If .List1.Selected(0) = True Then Call DiffusePicture(Typ)
If b <> 2 Then PB = 40: Call Balken(Typs2)
If b = 2 Then PB = 40: Call Balken(Typs2)
'Sch�rfen
If .List1.Selected(1) = True Then Call SharpenPicture(Typ)
If b <> 2 Then PB = 60: Call Balken(Typs2)
If b = 2 Then PB = 45: Call Balken(Typs2)
'Erhellen
If .List1.Selected(2) = True Then Call SolarizePicture(Typ)
If b <> 2 Then PB = 70: Call Balken(Typs2)
If b = 2 Then PB = 50: Call Balken(Typs2)
'Einf�rben
If .List1.Selected(3) = True Then Call Colit(Typ)
If b <> 2 Then PB = 80: Call Balken(Typs2)
If b = 2 Then PB = 55: Call Balken(Typs2)
'Graut�ne
If .List1.Selected(4) = True Then DoGrayScale Typ, vbWhite
If b <> 2 Then PB = 100: Call Balken(Typs2)
If b = 2 Then PB = 58: Call Balken(Typs2)

If b <> 2 Then
.Image7.Visible = False
.Picture6.BackColor = &H8000000D
End If

Typ.Visible = True


End With

End Sub

Public Function Tsize(Typ As Control)
  If LenB(Typ) Then
  If Val(Typ) > 0 And Val(Typ) < 500 Then
  Call PreInit(Main.Combo1, Main.Prev)
  Else
  Typ = 499
  End If
  End If
  
End Function

Public Sub ProgressBar(Prg%, Min%, Max%, Picborder As Object)
    Dim Fx&
    If Prg < Min Or Prg > Max Or Max <= Min Then Exit Sub
    Prg = Int(100 / (Max - Min)) * (Prg - Min)
    With Picborder
    Picborder.Cls
    If Prg > 0 Then
    Fx = (Picborder.ScaleWidth - 2) / 100 * Prg
    Picborder.Line (0, 0)-(Fx + 1, Picborder.ScaleHeight - 1), _
    &H8000000D, BF
    .CurrentX = Fx + 3
    .CurrentY = 0
    End If
    End With
End Sub

Sub Balken(Typ As PictureBox)
For Dauer = 1 To 101
Call ProgressBar(PB + Dauer, 100, 200, Typ)
Next Dauer
End Sub

Public Function DirExists(DirName As String) As Boolean
F$ = DirName
dirFolder = Dir(F$, vbDirectory)

If dirFolder <> "" Then
    DirExists = True
End If

End Function

Public Function CompactPath(oForm As Form, _
  ByVal sPath As String, _
  oControl As Control) As String

  Dim nWidth As Long
  Dim r As cRECT


  r.Right = oControl.Width / Screen.TwipsPerPixelX
  
  DrawText oForm.hDC, sPath, -1, r, _
    DT_PATH_ELLIPSIS Or DT_SINGLELINE Or DT_MODIFYSTRING
  
  CompactPath = sPath
End Function

Public Function FE(filename$) As Boolean
    FE = (Dir$(filename$) <> "")
End Function
