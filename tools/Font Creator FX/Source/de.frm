VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form Stil1 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Textur"
   ClientHeight    =   5340
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3990
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   356
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   266
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.HScrollBar ScrollBlau 
      Height          =   195
      Index           =   1
      LargeChange     =   10
      Left            =   2160
      Max             =   255
      TabIndex        =   34
      Top             =   1680
      Width           =   1455
   End
   Begin VB.HScrollBar ScrollGr�n 
      Height          =   195
      Index           =   1
      LargeChange     =   10
      Left            =   2160
      Max             =   255
      TabIndex        =   33
      Top             =   1440
      Width           =   1455
   End
   Begin VB.HScrollBar ScrollRot 
      Height          =   195
      Index           =   1
      LargeChange     =   10
      Left            =   2160
      Max             =   255
      TabIndex        =   32
      Top             =   1200
      Width           =   1455
   End
   Begin VB.HScrollBar ScrollBlau 
      Height          =   195
      Index           =   0
      LargeChange     =   10
      Left            =   240
      Max             =   255
      TabIndex        =   31
      Top             =   1680
      Width           =   1455
   End
   Begin VB.HScrollBar ScrollGr�n 
      Height          =   195
      Index           =   0
      LargeChange     =   10
      Left            =   240
      Max             =   255
      TabIndex        =   30
      Top             =   1440
      Width           =   1455
   End
   Begin VB.HScrollBar ScrollRot 
      Height          =   195
      Index           =   0
      LargeChange     =   10
      Left            =   240
      Max             =   255
      TabIndex        =   29
      Top             =   1200
      Width           =   1455
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4695
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   8281
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   529
      TabCaption(0)   =   "Muster"
      TabPicture(0)   =   "de.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label5"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "imgpreview"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame2"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "File1"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Command2"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   "Verlauf"
      TabPicture(1)   =   "de.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Text(1)"
      Tab(1).Control(1)=   "Text(0)"
      Tab(1).Control(2)=   "ColorWahl(1)"
      Tab(1).Control(3)=   "Label2"
      Tab(1).Control(4)=   "ColorWahl(0)"
      Tab(1).Control(5)=   "Label1"
      Tab(1).Control(6)=   "Label3"
      Tab(1).Control(7)=   "frDirection"
      Tab(1).Control(8)=   "Picture1"
      Tab(1).Control(9)=   "frOrientation"
      Tab(1).Control(10)=   "optDirection(0)"
      Tab(1).Control(11)=   "optDirection(1)"
      Tab(1).Control(12)=   "Check2"
      Tab(1).ControlCount=   13
      Begin VB.CheckBox Check2 
         Caption         =   "Verlauf anpassen "
         Height          =   255
         Left            =   -74880
         TabIndex        =   36
         Top             =   4320
         Width           =   1575
      End
      Begin VB.CommandButton Command2 
         Caption         =   "..."
         Height          =   285
         Left            =   3000
         TabIndex        =   20
         Top             =   2640
         Width           =   375
      End
      Begin VB.FileListBox File1 
         Height          =   1065
         Left            =   120
         Pattern         =   "*.jpg;*.bmp;*.gif"
         TabIndex        =   17
         Top             =   840
         Width           =   1785
      End
      Begin VB.OptionButton optDirection 
         Caption         =   "Rechts > Links"
         Height          =   255
         Index           =   1
         Left            =   -72960
         TabIndex        =   9
         Top             =   2880
         Value           =   -1  'True
         Width           =   1400
      End
      Begin VB.OptionButton optDirection 
         Caption         =   "Links > Rechts"
         Height          =   255
         Index           =   0
         Left            =   -72960
         TabIndex        =   8
         Top             =   2520
         Width           =   1400
      End
      Begin VB.Frame frOrientation 
         Caption         =   "Verlauf"
         Height          =   975
         Left            =   -74880
         TabIndex        =   3
         Top             =   2280
         Width           =   1455
         Begin VB.PictureBox Picture2 
            BorderStyle     =   0  'None
            Height          =   615
            Left            =   120
            ScaleHeight     =   615
            ScaleWidth      =   1095
            TabIndex        =   4
            Top             =   240
            Width           =   1095
            Begin VB.OptionButton optOrientation 
               Caption         =   "Vertical"
               Height          =   255
               Index           =   1
               Left            =   0
               TabIndex        =   6
               Top             =   360
               Value           =   -1  'True
               Width           =   1000
            End
            Begin VB.OptionButton optOrientation 
               Caption         =   "Horizontal"
               ForeColor       =   &H8000000C&
               Height          =   255
               Index           =   0
               Left            =   0
               MaskColor       =   &H000080FF&
               TabIndex        =   5
               Top             =   0
               Width           =   1000
            End
         End
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   795
         Left            =   -74880
         ScaleHeight     =   765
         ScaleWidth      =   3405
         TabIndex        =   7
         Top             =   3480
         Width           =   3435
      End
      Begin VB.Frame frDirection 
         Caption         =   "Richtung"
         Height          =   975
         Left            =   -73080
         TabIndex        =   10
         Top             =   2280
         Width           =   1575
      End
      Begin VB.Frame Frame2 
         Caption         =   "Fl�che"
         Height          =   1215
         Left            =   120
         TabIndex        =   21
         Top             =   3240
         Width           =   3495
         Begin VB.CheckBox Check1 
            Caption         =   "Muster wiederholen"
            Height          =   255
            Left            =   240
            TabIndex        =   35
            Top             =   360
            Value           =   1  'Checked
            Width           =   2535
         End
         Begin VB.TextBox TypT 
            Enabled         =   0   'False
            Height          =   285
            Index           =   0
            Left            =   960
            MaxLength       =   8
            TabIndex        =   23
            Text            =   "0"
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox TypT 
            Enabled         =   0   'False
            Height          =   285
            Index           =   1
            Left            =   2400
            MaxLength       =   8
            TabIndex        =   22
            Text            =   "0"
            Top             =   840
            Width           =   735
         End
         Begin VB.Label TypD 
            Caption         =   "X Pos:"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   25
            Top             =   880
            Width           =   495
         End
         Begin VB.Label TypD 
            Caption         =   "Y Pos:"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   1800
            TabIndex        =   24
            Top             =   880
            Width           =   615
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Optionen:"
         Height          =   1095
         Left            =   120
         TabIndex        =   26
         Top             =   2040
         Width           =   3495
         Begin VB.TextBox Text4 
            Height          =   285
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   27
            Top             =   600
            Width           =   2775
         End
         Begin VB.Label Label8 
            Caption         =   "Texturen-Pfad:"
            Height          =   255
            Left            =   120
            TabIndex        =   28
            Top             =   360
            Width           =   1095
         End
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "' nur in der fertigen Map"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -73200
         TabIndex        =   37
         Top             =   4365
         Width           =   1695
      End
      Begin VB.Image imgpreview 
         BorderStyle     =   1  'Fixed Single
         Height          =   1080
         Left            =   2040
         Stretch         =   -1  'True
         Top             =   840
         Width           =   1545
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Texturen"
         Height          =   195
         Left            =   720
         TabIndex        =   19
         Top             =   480
         Width           =   630
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Vorschau"
         Height          =   315
         Left            =   2400
         TabIndex        =   18
         Top             =   480
         Width           =   675
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Startfarbe:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   -74880
         TabIndex        =   16
         Top             =   480
         Width           =   945
      End
      Begin VB.Label ColorWahl 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   0
         Left            =   -74880
         TabIndex        =   15
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Endfarbe:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   -72960
         TabIndex        =   14
         Top             =   480
         Width           =   855
      End
      Begin VB.Label ColorWahl 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   1
         Left            =   -72960
         TabIndex        =   13
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Text 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   -74880
         TabIndex        =   12
         Top             =   1920
         Width           =   1455
      End
      Begin VB.Label Text 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   1
         Left            =   -72960
         TabIndex        =   11
         Top             =   1920
         Width           =   1455
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Abbrechen"
      Height          =   300
      Left            =   1560
      TabIndex        =   1
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "&Speichern"
      Height          =   300
      Left            =   2760
      TabIndex        =   0
      Top             =   4920
      Width           =   1095
   End
   Begin VB.Menu ne 
      Caption         =   "test"
      Visible         =   0   'False
      Begin VB.Menu load 
         Caption         =   "Grafik laden..."
      End
   End
End
Attribute VB_Name = "Stil1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type MY_RGB
   Rot As Byte
   Gr�n As Byte
   Blau As Byte
End Type
   
Dim arClr(1) As MY_RGB
Private Const cInitial = 0
Private Const cFinal = 1

Dim swSkipScroll As Boolean
Dim Backup As String

Private Sub Check1_Click()
Dim i As Long

  For i = 0 To 1
  TypT(i).Enabled = -(Check1.Value - 1)
  TypD(i).Enabled = -(Check1.Value - 1)
  Next i
End Sub

Private Sub cmdExit_Click()
Typ = SSTab1.Tab
Main.PicRes.Picture = imgpreview.Picture
Me.Visible = False

T1 = optOrientation(0).Value
T2 = optDirection(0).Value

CT1 = ColorWahl(cInitial).BackColor
CT2 = ColorWahl(cFinal).BackColor
Call PreInit(Main.Combo1, Main.Prev)
End Sub

Private Sub Command1_Click()
Me.Visible = False
End Sub

Private Sub Command2_Click()
Dim PfadT As String

  PfadT = Dialog("Bitte Texturen-Ordner w�hlen", Me)
  
  If PfadT <> vbNullString Then
    Text4.Text = PfadT
    tPath = PfadT
    File1.Path = FixPath(tPath)
  End If
  
End Sub

Private Sub File1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 Then
PopupMenu ne
End If

End Sub

Private Sub Form_Load()
gHW = Me.hwnd: Call Hook
Dim i As Long

  For i = 0 To 1
  ScrollGr�n(i).Visible = SSTab1.Tab
  ScrollRot(i).Visible = SSTab1.Tab
  ScrollBlau(i).Visible = SSTab1.Tab
  Next i
Picture1.ScaleMode = vbPixels
Picture1.AutoRedraw = True
SSTab1.Tab = Typ
ColorWahl(cInitial).BackColor = vbWhite:   Control cInitial
ColorWahl(cFinal).BackColor = vbBlue:    Control cFinal
imgpreview.Picture = Main.PicRes.Picture
File1.Path = FixPath(tPath)
Text4.Text = tPath
Call Check
End Sub

Private Sub ColorWahl_Click(Index As Integer)
Dim Color As Long

Color = ColorDialog(Me.hwnd, ColorWahl(Index).BackColor)
If Not Color = -1 Then ColorWahl(Index).BackColor = Color
Control Index
Call Check

End Sub

Private Sub load_Click()
Dim Dialog1 As New Dialog, MSer As String

MSer = Dialog1.OpenFile("Bilddateien (*.jpg;*.bmp;*.gif)|*.jpg;*.bmp;*.gif", Backup)
If MSer <> "" Then
Backup = MSer
DoEvents
imgpreview.Picture = LoadPicture(MSer)
End If
End Sub

Private Sub optDirection_Click(Index As Integer)
Call Check
End Sub

Private Sub optOrientation_Click(Index As Integer)
Call Check
End Sub


Private Sub scrollBlau_Change(Index As Integer)
If swSkipScroll Then Exit Sub
   
With arClr(Index)
 .Blau = ScrollBlau(Index).Value
 Text(Index) = Format(.Rot) & "," & Format(.Gr�n) & "," & Format(.Blau)
 ColorWahl(Index).BackColor = RGB(.Rot, .Gr�n, .Blau)
End With

Call Check
End Sub

Private Sub scrollGr�n_Change(Index As Integer)

If swSkipScroll Then Exit Sub
   
With arClr(Index)
 .Gr�n = ScrollGr�n(Index).Value
  Text(Index) = Format(.Rot) & "," & Format(.Gr�n) & "," & Format(.Blau)
  ColorWahl(Index).BackColor = RGB(.Rot, .Gr�n, .Blau)
End With
      
 Call Check
End Sub

Private Sub scrollRot_Change(Index As Integer)
If swSkipScroll Then Exit Sub
   
With arClr(Index)
 .Rot = ScrollRot(Index).Value
 Text(Index) = Format(.Rot) & "," & Format(.Gr�n) & "," & Format(.Blau)
 ColorWahl(Index).BackColor = RGB(.Rot, .Gr�n, .Blau)
End With
      
  Call Check
End Sub

Private Sub Control(sType As Integer)

   Dim arByte(3) As Byte
   
   CopyMemory arByte(0), ColorWahl(sType).BackColor, 4
   With arClr(sType)
      .Rot = arByte(0)
      .Gr�n = arByte(1)
      .Blau = arByte(2)
      
      Text(sType) = Format(.Rot) & "," & _
      Format(.Gr�n) & "," & Format(.Blau)
 
      swSkipScroll = True
     
      ScrollRot(sType).Value = .Rot
      ScrollGr�n(sType).Value = .Gr�n
      ScrollBlau(sType).Value = .Blau
      swSkipScroll = False
    End With
      
End Sub

Sub Check()
subShowGradient Picture1, optOrientation(0).Value, _
optDirection(0).Value, ColorWahl(cInitial).BackColor, _
ColorWahl(cFinal).BackColor
End Sub

Private Sub ScrollRot_Scroll(Index As Integer)
scrollRot_Change (Index)
End Sub

Private Sub ScrollGr�n_Scroll(Index As Integer)
scrollGr�n_Change (Index)
End Sub

Private Sub ScrollBlau_Scroll(Index As Integer)
scrollBlau_Change (Index)
End Sub

Private Sub File1_Click()
imgpreview.Picture = LoadPicture(FixPath(File1.Path) & File1.filename)
End Sub

Function FixPath(lzPath As String) As String
  FixPath = lzPath
  If Right(lzPath, 1) <> "\" Then FixPath = FixPath & "\"
End Function

Private Sub SSTab1_Click(PreviousTab As Integer)
Dim i As Long

  For i = 0 To 1
  ScrollGr�n(i).Visible = SSTab1.Tab
  ScrollRot(i).Visible = SSTab1.Tab
  ScrollBlau(i).Visible = SSTab1.Tab
  Next i
End Sub

Private Sub TypT_KeyPress(Index As Integer, KeyAscii As Integer)
Select Case KeyAscii: Case 48 To 57: Case 8, 45: Case Else: KeyAscii = 0: End Select
End Sub

Private Sub TypT_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 Then If Clipboard.GetFormat(vbCFText) Then If _
IsNumeric(Clipboard.GetText) = False Then TypT(Index).Locked = True Else TypT(Index).Locked = False
End Sub

Private Sub TypT_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
TypT(Index).Locked = False
End Sub

