Attribute VB_Name = "Verlauf"
Option Explicit

Public Declare Sub CopyMemory Lib "kernel32" Alias _
"RtlMoveMemory" (Destination As Any, _
Source As Any, ByVal Length As Long)

Private Declare Function GradientFill _
Lib "msimg32" (ByVal hDC As Long, _
ByRef pVertex As TRIVERTEX, _
ByVal dwNumVertex As Long, _
pMesh As Any, ByVal dwNumMesh As Long, _
ByVal dwMode As Long) As Integer

Private Declare Function BitBlt Lib "gdi32" ( _
  ByVal hDestDC As Long, _
  ByVal X As Long, ByVal Y As Long, _
  ByVal nWidth As Long, ByVal nHeight As Long, _
  ByVal hSrcDC As Long, _
  ByVal xSrc As Long, ByVal ySrc As Long, _
  ByVal dwRop As Long) As Long

Private Declare Function SetBkColor Lib "gdi32" ( _
  ByVal hDC As Long, _
  ByVal crColor As Long) As Long

Private Const SRCINVERT = &H660046
Private Const SRCAND = &H8800C6

Private Type TRIVERTEX
    X As Long
    Y As Long
    red As Integer
    green As Integer
    blue As Integer
    Alpha As Integer
End Type

Private Type GRADIENT_RECT
    UpperLeft As Long
    LowerRight As Long
End Type

Private Const GRADIENT_FILL_RECT_H = 0
Private Const GRADIENT_FILL_RECT_V = 1

Dim arVert(1) As TRIVERTEX
Dim gRect As GRADIENT_RECT


Public Sub subShowGradient(SPic As PictureBox, sOrientation As Long, sDirection As Long, sInitialColor As Long, sFinalColor As Long)
   Dim arByteClr(3) As Byte
   Dim arByteVert(7) As Byte
   Dim iOrientation As Long
   
    On Local Error Resume Next
    
   If sDirection Then
      arVert(0).X = 0: arVert(1).X = SPic.ScaleWidth
      arVert(0).Y = 0: arVert(1).Y = SPic.ScaleHeight
   Else
      arVert(0).X = SPic.ScaleWidth:  arVert(1).X = 0
      arVert(0).Y = SPic.ScaleHeight: arVert(1).Y = 0
      End If
   
   CopyMemory arByteClr(0), sInitialColor, 4
   arByteVert(1) = arByteClr(0)
   arByteVert(3) = arByteClr(1)
   arByteVert(5) = arByteClr(2)
   CopyMemory arVert(0).red, arByteVert(0), 8

   CopyMemory arByteClr(0), sFinalColor, 4
   arByteVert(1) = arByteClr(0)
   arByteVert(3) = arByteClr(1)
   arByteVert(5) = arByteClr(2)   '
   CopyMemory arVert(1).red, arByteVert(0), 8


   gRect.UpperLeft = 0
   gRect.LowerRight = 1
    
   iOrientation = IIf(sOrientation, GRADIENT_FILL_RECT_H, GRADIENT_FILL_RECT_V)
   GradientFill SPic.hDC, arVert(0), 2, gRect, 1, iOrientation
 
   SPic.Refresh
    On Error GoTo 0
End Sub



