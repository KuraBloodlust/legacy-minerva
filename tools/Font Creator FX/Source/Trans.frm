VERSION 5.00
Begin VB.Form Transt 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Transparenz"
   ClientHeight    =   1200
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4425
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   80
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   295
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Caption         =   "&Abbrechen"
      Height          =   300
      Left            =   2040
      TabIndex        =   4
      Top             =   840
      Width           =   1095
   End
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "&Speichern"
      Height          =   300
      Left            =   3240
      TabIndex        =   3
      Top             =   840
      Width           =   1095
   End
   Begin VB.HScrollBar Slide 
      Height          =   255
      LargeChange     =   100
      Left            =   240
      Max             =   249
      TabIndex        =   2
      Top             =   360
      Value           =   100
      Width           =   3255
   End
   Begin VB.Frame Frame1 
      Caption         =   "St�rke:"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4215
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "< 100 >"
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   3480
         TabIndex        =   1
         Top             =   240
         Width           =   735
      End
   End
End
Attribute VB_Name = "Transt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdExit_Click()
Tra = Slide.Value
Unload Me
DoEvents
If Main.List1.Selected(0) = True Then Call PreInit(Main.Combo1, Main.Prev)
End Sub

Private Sub Command1_Click()
Unload Me
End Sub

Private Sub Form_Load()
gHW = Me.hwnd: Hook
Slide.Value = Tra
End Sub

Private Sub Form_Unload(Cancel As Integer)
UnHook
End Sub

Private Sub Slide_Change()
Label3.Caption = "< " & Slide.Value + 1 & " >"
End Sub

Private Sub Slide_Scroll()
Label3.Caption = "< " & Slide.Value + 1 & " >"
End Sub
