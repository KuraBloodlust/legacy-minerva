VERSION 5.00
Begin VB.Form Main 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Font Creator FX"
   ClientHeight    =   4695
   ClientLeft      =   150
   ClientTop       =   540
   ClientWidth     =   7950
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   313
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   530
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox PicRes 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   1500
      Left            =   360
      Picture         =   "Form1.frx":0CCA
      ScaleHeight     =   1500
      ScaleWidth      =   1500
      TabIndex        =   21
      Top             =   4920
      Width           =   1500
   End
   Begin VB.PictureBox TexPic 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FF00FF&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   975
      Left            =   1920
      ScaleHeight     =   65
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   73
      TabIndex        =   20
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Speichern"
      Height          =   300
      Left            =   5280
      TabIndex        =   11
      Top             =   4320
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Wilkommen zum Font Creator FX"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4095
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   7695
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H80000008&
         Height          =   3495
         Left            =   3000
         ScaleHeight     =   3465
         ScaleWidth      =   4425
         TabIndex        =   3
         Top             =   360
         Width           =   4455
         Begin VB.CommandButton Command4 
            Caption         =   "�ndern"
            Height          =   300
            Left            =   3480
            TabIndex        =   17
            Top             =   3120
            Width           =   855
         End
         Begin VB.PictureBox Picture6 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   -8
            ScaleHeight     =   225
            ScaleWidth      =   4425
            TabIndex        =   16
            Top             =   2280
            Width           =   4455
            Begin VB.Image Image7 
               Height          =   540
               Left            =   0
               Picture         =   "Form1.frx":1919
               Top             =   0
               Width           =   1785
            End
         End
         Begin VB.TextBox Text1 
            Height          =   525
            Left            =   120
            MultiLine       =   -1  'True
            TabIndex        =   12
            Text            =   "Form1.frx":1C42
            Top             =   2880
            Width           =   3255
         End
         Begin VB.PictureBox Prev 
            Appearance      =   0  'Flat
            AutoRedraw      =   -1  'True
            BackColor       =   &H00FEFEFE&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   1575
            Left            =   480
            ScaleHeight     =   105
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   233
            TabIndex        =   9
            Top             =   360
            Width           =   3495
         End
         Begin VB.Label Lab 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Text:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   13
            Top             =   2640
            Width           =   495
         End
         Begin VB.Line Line6 
            X1              =   4305
            X2              =   4305
            Y1              =   120
            Y2              =   1790
         End
         Begin VB.Line Line5 
            X1              =   165
            X2              =   165
            Y1              =   600
            Y2              =   2150
         End
         Begin VB.Line Line4 
            X1              =   4370
            X2              =   720
            Y1              =   165
            Y2              =   165
         End
         Begin VB.Line Line1 
            X1              =   3805
            X2              =   105
            Y1              =   2085
            Y2              =   2085
         End
         Begin VB.Image Image1 
            Height          =   585
            Left            =   120
            Picture         =   "Form1.frx":1C61
            Top             =   120
            Width           =   645
         End
         Begin VB.Image Image2 
            Height          =   585
            Left            =   3720
            Picture         =   "Form1.frx":30BF
            Top             =   1560
            Width           =   645
         End
         Begin VB.Image Img 
            Height          =   3120
            Index           =   20
            Left            =   3120
            Picture         =   "Form1.frx":451D
            Top             =   1680
            Width           =   1785
         End
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H80000008&
         Height          =   3495
         Left            =   120
         ScaleHeight     =   3465
         ScaleWidth      =   2625
         TabIndex        =   2
         Top             =   360
         Width           =   2655
         Begin VB.CommandButton Command5 
            Caption         =   "Einstellungen"
            Enabled         =   0   'False
            Height          =   300
            Left            =   1320
            TabIndex        =   19
            Top             =   3120
            Width           =   1215
         End
         Begin VB.CommandButton Command3 
            Caption         =   "�bernehmen"
            Height          =   300
            Left            =   120
            TabIndex        =   18
            Top             =   3120
            Width           =   1095
         End
         Begin VB.ListBox List1 
            Height          =   1635
            ItemData        =   "Form1.frx":4AA1
            Left            =   120
            List            =   "Form1.frx":4ABA
            Style           =   1  'Checkbox
            TabIndex        =   15
            Top             =   1320
            Width           =   2415
         End
         Begin VB.ComboBox FSize 
            Height          =   315
            Left            =   840
            TabIndex        =   8
            Text            =   "12"
            ToolTipText     =   "Fontsize"
            Top             =   840
            Width           =   555
         End
         Begin VB.CheckBox Style 
            Caption         =   "F"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   1440
            Style           =   1  'Graphical
            TabIndex        =   7
            Top             =   840
            Width           =   375
         End
         Begin VB.CheckBox Style 
            Caption         =   "K"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   -1  'True
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   1800
            Style           =   1  'Graphical
            TabIndex        =   6
            Top             =   840
            Width           =   375
         End
         Begin VB.CheckBox Style 
            Caption         =   "U"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   5
            Top             =   840
            Width           =   375
         End
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   120
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   4
            Top             =   480
            Width           =   2415
         End
         Begin VB.Image Image3 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Height          =   300
            Left            =   120
            Top             =   840
            Width           =   615
         End
         Begin VB.Image Img 
            Height          =   3120
            Index           =   0
            Left            =   1560
            Picture         =   "Form1.frx":4B0D
            Top             =   1800
            Width           =   1785
         End
         Begin VB.Label Lab 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Einstellungen:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   71
            Left            =   120
            TabIndex        =   10
            Top             =   120
            Width           =   1455
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H80000008&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800000&
            FillStyle       =   0  'Solid
            Height          =   300
            Left            =   120
            Top             =   840
            Width           =   615
         End
      End
   End
   Begin VB.CommandButton Exita 
      Caption         =   "&Beenden"
      Height          =   300
      Left            =   6600
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Programm beenden"
      Top             =   4320
      Width           =   1215
   End
   Begin VB.Label Maus 
      Caption         =   "Font Creator FX � by R-PG Maker "
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   14
      Top             =   4320
      Width           =   4575
   End
   Begin VB.Menu data 
      Caption         =   "Datei"
      Begin VB.Menu save 
         Caption         =   "Speichern"
      End
      Begin VB.Menu dae 
         Caption         =   "-"
      End
      Begin VB.Menu quit 
         Caption         =   "Beenden"
      End
   End
   Begin VB.Menu data2 
      Caption         =   "Bearbeiten"
      Begin VB.Menu r 
         Caption         =   "Refresh"
         Index           =   1
      End
      Begin VB.Menu r 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu r 
         Caption         =   "Effekte"
         Index           =   3
         Begin VB.Menu s 
            Caption         =   "Verstreuen"
            Index           =   0
         End
         Begin VB.Menu s 
            Caption         =   "Sch�rfen"
            Index           =   1
         End
         Begin VB.Menu s 
            Caption         =   "�berbelichten"
            Index           =   2
         End
         Begin VB.Menu s 
            Caption         =   "Colorieren"
            Index           =   3
         End
         Begin VB.Menu s 
            Caption         =   "Graustufen"
            Index           =   4
         End
         Begin VB.Menu s 
            Caption         =   "Border"
            Index           =   5
         End
         Begin VB.Menu s 
            Caption         =   "Textur"
            Index           =   6
         End
      End
      Begin VB.Menu r 
         Caption         =   "Fett"
         Index           =   4
      End
   End
   Begin VB.Menu help 
      Caption         =   "?"
      Begin VB.Menu about 
         Caption         =   "�ber..."
      End
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub InitCommonControls Lib "comctl32" ()

Private Sub about_Click()
info.Show , Me
End Sub

Private Sub Form_Initialize()
    Call InitCommonControls
End Sub

Private Sub Form_Load()
  GetAllFontsCombo Me.hDC, Combo1
  Combo1.ListIndex = 0
  Call LoadSettings
  Call InitSize(FSize)

  T1 = 0
  T2 = 1
  CT1 = vbWhite
  CT2 = vbBlue
End Sub

Private Sub Combo1_Change()
If CheckDraw = True Then
  PixelsRead = False
  Call PreInit(Combo1, Prev)
End If
End Sub

Private Sub Combo1_Click()
  Call Combo1_Change
End Sub

Private Sub Command3_Click()
  Call PreInit(Combo1, Prev)
End Sub

Private Sub Command4_Click()
  Call PreInit(Combo1, Prev)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Call EndProg
End Sub

Private Sub FSize_Change()
  Call Tsize(FSize)
End Sub

Private Sub FSize_Click()
  Call FSize_Change
End Sub

Private Sub Command2_Click()
  Saves.Show vbModal
End Sub

Private Sub quit_Click()
Call EndProg
End Sub

Private Sub save_Click()
  Saves.Show vbModal
End Sub

Private Sub Style_Click(Index%)
  PixelsRead = False
  Call PreInit(Combo1, Prev)
  If Style(1).Value = 2 Then
  r(4).Checked = 0
  Else
  r(4).Checked = Style(1).Value
  End If
End Sub
Private Sub Image3_Click()
Dim Col As Long
Col = ColorDialog(Me.hwnd, Shape1.FillColor)
    If Col <> -1 Then
      If Col = vbWhite Then Col = &HFFFBFB
      Shape1.FillColor = Col
      Call PreInit(Combo1, Prev)
    End If
End Sub

Private Sub List1_Click()
  Select Case List1.ListIndex
   Case 3: Command5.Enabled = True
   Case 5: Command5.Enabled = True
   Case 6: Command5.Enabled = True
   Case Else: Command5.Enabled = False
  End Select
  s(List1.ListIndex).Checked = _
  List1.Selected(List1.ListIndex)
End Sub

Private Sub Command5_Click()
 Select Case List1.ListIndex
  Case 3: Wat = 1: Bord.Show vbModal
  Case 5: Wat = 2: Bord.Show vbModal
  Case 6: Stil1.Show vbModal
 End Select
End Sub

Private Sub Exita_Click()
Call EndProg
End Sub

Private Sub s_Click(Index As Integer)
  List1.Selected(Index) = Not s(Index).Checked
  List1.Refresh
  Call PreInit(Combo1, Prev)
End Sub

Private Sub r_Click(Index As Integer)
  Select Case Index
  Case 1: Call PreInit(Combo1, Prev)
  Case 4: Style(1).Value = (r(4).Checked + 1)
  End Select
End Sub

Private Sub EndProg()
  Dim i As Integer
  Call SaveSettings
  While Forms.Count > 1
  i = 0
  While Forms(i).Caption = Me.Caption
  i = i + 1
  Wend: Unload Forms(i)
  Wend: Unload Me
  End
End Sub
