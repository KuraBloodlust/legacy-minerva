VERSION 5.00
Begin VB.Form Bord 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Border"
   ClientHeight    =   825
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   1305
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   825
   ScaleWidth      =   1305
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "&Speichern"
      Height          =   300
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   1095
   End
   Begin VB.Image Image2 
      Height          =   270
      Left            =   840
      Picture         =   "Bord.frx":0000
      Top             =   120
      Visible         =   0   'False
      Width           =   300
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Farbe:"
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   130
      Width           =   450
   End
   Begin VB.Image Image1 
      Height          =   270
      Left            =   840
      Picture         =   "Bord.frx":007D
      Top             =   120
      Width           =   300
   End
   Begin VB.Shape Shape1 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00808000&
      FillStyle       =   0  'Solid
      Height          =   255
      Left            =   840
      Top             =   120
      Width           =   300
   End
End
Attribute VB_Name = "Bord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdExit_Click()
 If Wat = 1 Then
   Colft = Shape1.FillColor
   Unload Me
 
   If Main.List1.Selected(3) = True Then _
   Call PreInit(Main.Combo1, Main.Prev)
 Else
   Fcol = Shape1.FillColor
   Unload Me
 
   If Main.List1.Selected(5) = True Then _
   Call PreInit(Main.Combo1, Main.Prev)
 End If

End Sub

Private Sub Form_Load()
  If Wat = 1 Then
    Me.Caption = " Colorieren"
    Shape1.FillColor = Colft
    Else
    Me.Caption = " Border"
    Shape1.FillColor = Fcol
  End If
End Sub

Private Sub Image1_MouseDown(Button As Integer, _
Shift As Integer, X As Single, Y As Single)

Image2.Visible = True
Image1.Visible = False
End Sub

Private Sub Image1_MouseUp(Button As Integer, _
Shift As Integer, X As Single, Y As Single)

Dim Color As Long

Image1.Visible = True
Image2.Visible = False

Color = ColorDialog(Me.hwnd, Shape1.FillColor)
If Not Color = -1 Then Shape1.FillColor = Color
End Sub
