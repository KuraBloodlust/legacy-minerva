VERSION 5.00
Begin VB.Form Stil2 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " 3D-Text"
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3555
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   206
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   237
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.HScrollBar Flating 
      Height          =   195
      LargeChange     =   10
      Left            =   2040
      Max             =   24
      Min             =   -1
      TabIndex        =   10
      Top             =   480
      Value           =   9
      Width           =   1335
   End
   Begin VB.HScrollBar Color 
      Height          =   195
      Index           =   1
      LargeChange     =   10
      Left            =   240
      Max             =   255
      TabIndex        =   9
      Top             =   720
      Width           =   1455
   End
   Begin VB.HScrollBar Color 
      Height          =   195
      Index           =   2
      LargeChange     =   10
      Left            =   240
      Max             =   255
      TabIndex        =   8
      Top             =   960
      Width           =   1455
   End
   Begin VB.HScrollBar Color 
      Height          =   195
      Index           =   3
      LargeChange     =   10
      Left            =   240
      Max             =   255
      TabIndex        =   7
      Top             =   1200
      Width           =   1455
   End
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "&Speichern"
      Height          =   300
      Left            =   2400
      TabIndex        =   6
      Top             =   2760
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Abbrechen"
      Height          =   300
      Left            =   1200
      TabIndex        =   5
      Top             =   2760
      Width           =   1095
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   975
      Left            =   120
      ScaleHeight     =   63
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   223
      TabIndex        =   4
      Top             =   1680
      Width           =   3375
      Begin VB.Image Img 
         Height          =   3120
         Index           =   20
         Left            =   1680
         Picture         =   "Stil2.frx":0000
         Top             =   -480
         Width           =   1785
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tiefe:"
      Height          =   1455
      Left            =   1920
      TabIndex        =   2
      Top             =   120
      Width           =   1575
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "< 10 >"
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Farbe:"
      Height          =   1455
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1695
      Begin VB.Label ColorWahl 
         Appearance      =   0  'Flat
         BackColor       =   &H00FF0000&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1455
      End
   End
End
Attribute VB_Name = "Stil2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Type vRGB
   Rot As Byte
   Gr�n As Byte
   Blau As Byte
End Type
   
Dim Cont As vRGB

Private Sub cmdExit_Click()
Farb3(0) = Color(1).Value
Farb3(1) = Color(2).Value
Farb3(2) = Color(3).Value
Tief3 = Flating.Value - 1
Me.Visible = False
DoEvents
Call PreInit(Main.Combo1, Main.Prev)

End Sub

Private Sub Color_Change(Index%)
Call Check
End Sub

Private Sub Color_Scroll(Index%)
Call Check
End Sub

Private Sub ColorWahl_Click()
Dim Color As Long

Color = ColorDialog(Me.hwnd, ColorWahl.BackColor)
If Not Color = -1 Then ColorWahl.BackColor = Color
Call Control

Call Check

End Sub

Private Sub Command1_Click()
Me.Visible = False
End Sub

Private Sub Flating_Change()
Call Check
Label1.Caption = "< " & Flating.Value + 1 & " >"
End Sub

Private Sub Flating_Scroll()
Call Flating_Change
End Sub

Private Sub Form_Paint()
Call Check
End Sub


Private Sub Form_Load()
gHW = Me.hwnd: Hook
Call Control
Me.Show

End Sub

Private Sub Control()

   Dim aByte(3) As Byte
   
   CopyMemory aByte(0), ColorWahl.BackColor, 4
   With Cont
   .Rot = aByte(0)
   .Gr�n = aByte(1)
   .Blau = aByte(2)
      
   Color(1).Value = .Rot
   Color(2).Value = .Gr�n
   Color(3).Value = .Blau
   End With
      
End Sub

Sub Check()
ColorWahl.BackColor = RGB(Color(1), Color(2), Color(3))
Picture1.Picture = LoadPicture("")
DText Picture1, Main.Text1.Text, Flating.Value, Color(1).Value, _
Color(2).Value, Color(3).Value, , 12, Main.Shape1.FillColor
Picture1.Print Main.Text1.Text
End Sub

