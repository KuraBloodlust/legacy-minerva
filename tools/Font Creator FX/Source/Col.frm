VERSION 5.00
Begin VB.Form Col 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Colorieren"
   ClientHeight    =   840
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   1380
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   56
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   92
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "&Speichern"
      Height          =   300
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Farbe:"
      ForeColor       =   &H8000000D&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   180
      Width           =   615
   End
   Begin VB.Label ColorWahl 
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   720
      TabIndex        =   1
      Top             =   120
      Width           =   495
   End
End
Attribute VB_Name = "Col"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ColorWahl_Click()
Dim Color As Long

Color = ColorDialog(Me.hwnd, ColorWahl.BackColor)
If Not Color = -1 Then ColorWahl.BackColor = Color
End Sub

Private Sub Form_Load()
ColorWahl.BackColor = Colft
End Sub
