VERSION 5.00
Begin VB.Form frmPreview 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Preview"
   ClientHeight    =   1320
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   1665
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1320
   ScaleWidth      =   1665
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   200
      Left            =   0
      Top             =   0
   End
   Begin VB.PictureBox picPreview 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   1335
      Left            =   0
      ScaleHeight     =   1335
      ScaleWidth      =   1695
      TabIndex        =   0
      Top             =   0
      Width           =   1695
   End
End
Attribute VB_Name = "frmPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Timer1_Timer()

If AniCount > 0 Then

picPreview.Cls
TransparentBlt picPreview.hdc, 0, 0, frmMain.txtAW.Text, frmMain.txtAH.Text, MyDC, AniCol(CPreview).x, AniCol(CPreview).y, frmMain.txtAW.Text, frmMain.txtAH.Text, frmMain.picColor.BackColor

If CPreview < AniCount Then
CPreview = CPreview + 1
Else
CPreview = 1
End If

End If

End Sub
