VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Plugin"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public PlugHost As Object
Public WindowID As Long
Public Opened As Boolean

Public Function PluginListDblClick() As Object

AppPath = PlugHost.AppPath
CurProj = "projects\" & PlugHost.projectname
Set PluginListDblClick = frmMain

End Function

Public Function Init() As Boolean
Set Plg = Me

Init = True
End Function

Public Function GetName() As String
GetName = "r-pg Rip Helper Plugin"
End Function

Public Function GetCategory() As String
GetCategory = "Tools"
End Function

Public Function GetAutoResize() As Boolean
GetAutoResize = True
End Function

Public Function ShowFile(f As String) As Object
fn = f

AppPath = PlugHost.AppPath
CurProj = f
CurProj = Left(CurProj, InStrRev(CurProj, "\") - 1)
CurProj = Left(CurProj, InStrRev(CurProj, "\") - 1)

CurProj = Mid(CurProj, InStrRev(CurProj, "\") + 1)
CurProj = "projects\" & CurProj

QUIT = False

Main
Set ShowFile = frmMain
End Function

