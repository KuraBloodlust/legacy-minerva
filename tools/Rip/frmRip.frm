VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMain 
   BackColor       =   &H00000000&
   Caption         =   "R-PG Rip Helper V."
   ClientHeight    =   4740
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   6450
   Icon            =   "frmRip.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   316
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   430
   StartUpPosition =   3  'Windows Default
   Tag             =   "Rip Helper Plugin"
   Begin MSComctlLib.StatusBar Status 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   4485
      Width           =   6450
      _ExtentX        =   11377
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picClipboard2 
      BorderStyle     =   0  'None
      Height          =   6195
      Left            =   4560
      ScaleHeight     =   413
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   533
      TabIndex        =   3
      Top             =   1680
      Visible         =   0   'False
      Width           =   7995
   End
   Begin VB.PictureBox picClipboard 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   3780
      ScaleHeight     =   57
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   69
      TabIndex        =   2
      Top             =   480
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   5940
      Top             =   3240
   End
   Begin MSComctlLib.Toolbar tlb 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   6450
      _ExtentX        =   11377
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      ImageList       =   "imlDriveFileList2"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "open"
            Object.ToolTipText     =   "Import Image"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   4
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "paste"
            Object.ToolTipText     =   "Paste Image from Clipboard"
            ImageIndex      =   12
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "copy"
            Object.ToolTipText     =   "Copy Selection to Clipboard"
            ImageIndex      =   11
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "zin"
            Object.ToolTipText     =   "Zoom In"
            ImageIndex      =   13
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "zout"
            Object.ToolTipText     =   "Zoom Out"
            ImageIndex      =   13
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   1e-4
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "end"
            Object.ToolTipText     =   "Close Program"
            ImageIndex      =   14
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComDlg.CommonDialog cd1 
      Left            =   5700
      Top             =   3960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "Image Files (*.png *.gif *.bmp *.jpg)|*.png;*.gif;*.bmp;*.jpg"
   End
   Begin VB.PictureBox picEdit 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Height          =   2895
      Left            =   2880
      ScaleHeight     =   193
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   237
      TabIndex        =   0
      Top             =   1800
      Visible         =   0   'False
      Width           =   3555
      Begin VB.Shape shpCopy 
         BorderColor     =   &H000000FF&
         FillColor       =   &H00C0C0FF&
         Height          =   195
         Left            =   180
         Top             =   600
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Shape shpSelect 
         Height          =   240
         Left            =   0
         Top             =   0
         Width           =   240
      End
   End
   Begin MSComctlLib.ImageList imlDriveFileList2 
      Left            =   8700
      Top             =   2340
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":08E1
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":0C7B
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":1015
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":13AF
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":1749
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":1AE3
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":1E7D
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":2217
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":25B1
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":294B
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":2CE5
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":307F
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":3419
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRip.frx":37B3
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuImp 
         Caption         =   "&Import Image"
      End
      Begin VB.Menu Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEnd 
         Caption         =   "&End Program"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuCopy 
         Caption         =   "&Copy"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuPaste 
         Caption         =   "&Paste"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuZIN 
         Caption         =   "&Zoom In"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuZOUT 
         Caption         =   "&Zoom Out"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "&About"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private devil As New cDevIL
Private devilimage As New cDevILImage

Public MyDC As Long, mBitmap As Long

Private PicMove As Boolean
Private MoveX As Integer
Private MoveY As Integer

Private TileWidth As Integer
Private TileHeight As Integer

Private ImageWidth As Integer
Private ImageHeight As Integer

Private ZoomFaktor As Integer

Private GridAllocationX As Integer
Private GridAllocationY As Integer

Private SelectColor As Integer
Private SelectBack As Boolean

Private SelectOn As Boolean
Private SelectX As Integer
Private SelectY As Integer
Private SelectWidth As Integer
Private SelectHeight As Integer

Private Function DrawGrid()

For Y = (GridAllocationY * (TileHeight / 16)) + Snap((picEdit.Top * -1) + tlb.Height, TileHeight) To (GridAllocationY * (TileHeight / 16)) + (picEdit.Top * -1) + frmMain.ScaleHeight Step TileHeight

picEdit.Line (picEdit.Left * -1, Y)- _
(picEdit.Left * -1 + frmMain.ScaleWidth, Y), &H8000000C


Next Y

For X = (GridAllocationX * (TileWidth / 16)) + Snap((picEdit.Left * -1), TileWidth) To (GridAllocationX * (TileWidth / 16)) + (picEdit.Left * -1) + frmMain.ScaleWidth Step TileWidth

picEdit.Line (X, (picEdit.Top * -1) + tlb.Height)- _
(X, picEdit.Top * -1 + frmMain.ScaleHeight), &H8000000C

Next X

End Function


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyLeft Then
If Not GridAllocationX = 0 Then GridAllocationX = GridAllocationX - 1: Redraw
End If

If KeyCode = vbKeyRight Then
If Not GridAllocationX = 16 Then GridAllocationX = GridAllocationX + 1: Redraw
End If

If KeyCode = vbKeyUp Then
If Not GridAllocationY = 0 Then GridAllocationY = GridAllocationY - 1: Redraw
End If

If KeyCode = vbKeyDown Then
If Not GridAllocationY = 16 Then GridAllocationY = GridAllocationY + 1: Redraw
End If

If KeyCode = vbKeyDelete Then
Dim br As Long, pn As Long
pn = CreatePen(PS_NULL, 1, vbMagenta)
br = CreateSolidBrush(vbMagenta)
SelectObject MyDC, br
SelectObject MyDC, pn

Dim toX, toY
toX = Snap(SelectX, TileWidth) / (TileWidth / 16) + GridAllocationX
toY = Snap(SelectY, TileHeight) / (TileWidth / 16) + GridAllocationY

Rectangle MyDC, toX, toY, toX + (SelectWidth / (TileWidth / 16)) + 1, toY + (SelectHeight / (TileHeight / 16)) + 1
Redraw
End If


If (Shift And vbCtrlMask) > 0 Then

If KeyCode = vbKeyC Then
CallCopy
End If

If KeyCode = vbKeyV Then
CallPaste
End If
End If
End Sub

Private Sub Form_Load()
Me.Show
TileWidth = 16
TileHeight = 16
ZoomFaktor = 2
GridAllocationY = 0
GridAllocationX = 0
Me.Caption = "R-PG Rip Helper V." & App.Major & "." & App.Minor & "." & App.Revision

End Sub

Public Sub PluginResize()

End Sub

Public Sub PluginUnload()
Plg.PlugHost.windowclosed Plg
Unload Me
End Sub

Private Sub Form_Resize()
If Not MyDC = 0 Then
Redraw

If Me.ScaleWidth > picEdit.ScaleWidth + picEdit.Left Then
picEdit.Left = -picEdit.ScaleWidth + Me.ScaleWidth
End If

If Me.ScaleHeight > picEdit.ScaleHeight + picEdit.Top Then
picEdit.Top = -picEdit.ScaleHeight + Me.ScaleHeight
End If


If picEdit.ScaleWidth < Me.ScaleWidth Then
picEdit.Left = Me.ScaleWidth / 2 - picEdit.ScaleWidth / 2
Else
If picEdit.Left > 0 Then picEdit.Left = 0
End If

If picEdit.ScaleHeight < Me.ScaleHeight Then
picEdit.Top = Me.ScaleHeight / 2 - picEdit.ScaleHeight / 2
Else
If picEdit.Top > 0 Then picEdit.Top = 0
End If

Redraw
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
End
End Sub

Private Sub mnuAbout_Click()
MsgBox "R-PG Rip Helper" & vbCrLf _
& "2004 by SmokingFish" & vbCrLf _
& "mail@SmokingFish.de & vbcrlf" _
& "www.r-pg.net", vbInformation, "AniQ Editor"
End Sub

Private Sub mnuCopy_Click()
CallCopy
End Sub

Private Sub mnuEnd_Click()
Unload Me
End Sub

Private Sub mnuImp_Click()
OpenImage
End Sub

Private Sub mnuPaste_Click()
CallPaste
End Sub

Private Sub mnuZIN_Click()
ZoomIn
End Sub

Private Sub mnuZOUT_Click()
ZoomOut
End Sub

Private Sub picEdit_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Button = 4 Then

    If SelectWidth = 0 Then
    SelectX = Snap(X - (GridAllocationX * (TileWidth / 16)), TileWidth)
    SelectY = Snap(Y - (GridAllocationY * (TileHeight / 16)), TileHeight)
    SelectWidth = TileWidth
    SelectHeight = TileHeight
    End If
    
    MoveX = X
    MoveY = Y
End If

If Button = 2 Then
    PicMove = True
    MoveX = X
    MoveY = Y
End If

If Button = 1 Then
    SelectOn = True
    SelectX = Snap(X - (GridAllocationX * (TileWidth / 16)), TileWidth)
    SelectY = Snap(Y - (GridAllocationY * (TileHeight / 16)), TileHeight)
    SelectWidth = TileWidth
    SelectHeight = TileHeight
    
    shpCopy.Visible = True
    shpCopy.Move SelectX + (GridAllocationX * (TileWidth / 16)), SelectY + (GridAllocationY * (TileHeight / 16)), SelectWidth + 1, SelectHeight + 1
End If

End Sub

Private Sub picEdit_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If PicMove Then
    If Not picEdit.Left + X - MoveX > 0 Then
    If Not picEdit.Left + X - MoveX < Me.ScaleWidth - ImageWidth Then
        picEdit.Left = picEdit.Left + X - MoveX
    End If
    End If
    
    If Not picEdit.Top + Y - MoveY > tlb.Height Then
    If Not picEdit.Top + Y - MoveY < Me.ScaleHeight - ImageHeight Then
    picEdit.Top = picEdit.Top + Y - MoveY
    End If
    End If
    
End If

If SelectOn Then
    SelectWidth = Snap(X - (GridAllocationX * (TileWidth / 16)), TileWidth) - SelectX + TileWidth
    SelectHeight = Snap(Y - (GridAllocationY * (TileHeight / 16)), TileHeight) - SelectY + TileHeight
    
    If SelectWidth < TileWidth Then SelectWidth = TileWidth
    If SelectHeight < TileHeight Then SelectHeight = TileHeight
    
    If Not Button = 4 Then
    shpCopy.Move (SelectX + (GridAllocationX * (TileWidth / 16))), (SelectY + (GridAllocationY * (TileHeight / 16))), SelectWidth + 1, SelectHeight + 1
    End If
    
End If

If Not Button = 4 Then
shpSelect.Move Snap(X - (GridAllocationX * (TileWidth / 16)), TileWidth) + (GridAllocationX * (TileWidth / 16)), Snap(Y - (GridAllocationY * (TileHeight / 16)), TileHeight) + (GridAllocationY * (TileHeight / 16)), TileWidth + 1, TileHeight + 1
End If

If Button = 4 Then
    shpCopy.FillStyle = 7
    toX = Snap(X, TileWidth) + GridAllocationX / (TileWidth / 16)
    toY = Snap(Y, TileHeight) + GridAllocationY / (TileWidth / 16)
    toX = Snap(toX + SelectX - MoveX, TileWidth) + TileWidth + GridAllocationX * (TileWidth / 16)
    toY = Snap(toY + SelectY - MoveY, TileHeight) + TileHeight + GridAllocationY * (TileHeight / 16)
            
    shpCopy.Move toX, toY, SelectWidth + 1, SelectHeight + 1
End If

End Sub

Private Sub picEdit_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 4 Then

    Dim toX As Long, toY As Long
    toX = Snap(X, TileWidth) + GridAllocationX / (TileWidth / 16)
    toY = Snap(Y, TileHeight) + GridAllocationY / (TileHeight / 16)
    toX = Snap(toX + SelectX - MoveX, TileWidth) + TileWidth + GridAllocationX * (TileWidth / 16)
    toY = Snap(toY + SelectY - MoveY, TileHeight) + TileHeight + GridAllocationY * (TileHeight / 16)
    toX = toX / (TileWidth / 16)
    toY = toY / (TileHeight / 16)

      '  toX = Snap(X / (TileWidth / 16), TileWidth) + GridAllocationX
      '  toY = Snap(Y / (TileHeight / 16), TileHeight) + GridAllocationY
        
       ' toX = Snap(X - (GridAllocationX * (TileWidth / 16)), TileWidth) / (TileWidth / 16)
       ' toY = Snap(Y - (GridAllocationY * (TileHeight / 16)), TileHeight) / (TileHeight / 16)
        
    StretchBlt MyDC, toX, toY, (SelectWidth / (TileWidth / 16)), (SelectHeight / (TileHeight / 16)), MyDC, (SelectX / (TileWidth / 16)) + (GridAllocationX), (SelectY / (TileHeight / 16)) + (GridAllocationY), (SelectWidth / (TileWidth / 16)), (SelectHeight / (TileHeight / 16)), vbSrcCopy
    'StretchBlt MyDC, toX, toY, SelectWidth, SelectHeight, MyDC, SelectX + GridAllocationX, SelectY + GridAllocationY, SelectWidth, SelectHeight, SRCCOPY
    shpCopy.FillStyle = 1
Redraw
End If



If Button = 2 Then
    PicMove = False
    Redraw
End If
If Button = 1 Then
    SelectOn = False
End If
End Sub

Private Sub Timer1_Timer()

If SelectColor = 256 Then SelectBack = False
If SelectColor = 0 Then SelectBack = True

If SelectBack Then SelectColor = SelectColor + 2
If Not SelectBack Then SelectColor = SelectColor - 2

shpSelect.BorderColor = RGB(20, SelectColor, 200)
End Sub

Private Function OpenImage()
        cd1.ShowOpen
        
        If cd1.FileName = "" Then
        MsgBox "Error loading File , please try again", vbInformation, "Load File"
        Exit Function
        End If
        
        picEdit.Visible = True
        Set devilimage = devil.LoadImage(cd1.FileName)
        picEdit.Move 0, tlb.Height, devilimage.Width, devilimage.Height

        devilimage.RenderTo picEdit.hdc, 0, 0, 0, 0, devilimage.Width, devilimage.Height
        
        TileWidth = 16
        TileHeight = 16
        ImageWidth = devilimage.Width
        ImageHeight = devilimage.Height
        
        DrawGrid
    
        picEdit.Refresh

        MyDC = CreateCompatibleDC(GetDC(0))
        mBitmap = CreateCompatibleBitmap(GetDC(0), devilimage.Width, devilimage.Height)
        SelectObject MyDC, mBitmap
        
        tlb.Buttons(4).Enabled = True
        tlb.Buttons(5).Enabled = True
        tlb.Buttons(6).Enabled = True
        
        mnuCopy.Enabled = True
        mnuZIN.Enabled = True
        mnuZOUT.Enabled = True
        
        devilimage.RenderTo MyDC, 0, 0, 0, 0, devilimage.Width, devilimage.Height
        
        Form_Resize
End Function

Private Function ZoomIn()
If TileWidth = 8 Then Exit Function
    
    picEdit.Cls
    
    TileWidth = TileWidth / ZoomFaktor
    TileHeight = TileHeight / ZoomFaktor
    
    ImageWidth = ImageWidth / ZoomFaktor
    ImageHeight = ImageHeight / ZoomFaktor
    
    picEdit.Move 0, tlb.Height, ImageWidth, ImageHeight
    StretchBlt picEdit.hdc, 0, 0, ImageWidth, ImageHeight, MyDC, 0, 0, devilimage.Width, devilimage.Height, vbSrcCopy
        
    DrawGrid
    
    Form_Resize
End Function

Private Function ZoomOut()

    If TileWidth = 32 Then Exit Function
    
    picEdit.Cls
    
    TileWidth = TileWidth * ZoomFaktor
    TileHeight = TileHeight * ZoomFaktor
    
    ImageWidth = ImageWidth * ZoomFaktor
    ImageHeight = ImageHeight * ZoomFaktor
    
    picEdit.Move 0, tlb.Height, ImageWidth, ImageHeight
    StretchBlt picEdit.hdc, 0, 0, ImageWidth, ImageHeight, MyDC, 0, 0, devilimage.Width, devilimage.Height, vbSrcCopy
        
    DrawGrid
    
    Form_Resize
End Function

Private Sub tlb_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
    Case "end"
        Unload Me
    Case "open"
    
       OpenImage
    Case "copy"
    
        CallCopy
    Case "paste"
    
        CallPaste
    Case "zin"
        ZoomIn
    Case "zout"
        ZoomOut
    
End Select
End Sub

Private Function Redraw()
    'picEdit.Move 0, tlb.Height, ImageWidth, ImageHeight
    SelectX = 0
    SelectY = 0
    SelectWidth = 0
    SelectHeight = 0
    shpCopy.Visible = False
    
    
    picEdit.Cls
    StretchBlt picEdit.hdc, 0, 0, ImageWidth, ImageHeight, MyDC, 0, 0, devilimage.Width, devilimage.Height, vbSrcCopy
    DrawGrid
End Function

Private Function CallPaste()
picEdit.Visible = True
On Error GoTo errout
        
        If Clipboard.GetData = 0 Then GoTo errout
        
        SelectX = 0
        SelectY = 0
        SelectWidth = 0
        SelectHeight = 0
        shpCopy.Visible = False
        
        picEdit.Picture = Clipboard.GetData
        SavePicture picEdit.Picture, App.Path & "\temp.bmp"
        
        picEdit.Cls
        
        Set devilimage = devil.LoadImage(App.Path & "\temp.bmp")
        picEdit.Move 0, tlb.Height, devilimage.Width, devilimage.Height

        devilimage.RenderTo picEdit.hdc, 0, 0, 0, 0, devilimage.Width, devilimage.Height
        
        TileWidth = 16
        TileHeight = 16
        ImageWidth = devilimage.Width
        ImageHeight = devilimage.Height
        
        DrawGrid
    
        picEdit.Refresh

        MyDC = CreateCompatibleDC(GetDC(0))
        mBitmap = CreateCompatibleBitmap(GetDC(0), devilimage.Width, devilimage.Height)
        SelectObject MyDC, mBitmap

        devilimage.RenderTo MyDC, 0, 0, 0, 0, devilimage.Width, devilimage.Height
                
        Kill App.Path & "\temp.bmp"
        Exit Function
errout:
MsgBox "Error occured.", vbInformation, "Paste"

End Function

Private Function CallCopy()
        picClipboard.Width = SelectWidth / (TileWidth / 16)
        picClipboard.Height = SelectHeight / (TileHeight / 16)
        
        picClipboard2.Width = picClipboard.Width
        picClipboard2.Height = picClipboard.Height
        
        picClipboard.Cls
        picClipboard2.Cls
               
        BitBlt picClipboard.hdc, 0, 0, (SelectWidth / (TileWidth / 16)), (SelectHeight / (TileHeight / 16)), MyDC, (SelectX / (TileWidth / 16)) + (GridAllocationX), (SelectY / (TileHeight / 16)) + (GridAllocationY), vbSrcCopy
        SavePicture picClipboard.Image, App.Path & "\temp.bmp"
        
        picClipboard2.Visible = True
        picClipboard2.Picture = LoadPicture(App.Path & "\temp.bmp")
        Kill App.Path & "\temp.bmp"
                            
        Clipboard.Clear
        Clipboard.SetData picClipboard2.Picture
        picClipboard2.Visible = False
End Function
