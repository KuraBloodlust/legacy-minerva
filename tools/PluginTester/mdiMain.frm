VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.MDIForm mdiMain 
   BackColor       =   &H8000000C&
   Caption         =   "Minerva Plugin Tester | 2006 by semi"
   ClientHeight    =   7050
   ClientLeft      =   165
   ClientTop       =   480
   ClientWidth     =   10200
   Icon            =   "mdiMain.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'Bildschirmmitte
   WindowState     =   2  'Maximiert
   Begin MSComctlLib.StatusBar stb 
      Align           =   2  'Unten ausrichten
      Height          =   255
      Left            =   0
      TabIndex        =   8
      Top             =   6795
      Width           =   10200
      _ExtentX        =   17992
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picToolbar2 
      Align           =   1  'Oben ausrichten
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'Kein
      Height          =   1005
      Left            =   0
      ScaleHeight     =   1005
      ScaleWidth      =   10200
      TabIndex        =   3
      Top             =   0
      Width           =   10200
      Begin VB.CommandButton cmdQuickload 
         Caption         =   "Go"
         Height          =   255
         Left            =   5040
         TabIndex        =   6
         Top             =   120
         Width           =   735
      End
      Begin VB.TextBox txtQuickload 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   5
         Top             =   120
         Width           =   2535
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   120
         Picture         =   "mdiMain.frx":08CA
         Top             =   120
         Width           =   480
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   $"mdiMain.frx":1194
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   705
         TabIndex        =   7
         Top             =   480
         Width           =   8625
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Plugin Quickload:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   720
         TabIndex        =   4
         Top             =   120
         Width           =   1680
      End
   End
   Begin VB.PictureBox picToolbar 
      Align           =   3  'Links ausrichten
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'Kein
      Height          =   5790
      Left            =   0
      ScaleHeight     =   5790
      ScaleWidth      =   3045
      TabIndex        =   0
      Top             =   1005
      Width           =   3045
      Begin MSComctlLib.ImageList imlTrv 
         Left            =   480
         Top             =   3360
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   5
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "mdiMain.frx":127A
               Key             =   "plug"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "mdiMain.frx":13D4
               Key             =   "err"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "mdiMain.frx":1926
               Key             =   "fldrclosed"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "mdiMain.frx":1EC0
               Key             =   "fldropen"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "mdiMain.frx":245A
               Key             =   "file"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView lstPlugins 
         Height          =   2415
         Left            =   60
         TabIndex        =   2
         Top             =   45
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   4260
         _Version        =   393217
         Indentation     =   0
         Style           =   7
         ImageList       =   "imlTrv"
         Appearance      =   0
      End
      Begin VB.PictureBox picSizer 
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'Kein
         Height          =   7050
         Left            =   2040
         MousePointer    =   9  'Größenänderung W O
         ScaleHeight     =   7050
         ScaleWidth      =   30
         TabIndex        =   1
         Top             =   0
         Width           =   30
      End
   End
   Begin MSComDlg.CommonDialog cdl 
      Left            =   4320
      Top             =   1920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Menu mnuPlugin 
      Caption         =   "Plugin"
      Begin VB.Menu mnuPluginLoad 
         Caption         =   "Load Plugin..."
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "Open File (using Plugin as Editor)..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      Begin VB.Menu mnuHelpLimitations 
         Caption         =   "Known Limitations"
      End
      Begin VB.Menu mnuHelpDivider1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About"
      End
   End
End
Attribute VB_Name = "mdiMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public PH As New cEditorPlugHost

Dim p As String

Dim s_x As Integer

Private Sub cmdQuickload_Click()
If PH.TryQuickCreate(txtQuickload.Text) Then
    txtQuickload.Text = ""
    ListPlugins
Else
    MsgBox "Could not create Plugin.", vbCritical
End If

End Sub

Private Sub MDIForm_Load()
modLogging.Logging = True
modLogging.LogToFile = True
modLogging.LogToGUI = True
modLogging.InitLogging


p = UpOneFolder(UpOneFolder(App.Path))
p = p & "\Minerva\Plugins"

PH.Initialize p, Nothing

ListPlugins

SetFocusAPI txtQuickload.hwnd
End Sub

Private Sub MDIForm_Resize()
frmScriptLog.Move 0, Me.ScaleHeight - 15 * 150, Me.ScaleWidth, 15 * 150

End Sub

Private Sub mnuFileOpen_Click()
    cdl.DialogTitle = "Load Files"

    Dim Filter As String
    Dim FT As String
    Dim i As Integer, j As Integer
    Dim C As Collection

    Dim Instance As Object

    For i = 1 To PH.Plugins.Count
        Set Instance = PH.Plugins(i)
        Set C = Instance.IEditorPlugin_FileTypes()
    
        If C.Count > 0 Then
            Filter = Filter & Instance.IEditorPlugin_PluginName()
            FT = ""

            For j = 1 To C.Count
                FT = FT & "*." & C(j) & ";"
            Next j
            
            FT = Left$(FT, Len(FT) - 1)
            Filter = Filter & " (" & FT & ")|" & FT & "|"
        End If
    
    Next i

    If Filter = "" Then
        MsgBox "Please add Plugins that support File editing first!", vbCritical, "Cannot Open File without Editor"
        Exit Sub
    End If

    Filter = Left$(Filter, Len(Filter) - 1)

    cdl.Filter = Filter
    cdl.ShowOpen
    
    
    If cdl.FileName = "" Then Exit Sub
    
    PH.AttemptEdit cdl.FileName
End Sub

Private Sub mnuHelpAbout_Click()
MsgBox "Minerva Plugin tester was coded by semi of the R-PG team." & vbCrLf & vbCrLf & "See http://www.r-pg.net for more details.", vbInformation, "About"
End Sub

Private Sub mnuHelpLimitations_Click()
frmLimitations.Show vbModal
End Sub

Private Sub mnuPluginLoad_Click()
cdl.DialogTitle = "Load Plugin"
cdl.Filter = "Dynamic Link Libraries (*.dll)|*.dll"
cdl.FileName = p & "\*.dll"
cdl.ShowOpen

If cdl.FileName = "" Then Exit Sub

Dim fn As String
fn = cdl.FileName

Dim cn As String
cn = Mid$(fn, InStrRev(fn, "\") + 1)
cn = Replace$(cn, "MPlug_", "", , 1)
cn = Replace$(cn, ".dll", "", , 1)

PH.AddPluginByPath cdl.FileName, cn
ListPlugins
End Sub

Private Sub picSizer_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
If Button = 1 Then
    s_x = x
End If
End Sub

Private Sub picSizer_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
If Button = 1 Then
    picToolbar.Width = picToolbar.Width + (x - s_x)
    's_x = X
End If
End Sub

Private Sub picToolbar_Resize()
On Error Resume Next
lstPlugins.Move lstPlugins.Left, lstPlugins.Left, picToolbar.ScaleWidth - picSizer.Width - 2 * lstPlugins.Left, picToolbar.ScaleHeight - 2 * lstPlugins.Left
picSizer.Move picToolbar.ScaleWidth - picSizer.Width, 0, picSizer.Width, picToolbar.ScaleHeight
End Sub

Public Sub ListPlugins()
    lstPlugins.Nodes.Clear
    
    Dim x As Node
    Dim n As Node, m As Node



    If PH.Plugins.Count <= 0 Then
        Set n = lstPlugins.Nodes.Add(, , , "No Plugins loaded.", "err")
        Set n = lstPlugins.Nodes.Add(, , , "To Add Plugins", "err")
        Set lstPlugins.Nodes.Add(, , , "or the Quickload tool", "file").Parent = n
        Set lstPlugins.Nodes.Add(, , , "use the Plugin Menu", "file").Parent = n

    Else
        

        Dim i As Integer
        Dim j As Integer
        Dim p As Object
        Dim C As Collection
    
        For i = 1 To PH.Plugins.Count
    
            Set p = PH.Plugins(i)
            Set n = lstPlugins.Nodes.Add(, , , p.IEditorPlugin_PluginName, "plug")


                        
            Set C = p.IEditorPlugin_FileTypes()

            If C.Count > 0 Then
                Set m = lstPlugins.Nodes.Add(, , , "Regged File Types", "fldrclosed")
                m.ExpandedImage = "fldropen"
                Set m.Parent = n
                
                For j = 1 To C.Count
                    Set lstPlugins.Nodes.Add(, , , C(j), "file").Parent = m
                Next j
                 m.Expanded = False
   
            End If
            
            
            Set lstPlugins.Nodes.Add(, , , "ClassName: '" & PH.Plugins.Key(i) & "'", "file").Parent = n

        Next i

    End If

End Sub

Private Sub txtQuickload_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyReturn Then cmdQuickload_Click
End Sub
