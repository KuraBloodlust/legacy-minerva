Attribute VB_Name = "modPGlobal"
Option Explicit

Public Declare Function SetParentAPI Lib "user32" Alias "SetParent" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Function SetFocusAPI Lib "user32" Alias "SetFocus" (ByVal hwnd As Long) As Long




Public Function UpOneFolder(ByVal sPath As String) As String
Dim s As String
On Error Resume Next
  
  s$ = sPath$
  If Right$(s$, 1) = "\" Then s$ = Left(s$, Len(s$) - 1)
  s$ = Left(s, InStrRev(s$, "\") - 1)
  UpOneFolder$ = s$
End Function
