VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cEditorPlugHost"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Private m_PluginPath As String

Private m_VBEclipseCtl As Object

Private m_Plugins As CHive
Private m_PluginsByFileTypes As CHive


Public ReggedViews As CHive


Public Sub Log(Message As String)
    modLogging.Log Message, ltNone
End Sub

Public Function ProvideFile(FileName As String, usedTempFile As Boolean) As String
    ProvideFile = FileName
End Function

Public Function StoreFile(FileName As String, Content As String) As Boolean
    If FileExist(FileName) Then Kill FileName
    Open FileName For Binary Access Write As #1
    Put #1, , Content
    Close 1
    StoreFile = True
End Function



Public Property Get VBEclipseCtl() As Object
    Set VBEclipseCtl = m_VBEclipseCtl
End Property

Public Property Get Plugins() As CHive
    Set Plugins = m_Plugins
End Property

Public Property Get PluginsByFileTypes() As CHive
    Set PluginsByFileTypes = m_PluginsByFileTypes
End Property



Public Function Initialize(sPluginPath As String, oVBEclipseCtl As Object) As Boolean
    modLogging.Log "Initializing PlugHost...", ltAction
    modLogging.Log "Path: " & sPluginPath, ltInformation
    
    m_PluginPath = sPluginPath & "\"
    
    'If Not TypeOf oVBEclipseCtl Is ucPerspective Then
    '    modLogging.Log "Error initializing PlugHost: No VBEclipse Control referenced!", ltError
    '    Exit Function
    'End If
    
    
    Set m_VBEclipseCtl = oVBEclipseCtl
    
    
    Set m_PluginsByFileTypes = New CHive
    Set m_Plugins = New CHive
    Set ReggedViews = New CHive
    
    Initialize = True
End Function


Public Function AddPluginByPath(sPath As String, ClassName As String) As Boolean
    Dim s As String
    
    If Not FileExist(sPath) Then
        modLogging.Log "Could not find Plugin '" & sPath & "'!", ltError
        Exit Function
    End If
    
    Dim o As Object
    
    Set o = TryCreateObject("MPlug_" & ClassName & ".Plugin")
    
    If o Is Nothing Then
        RegisterDLL (sPath)
        Set o = TryCreateObject("MPlug_" & ClassName & ".Plugin")
    End If
    
    
    If o Is Nothing Then
        modLogging.Log "Could not create Plugin Object 'MPlug_" & ClassName & ".Plugin'!", ltError
        Exit Function
    End If
    
    If Not TypeName(o) = "Plugin" Then
        modLogging.Log "Warning: Plugin Type name mismatch.", ltAlert
    End If
    
    AddPluginByInstance o, ClassName
End Function

Private Function FileExist(sPath As String) As Boolean
On Error GoTo notfound

FileExist = (FileLen(sPath) > 0)

Exit Function
notfound:
FileExist = False
End Function


Private Function TryCreateObject(ClassName As String) As Object
    On Error GoTo errcreate
    
        Set TryCreateObject = CreateObject(ClassName)
    
    Exit Function
    
errcreate:
    Set TryCreateObject = Nothing
End Function


Public Function AddPluginByInstance(Instance As Object, ClassName As String) As Boolean
    modLogging.Log "Plugin '" & Instance.IEditorPlugin_PluginName & "' created successfully!", ltSuccess
    
    Instance.IEditorPlugin_Initialize Me
    
    m_Plugins.Add Instance, ClassName
    
    Dim C As Collection
    Set C = Instance.IEditorPlugin_FileTypes()
    
    Dim s As String
    
    Dim i As Integer
    For i = 1 To C.Count
        s = s & C(i) & " "
    
        If PluginForFileType(LCase(C(i))) Is Nothing Then
          m_PluginsByFileTypes.Add Instance, LCase(C(i))
        Else
          modLogging.Log "Double File type registration for " & C(i) & "!", ltAlert
        End If
        
    Next i
    
    modLogging.Log "Plugin supports filetypes: " & s, ltInformation
    
    Instance.IEditorPlugin_AddViews
    
End Function

Public Function PluginForFileType(filetype As String) As Object
    Dim i As Integer
    
    For i = 1 To m_PluginsByFileTypes.Count
        If m_PluginsByFileTypes.Key(i) = LCase$(filetype) Then
            Set PluginForFileType = m_PluginsByFileTypes(i)
            Exit For
        End If
    Next i
End Function


Private Function RegisterDLL(isPluginPath As String) As Boolean
On Error GoTo HandleError:

    modLogging.Log "Registering '" & isPluginPath & "'...", ltAction

    Dim lnResult As Double
    lnResult = Shell("regsvr32 """ & isPluginPath & """ /s")

HandleError:

End Function

Public Function AttemptEdit(ResourceURL As String) As Boolean
Dim FT As String
FT = GetFileType(ResourceURL)
Dim o As Object
Set o = PluginForFileType(FT)

If o Is Nothing Then
    MsgBox "Unfortunately, there is no editor registered for the file type '" & FT & "'." & vbCrLf & "Please make sure that the plugin editing this file type is properly installed.", vbInformation, "No Editor avaliable"
Else
    Dim f As Object
    Set f = o.IEditorPlugin_OpenFile(ResourceURL)
    
    f.Caption = "[E filename='" & ResourceURL & "'] " & f.Caption

    
    f.Show
    SetParentAPI f.hWnd, mdiMain.hWnd
    
    
    
    AttemptEdit = True
End If
End Function

Private Function GetFileType(sPath As String) As String
Dim p As Integer

p = InStrRev(sPath, ".")

If p = 0 Then Exit Function

GetFileType = Mid$(sPath, p + 1)
End Function

Public Function RegisterView(ViewId As String, _
                             Form As Object, _
                             Optional DesiredFolderID As String = "Plugins", _
                             Optional DesiredRel As Integer, _
                             Optional DesiredRatio As Double = 0.5, _
                             Optional DesiredRefId As String = "", _
                             Optional DesiredFloatX As Long = 100, _
                             Optional DesiredFloatY As Long = 100) As Boolean
    
    ReggedViews.Add Form
    
    Form.Show
    SetParentAPI Form.hWnd, mdiMain.hWnd
    
    Form.Caption = "[V FolderID='" & DesiredFolderID & "', rel=" & DesiredRel & ", ratio=" & DesiredRatio & ", RefId='" & DesiredRefId & "')] " & Form.Caption
    

End Function

Public Function TryQuickCreate(ClassName As String) As Boolean
Dim o As Object
Set o = TryCreateObject("MPlug_" & ClassName & ".Plugin")
    
    If o Is Nothing Then
        modLogging.Log "Could not create Plugin Object 'MPlug_" & ClassName & ".Plugin'!", ltError
        Exit Function
    End If
    
    If Not TypeName(o) = "Plugin" Then
        modLogging.Log "Warning: Plugin Type name mismatch.", ltAlert
    End If
    
    AddPluginByInstance o, ClassName
    
    
TryQuickCreate = True
End Function

Public Sub TriggerRegisterViews()
Dim i As Integer
For i = 1 To m_Plugins.Count
    m_Plugins(i).IEditorPlugin_AddViews
Next i
End Sub
