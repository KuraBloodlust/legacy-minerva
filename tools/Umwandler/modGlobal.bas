Attribute VB_Name = "modGlobal"
Public Result As New CMemoryDC
Public Files As New Collection
Public FileCount As Integer

Public Function GenerateImage()
    
    If Files.Count <= 0 Then Exit Function
    
    Dim Images() As New CMemoryDC
    ReDim Images(FileCount)
    Dim Png As New StdPNG
    
    Dim LastWidth As Integer
    Dim LastHeight As Integer
    
    For i = 1 To FileCount
        If LCase(Right(Files(i), 3)) = "png" Then
            Png.LoadFile Files(i)
            Images(i).Width = Png.Width
            Images(i).Height = Png.Height
            BitBlt Images(i).hDC, 0, 0, Png.Width, Png.Height, Png.hDC, 0, 0, vbSrcCopy
        Else
            Set Images(i).Picture = LoadPicture(Files(i))
        End If
        
        If i = 1 Then
            LastWidth = Images(i).Width
            LastHeight = Images(i).Height
        End If
        
        If LastWidth = Images(i).Width And LastHeight = Images(i).Height Then
                LastWidth = Images(i).Width
                LastHeight = Images(i).Height
            Else
                MsgBox "Resolution mismatch, canceling"
                Exit Function
        End If
        
    Next i
    
    Dim ImagesPerRow As Integer
    ImagesPerRow = Ceil(Sqr(FileCount), 0)
    
    Dim YRow As Integer
    Dim Counter As Integer
    
    Result.Width = ImagesPerRow * LastWidth
    Result.Height = Ceil((FileCount / ImagesPerRow), 0) * LastHeight
    
    For i = 1 To FileCount
        BitBlt Result.hDC, Counter * LastWidth, YRow * LastHeight, LastWidth, LastHeight, Images(i).hDC, 0, 0, vbSrcCopy
        Counter = Counter + 1
        If Counter = ImagesPerRow Then
            Counter = 0
            YRow = YRow + 1
        End If
    Next i
    
    frmResult.Show vbModal
    
End Function

Public Function Ceil(InValue As Double, InDecimal As Integer) As Double

    Dim lDblProcess As Double
    lDblProcess = InValue * (10 ^ InDecimal)
    If Int(lDblProcess) < lDblProcess Then
    lDblProcess = Int(lDblProcess) + 1
    Else
    lDblProcess = Int(lDblProcess)
    End If
    
    Ceil = lDblProcess / (10 ^ InDecimal)
    
End Function
