VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "AniQ Preprocessing Tool : Merge"
   ClientHeight    =   4890
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3735
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   326
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   249
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   750
      Left            =   0
      Picture         =   "frmMain.frx":08CA
      ScaleHeight     =   750
      ScaleWidth      =   3750
      TabIndex        =   12
      Top             =   0
      Width           =   3750
   End
   Begin VB.Frame Frame3 
      Caption         =   "Kram"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   6840
      TabIndex        =   10
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
      Begin VB.FileListBox File1 
         Height          =   480
         Left            =   720
         TabIndex        =   11
         Top             =   240
         Width           =   735
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   120
         Top             =   240
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   9
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMain.frx":9BEC
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMain.frx":A03E
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMain.frx":A490
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMain.frx":A8E2
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMain.frx":AD34
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMain.frx":B186
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMain.frx":B5D8
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMain.frx":BA2A
               Key             =   ""
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMain.frx":BE7C
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Animation Order"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      TabIndex        =   4
      Top             =   840
      Visible         =   0   'False
      Width           =   3540
      Begin VB.CommandButton Command3 
         Caption         =   "Back"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   1560
         TabIndex        =   13
         Top             =   3240
         Width           =   735
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Result !"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   2400
         TabIndex        =   7
         Top             =   3240
         Width           =   975
      End
      Begin VB.ListBox lst 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2955
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   3255
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   330
         Left            =   120
         TabIndex        =   6
         Top             =   3240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   582
         ButtonWidth     =   609
         ButtonHeight    =   582
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   4
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   6
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   5
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   1
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   9
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Source Images"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   3495
      Begin VB.CommandButton Command2 
         Caption         =   "Add"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2520
         TabIndex        =   9
         Top             =   240
         Width           =   855
      End
      Begin VB.ListBox lstFiles 
         Appearance      =   0  'Flat
         Height          =   1395
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   8
         Top             =   2280
         Width           =   3255
      End
      Begin VB.DriveListBox Drive1 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   2295
      End
      Begin VB.DirListBox Dir1 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1665
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   3255
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   4635
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   450
      Style           =   1
      SimpleText      =   "Ready"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    GenerateImage
End Sub

Private Sub Command2_Click()
        
    For i = 0 To lstFiles.ListCount - 1
        If lstFiles.Selected(i) Then
            lst.AddItem lstFiles.List(i)
            FileCount = FileCount + 1
            Files.Add Dir1.Path & "\" & lstFiles.List(i)
        End If
    Next i
    
    Frame1.Visible = False
    Frame2.Visible = True
        
End Sub

Private Sub Command3_Click()
    
    Frame2.Visible = False
    Frame1.Visible = True
    
End Sub

Private Sub Command4_Click()
cd1.ShowOpen
End Sub

Private Sub Dir1_Change()

    File1.Path = Dir1.Path
    
    lstFiles.Clear
    For i = 0 To File1.ListCount - 1
        lstFiles.AddItem File1.List(i)
    Next i
    
End Sub

Private Sub Drive1_Change()
    Dir1.Path = Drive1.Drive
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    If Button.Index = 3 Then
        If lst.ListIndex > -1 Then
            Files.Remove lst.ListIndex + 1
            lst.RemoveItem lst.ListIndex
            FileCount = FileCount - 1
        End If
    End If
    
    Dim tempEntry As String
    Dim lastIndex As Integer
                
    If Button.Index = 1 Then
        If Not lst.ListIndex = 0 Then
            If lst.ListIndex > -1 Then
                tempEntry = lst.List(lst.ListIndex)
                lastIndex = lst.ListIndex
                lst.RemoveItem lst.ListIndex
                lst.AddItem tempEntry, lastIndex - 1
                lst.ListIndex = lastIndex - 1
                
                tempEntry = Files(lastIndex + 1)
                lastIndex = lastIndex + 1
                Files.Remove lastIndex
                Files.Add tempEntry, , lastIndex - 1
            End If
        End If
    End If
    
    If Button.Index = 2 Then
        If Not lst.ListIndex = lst.ListCount - 1 Then
            If lst.ListIndex > -1 Then
                tempEntry = lst.List(lst.ListIndex)
                lastIndex = lst.ListIndex
                lst.RemoveItem lst.ListIndex
                lst.AddItem tempEntry, lastIndex + 1
                lst.ListIndex = lastIndex + 1
                
                tempEntry = Files(lastIndex + 1)
                lastIndex = lastIndex + 1
                Files.Remove lastIndex
                If lastIndex + 1 > Files.Count Then
                    Files.Add tempEntry
                Else
                    Files.Add tempEntry, , lastIndex + 1
                End If
            End If
        End If
    End If
    
    If Button.Index = 4 Then
        lst.Clear
        For i = Files.Count To 1 Step -1
            Files.Remove i
        Next i
        FileCount = 0
    End If
    
End Sub
