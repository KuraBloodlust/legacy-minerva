VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmResult 
   AutoRedraw      =   -1  'True
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Result (Double-Click to save)"
   ClientHeight    =   4005
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4950
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   267
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   330
   StartUpPosition =   1  'CenterOwner
   Begin MSComDlg.CommonDialog cmd 
      Left            =   120
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmResult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_DblClick()
    
    cmd.Filter = "Graphic Files (*.png)|*.png"
    cmd.DialogTitle = "Save Result"
    cmd.ShowSave
    
    If Not cmd.FileName = "" Then
    
        Dim Devil As New cDevIL
        Dim DevilImage As New cDevILImage
        
        If FileExists(cmd.FileName) Then
            Dim ret As Integer
            ret = MsgBox("Do you want to overwrite this Image?", vbYesNo, "Save")
            If ret = vbYes Then
                Kill cmd.FileName
            Else
                Exit Sub
            End If
        End If
        
        SavePicture Result.Picture, App.Path & "\temp.bmp"
        Set DevilImage = Devil.LoadImage(App.Path & "\temp.bmp")
        Devil.SaveImage DevilImage, cmd.FileName
        Kill App.Path & "\temp.bmp"
        
    End If
    
End Sub

Private Sub Form_Load()

    Me.Width = (Result.Width * Screen.TwipsPerPixelX) + (Me.Width - Me.ScaleWidth * Screen.TwipsPerPixelX)
    Me.Height = (Result.Height * Screen.TwipsPerPixelY) + (Me.Height - Me.ScaleHeight * Screen.TwipsPerPixelX)
    
    BitBlt Me.hDC, 0, 0, Result.Width, Result.Height, Result.hDC, 0, 0, vbSrcCopy
    Me.Refresh
End Sub

Public Function FileExists(ByVal Datei As String) As Boolean
  On Error GoTo fehler

  If Dir(Datei$) <> "" Then
    If FileLen(Datei$) > 0 Then
        FileExists = True
    End If
  End If

fehler:
End Function

