Attribute VB_Name = "modGlobal"
Public Function FileExists(FullFileName As String) As Boolean
    On Error GoTo Out
        Open FullFileName For Input As #1
        Close #1
        FileExists = True
    Exit Function
Out:
        FileExists = False
    Exit Function
End Function

Public Function DirExists(Dirname As String) As Boolean

    f$ = Dirname
    dirFolder = Dir(f$, vbDirectory)
    
    If dirFolder <> "" Then
        DirExists = True
    End If

End Function

