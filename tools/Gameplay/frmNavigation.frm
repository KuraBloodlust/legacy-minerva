VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form frmNavigation 
   Caption         =   "Navigation"
   ClientHeight    =   7845
   ClientLeft      =   7575
   ClientTop       =   7830
   ClientWidth     =   2280
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7845
   ScaleWidth      =   2280
   Begin MSComctlLib.TreeView trvNavigation 
      Height          =   6855
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   12091
      _Version        =   393217
      Style           =   7
      FullRowSelect   =   -1  'True
      Appearance      =   1
   End
   Begin MSForms.CommandButton cmdAdd 
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   7080
      Width           =   375
      Size            =   "661;661"
      Picture         =   "frmNavigation.frx":0000
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
   Begin MSForms.CommandButton cmdDelete 
      Height          =   375
      Left            =   600
      TabIndex        =   4
      Top             =   7080
      Width           =   375
      Size            =   "661;661"
      Picture         =   "frmNavigation.frx":0452
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
   Begin MSForms.TabStrip tabNavigation 
      Height          =   7815
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   2295
      ListIndex       =   0
      Size            =   "4048;13785"
      Items           =   "Tab1;Tab2;"
      TabOrientation  =   1
      TipStrings      =   ";;"
      Names           =   "Tab1;Tab2;"
      NewVersion      =   -1  'True
      TabsAllocated   =   2
      Tags            =   ";;"
      TabData         =   2
      Accelerator     =   ";;"
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
      TabState        =   "3;3"
   End
   Begin MSForms.CommandButton cmdMonster 
      Height          =   375
      Left            =   3600
      TabIndex        =   1
      Top             =   5760
      Width           =   375
      Size            =   "661;661"
      Picture         =   "frmNavigation.frx":08A4
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
   Begin MSForms.CommandButton cmdItems 
      Height          =   375
      Left            =   4080
      TabIndex        =   0
      Top             =   5760
      Width           =   375
      Size            =   "661;661"
      Picture         =   "frmNavigation.frx":0CF6
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
End
Attribute VB_Name = "frmNavigation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAdd_Click()
    frmAdd.Mode = tabNavigation.SelectedItem.Name
    frmAdd.Show vbModal
End Sub

Private Sub cmdDelete_Click()
    Dim Temp As Object
    
    Dim Node As MSComctlLib.Node
    
    If trvNavigation.SelectedItem Is Nothing Then Exit Sub
    Set Node = trvNavigation.SelectedItem
    
    If tabNavigation.SelectedItem.Name = "Monster" Then
        Set Temp = mdiMain.MonsterById(Node.Key)
        Temp.bDeleted = True
        Call trvNavigation.Nodes.Remove(Node.Key)
    ElseIf tabNavigation.SelectedItem.Name = "Items" Then
        Set Temp = mdiMain.ItemById(Node.Key)
        Temp.bDeleted = True
        Call trvNavigation.Nodes.Remove(Node.Key)
    ElseIf tabNavigation.SelectedItem.Name = "Techniken" Then
        Set Temp = mdiMain.TechnikById(Node.Key)
        Temp.bDeleted = True
        Call trvNavigation.Nodes.Remove(Node.Key)
    ElseIf tabNavigation.SelectedItem.Name = "Zauber" Then
        Set Temp = mdiMain.ZauberById(Node.Key)
        Temp.bDeleted = True
        Call trvNavigation.Nodes.Remove(Node.Key)
    End If
End Sub

Private Sub Form_Load()
    tabNavigation.Tabs.Clear
    tabNavigation.Tabs.Add "Monster", "Monster"
    tabNavigation.Tabs.Add "Items", "Items"
    tabNavigation.Tabs.Add "Techniken", "Techniken"
    tabNavigation.Tabs.Add "Zauber", "Zauber"
End Sub

Private Sub Form_Resize()
    trvNavigation.Height = frmNavigation.Height - 1440
    trvNavigation.Width = frmNavigation.Width - 360
    cmdAdd.Top = frmNavigation.Height - 1215
    cmdDelete.Top = frmNavigation.Height - 1215
    tabNavigation.Width = frmNavigation.Width - 105
    tabNavigation.Height = frmNavigation.Height - 480
End Sub

Private Sub Form_Unload(Cancel As Integer)
    mdiMain.mnuNavigation.Checked = False
End Sub

Private Sub tabNavigation_Change()
    If tabNavigation.SelectedItem.Name = "Monster" Then
        mdiMain.MonsterRefresh
    ElseIf tabNavigation.SelectedItem.Name = "Items" Then
        mdiMain.ItemsRefresh
    ElseIf tabNavigation.SelectedItem.Name = "Techniken" Then
        mdiMain.TechnikenRefresh
    ElseIf tabNavigation.SelectedItem.Name = "Zauber" Then
        mdiMain.ZauberRefresh
    End If
End Sub

Private Sub trvNavigation_DblClick()
    Dim Temp As Object
    Dim Form As Form
    Dim Node As MSComctlLib.Node
    
    If trvNavigation.SelectedItem Is Nothing Then Exit Sub
    Set Node = trvNavigation.SelectedItem
        
    If tabNavigation.SelectedItem.Name = "Monster" Then
        Set Form = New frmMonster
        Set Temp = mdiMain.MonsterById(Node.Key)
        Form.Show
        Call Form.Init(Temp)
    ElseIf tabNavigation.SelectedItem.Name = "Items" Then
        Set Form = New frmItem
        Set Temp = mdiMain.ItemById(Node.Key)
        Form.Show
        Call Form.Init(Temp)
    ElseIf tabNavigation.SelectedItem.Name = "Techniken" Then
        Set Form = New frmTechnik
        Set Temp = mdiMain.TechnikById(Node.Key)
        Form.Mode = "Techniken"
        Form.Show
        Call Form.Init(Temp)
    ElseIf tabNavigation.SelectedItem.Name = "Zauber" Then
        Set Form = New frmTechnik
        Set Temp = mdiMain.ZauberById(Node.Key)
        Form.Mode = "Zauber"
        Form.Show
        Call Form.Init(Temp)
    End If
End Sub


Private Sub trvNavigation_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        Call cmdDelete_Click
    End If
End Sub
