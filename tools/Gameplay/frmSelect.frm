VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form frmSelect 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Navigation"
   ClientHeight    =   7530
   ClientLeft      =   7560
   ClientTop       =   7815
   ClientWidth     =   2460
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7530
   ScaleWidth      =   2460
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Abbrechen"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   7080
      Width           =   975
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Height          =   375
      Left            =   1200
      TabIndex        =   3
      Top             =   7080
      Width           =   1095
   End
   Begin MSComctlLib.TreeView trvSelect 
      Height          =   6855
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   12091
      _Version        =   393217
      Style           =   7
      FullRowSelect   =   -1  'True
      Appearance      =   1
   End
   Begin MSForms.CommandButton cmdMonster 
      Height          =   375
      Left            =   3600
      TabIndex        =   1
      Top             =   5760
      Width           =   375
      Size            =   "661;661"
      Picture         =   "frmSelect.frx":0000
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
   Begin MSForms.CommandButton cmdItems 
      Height          =   375
      Left            =   4080
      TabIndex        =   0
      Top             =   5760
      Width           =   375
      Size            =   "661;661"
      Picture         =   "frmSelect.frx":0452
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
      ParagraphAlign  =   3
   End
End
Attribute VB_Name = "frmSelect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Mode As String
Public ParentForm As Form

Private Sub trvSelect_DblClick()
    Dim Temp As Object
    Dim Form As Form
    Dim Node As MSComctlLib.Node
    
    On Error GoTo Fehler
    Set Node = trvSelect.SelectedItem
        
    If Mode = "Items" Then
        ParentForm.lstItems.AddItem Node.Key
    ElseIf Mode = "Itemdrop" Then
        ParentForm.lstItemdrop.AddItem Node.Key
    ElseIf Mode = "Techniken" Then
        ParentForm.lstTechniken.AddItem Node.Key
    ElseIf Mode = "Zauber" Then
        ParentForm.lstZauber.AddItem Node.Key
    End If
    
    Unload Me
Fehler:
    Exit Sub
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    Call trvSelect_DblClick
End Sub

