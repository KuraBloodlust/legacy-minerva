VERSION 5.00
Begin VB.Form frmAdd 
   Caption         =   "Eindeutige ID angeben"
   ClientHeight    =   1125
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   3720
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   1125
   ScaleWidth      =   3720
   StartUpPosition =   3  'Windows-Standard
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Abbrechen"
      Height          =   375
      Left            =   2040
      TabIndex        =   3
      Top             =   720
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   600
      TabIndex        =   2
      Top             =   720
      Width           =   1215
   End
   Begin VB.TextBox txtID 
      Height          =   285
      Left            =   360
      TabIndex        =   1
      Top             =   360
      Width           =   3255
   End
   Begin VB.Label Label1 
      Caption         =   "Eindeutige ID angeben:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2055
   End
End
Attribute VB_Name = "frmAdd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Mode As String

Private Sub cmdCancel_Click()
    txtID.Text = ""
    Unload Me
End Sub

Private Sub cmdOK_Click()
    Me.Hide
    If Mode = "Monster" Then  'Monster Modus
        If mdiMain.MonsterExists(txtID.Text) Then
            MsgBox "ID Bereits vergeben"
            Exit Sub
        End If
        
        Call mdiMain.MonsterAdd(txtID.Text)
    ElseIf Mode = "Items" Then  'Item Modus
        If mdiMain.ItemExists(txtID.Text) Then
            MsgBox "ID Bereits vergeben"
            Exit Sub
        End If
        
        Call mdiMain.ItemAdd(txtID.Text)
    ElseIf Mode = "Techniken" Then  'Technik Modus
        If mdiMain.TechnikExists(txtID.Text) Then
            MsgBox "ID Bereits vergeben"
            Exit Sub
        End If
        
        Call mdiMain.TechnikAdd(txtID.Text)
    ElseIf Mode = "Zauber" Then  'Zauber Modus
        If mdiMain.ZauberExists(txtID.Text) Then
            MsgBox "ID Bereits vergeben"
            Exit Sub
        End If
        
        Call mdiMain.ZauberAdd(txtID.Text)
    End If
    txtID.Text = ""
    Unload Me
End Sub
