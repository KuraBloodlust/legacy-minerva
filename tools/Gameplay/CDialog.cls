VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const MAX_PATH = 260

Private Type OPENFILENAME
        lStructSize As Long
        hwndOwner As Long
        hInstance As Long
        lpstrFilter As String
        lpstrCustomFilter As Long
        nMaxCustFilter As Long
        nFilterIndex As Long
        lpstrFile As String
        nMaxFile As Long
        lpstrFileTitle As Long
        nMaxFileTitle As Long
        lpstrInitialDir As String
        lpstrTitle As Long
        Flags As Long
        nFileOffset As Integer
        nFileExtension As Integer
        lpstrDefExt As Long
        lCustData As Long
        lpfnHook As Long
        lpTemplateName As Long
End Type

Private Declare Function GetOpenFileName Lib "comdlg32.dll" Alias "GetOpenFileNameA" (pOpenfilename As OPENFILENAME) As Long
Private Declare Function GetSaveFileName Lib "comdlg32.dll" Alias "GetSaveFileNameA" (pOpenfilename As OPENFILENAME) As Long


Private Const OFN_READONLY = &H1
Private Const OFN_OVERWRITEPROMPT = &H2
Private Const OFN_HIDEREADONLY = &H4
Private Const OFN_NOCHANGEDIR = &H8
Private Const OFN_SHOWHELP = &H10
Private Const OFN_ENABLEHOOK = &H20
Private Const OFN_ENABLETEMPLATE = &H40
Private Const OFN_ENABLETEMPLATEHANDLE = &H80
Private Const OFN_NOVALIDATE = &H100
Private Const OFN_ALLOWMULTISELECT = &H200
Private Const OFN_EXTENSIONDIFFERENT = &H400
Private Const OFN_PATHMUSTEXIST = &H800
Private Const OFN_FILEMUSTEXIST = &H1000
Private Const OFN_CREATEPROMPT = &H2000
Private Const OFN_SHAREAWARE = &H4000
Private Const OFN_NOREADONLYRETURN = &H8000
Private Const OFN_NOTESTFILECREATE = &H10000
Private Const OFN_NONETWORKBUTTON = &H20000
Private Const OFN_NOLONGNAMES = &H40000                      '  force no long names for 4.x modules
Private Const OFN_EXPLORER = &H80000                         '  new look commdlg
Private Const OFN_NODEREFERENCELINKS = &H100000
Private Const OFN_LONGNAMES = &H200000                       '  force long names for 3.x modules

Private Const OFN_SHAREFALLTHROUGH = 2
Private Const OFN_SHARENOWARN = 1
Private Const OFN_SHAREWARN = 0


Public Function OpenFile(Optional filterstring, Optional verzeichnis) As String
Dim pOpenfilename As OPENFILENAME
Dim filename$, initdir$, filter$
Dim l As Long

    filename = Chr(0) + Space(MAX_PATH)
    
    If IsMissing(filterstring) Then
        filter = "Alle Dateien (*.*)|*.*"
    Else
        filter = filterstring
    End If
    If IsMissing(verzeichnis) Then
        initdir = CurDir
    Else
        initdir = verzeichnis
    End If
    
    If Right(filter, 1) <> "|" Then filter = filter & "|"
    For l = 1 To Len(filter)
        If Mid(filter, l, 1) = "|" Then Mid(filter, l, 1) = Chr(0)
    Next l
    filter = filter & Chr(0) & Chr(0)
    
    With pOpenfilename
        .lStructSize = Len(pOpenfilename)
        .hwndOwner = frmMain.hWnd
        .lpstrFile = filename
        .nMaxFile = MAX_PATH
        .lpstrFilter = filter
        .nFilterIndex = 1
        .lpstrInitialDir = initdir
        .Flags = OFN_EXPLORER Or OFN_PATHMUSTEXIST Or OFN_FILEMUSTEXIST
    End With
    If GetOpenFileName(pOpenfilename) <> 0 Then
        filename = pOpenfilename.lpstrFile
        OpenFile = Left(filename, InStr(filename, Chr(0)) - 1)
    Else
        OpenFile = ""
    End If
    
End Function


Public Function SaveFile(Optional dateiname, Optional filterstring, Optional verzeichnis) As String
Dim pOpenfilename As OPENFILENAME
Dim filename$, initdir$, filter$
Dim l As Long

    If IsMissing(dateiname) Then
        filename = Chr(0) + Space(MAX_PATH)
    Else
        filename = dateiname + Chr(0) + Space(MAX_PATH)
    End If
    
    If IsMissing(filterstring) Then
        filter = "Alle Dateien (*.*)|*.*"
    Else
        filter = filterstring
    End If
    If IsMissing(verzeichnis) Then
        initdir = CurDir
    Else
        initdir = verzeichnis
    End If
    
    If Right(filter, 1) <> "|" Then filter = filter & "|"
    For l = 1 To Len(filter)
        If Mid(filter, l, 1) = "|" Then Mid(filter, l, 1) = Chr(0)
    Next l
    filter = filter & Chr(0) & Chr(0)
    
    With pOpenfilename
        .lStructSize = Len(pOpenfilename)
        .hwndOwner = Screen.ActiveForm.hWnd
        .lpstrFile = filename
        .nMaxFile = MAX_PATH
        .lpstrFilter = filter
        .nFilterIndex = 1
        .lpstrInitialDir = initdir
        .Flags = OFN_EXPLORER
    End With
    If GetSaveFileName(pOpenfilename) <> 0 Then
        filename = pOpenfilename.lpstrFile
        SaveFile = Left(filename, InStr(filename, Chr(0)) - 1)
    Else
        SaveFile = ""
    End If
End Function




