VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.MDIForm mdiMain 
   BackColor       =   &H8000000C&
   Caption         =   "Phoenix Gameplay Studio"
   ClientHeight    =   8685
   ClientLeft      =   2235
   ClientTop       =   3090
   ClientWidth     =   10830
   LinkTopic       =   "MDIForm1"
   NegotiateToolbars=   0   'False
   ScrollBars      =   0   'False
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9600
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiMain.frx":0000
            Key             =   "imgOpen"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiMain.frx":0112
            Key             =   "imgSave"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiMain.frx":0224
            Key             =   "imgNew"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Oben ausrichten
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10830
      _ExtentX        =   19103
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "tlbNew"
            Object.ToolTipText     =   "Neu"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "tlbOpen"
            Object.ToolTipText     =   "�ffnen"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "tlbSave"
            Object.ToolTipText     =   "Speichern"
            ImageIndex      =   2
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog cdlFile 
      Left            =   9600
      Top             =   1680
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "R-PG Pak Files (*.rpak)|*.rpak"
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&Datei"
      Begin VB.Menu mnuNew 
         Caption         =   "&Neu"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuOpen 
         Caption         =   "�&ffnen..."
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Speichern..."
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSaveAs 
         Caption         =   "Speichern &unter..."
      End
      Begin VB.Menu mnuSeperator 
         Caption         =   "-"
      End
      Begin VB.Menu mnuQuit 
         Caption         =   "&Beenden"
      End
   End
   Begin VB.Menu mnuEinf�gen 
      Caption         =   "&Einf�gen"
      Begin VB.Menu mnuMonster 
         Caption         =   "&Monster"
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuItem 
         Caption         =   "&Item"
         Shortcut        =   ^I
      End
      Begin VB.Menu mnuTechnik 
         Caption         =   "&Technik"
         Shortcut        =   ^T
      End
      Begin VB.Menu mnuZauber 
         Caption         =   "&Zauber"
         Shortcut        =   ^Z
      End
   End
   Begin VB.Menu mnuWindow 
      Caption         =   "&Fenster"
      Begin VB.Menu mnuNavigation 
         Caption         =   "Navigation"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuConnect 
      Caption         =   "mnuConnect"
      Visible         =   0   'False
      Begin VB.Menu mnuAdd 
         Caption         =   "&Hinzuf�gen"
      End
      Begin VB.Menu mnuDelete 
         Caption         =   "&L�schen"
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "&Bearbeiten"
      End
   End
End
Attribute VB_Name = "mdiMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Monster() As CMonster
Private Items() As CItem
Private Techniken() As CTechnik
Private Zauber() As CTechnik
Public menuX, menuY As Single
Private CurrentFile, CurrentPass As String

' Ben�tigte API-Deklarationen
Private Declare Function PathFileExists Lib "shlwapi.dll" _
  Alias "PathFileExistsA" ( _
  ByVal pszPath As String) As Long
  
' Aufruf der API
Public Function IsFilePath(strPath As String) As Boolean
  IsFilePath = CBool(PathFileExists(strPath))
End Function

Private Sub MDIForm_Load()
    ReDim Monster(0) As CMonster
    ReDim Items(0) As CItem
    ReDim Techniken(0) As CTechnik
    ReDim Zauber(0) As CTechnik
    
    frmNavigation.Show
    frmNavigation.Top = 0
    frmNavigation.Left = 0
    frmNavigation.Height = mdiMain.Height - 1300
    frmNavigation.Width = 2415
End Sub

Private Sub mnuMonster_Click()
    frmAdd.Mode = "Monster"
    frmAdd.Show vbModal
End Sub

Private Sub mnuItem_Click()
    frmAdd.Mode = "Items"
    frmAdd.Show vbModal
End Sub

Private Sub mnuNew_Click()
    CurrentFile = ""
    CurrentPass = ""
    
    ReDim Monster(0) As CMonster
    ReDim Items(0) As CItem
    ReDim Techniken(0) As CTechnik
    ReDim Zauber(0) As CTechnik
    
    frmNavigation.trvNavigation.Nodes.Clear
End Sub

Private Sub mnuSave_Click()
    If CurrentFile = "" Then
        Call mnuSaveAs_Click
    Else
        Call Save(CurrentFile)
    End If
End Sub

Private Sub mnuSaveAs_Click()
    cdlFile.Filter = "R-PG Pak Files (*.rpak)|*.rpak"
    cdlFile.ShowSave

    If cdlFile.FileName = "" Then Exit Sub
    
    CurrentFile = cdlFile.FileName
    
    Call Save(CurrentFile)
End Sub

Private Sub mnuTechnik_Click()
    frmAdd.Mode = "Techniken"
    frmAdd.Show vbModal
End Sub

Private Sub mnuZauber_Click()
    frmAdd.Mode = "Zauber"
    frmAdd.Show vbModal
End Sub

Private Sub mnuNavigation_Click()
    If mnuNavigation.Checked Then
        frmNavigation.Hide
        mnuNavigation.Checked = False
    Else
        frmNavigation.Show
        frmNavigation.Top = 0
        frmNavigation.Left = 0
        frmNavigation.Height = mdiMain.Height - 1000
        frmNavigation.Width = 2415
        mnuNavigation.Checked = True
    End If
End Sub

Private Sub mnuOpen_Click()
    Dim Index, i As Integer
    Dim Tempstring As String
    Dim Lines() As String
    Dim CurrentFolder As cRPakFolder
    Dim FileName As String
    ReDim Monster(0) As CMonster
    ReDim Items(0) As CItem
    ReDim Techniken(0) As CTechnik
    ReDim Zauber(0) As CTechnik
    
    Dim pak As New cRPakArchive
    Dim pwd As String
    
    cdlFile.DialogTitle = "Open an RPAK Archive"
    cdlFile.ShowOpen

    If cdlFile.FileName = "" Then Exit Sub

    If Not pak.FileOpen(cdlFile.FileName, "") Then
    If pak.ErrDesc <> "Invalid password." Then MsgBox pak.ErrDesc, vbCritical, pak.ErrSource: Exit Sub
    
    CurrentPass = PasswordBox("Enter RPAK password:", True)
    
    If Not pak.FileOpen(cdlFile.FileName, CurrentPass) Then MsgBox pak.ErrDesc, vbCritical, pak.ErrSource: Exit Sub
    End If

    '--------------
    ' Monster
    '--------------
    frmNavigation.trvNavigation.Nodes.Clear
    Set CurrentFolder = pak.Root.GetFolder("Monster")
    ReDim Preserve Monster(CurrentFolder.FileCount) As CMonster
    
    For i = 1 To CurrentFolder.FileCount
        Tempstring = CurrentFolder.FileContent(i - 1)
        Lines = Split(Tempstring, vbNewLine)
        Set Monster(i) = New CMonster
        
        Monster(i).sID = Left(CurrentFolder.FileName(i - 1), Len(CurrentFolder.FileName(i - 1)) - 4)
        
        Monster(i).sName = Lines(0)
        
        Monster(i).sKlasse = Lines(1)
        Monster(i).nLevel = Lines(2)
        
        Monster(i).nStaerke = Lines(3)
        Monster(i).nStaerkeBonus = Lines(4)
        Monster(i).nWillenskraft = Lines(5)
        Monster(i).nWillenskraftBonus = Lines(6)
        Monster(i).nKonstitution = Lines(7)
        Monster(i).nKonstitutionBonus = Lines(8)
        Monster(i).nGeschicklichkeit = Lines(9)
        Monster(i).nGeschicklichkeitBonus = Lines(10)
        Monster(i).nSchnelligkeit = Lines(11)
        Monster(i).nSchnelligkeitBonus = Lines(12)
        
        Monster(i).sItems = Lines(13)
        Monster(i).sItemdrop = Lines(14)

        Monster(i).nExp = Lines(15)
        Monster(i).nGold = Lines(16)
        
        Monster(i).nUrsprungAffinitaet = Lines(17)
        Monster(i).nErdeAffinitaet = Lines(18)
        Monster(i).nHimmelAffinitaet = Lines(19)
        Monster(i).nFeuerAffinitaet = Lines(20)
        Monster(i).nKaelteAffinitaet = Lines(21)
        Monster(i).nMagiepotenzial = Lines(22)
        
        Monster(i).nUrsprungVwb = Lines(23)
        Monster(i).nErdeVwb = Lines(24)
        Monster(i).nHimmelVwb = Lines(25)
        Monster(i).nFeuerVwb = Lines(26)
        Monster(i).nKaelteVwb = Lines(27)
        
        Monster(i).sTechniken = Lines(28)
        Monster(i).sZauber = Lines(29)
        
        Monster(i).nAttacke = Lines(30)
        Monster(i).nVerteidigung = Lines(31)
        
        Monster(i).sBeschreibung = Replace(Lines(32), "\n", vbNewLine)
        
        Monster(i).nHP = Lines(33)
        Monster(i).nMP = Lines(34)
        
        If frmNavigation.tabNavigation.SelectedItem.Name = "Monster" Then
            frmNavigation.trvNavigation.Nodes.Add , , Monster(i).sID, Monster(i).sName
        End If
    Next
    
    '--------------
    ' Items
    '--------------
    Set CurrentFolder = pak.Root.GetFolder("Items")
    ReDim Preserve Items(CurrentFolder.FileCount) As CItem
    
    For i = 1 To CurrentFolder.FileCount
        Tempstring = CurrentFolder.FileContent(i - 1)
        Lines = Split(Tempstring, vbNewLine)
        Set Items(i) = New CItem
        
        Items(i).sID = Left(CurrentFolder.FileName(i - 1), Len(CurrentFolder.FileName(i - 1)) - 4)
        
        Items(i).sName = Lines(0)
        
        Items(i).nWert = Lines(1)
        
        Items(i).sTechniken = Lines(2)
        Items(i).sZauber = Lines(3)
        
        Items(i).nAttacke = Lines(4)
        Items(i).nVerteidigung = Lines(5)
        
        Items(i).sWirkung = Lines(6)
        
        Items(i).nZiel = Lines(7)
        
        Items(i).sBeschreibung = Replace(Lines(8), "\n", vbNewLine)
        
        If frmNavigation.tabNavigation.SelectedItem.Name = "Items" Then
            frmNavigation.trvNavigation.Nodes.Add , , Items(i).sID, Items(i).sName
        End If
    Next
    
    '--------------
    ' Techniken
    '--------------
    Set CurrentFolder = pak.Root.GetFolder("Techniken")
    ReDim Preserve Techniken(CurrentFolder.FileCount) As CTechnik
    
    For i = 1 To CurrentFolder.FileCount
        Tempstring = CurrentFolder.FileContent(i - 1)
        Lines = Split(Tempstring, vbNewLine)
        Set Techniken(i) = New CTechnik
        
        Techniken(i).sID = Left(CurrentFolder.FileName(i - 1), Len(CurrentFolder.FileName(i - 1)) - 4)
        
        Techniken(i).sName = Lines(0)
        
        Techniken(i).nSchaden = Lines(1)
        Techniken(i).sWirkung = Lines(2)
        
        Techniken(i).nZiel = Lines(3)
        Techniken(i).nElement = Lines(4)
        Techniken(i).nStatus = Lines(5)
        
        Techniken(i).nMana = Lines(6)
        
        Techniken(i).sBeschreibung = Replace(Lines(7), "\n", vbNewLine)
        
        If frmNavigation.tabNavigation.SelectedItem.Name = "Techniken" Then
            frmNavigation.trvNavigation.Nodes.Add , , Techniken(i).sID, Techniken(i).sName
        End If
    Next
    
    '--------------
    ' Zauber
    '--------------
    Set CurrentFolder = pak.Root.GetFolder("Zauber")
    ReDim Preserve Zauber(CurrentFolder.FileCount) As CTechnik
    
    For i = 1 To CurrentFolder.FileCount
        Tempstring = CurrentFolder.FileContent(i - 1)
        Lines = Split(Tempstring, vbNewLine)
        Set Zauber(i) = New CTechnik
        
        Zauber(i).sID = Left(CurrentFolder.FileName(i - 1), Len(CurrentFolder.FileName(i - 1)) - 4)
        
        Zauber(i).sName = Lines(0)
        
        Zauber(i).nSchaden = Lines(1)
        Zauber(i).sWirkung = Lines(2)
        
        Zauber(i).nZiel = Lines(3)
        Zauber(i).nElement = Lines(4)
        Zauber(i).nStatus = Lines(5)
        
        Zauber(i).nMana = Lines(6)
        
        Zauber(i).sBeschreibung = Replace(Lines(7), "\n", vbNewLine)
        
        If frmNavigation.tabNavigation.SelectedItem.Name = "Zauber" Then
            frmNavigation.trvNavigation.Nodes.Add , , Zauber(i).sID, Zauber(i).sName
        End If
    Next
End Sub

Public Function MonsterById(sID As String) As CMonster
    Dim Index As Integer
    
    For Index = 1 To UBound(Monster)
        If Monster(Index).sID = sID And Monster(Index).bDeleted = False Then Set MonsterById = Monster(Index)
    Next
End Function

Public Function MonsterExists(sID As String) As Boolean
    Dim Index As Integer
    
    For Index = 1 To UBound(Monster)
        If Monster(Index).sID = sID And Monster(Index).bDeleted = False Then MonsterExists = True
    Next
End Function

Public Sub MonsterRefresh()
    Dim Index As Integer
    Call frmNavigation.trvNavigation.Nodes.Clear
    For Index = 1 To UBound(Monster)
        If Monster(Index).bDeleted = False Then
            Call frmNavigation.trvNavigation.Nodes.Add(, , Monster(Index).sID, Monster(Index).sName)
        End If
    Next
End Sub

Public Sub MonsterAdd(sID As String)
    Dim Form As frmMonster
    Dim Index As Integer
    
    Index = UBound(Monster) + 1
    ReDim Preserve Monster(Index) As CMonster
    Set Monster(Index) = New CMonster
    
    Monster(Index).sID = sID
    Monster(Index).sName = sID
    If frmNavigation.tabNavigation.SelectedItem.Name = "Monster" Then
        frmNavigation.trvNavigation.Nodes.Add , , Monster(Index).sID, Monster(Index).sName
    End If
    
    Set Form = New frmMonster
    Call Form.Init(Monster(Index))
    Form.Show
End Sub


Public Sub ItemsRefresh()
    Dim Index As Integer
    Call frmNavigation.trvNavigation.Nodes.Clear
    For Index = 1 To UBound(Items)
        If Items(Index).bDeleted = False Then
            Call frmNavigation.trvNavigation.Nodes.Add(, , Items(Index).sID, Items(Index).sName)
        End If
    Next
End Sub

Public Sub ItemsSelecter()
    Dim Index As Integer
    Call frmSelect.trvSelect.Nodes.Clear
    For Index = 1 To UBound(Items)
        If Items(Index).bDeleted = False Then
            Call frmSelect.trvSelect.Nodes.Add(, , Items(Index).sID, Items(Index).sName)
        End If
    Next
End Sub

Public Function ItemById(sID As String) As CItem
    Dim Index As Integer
    
    For Index = 1 To UBound(Items)
        If Items(Index).sID = sID And Items(Index).bDeleted = False Then Set ItemById = Items(Index)
    Next
End Function

Public Function ItemExists(sID As String) As Boolean
    Dim Index As Integer
    
    For Index = 1 To UBound(Items)
        If Items(Index).sID = sID And Items(Index).bDeleted = False Then ItemExists = True
    Next
End Function

Public Sub ItemAdd(sID As String)
    Dim Form As frmItem
    Dim Index As Integer
    
    Index = UBound(Items) + 1
    ReDim Preserve Items(Index) As CItem
    Set Items(Index) = New CItem
    
    Items(Index).sID = sID
    Items(Index).sName = sID
    If frmNavigation.tabNavigation.SelectedItem.Name = "Items" Then
        frmNavigation.trvNavigation.Nodes.Add , , Items(Index).sID, Items(Index).sName
    End If
    
    Set Form = New frmItem
    Call Form.Init(Items(Index))
    Form.Show
End Sub

Public Sub TechnikenRefresh()
    Dim Index As Integer
    Call frmNavigation.trvNavigation.Nodes.Clear
    For Index = 1 To UBound(Techniken)
        If Techniken(Index).bDeleted = False Then
            Call frmNavigation.trvNavigation.Nodes.Add(, , Techniken(Index).sID, Techniken(Index).sName)
        End If
    Next
End Sub

Public Sub TechnikenSelecter()
    Dim Index As Integer
    Call frmSelect.trvSelect.Nodes.Clear
    For Index = 1 To UBound(Techniken)
        If Techniken(Index).bDeleted = False Then
            Call frmSelect.trvSelect.Nodes.Add(, , Techniken(Index).sID, Techniken(Index).sName)
        End If
    Next
End Sub

Public Function TechnikById(sID As String) As CTechnik
    Dim Index As Integer
    
    For Index = 1 To UBound(Techniken)
        If Techniken(Index).sID = sID And Techniken(Index).bDeleted = False Then Set TechnikById = Techniken(Index)
    Next
End Function

Public Function TechnikExists(sID As String) As Boolean
    Dim Index As Integer
    
    For Index = 1 To UBound(Techniken)
        If Techniken(Index).sID = sID And Techniken(Index).bDeleted = False Then TechnikExists = True
    Next
End Function

Public Sub TechnikAdd(sID As String)
    Dim Form As frmTechnik
    Dim Index As Integer
    
    Index = UBound(Techniken) + 1
    ReDim Preserve Techniken(Index) As CTechnik
    Set Techniken(Index) = New CTechnik
    
    Techniken(Index).sID = sID
    Techniken(Index).sName = sID
    If frmNavigation.tabNavigation.SelectedItem.Name = "Techniken" Then
        frmNavigation.trvNavigation.Nodes.Add , , Techniken(Index).sID, Techniken(Index).sName
    End If
    
    Set Form = New frmTechnik
    Form.Mode = "Techniken"
    Call Form.Init(Techniken(Index))
    Form.Show
End Sub

Public Sub ZauberRefresh()
    Dim Index As Integer
    Call frmNavigation.trvNavigation.Nodes.Clear
    For Index = 1 To UBound(Zauber)
        If Zauber(Index).bDeleted = False Then
            Call frmNavigation.trvNavigation.Nodes.Add(, , Zauber(Index).sID, Zauber(Index).sName)
        End If
    Next
End Sub

Public Sub ZauberSelecter()
    Dim Index As Integer
    Call frmSelect.trvSelect.Nodes.Clear
    For Index = 1 To UBound(Zauber)
        If Zauber(Index).bDeleted = False Then
            Call frmSelect.trvSelect.Nodes.Add(, , Zauber(Index).sID, Zauber(Index).sName)
        End If
    Next
End Sub

Public Function ZauberById(sID As String) As CTechnik
    Dim Index As Integer
    
    For Index = 1 To UBound(Zauber)
        If Zauber(Index).sID = sID And Zauber(Index).bDeleted = False Then Set ZauberById = Zauber(Index)
    Next
End Function

Public Function ZauberExists(sID As String) As Boolean
    Dim Index As Integer
    
    For Index = 1 To UBound(Zauber)
        If Zauber(Index).sID = sID And Zauber(Index).bDeleted = False Then ZauberExists = True
    Next
End Function

Public Sub ZauberAdd(sID As String)
    Dim Form As frmTechnik
    Dim Index As Integer
    
    Index = UBound(Zauber) + 1
    ReDim Preserve Zauber(Index) As CTechnik
    Set Zauber(Index) = New CTechnik
    
    Zauber(Index).sID = sID
    Zauber(Index).sName = sID
    If frmNavigation.tabNavigation.SelectedItem.Name = "Zauber" Then
        frmNavigation.trvNavigation.Nodes.Add , , Zauber(Index).sID, Zauber(Index).sName
    End If
    
    Set Form = New frmTechnik
    Form.Mode = "Techniken"
    Call Form.Init(Zauber(Index))
    Form.Show
End Sub

Private Sub Save(ByVal File As String)
    Dim Index As Integer
    Dim Data As String
    
    Dim pak As New cRPakArchive

    Dim CurrentFolder As cRPakFolder
    
    If CurrentPass = "" Then
      CurrentPass = PasswordBox("Please specify a password:", False)
      If RetOk = 0 Then Exit Sub
    End If

    pak.FileNew File, CurrentPass

    pak.Root.AddFolder "Monster"
    Set CurrentFolder = pak.Root.GetFolder("Monster")
    
    For Index = 1 To UBound(Monster)
        If Not Monster(Index).bDeleted Then
            Data = Monster(Index).sName & vbNewLine
            Data = Data & Monster(Index).sKlasse & vbNewLine
            Data = Data & Monster(Index).nLevel & vbNewLine
        
            Data = Data & Monster(Index).nStaerke & vbNewLine
            Data = Data & Monster(Index).nStaerkeBonus & vbNewLine
            Data = Data & Monster(Index).nWillenskraft & vbNewLine
            Data = Data & Monster(Index).nWillenskraftBonus & vbNewLine
            Data = Data & Monster(Index).nKonstitution & vbNewLine
            Data = Data & Monster(Index).nKonstitutionBonus & vbNewLine
            Data = Data & Monster(Index).nGeschicklichkeit & vbNewLine
            Data = Data & Monster(Index).nGeschicklichkeitBonus & vbNewLine
            Data = Data & Monster(Index).nSchnelligkeit & vbNewLine
            Data = Data & Monster(Index).nSchnelligkeitBonus & vbNewLine
        
            Data = Data & Monster(Index).sItems & vbNewLine
            Data = Data & Monster(Index).sItemdrop & vbNewLine
            Data = Data & Monster(Index).nExp & vbNewLine
            Data = Data & Monster(Index).nGold & vbNewLine
        
            Data = Data & Monster(Index).nUrsprungAffinitaet & vbNewLine
            Data = Data & Monster(Index).nErdeAffinitaet & vbNewLine
            Data = Data & Monster(Index).nHimmelAffinitaet & vbNewLine
            Data = Data & Monster(Index).nFeuerAffinitaet & vbNewLine
            Data = Data & Monster(Index).nKaelteAffinitaet & vbNewLine
            Data = Data & Monster(Index).nMagiepotenzial & vbNewLine
        
            Data = Data & Monster(Index).nUrsprungVwb & vbNewLine
            Data = Data & Monster(Index).nErdeVwb & vbNewLine
            Data = Data & Monster(Index).nHimmelVwb & vbNewLine
            Data = Data & Monster(Index).nFeuerVwb & vbNewLine
            Data = Data & Monster(Index).nKaelteVwb & vbNewLine
        
            Data = Data & Monster(Index).sTechniken & vbNewLine
            Data = Data & Monster(Index).sZauber & vbNewLine
            
            Data = Data & Monster(Index).nAttacke & vbNewLine
            Data = Data & Monster(Index).nVerteidigung & vbNewLine
            
            Data = Data & Replace(Monster(Index).sBeschreibung, vbNewLine, "\n") & vbNewLine
            
            Data = Data & Monster(Index).nHP & vbNewLine
            Data = Data & Monster(Index).nMP
            
            Call CurrentFolder.AddFile(Monster(Index).sID & ".mst", Data)
        End If
    Next
    
    pak.Root.AddFolder "Items"
    Set CurrentFolder = pak.Root.GetFolder("Items")
    
    For Index = 1 To UBound(Items)
        If Not Items(Index).bDeleted Then
            Data = Items(Index).sName & vbNewLine
            Data = Data & Items(Index).nWert & vbNewLine
            Data = Data & Items(Index).sTechniken & vbNewLine
            Data = Data & Items(Index).sZauber & vbNewLine
            Data = Data & Items(Index).nAttacke & vbNewLine
            Data = Data & Items(Index).nVerteidigung & vbNewLine
            Data = Data & Items(Index).sWirkung & vbNewLine
            Data = Data & Items(Index).nZiel & vbNewLine
            
            Data = Data & Replace(Items(Index).sBeschreibung, vbNewLine, "\n") & vbNewLine
                        
            Call CurrentFolder.AddFile(Items(Index).sID & ".itm", Data)
        End If
    Next
    
    pak.Root.AddFolder "Techniken"
    Set CurrentFolder = pak.Root.GetFolder("Techniken")
    
    For Index = 1 To UBound(Techniken)
        If Not Techniken(Index).bDeleted Then
            Data = Techniken(Index).sName & vbNewLine
            Data = Data & Techniken(Index).nSchaden & vbNewLine
            Data = Data & Techniken(Index).sWirkung & vbNewLine
            Data = Data & Techniken(Index).nZiel & vbNewLine
            Data = Data & Techniken(Index).nElement & vbNewLine
            Data = Data & Techniken(Index).nStatus & vbNewLine
            Data = Data & Techniken(Index).nMana & vbNewLine
            
            Data = Data & Replace(Techniken(Index).sBeschreibung, vbNewLine, "\n") & vbNewLine
            
            Call CurrentFolder.AddFile(Techniken(Index).sID & ".tec", Data)
        End If
    Next
    
    pak.Root.AddFolder "Zauber"
    Set CurrentFolder = pak.Root.GetFolder("Zauber")
    
    If Not IsFilePath(App.Path & "\Zauber") Then MkDir App.Path & "\Zauber"
    For Index = 1 To UBound(Zauber)
        If Not Zauber(Index).bDeleted Then
            Data = Zauber(Index).sName & vbNewLine
            Data = Data & Zauber(Index).nSchaden & vbNewLine
            Data = Data & Zauber(Index).sWirkung & vbNewLine
            Data = Data & Zauber(Index).nZiel & vbNewLine
            Data = Data & Zauber(Index).nElement & vbNewLine
            Data = Data & Zauber(Index).nStatus & vbNewLine
            Data = Data & Zauber(Index).nMana & vbNewLine
            
            Data = Data & Replace(Zauber(Index).sBeschreibung, vbNewLine, "\n") & vbNewLine
            
            Call CurrentFolder.AddFile(Zauber(Index).sID & ".zau", Data)
        End If
    Next
    
    pak.FileSave File
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    If Button.Key = "tlbOpen" Then
        Call mnuOpen_Click
    ElseIf Button.Key = "tlbSave" Then
        Call mnuSave_Click
    ElseIf Button.Key = "tlbNew" Then
        Call mnuNew_Click
    End If
End Sub

Public Sub mnuAdd_Click()
    If frmSelect.Mode = "Items" Or frmSelect.Mode = "Itemdrop" Then
        Call ItemsSelecter
    ElseIf frmSelect.Mode = "Techniken" Then
        Call TechnikenSelecter
    ElseIf frmSelect.Mode = "Zauber" Then
        Call ZauberSelecter
    End If
    
    frmSelect.Show vbModal
End Sub

Public Sub mnuDelete_Click()
    If frmSelect.Mode = "Items" Then
        frmSelect.ParentForm.lstItems.RemoveItem (frmSelect.ParentForm.lstItems.ListIndex)
    ElseIf frmSelect.Mode = "Itemdrop" Then
        frmSelect.ParentForm.lstItemdrop.RemoveItem (frmSelect.ParentForm.lstItemdrop.ListIndex)
    ElseIf frmSelect.Mode = "Techniken" Then
        frmSelect.ParentForm.lstTechniken.RemoveItem (frmSelect.ParentForm.lstTechniken.ListIndex)
    ElseIf frmSelect.Mode = "Zauber" Then
        frmSelect.ParentForm.lstZauber.RemoveItem (frmSelect.ParentForm.lstZauber.ListIndex)
    End If
End Sub

Private Sub mnuEdit_Click()
    Dim Temp As Object
    Dim Form As Form
    
        
    If frmSelect.Mode = "Items" Then
        Set Form = New frmMonster
        Set Temp = mdiMain.ItemById(frmSelect.ParentForm.lstItems.List(frmSelect.ParentForm.lstItems.ListIndex))
        Form.Show
        Call Form.Init(Temp)
    ElseIf frmSelect.Mode = "Itemdrop" Then
        Set Form = New frmItem
        Set Temp = mdiMain.ItemById(frmSelect.ParentForm.lstItemdrop.List(frmSelect.ParentForm.lstItemdrop.ListIndex))
        Form.Show
        Call Form.Init(Temp)
    ElseIf frmSelect.Mode = "Techniken" Then
        Set Form = New frmTechnik
        Set Temp = mdiMain.TechnikById(frmSelect.ParentForm.lstTechniken.List(frmSelect.ParentForm.lstTechniken.ListIndex))
        Form.Mode = "Techniken"
        Form.Show
        Call Form.Init(Temp)
    ElseIf frmSelect.Mode = "Zauber" Then
        Set Form = New frmItem
        Set Temp = mdiMain.ZauberById(frmSelect.ParentForm.lstZauber.List(frmSelect.ParentForm.lstZauber.ListIndex))
        Form.Mode = "Zauber"
        Form.Show
        Call Form.Init(Temp)
    End If
End Sub

