Attribute VB_Name = "modPakEditor"
Option Explicit

Public RetPassword As String
Public RetOk As Integer

'Looks whether a file exists
Public Function FileExists(ByVal Datei As String) As Boolean
  On Error GoTo fehler

  If Dir(Datei$) <> "" Then
    If FileLen(Datei$) > 0 Then
        FileExists = True
    End If
  End If

fehler:
End Function

Public Function PasswordBox(Prompt As String, Optional MaskPassword As Boolean = True) As String
frmPassword.lblPrompt.Caption = Prompt
frmPassword.chkMask.Value = IIf(MaskPassword, 1, 0)
frmPassword.txtPassword.PasswordChar = IIf(MaskPassword, "*", "")

frmPassword.Show vbModal

If RetOk = 1 Then PasswordBox = RetPassword
End Function
