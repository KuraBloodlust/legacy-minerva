VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form frmMonster 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Monster"
   ClientHeight    =   7665
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   9210
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7665
   ScaleWidth      =   9210
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtHP 
      Height          =   285
      Left            =   240
      TabIndex        =   64
      Top             =   3000
      Width           =   855
   End
   Begin VB.TextBox txtMP 
      Height          =   285
      Left            =   1440
      TabIndex        =   63
      Top             =   3000
      Width           =   855
   End
   Begin VB.TextBox txtBeschreibung 
      Height          =   1575
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertikal
      TabIndex        =   62
      Top             =   5520
      Width           =   8895
   End
   Begin VB.TextBox txtMagiepotenzial 
      Height          =   285
      Left            =   7080
      TabIndex        =   59
      Top             =   2400
      Width           =   855
   End
   Begin VB.TextBox txtVerteidigung 
      Height          =   285
      Left            =   3720
      TabIndex        =   57
      Top             =   2400
      Width           =   855
   End
   Begin VB.TextBox txtAttacke 
      Height          =   285
      Left            =   2520
      TabIndex        =   55
      Top             =   2400
      Width           =   855
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   3120
      TabIndex        =   54
      Top             =   7200
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Abbrechen"
      Height          =   375
      Left            =   4800
      TabIndex        =   53
      Top             =   7200
      Width           =   1455
   End
   Begin VB.ListBox lstTechniken 
      Height          =   1425
      Left            =   4800
      TabIndex        =   50
      Top             =   3600
      Width           =   2055
   End
   Begin VB.ListBox lstZauber 
      Height          =   1425
      Left            =   7080
      TabIndex        =   49
      Top             =   3600
      Width           =   2055
   End
   Begin VB.TextBox txtKaelteVwb 
      Height          =   285
      Left            =   8280
      TabIndex        =   48
      Top             =   1800
      Width           =   855
   End
   Begin VB.TextBox txtKaelteAffinitaet 
      Height          =   285
      Left            =   7080
      TabIndex        =   47
      Top             =   1800
      Width           =   855
   End
   Begin VB.TextBox txtFeuerVwb 
      Height          =   285
      Left            =   8280
      TabIndex        =   45
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtFeuerAffinitaet 
      Height          =   285
      Left            =   7080
      TabIndex        =   44
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtHimmelVwb 
      Height          =   285
      Left            =   6000
      TabIndex        =   42
      Top             =   2400
      Width           =   855
   End
   Begin VB.TextBox txtHimmelAffinitaet 
      Height          =   285
      Left            =   4800
      TabIndex        =   41
      Top             =   2400
      Width           =   855
   End
   Begin VB.TextBox txtErdeVwb 
      Height          =   285
      Left            =   6000
      TabIndex        =   39
      Top             =   1800
      Width           =   855
   End
   Begin VB.TextBox txtErdeAffinitaet 
      Height          =   285
      Left            =   4800
      TabIndex        =   38
      Top             =   1800
      Width           =   855
   End
   Begin VB.TextBox txtUrsprungVwb 
      Height          =   285
      Left            =   6000
      TabIndex        =   36
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtUrsprungAffinitaet 
      Height          =   285
      Left            =   4800
      TabIndex        =   35
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtGold 
      Height          =   285
      Left            =   7080
      TabIndex        =   33
      Top             =   360
      Width           =   2055
   End
   Begin VB.TextBox txtExp 
      Height          =   285
      Left            =   6000
      TabIndex        =   31
      Top             =   360
      Width           =   855
   End
   Begin VB.ListBox lstItemdrop 
      Height          =   1425
      Left            =   2520
      TabIndex        =   29
      Top             =   3600
      Width           =   2055
   End
   Begin VB.ListBox lstItems 
      Height          =   1425
      Left            =   240
      TabIndex        =   27
      Top             =   3600
      Width           =   2055
   End
   Begin VB.TextBox txtSchnelligkeit 
      Height          =   285
      Left            =   2520
      TabIndex        =   23
      Top             =   1800
      Width           =   855
   End
   Begin VB.TextBox txtSchnelligkeitBonus 
      Height          =   285
      Left            =   3720
      TabIndex        =   22
      Top             =   1800
      Width           =   855
   End
   Begin VB.TextBox txtGeschicklichkeitBonus 
      Height          =   285
      Left            =   3720
      TabIndex        =   18
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtGeschicklichkeit 
      Height          =   285
      Left            =   2520
      TabIndex        =   17
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtKonstitutionBonus 
      Height          =   285
      Left            =   1440
      TabIndex        =   15
      Top             =   2400
      Width           =   855
   End
   Begin VB.TextBox txtKonstitution 
      Height          =   285
      Left            =   240
      TabIndex        =   14
      Top             =   2400
      Width           =   855
   End
   Begin VB.TextBox txtWillenskraftBonus 
      Height          =   285
      Left            =   1440
      TabIndex        =   12
      Top             =   1800
      Width           =   855
   End
   Begin VB.TextBox txtWillenskraft 
      Height          =   285
      Left            =   240
      TabIndex        =   10
      Top             =   1800
      Width           =   855
   End
   Begin VB.TextBox txtStaerkeBonus 
      Height          =   285
      Left            =   1440
      TabIndex        =   8
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtStaerke 
      Height          =   285
      Left            =   240
      TabIndex        =   7
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtLevel 
      Height          =   285
      Left            =   4800
      TabIndex        =   5
      Top             =   360
      Width           =   855
   End
   Begin VB.TextBox txtKlasse 
      Height          =   285
      Left            =   2520
      TabIndex        =   3
      Top             =   360
      Width           =   2055
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   240
      TabIndex        =   1
      Top             =   360
      Width           =   2055
   End
   Begin VB.Label Label30 
      Caption         =   "HP:"
      Height          =   255
      Left            =   120
      TabIndex        =   66
      Top             =   2760
      Width           =   975
   End
   Begin VB.Label Label29 
      Caption         =   "MP:"
      Height          =   255
      Left            =   1320
      TabIndex        =   65
      Top             =   2760
      Width           =   975
   End
   Begin VB.Label Label28 
      Caption         =   "Beschreibung:"
      Height          =   255
      Left            =   120
      TabIndex        =   61
      Top             =   5280
      Width           =   1695
   End
   Begin VB.Label Label23 
      Caption         =   "Magiepotenzial:"
      Height          =   255
      Left            =   6960
      TabIndex        =   60
      Top             =   2160
      Width           =   1335
   End
   Begin VB.Label Label27 
      Caption         =   "Verteidigung:"
      Height          =   255
      Left            =   3600
      TabIndex        =   58
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label Label26 
      Caption         =   "Attacke:"
      Height          =   255
      Left            =   2400
      TabIndex        =   56
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label Label25 
      Caption         =   "Techniken:"
      Height          =   255
      Left            =   4680
      TabIndex        =   52
      Top             =   3360
      Width           =   975
   End
   Begin VB.Label Label24 
      Caption         =   "Zauber:"
      Height          =   255
      Left            =   6960
      TabIndex        =   51
      Top             =   3360
      Width           =   1335
   End
   Begin VB.Label Label22 
      Caption         =   "K�lte (Affinit�t/Vwb)"
      Height          =   255
      Left            =   6960
      TabIndex        =   46
      Top             =   1560
      Width           =   2175
   End
   Begin VB.Label Label21 
      Caption         =   "Feuer (Affinit�t/Vwb)"
      Height          =   255
      Left            =   6960
      TabIndex        =   43
      Top             =   960
      Width           =   2175
   End
   Begin VB.Label Label20 
      Caption         =   "Himmel (Affinit�t/Vwb)"
      Height          =   255
      Left            =   4680
      TabIndex        =   40
      Top             =   2160
      Width           =   2175
   End
   Begin VB.Label Label19 
      Caption         =   "Erde (Affinit�t/Vwb)"
      Height          =   255
      Left            =   4680
      TabIndex        =   37
      Top             =   1560
      Width           =   2175
   End
   Begin VB.Label Label18 
      Caption         =   "Ursprung (Affinit�t/Vwb)"
      Height          =   255
      Left            =   4680
      TabIndex        =   34
      Top             =   960
      Width           =   2175
   End
   Begin VB.Label Label17 
      Caption         =   "Gold:"
      Height          =   255
      Left            =   6960
      TabIndex        =   32
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label16 
      Caption         =   "Erfahrung:"
      Height          =   255
      Left            =   5880
      TabIndex        =   30
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label15 
      Caption         =   "Itemdrop:"
      Height          =   255
      Left            =   2400
      TabIndex        =   28
      Top             =   3360
      Width           =   1335
   End
   Begin VB.Label Label14 
      Caption         =   "Items:"
      Height          =   255
      Left            =   120
      TabIndex        =   26
      Top             =   3360
      Width           =   975
   End
   Begin VB.Label Label13 
      Alignment       =   2  'Zentriert
      Caption         =   "+"
      Height          =   255
      Left            =   3360
      TabIndex        =   25
      Top             =   1800
      Width           =   375
   End
   Begin VB.Label Label12 
      Caption         =   "Schnelligkeit:"
      Height          =   255
      Left            =   2400
      TabIndex        =   24
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Zentriert
      Caption         =   "+"
      Height          =   255
      Left            =   3360
      TabIndex        =   21
      Top             =   1200
      Width           =   375
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Zentriert
      Caption         =   "+"
      Height          =   255
      Left            =   1080
      TabIndex        =   20
      Top             =   2400
      Width           =   375
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Zentriert
      Caption         =   "+"
      Height          =   255
      Left            =   1080
      TabIndex        =   19
      Top             =   1200
      Width           =   375
   End
   Begin VB.Label Label10 
      Caption         =   "Geschicklichkeit:"
      Height          =   255
      Left            =   2400
      TabIndex        =   16
      Top             =   960
      Width           =   1215
   End
   Begin VB.Label Label8 
      Caption         =   "Konstitution:"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Zentriert
      Caption         =   "+"
      Height          =   255
      Left            =   1080
      TabIndex        =   11
      Top             =   1800
      Width           =   375
   End
   Begin VB.Label Label6 
      Caption         =   "Willenskraft:"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   1560
      Width           =   975
   End
   Begin MSForms.Label Label4 
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   615
      Caption         =   "St�rke:"
      Size            =   "1085;450"
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
   End
   Begin VB.Label Label3 
      Caption         =   "Level:"
      Height          =   255
      Left            =   4680
      TabIndex        =   4
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Klasse:"
      Height          =   255
      Left            =   2400
      TabIndex        =   2
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Name:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmMonster"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Monster As CMonster

Public Sub Init(mMonster As CMonster)
    Dim Tempstring() As String
    Dim Item As Variant
    
    lstItems.Clear
    lstItemdrop.Clear
    lstTechniken.Clear
    lstZauber.Clear
    
    Set Monster = mMonster
    Caption = "Monster - " & Monster.sName & "(" & Monster.sID & ")"
    txtName.Text = Monster.sName
    txtKlasse.Text = Monster.sKlasse
    txtLevel.Text = Monster.nLevel
    
    txtStaerke.Text = Monster.nStaerke
    txtStaerkeBonus.Text = Monster.nStaerkeBonus
    txtWillenskraft.Text = Monster.nWillenskraft
    txtWillenskraftBonus.Text = Monster.nWillenskraftBonus
    txtKonstitution.Text = Monster.nKonstitution
    txtKonstitutionBonus.Text = Monster.nKonstitutionBonus
    txtGeschicklichkeit.Text = Monster.nGeschicklichkeit
    txtGeschicklichkeitBonus.Text = Monster.nGeschicklichkeitBonus
    txtSchnelligkeit.Text = Monster.nSchnelligkeit
    txtSchnelligkeitBonus.Text = Monster.nSchnelligkeitBonus
    
    Tempstring = Split(Monster.sItems, "|")
    For Each Item In Tempstring
        lstItems.AddItem Item
    Next
    
    Tempstring = Split(Monster.sItemdrop, "|")
    For Each Item In Tempstring
        lstItemdrop.AddItem Item
    Next
    
    txtExp.Text = Monster.nExp
    txtGold.Text = Monster.nGold
    
    txtUrsprungAffinitaet.Text = Monster.nUrsprungAffinitaet
    txtErdeAffinitaet.Text = Monster.nErdeAffinitaet
    txtHimmelAffinitaet.Text = Monster.nHimmelAffinitaet
    txtFeuerAffinitaet.Text = Monster.nFeuerAffinitaet
    txtKaelteAffinitaet.Text = Monster.nKaelteAffinitaet
    txtMagiepotenzial.Text = Monster.nMagiepotenzial
    
    txtUrsprungVwb.Text = Monster.nUrsprungVwb
    txtErdeVwb.Text = Monster.nErdeVwb
    txtHimmelVwb.Text = Monster.nHimmelVwb
    txtFeuerVwb.Text = Monster.nFeuerVwb
    txtKaelteVwb.Text = Monster.nKaelteVwb
    
    Tempstring = Split(Monster.sTechniken, "|")
    For Each Item In Tempstring
        lstTechniken.AddItem Item
    Next
    
    Tempstring = Split(Monster.sZauber, "|")
    For Each Item In Tempstring
        lstZauber.AddItem Item
    Next
    
    txtAttacke.Text = Monster.nAttacke
    txtVerteidigung.Text = Monster.nVerteidigung
    
    txtBeschreibung.Text = Monster.sBeschreibung
    
    txtHP.Text = Monster.nHP
    txtMP.Text = Monster.nMP
End Sub

Private Sub cmdOK_Click()
    Dim Tempstring As String
    Dim Index As Integer
    Monster.sName = txtName.Text
    Monster.sKlasse = txtKlasse.Text
    Monster.nLevel = txtLevel.Text
    
    Monster.nStaerke = txtStaerke.Text
    Monster.nStaerkeBonus = txtStaerkeBonus.Text
    Monster.nWillenskraft = txtWillenskraft.Text
    Monster.nWillenskraftBonus = txtWillenskraftBonus.Text
    Monster.nKonstitution = txtKonstitution.Text
    Monster.nKonstitutionBonus = txtKonstitutionBonus.Text
    Monster.nGeschicklichkeit = txtGeschicklichkeit.Text
    Monster.nGeschicklichkeitBonus = txtGeschicklichkeitBonus.Text
    Monster.nSchnelligkeit = txtSchnelligkeit.Text
    Monster.nSchnelligkeitBonus = txtSchnelligkeitBonus.Text
    
    Monster.sItems = ""
    For Index = 0 To lstItems.ListCount - 1
        Monster.sItems = Monster.sItems & lstItems.List(Index) & "|"
    Next
    If Monster.sItems <> "" Then
        Monster.sItems = Left(Monster.sItems, Len(Monster.sItems) - 1)
    End If
    
    Monster.sItemdrop = ""
    For Index = 0 To lstItemdrop.ListCount - 1
        Monster.sItemdrop = Monster.sItemdrop & lstItemdrop.List(Index) & "|"
    Next
    If Monster.sItemdrop <> "" Then
        Monster.sItemdrop = Left(Monster.sItemdrop, Len(Monster.sItemdrop) - 1)
    End If
    
    Monster.nUrsprungAffinitaet = txtUrsprungAffinitaet.Text
    Monster.nErdeAffinitaet = txtErdeAffinitaet.Text
    Monster.nHimmelAffinitaet = txtHimmelAffinitaet.Text
    Monster.nFeuerAffinitaet = txtFeuerAffinitaet.Text
    Monster.nKaelteAffinitaet = txtKaelteAffinitaet.Text
    Monster.nMagiepotenzial = txtMagiepotenzial.Text
    
    Monster.nUrsprungVwb = txtUrsprungVwb.Text
    Monster.nErdeVwb = txtErdeVwb.Text
    Monster.nHimmelVwb = txtHimmelVwb.Text
    Monster.nFeuerVwb = txtFeuerVwb.Text
    Monster.nKaelteVwb = txtKaelteVwb.Text
    
    Monster.sTechniken = ""
    For Index = 0 To lstTechniken.ListCount - 1
        Monster.sTechniken = Monster.sTechniken & lstTechniken.List(Index) & "|"
    Next
    If Monster.sTechniken <> "" Then
        Monster.sTechniken = Left(Monster.sTechniken, Len(Monster.sTechniken) - 1)
    End If
    
    Monster.sZauber = ""
    For Index = 0 To lstZauber.ListCount - 1
        Monster.sZauber = Monster.sZauber & lstZauber.List(Index) & "|"
    Next
    If Monster.sZauber <> "" Then
        Monster.sZauber = Left(Monster.sZauber, Len(Monster.sZauber) - 1)
    End If
    
    Monster.nAttacke = txtAttacke.Text
    Monster.nVerteidigung = txtVerteidigung.Text
    
    Monster.sBeschreibung = txtBeschreibung.Text
    
    Monster.nHP = txtHP.Text
    Monster.nMP = txtMP.Text
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub


Private Sub Form_Unload(Cancel As Integer)
    If frmNavigation.tabNavigation.SelectedItem.Name = "Monster" Then
        frmNavigation.trvNavigation.Nodes(Monster.sID).Text = Monster.sName
    End If
End Sub

Private Sub txtBeschreibung_GotFocus()
    Me.KeyPreview = False
    cmdOK.Default = False
End Sub

Private Sub txtBeschreibung_LostFocus()
    Me.KeyPreview = True
    cmdOK.Default = True
End Sub

Private Sub txtName_Change()
    Caption = "Monster - " & txtName.Text & "(" & Monster.sID & ")"
    
    If frmNavigation.tabNavigation.SelectedItem.Name = "Monster" Then
        frmNavigation.trvNavigation.Nodes(Monster.sID).Text = txtName.Text
    End If
End Sub

Private Sub lstItems_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lngItem As Long
  
    If Button = vbRightButton Then
        lngItem = PopUpListBox(lstItems, X, Y)
        frmSelect.Mode = "Items"
        Set frmSelect.ParentForm = Me
        If lngItem <> -1 And lngItem < lstItems.ListCount Then
            mdiMain.mnuDelete.Enabled = True
            mdiMain.mnuEdit.Enabled = True
            lstItems.ListIndex = lngItem
        Else
            mdiMain.mnuDelete.Enabled = False
            mdiMain.mnuEdit.Enabled = False
        End If
        PopupMenu mdiMain.mnuConnect
    End If
End Sub

Private Sub lstItemdrop_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lngItem As Long
  
    If Button = vbRightButton Then
        lngItem = PopUpListBox(lstItemdrop, X, Y)
        frmSelect.Mode = "Itemdrop"
        Set frmSelect.ParentForm = Me
        If lngItem <> -1 And lngItem < lstItemdrop.ListCount Then
            mdiMain.mnuDelete.Enabled = True
            mdiMain.mnuEdit.Enabled = True
            lstItemdrop.ListIndex = lngItem
        Else
            mdiMain.mnuDelete.Enabled = False
            mdiMain.mnuEdit.Enabled = False
        End If
        PopupMenu mdiMain.mnuConnect
    End If
End Sub

Private Sub lstTechniken_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lngItem As Long
  
    If Button = vbRightButton Then
        lngItem = PopUpListBox(lstTechniken, X, Y)
        frmSelect.Mode = "Techniken"
        Set frmSelect.ParentForm = Me
        If lngItem <> -1 And lngItem < lstTechniken.ListCount Then
            mdiMain.mnuDelete.Enabled = True
            mdiMain.mnuEdit.Enabled = True
            lstTechniken.ListIndex = lngItem
        Else
            mdiMain.mnuDelete.Enabled = False
            mdiMain.mnuEdit.Enabled = False
        End If
        PopupMenu mdiMain.mnuConnect
    End If
End Sub

Private Sub lstZauber_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lngItem As Long
  
    If Button = vbRightButton Then
        lngItem = PopUpListBox(lstZauber, X, Y)
        frmSelect.Mode = "Zauber"
        Set frmSelect.ParentForm = Me
        If lngItem <> -1 And lngItem < lstZauber.ListCount Then
            mdiMain.mnuDelete.Enabled = True
            mdiMain.mnuEdit.Enabled = True
            lstZauber.ListIndex = lngItem
        Else
            mdiMain.mnuDelete.Enabled = False
            mdiMain.mnuEdit.Enabled = False
        End If
        PopupMenu mdiMain.mnuConnect
    End If
End Sub
