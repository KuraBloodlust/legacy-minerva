VERSION 5.00
Begin VB.Form frmItem 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Form1"
   ClientHeight    =   5370
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8760
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   8760
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtBeschreibung 
      Height          =   1575
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertikal
      TabIndex        =   23
      Top             =   3240
      Width           =   8655
   End
   Begin VB.OptionButton optAusruestung 
      Caption         =   "Ausrüstung"
      Height          =   255
      Left            =   4680
      TabIndex        =   22
      Top             =   2640
      Width           =   1455
   End
   Begin VB.OptionButton optSelbst 
      Caption         =   "Selbst"
      Height          =   255
      Left            =   4680
      TabIndex        =   21
      Top             =   2280
      Width           =   1455
   End
   Begin VB.OptionButton optEinHeld 
      Caption         =   "Ein Held"
      Height          =   255
      Left            =   2280
      TabIndex        =   20
      Top             =   2280
      Width           =   1455
   End
   Begin VB.OptionButton optAlleHelden 
      Caption         =   "Alle Helden"
      Height          =   255
      Left            =   2280
      TabIndex        =   19
      Top             =   2640
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Abbrechen"
      Height          =   375
      Left            =   4200
      TabIndex        =   18
      Top             =   4920
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   2520
      TabIndex        =   17
      Top             =   4920
      Width           =   1455
   End
   Begin VB.OptionButton optAlleGegner 
      Caption         =   "Alle Gegner"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   2640
      Width           =   1455
   End
   Begin VB.OptionButton optEinGegner 
      Caption         =   "Ein Gegner"
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   2280
      Width           =   1455
   End
   Begin VB.TextBox txtWirkung 
      Height          =   285
      Left            =   120
      TabIndex        =   13
      Top             =   1680
      Width           =   4095
   End
   Begin VB.TextBox txtVerteidigung 
      Height          =   285
      Left            =   3000
      TabIndex        =   11
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox txtAttacke 
      Height          =   285
      Left            =   1560
      TabIndex        =   9
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox txtWert 
      Height          =   285
      Left            =   120
      TabIndex        =   7
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   120
      TabIndex        =   2
      Top             =   240
      Width           =   4095
   End
   Begin VB.ListBox lstZauber 
      Height          =   1620
      ItemData        =   "frmItem.frx":0000
      Left            =   6720
      List            =   "frmItem.frx":0002
      TabIndex        =   1
      Top             =   240
      Width           =   1935
   End
   Begin VB.ListBox lstTechniken 
      Height          =   1620
      ItemData        =   "frmItem.frx":0004
      Left            =   4560
      List            =   "frmItem.frx":0006
      TabIndex        =   0
      Top             =   240
      Width           =   1935
   End
   Begin VB.Label Label28 
      Caption         =   "Beschreibung:"
      Height          =   255
      Left            =   0
      TabIndex        =   24
      Top             =   3000
      Width           =   1695
   End
   Begin VB.Label Label7 
      Caption         =   "Ziel:"
      Height          =   255
      Left            =   0
      TabIndex        =   14
      Top             =   2040
      Width           =   975
   End
   Begin VB.Label Label6 
      Caption         =   "Wirkung:"
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   1440
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "Verteidigung:"
      Height          =   255
      Left            =   2880
      TabIndex        =   10
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label4 
      Caption         =   "Attacke:"
      Height          =   255
      Left            =   1440
      TabIndex        =   8
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Wert:"
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Name:"
      Height          =   255
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   1215
   End
   Begin VB.Label Label24 
      Caption         =   "Zauber:"
      Height          =   255
      Left            =   6600
      TabIndex        =   4
      Top             =   0
      Width           =   1335
   End
   Begin VB.Label Label25 
      Caption         =   "Techniken:"
      Height          =   255
      Left            =   4440
      TabIndex        =   3
      Top             =   0
      Width           =   975
   End
End
Attribute VB_Name = "frmItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Item As CItem

Public Sub Init(iItem As CItem)
    Dim Tempstring() As String
    Dim Technik As Variant
    
    Set Item = iItem
    Caption = "Item - " & Item.sName & "(" & Item.sID & ")"
    txtName.Text = Item.sName
    txtWert.Text = Item.nWert
    txtAttacke.Text = Item.nAttacke
    txtVerteidigung.Text = Item.nVerteidigung
    txtWirkung.Text = Item.sWirkung
    
    Tempstring = Split(Item.sTechniken, "|")
    For Each Technik In Tempstring
        lstTechniken.AddItem Technik
    Next
    
    Tempstring = Split(Item.sZauber, "|")
    For Each Technik In Tempstring
        lstZauber.AddItem Technik
    Next
    
    If Item.nZiel = 0 Then
        optEinGegner.Value = True
    ElseIf Item.nZiel = 1 Then
        optEinHeld.Value = True
    ElseIf Item.nZiel = 2 Then
        optAlleGegner.Value = True
    ElseIf Item.nZiel = 3 Then
        optAlleHelden.Value = True
    ElseIf Item.nZiel = 4 Then
        optSelbst.Value = True
    ElseIf Item.nZiel = 5 Then
        optAusruestung.Value = True
    End If
    
    txtBeschreibung.Text = Item.sBeschreibung
End Sub

Private Sub cmdOK_Click()
    Dim Index As Integer
    
    Item.sName = txtName.Text
    Item.nWert = txtWert.Text
    Item.nAttacke = txtAttacke.Text
    Item.nVerteidigung = txtVerteidigung.Text
    Item.sWirkung = txtWirkung.Text
    
    Item.sTechniken = ""
    For Index = 0 To lstTechniken.ListCount - 1
        Item.sTechniken = Item.sTechniken & lstTechniken.List(Index) & "|"
    Next
    If Item.sTechniken <> "" Then
        Item.sTechniken = Left(Item.sTechniken, Len(Item.sTechniken) - 1)
    End If
    
    Item.sZauber = ""
    For Index = 0 To lstZauber.ListCount - 1
        Item.sZauber = Item.sZauber & lstZauber.List(Index) & "|"
    Next
    If Item.sZauber <> "" Then
        Item.sZauber = Left(Item.sZauber, Len(Item.sZauber) - 1)
    End If
    
    If optEinGegner.Value = True Then
        Item.nZiel = 0
    ElseIf optEinHeld.Value = True Then
        Item.nZiel = 1
    ElseIf optAlleGegner.Value = True Then
        Item.nZiel = 2
    ElseIf optAlleHelden.Value = True Then
        Item.nZiel = 3
    ElseIf optSelbst.Value = True Then
        Item.nZiel = 4
    ElseIf optAusruestung.Value = True Then
        Item.nZiel = 5
    End If
    
    Item.sBeschreibung = txtBeschreibung.Text
    
    Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If frmNavigation.tabNavigation.SelectedItem.Name = "Items" Then
        frmNavigation.trvNavigation.Nodes(Item.sID).Text = Item.sName
    End If
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub txtName_Change()
    Caption = "Item - " & txtName.Text & "(" & Item.sID & ")"
    
    If frmNavigation.tabNavigation.SelectedItem.Name = "Items" Then
        frmNavigation.trvNavigation.Nodes(Item.sID).Text = txtName.Text
    End If
End Sub

Private Sub lstTechniken_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lngItem As Long
  
    If Button = vbRightButton Then
        lngItem = PopUpListBox(lstTechniken, X, Y)
        frmSelect.Mode = "Techniken"
        Set frmSelect.ParentForm = Me
        If lngItem <> -1 And lngItem < lstTechniken.ListCount Then
            mdiMain.mnuDelete.Enabled = True
            mdiMain.mnuEdit.Enabled = True
            lstTechniken.ListIndex = lngItem
        Else
            mdiMain.mnuDelete.Enabled = False
            mdiMain.mnuEdit.Enabled = False
        End If
        PopupMenu mdiMain.mnuConnect
    End If
End Sub

Private Sub lstZauber_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lngItem As Long
  
    If Button = vbRightButton Then
        lngItem = PopUpListBox(lstZauber, X, Y)
        frmSelect.Mode = "Zauber"
        Set frmSelect.ParentForm = Me
        If lngItem <> -1 And lngItem < lstZauber.ListCount Then
            mdiMain.mnuDelete.Enabled = True
            mdiMain.mnuEdit.Enabled = True
            lstZauber.ListIndex = lngItem
        Else
            mdiMain.mnuDelete.Enabled = False
            mdiMain.mnuEdit.Enabled = False
        End If
        PopupMenu mdiMain.mnuConnect
    End If
End Sub
