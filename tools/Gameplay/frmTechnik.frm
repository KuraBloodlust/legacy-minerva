VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form frmTechnik 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Form1"
   ClientHeight    =   5415
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8700
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5415
   ScaleWidth      =   8700
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtBeschreibung 
      Height          =   1575
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertikal
      TabIndex        =   25
      Top             =   3240
      Width           =   8535
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'Kein
      Height          =   1095
      Left            =   4560
      ScaleHeight     =   1095
      ScaleWidth      =   4095
      TabIndex        =   19
      Top             =   240
      Width           =   4095
      Begin VB.OptionButton optFeuer 
         Caption         =   "Feuer"
         Height          =   255
         Left            =   2160
         TabIndex        =   24
         Top             =   360
         Width           =   1455
      End
      Begin VB.OptionButton optErde 
         Caption         =   "Erde"
         Height          =   255
         Left            =   2160
         TabIndex        =   23
         Top             =   0
         Width           =   1455
      End
      Begin VB.OptionButton optKaelte 
         Caption         =   "K�lte"
         Height          =   255
         Left            =   0
         TabIndex        =   22
         Top             =   720
         Width           =   1455
      End
      Begin VB.OptionButton optHimmel 
         Caption         =   "Himmel"
         Height          =   255
         Left            =   0
         TabIndex        =   21
         Top             =   0
         Width           =   1455
      End
      Begin VB.OptionButton optUrsprung 
         Caption         =   "Ursprung"
         Height          =   255
         Left            =   0
         TabIndex        =   20
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.ComboBox cmbStatus 
      Height          =   315
      Left            =   4560
      Style           =   2  'Dropdown-Liste
      TabIndex        =   18
      Top             =   1560
      Width           =   4095
   End
   Begin VB.TextBox txtMana 
      Height          =   285
      Left            =   1560
      TabIndex        =   14
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   120
      TabIndex        =   9
      Top             =   240
      Width           =   4095
   End
   Begin VB.TextBox txtSchaden 
      Height          =   285
      Left            =   120
      TabIndex        =   8
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox txtWirkung 
      Height          =   285
      Left            =   120
      TabIndex        =   7
      Top             =   1440
      Width           =   4095
   End
   Begin VB.OptionButton optEinGegner 
      Caption         =   "Ein Gegner"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   2040
      Width           =   1455
   End
   Begin VB.OptionButton optAlleGegner 
      Caption         =   "Alle Gegner"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   2400
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   2760
      TabIndex        =   4
      Top             =   4920
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Abbrechen"
      Height          =   375
      Left            =   4440
      TabIndex        =   3
      Top             =   4920
      Width           =   1455
   End
   Begin VB.OptionButton optAlleHelden 
      Caption         =   "Alle Helden"
      Height          =   255
      Left            =   2280
      TabIndex        =   2
      Top             =   2400
      Width           =   1455
   End
   Begin VB.OptionButton optEinHeld 
      Caption         =   "Ein Held"
      Height          =   255
      Left            =   2280
      TabIndex        =   1
      Top             =   2040
      Width           =   1455
   End
   Begin VB.OptionButton optSelbst 
      Caption         =   "Selbst"
      Height          =   255
      Left            =   4560
      TabIndex        =   0
      Top             =   2040
      Width           =   1455
   End
   Begin VB.Label Label28 
      Caption         =   "Beschreibung:"
      Height          =   255
      Left            =   0
      TabIndex        =   26
      Top             =   3000
      Width           =   1695
   End
   Begin MSForms.Label Label5 
      Height          =   255
      Left            =   4440
      TabIndex        =   17
      Top             =   1320
      Width           =   975
      Caption         =   "Status:"
      Size            =   "1720;450"
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
   End
   Begin VB.Label Label3 
      Caption         =   "Element:"
      Height          =   255
      Left            =   4440
      TabIndex        =   16
      Top             =   0
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Mana:"
      Height          =   255
      Left            =   1440
      TabIndex        =   15
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Name:"
      Height          =   255
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   1215
   End
   Begin VB.Label Label4 
      Caption         =   "Attacke:"
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "Wirkung:"
      Height          =   255
      Left            =   0
      TabIndex        =   11
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label7 
      Caption         =   "Ziel:"
      Height          =   255
      Left            =   0
      TabIndex        =   10
      Top             =   1800
      Width           =   975
   End
End
Attribute VB_Name = "frmTechnik"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Technik As CTechnik
Public Mode As String

Public Sub Init(tTechnik As CTechnik)
    Dim Tempstring() As String
    
    Set Technik = tTechnik
    Caption = Mode & " - " & Technik.sName & "(" & Technik.sID & ")"
    
    txtName.Text = Technik.sName
    txtSchaden.Text = Technik.nSchaden
    txtWirkung.Text = Technik.sWirkung
    txtMana.Text = Technik.nMana
    
    If Technik.nZiel = 0 Then
        optEinGegner.Value = True
    ElseIf Technik.nZiel = 1 Then
        optEinHeld.Value = True
    ElseIf Technik.nZiel = 2 Then
        optAlleGegner.Value = True
    ElseIf Technik.nZiel = 3 Then
        optAlleHelden.Value = True
    ElseIf Technik.nZiel = 4 Then
        optSelbst.Value = True
    End If
    
    If Technik.nElement = 0 Then
        optUrsprung.Value = True
    ElseIf Technik.nElement = 1 Then
        optErde.Value = True
    ElseIf Technik.nElement = 2 Then
        optHimmel.Value = True
    ElseIf Technik.nElement = 3 Then
        optFeuer.Value = True
    ElseIf Technik.nElement = 4 Then
        optKaelte.Value = True
    End If
    
    cmbStatus.ListIndex = Technik.nStatus
    
    txtBeschreibung.Text = Technik.sBeschreibung
End Sub

Private Sub cmdOK_Click()
    Dim Index As Integer
    
    Technik.sName = txtName.Text
    Technik.nSchaden = txtSchaden.Text
    Technik.sWirkung = txtWirkung.Text
    Technik.nMana = txtMana.Text
       
    If optEinGegner.Value = True Then
        Technik.nZiel = 0
    ElseIf optEinHeld.Value = True Then
        Technik.nZiel = 1
    ElseIf optAlleGegner.Value = True Then
        Technik.nZiel = 2
    ElseIf optAlleHelden.Value = True Then
        Technik.nZiel = 3
    ElseIf optSelbst.Value = True Then
        Technik.nZiel = 4
    End If
    
    If optUrsprung.Value = True Then
        Technik.nElement = 0
    ElseIf optErde.Value = True Then
        Technik.nElement = 1
    ElseIf optHimmel.Value = True Then
        Technik.nElement = 2
    ElseIf optFeuer.Value = True Then
        Technik.nElement = 3
    ElseIf optKaelte.Value = True Then
        Technik.nElement = 4
    End If
    
    Technik.nStatus = cmbStatus.ListIndex
    
    Technik.sBeschreibung = txtBeschreibung.Text
    
    Unload Me
End Sub

Private Sub Form_Load()
    cmbStatus.AddItem ""
    cmbStatus.AddItem "Verlangsamt"
    cmbStatus.AddItem "Beschleunigt"
    cmbStatus.AddItem "Eingefroren"
    cmbStatus.AddItem "Angehalten"
    cmbStatus.AddItem "Aufgeladen"
    cmbStatus.AddItem "Verwirrt"
    cmbStatus.AddItem "Verbrannt"
    cmbStatus.AddItem "Vergiftet"
    cmbStatus.AddItem "Angeschlagen"
    cmbStatus.AddItem "Regenerierend"
    cmbStatus.AddItem "Gesch�tzt"
    cmbStatus.AddItem "Ger�stet"
    cmbStatus.AddItem "Reflektierend"
    cmbStatus.AddItem "Ohnm�chtig"
    cmbStatus.AddItem "Blitzschnell"
    cmbStatus.AddItem "Rasend"
    cmbStatus.AddItem "Verwandelt"
    cmbStatus.AddItem "Erblindet"
    cmbStatus.AddItem "Verkr�ppelt"
    cmbStatus.AddItem "Verstrahlt"
    cmbStatus.AddItem "Zerfallend"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If frmNavigation.tabNavigation.SelectedItem.Name = Mode Then
        frmNavigation.trvNavigation.Nodes(Technik.sID).Text = Technik.sName
    End If
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub txtName_Change()
    Caption = Mode & " - " & txtName.Text & "(" & Technik.sID & ")"
    
    If frmNavigation.tabNavigation.SelectedItem.Name = Mode Then
        frmNavigation.trvNavigation.Nodes(Technik.sID).Text = txtName.Text
    End If
End Sub
