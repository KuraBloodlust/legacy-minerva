VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMonster"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "Monsterdaten f�r Phoenix"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public bDeleted As Boolean

Public sName As String

Public sKlasse As String

Public nLevel As Integer

Public nStaerke As Integer

Public nStaerkeBonus As Integer

Public nWillenskraft As Integer

Public nWillenskraftBonus As Integer

Public nKonstitution As Integer

Public nKonstitutionBonus As Integer

Public nGeschicklichkeit As Integer

Public nGeschicklichkeitBonus As Integer

Public nSchnelligkeit As Integer

Public nSchnelligkeitBonus As Integer

Public sItems As String

Public sID As String

Public sItemdrop As String

Public nExp As Integer

Public nGold As Integer

Public nUrsprungAffinitaet As Integer

Public nErdeAffinitaet As Integer

Public nHimmelAffinitaet As Integer

Public nFeuerAffinitaet As Integer

Public nKaelteAffinitaet As Integer

Public nMagiepotenzial As Integer

Public nUrsprungVwb As Integer

Public nErdeVwb As Integer

Public nHimmelVwb As Integer

Public nFeuerVwb As Integer

Public nKaelteVwb As Integer

Public sZauber As String

Public sTechniken As String

Public nAttacke As Integer

Public nVerteidigung As Integer

Public sBeschreibung As String

Public nHP As Integer

Public nMP As Integer
