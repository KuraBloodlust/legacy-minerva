Attribute VB_Name = "modMiniDevILWrapper"
Private Declare Sub ilInit Lib "Devil" ()
Private Declare Sub iluInit Lib "ilu" ()
Private Declare Sub ilutInit Lib "ilut" ()
Private Declare Function ilutRenderer Lib "ilut" (ByVal Renderer As Long) As Byte
Private Declare Sub ilGenImages Lib "Devil" (ByVal Num As Long, ByRef Images As Long)
Private Declare Sub ilBindImage Lib "Devil" (ByVal Image As Long)
Private Declare Function ilLoadImage Lib "Devil" (ByVal FileName As String) As Byte
Private Declare Sub iluGetImageInfo Lib "ilu" (ByRef Info As ILinfo)
Private Declare Function ilutConvertToHBitmap Lib "ilut" (ByVal hDC As Integer) As Integer
Public Declare Function ilSaveImage Lib "Devil" (ByVal FileName As String) As Byte

Private Declare Function CreateCompatibleDC Lib "gdi32" (ByVal hDC As Long) As Long
Private Declare Function SelectObject Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long) As Long
Private Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Declare Function StretchBlt Lib "gdi32" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long

Public Type ILinfo
  ID As Long          ' the image's id
  DataPointer As Long ' pointer to the image's data (useless in VB)
  Width As Long       ' the image's width
  Height As Long      ' the image's height
  Depth As Long       ' the image's depth
  Bpp As Byte         ' bytes per pixel (not bits) of the image
  SizeOfData As Long  ' the total size of the data (in bytes)
  Format As Long      ' image format (in IL enum style)
  IType As Long       ' image type (in IL enum style)
  Origin As Long      ' origin of the image
  PalettePointer As Long ' pointer to the image's palette (useless in VB)
  PalType As Long     ' palette type
  PalSize As Long     ' palette size
  NumNext As Long     ' number of images following
  NumMips As Long     ' number of mipmaps
  NumLayers  As Long  ' number of layers
End Type

Private Const SRCCOPY = &HCC0020
Private Const ILUT_WIN32 = 2

Public Function CreateEmptyDC(Width As Integer, Height As Integer) As Long
    
    Dim MyDC As Long
    Dim mBitmap As Long
    
    MyDC = CreateCompatibleDC(GetDC(0))
    mBitmap = CreateCompatibleBitmap(GetDC(0), Width, Height)
    SelectObject MyDC, mBitmap
    
    CreateEmptyDC = MyDC
    
End Function

Private Function DevilInitialize()
    
    ilInit
    iluInit
    ilutInit
    
    ilutRenderer ILUT_WIN32
    
    Dim ID As Long
    ilGenImages 1, ID
    ilBindImage ID
    
End Function

Public Function LoadAndSaveImage(PathFrom As String, PathTo As String)

    DevilInitialize
    ilLoadImage PathFrom
    ilSaveImage PathTo
    
End Function

Public Function GetImageInformation(Path As String) As ILinfo
        
    DevilInitialize
    ilLoadImage Path
    
    Dim inf As ILinfo
    iluGetImageInfo inf
    
    GetImageInformation = inf
    
End Function

Public Function RenderImageToHDC(Path As String, hDC As Long, x As Integer, y As Integer, SourceX As Integer, SourceY As Integer, Width As Integer, Height As Integer)
    
    DevilInitialize
    
    ilLoadImage Path
    
    Dim inf As ILinfo
    iluGetImageInfo inf
    
    If Width = -1 Then
        Width = inf.Width
    End If
    
    If Height = -1 Then
        Height = inf.Height
    End If
    
    hjpgdc = CreateCompatibleDC(0)
    hJpgBmp = ilutConvertToHBitmap(0)
    SelectObject hjpgdc, hJpgBmp
    
    If Not Width = inf.Width Or Not Height = inf.Height Then
        StretchBlt hDC, x, y, Width, Height, hjpgdc, SourceX, SourceY, inf.Width, inf.Height, vbSrcCopy
    Else
        BitBlt hDC, x, y, Width, Height, hjpgdc, SourceX, SourceY, SRCCOPY
    End If

End Function
