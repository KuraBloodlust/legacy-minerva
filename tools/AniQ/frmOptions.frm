VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmOptions 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Options"
   ClientHeight    =   3960
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   4440
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3960
   ScaleWidth      =   4440
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame4 
      Caption         =   "Additional Editor and Animation Options"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   120
      TabIndex        =   16
      Top             =   1440
      Width           =   4215
      Begin VB.CheckBox Check1 
         Caption         =   "Use API for Preview"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   180
         TabIndex        =   32
         Top             =   1500
         Width           =   2535
      End
      Begin VB.Frame FrameColor 
         BorderStyle     =   0  'None
         Caption         =   "Transparent Color"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   28
         Top             =   360
         Width           =   3555
         Begin VB.PictureBox picColor 
            BackColor       =   &H00FF00FF&
            Height          =   315
            Left            =   0
            ScaleHeight     =   255
            ScaleWidth      =   1215
            TabIndex        =   29
            Top             =   0
            Width           =   1275
            Begin VB.CommandButton Command2 
               DragIcon        =   "frmOptions.frx":0000
               Enabled         =   0   'False
               Height          =   255
               Left            =   900
               Picture         =   "frmOptions.frx":0CCA
               Style           =   1  'Graphical
               TabIndex        =   30
               Top             =   0
               Width           =   315
            End
         End
         Begin VB.Label Label4 
            Caption         =   "Transparent Color"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1320
            TabIndex        =   31
            Top             =   0
            Width           =   1695
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "Animation  Height"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   21
         Top             =   1080
         Width           =   2595
         Begin VB.PictureBox Picture2 
            Height          =   315
            Left            =   0
            ScaleHeight     =   255
            ScaleWidth      =   1215
            TabIndex        =   22
            Top             =   0
            Width           =   1275
            Begin VB.VScrollBar VScroll1 
               Height          =   255
               Left            =   840
               Max             =   1
               Min             =   10000
               SmallChange     =   10
               TabIndex        =   25
               Top             =   0
               Value           =   32
               Width           =   255
            End
            Begin VB.TextBox txtAS 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   0
               Locked          =   -1  'True
               TabIndex        =   24
               Text            =   "32"
               Top             =   0
               Width           =   855
            End
            Begin VB.CommandButton cmdScroll3 
               Caption         =   "*"
               Height          =   255
               Left            =   1080
               MousePointer    =   7  'Size N S
               TabIndex        =   23
               Top             =   0
               Width           =   135
            End
         End
         Begin VB.Label Label8 
            Caption         =   "Label1"
            Height          =   195
            Left            =   1320
            TabIndex        =   27
            Top             =   60
            Width           =   15
         End
         Begin VB.Label Label9 
            Caption         =   "Interval"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1320
            TabIndex        =   26
            Top             =   60
            Width           =   1455
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Caption         =   "Transparent Color"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   17
         Top             =   720
         Width           =   2955
         Begin VB.PictureBox picColor2 
            BackColor       =   &H00000000&
            Height          =   315
            Left            =   0
            ScaleHeight     =   255
            ScaleWidth      =   1215
            TabIndex        =   18
            Top             =   0
            Width           =   1275
            Begin VB.CommandButton Command4 
               DragIcon        =   "frmOptions.frx":0F74
               Enabled         =   0   'False
               Height          =   255
               Left            =   900
               Picture         =   "frmOptions.frx":1C3E
               Style           =   1  'Graphical
               TabIndex        =   19
               Top             =   0
               Width           =   315
            End
         End
         Begin VB.Label Label5 
            Caption         =   "Grid Color"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1320
            TabIndex        =   20
            Top             =   0
            Width           =   1455
         End
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Animation Size"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   4215
      Begin VB.Frame FrameHeight 
         BorderStyle     =   0  'None
         Caption         =   "Animation  Height"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   9
         Top             =   720
         Width           =   1875
         Begin VB.PictureBox Picture1 
            Height          =   315
            Left            =   0
            ScaleHeight     =   255
            ScaleWidth      =   1215
            TabIndex        =   10
            Top             =   0
            Width           =   1275
            Begin VB.TextBox txtAH 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   0
               Locked          =   -1  'True
               TabIndex        =   13
               Text            =   "32"
               Top             =   0
               Width           =   855
            End
            Begin VB.VScrollBar VScroll3 
               Height          =   255
               Left            =   840
               Max             =   4096
               Min             =   4
               SmallChange     =   4
               TabIndex        =   12
               Top             =   0
               Value           =   128
               Width           =   255
            End
            Begin VB.CommandButton cmdScroll2 
               Caption         =   "*"
               Height          =   255
               Left            =   1080
               MousePointer    =   7  'Size N S
               TabIndex        =   11
               Top             =   0
               Width           =   135
            End
         End
         Begin VB.Label Label2 
            Caption         =   "Height"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1320
            TabIndex        =   15
            Top             =   0
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Label1"
            Height          =   195
            Left            =   1320
            TabIndex        =   14
            Top             =   60
            Width           =   15
         End
      End
      Begin VB.Frame FrameWidth 
         BorderStyle     =   0  'None
         Caption         =   "Animation  Width"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   1815
         Begin VB.PictureBox picContainer1 
            Height          =   315
            Left            =   0
            ScaleHeight     =   255
            ScaleWidth      =   1215
            TabIndex        =   4
            Top             =   0
            Width           =   1275
            Begin VB.VScrollBar VScroll2 
               Height          =   255
               Left            =   840
               Max             =   4096
               Min             =   4
               SmallChange     =   4
               TabIndex        =   7
               Top             =   0
               Value           =   128
               Width           =   255
            End
            Begin VB.TextBox txtAW 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   0
               Locked          =   -1  'True
               TabIndex        =   6
               Text            =   "24"
               Top             =   0
               Width           =   855
            End
            Begin VB.CommandButton cmdScroll1 
               Caption         =   "*"
               Height          =   255
               Left            =   1080
               MousePointer    =   7  'Size N S
               TabIndex        =   5
               Top             =   0
               Width           =   135
            End
         End
         Begin VB.Label Label3 
            Caption         =   "Width"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1320
            TabIndex        =   8
            Top             =   60
            Width           =   1455
         End
      End
      Begin MSComDlg.CommonDialog cd 
         Left            =   240
         Top             =   360
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   3600
         Picture         =   "frmOptions.frx":1EE8
         Top             =   240
         Width           =   480
      End
   End
   Begin VB.CommandButton Command3 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   1
      Top             =   3480
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Save as Standart Values"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   3480
      Width           =   2655
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private AChange As Boolean

Private lY As Integer
Private lX As Integer

Private onload As Boolean

Private Sub Check1_Click()
    ForceAlternate = Check1.Value
End Sub

Private Sub Command1_Click()
    frmMain.SaveINIFile
End Sub

Private Sub Command2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    PickMode = True
End Sub

Private Sub Command3_Click()
    frmMain.RefreshView
    Unload Me
End Sub

Private Sub Form_Load()
    onload = True
    VScroll2.Value = Animations(CurrentAnimation).SelectionW
    VScroll3.Value = Animations(CurrentAnimation).SelectionH
    VScroll1.Value = Animations(CurrentAnimation).AnimationSpeed
    picColor.BackColor = Animations(CurrentAnimation).TColor
    picColor2.BackColor = frmMain.GridColor
    onload = False
End Sub

Private Sub Form_Unload(Cancel As Integer)

    frmMain.RedrawAll

End Sub

Private Sub PicColor2_Click()

    cd.ShowColor
    picColor2.BackColor = cd.Color
    frmMain.GridColor = cd.Color

End Sub

Private Sub txtAH_Change()

    If Not onload Then
        If Animations(CurrentAnimation).AniCount > 0 Then
            Dim ret
            ret = MsgBox("If you change Animationsize, all Data will be lost, sure?", vbYesNo, "Animationsize")
            
            If ret = vbYes Then
                frmMain.mnuC2_Click
            Else
                Exit Sub
            End If
        End If
        
        frmMain.shpSelect.Height = txtAH.Text + 1
        
        For i = 1 To AnimationCount
            Animations(i).SelectionH = txtAH.Text
        Next i
    End If

End Sub

Private Sub txtAS_Change()

    frmMain.Timer1.Interval = txtAS.Text
    
    For u = 1 To AnimationCount
        Animations(u).AnimationSpeed = txtAS.Text
        For i = 1 To Animations(u).AniCount
            Animations(u).AniCol(i).Interval = Animations(u).AnimationSpeed
        Next i
    Next u

End Sub

Private Sub txtAW_Change()

    If Not onload Then
        If Animations(CurrentAnimation).AniCount > 0 Then
            Dim ret
            ret = MsgBox("If you change Animationsize, all Data will be lost, sure?", vbYesNo, "Animationsize")
            
            If ret = vbYes Then
                frmMain.mnuC2_Click
            Else
                Exit Sub
            End If
        End If
        
        frmMain.shpSelect.Width = txtAW.Text + 1
        
        For i = 1 To AnimationCount
            Animations(i).SelectionW = txtAW.Text
        Next i
    End If

End Sub

Private Sub VScroll1_Change()
    txtAS.Text = VScroll1.Value
End Sub

Private Sub VScroll2_Change()
    txtAW.Text = VScroll2.Value
End Sub

Private Sub VScroll3_Change()
    txtAH.Text = VScroll3.Value
End Sub

Private Sub cmdScroll1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = True
    lY = y + (txtAW.Text * Screen.TwipsPerPixelY)
End Sub

Private Sub cmdScroll1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    If AChange = True Then
        Dim NewValue As Integer
        
        NewValue = (-y + lY) / Screen.TwipsPerPixelY
        
        If NewValue < 4 Then NewValue = 4
        If NewValue > 4096 Then NewValue = 4096
        txtAW.Text = Snap(NewValue, 4)
        VScroll2.Value = Snap(NewValue, 4)
    End If
    
End Sub

Private Sub cmdScroll1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = False
End Sub

Private Sub cmdScroll2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = True
    lY = y + (txtAH.Text * Screen.TwipsPerPixelY)
End Sub

Private Sub cmdScroll2_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    If AChange = True Then
        Dim NewValue As Integer
        
        NewValue = (-y + lY) / Screen.TwipsPerPixelY
        If NewValue < 4 Then NewValue = 4
        If NewValue > 4096 Then NewValue = 4096
        txtAH.Text = Snap(NewValue, 4)
        VScroll3.Value = Snap(NewValue, 4)
    End If
    
End Sub

Private Sub cmdScroll2_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = False
End Sub
Private Sub picColor_Click()

    cd.ShowColor
    picColor.BackColor = cd.Color
    
    For i = 1 To AnimationCount
        Animations(i).TColor = cd.Color
    Next i

End Sub

Private Sub cmdScroll3_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

    AChange = True
    lY = y + (txtAS.Text * Screen.TwipsPerPixelY)
    
End Sub

Private Sub cmdScroll3_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    If AChange = True Then
        Dim NewValue As Integer
        
        NewValue = (-y + lY) / Screen.TwipsPerPixelY
        If NewValue < 10 Then NewValue = 10
        If NewValue > 10000 Then NewValue = 1000
        txtAS.Text = Snap(NewValue, 10)
        VScroll1.Value = Snap(NewValue, 10)
    End If
    
End Sub

Private Sub cmdScroll3_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = False
End Sub
