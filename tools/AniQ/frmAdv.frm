VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAdv 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Advanced Animation Control"
   ClientHeight    =   5160
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8265
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   344
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   551
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame frame2 
      Caption         =   "Frames"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4935
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   3015
      Begin VB.CommandButton Command2 
         Caption         =   "Deselect All"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   4440
         Width           =   2775
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Select All"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   3960
         Width           =   2775
      End
      Begin VB.ListBox lstFrames 
         Columns         =   2
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00004000&
         Height          =   3570
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   2
         Top             =   240
         Width           =   2775
      End
   End
   Begin VB.Frame frame1 
      Caption         =   "Frame Settings"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4935
      Left            =   3240
      TabIndex        =   0
      Top             =   120
      Width           =   4935
      Begin VB.Frame framenone 
         BorderStyle     =   0  'None
         Caption         =   "Frame3"
         Height          =   4575
         Left            =   120
         TabIndex        =   26
         Top             =   240
         Width           =   4695
         Begin VB.Label Label5 
            Caption         =   "Select at least one Frame"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000C0&
            Height          =   255
            Left            =   960
            TabIndex        =   27
            Top             =   2040
            Width           =   3015
         End
      End
      Begin VB.OptionButton oInterval 
         Caption         =   "Interval"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   37
         Top             =   4440
         Width           =   1335
      End
      Begin VB.OptionButton oRotation 
         Caption         =   "Rotation"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1080
         TabIndex        =   36
         Top             =   4440
         Width           =   1335
      End
      Begin VB.OptionButton oAlpha 
         Caption         =   "Alpha"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   4440
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Fade"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3720
         TabIndex        =   34
         Top             =   4440
         Width           =   1095
      End
      Begin MSComctlLib.ProgressBar progStart 
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   3240
         Width           =   4695
         _ExtentX        =   8281
         _ExtentY        =   450
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   0
         Max             =   255
         Scrolling       =   1
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Apply"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3720
         TabIndex        =   22
         Top             =   2520
         Width           =   1095
      End
      Begin VB.PictureBox Picture3 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         ScaleHeight     =   255
         ScaleWidth      =   1215
         TabIndex        =   18
         Top             =   840
         Width           =   1275
         Begin VB.CommandButton cmdScroll1 
            Caption         =   "*"
            Height          =   255
            Left            =   1080
            MousePointer    =   7  'Size N S
            TabIndex        =   21
            Top             =   0
            Width           =   135
         End
         Begin VB.VScrollBar VScroll2 
            Height          =   255
            Left            =   840
            Max             =   1
            Min             =   360
            TabIndex        =   20
            Top             =   0
            Value           =   32
            Width           =   255
         End
         Begin VB.TextBox txtR 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   19
            Text            =   "0"
            Top             =   0
            Width           =   855
         End
      End
      Begin VB.PictureBox Picture2 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         ScaleHeight     =   255
         ScaleWidth      =   1215
         TabIndex        =   12
         Top             =   1560
         Width           =   1275
         Begin VB.CommandButton cmdScroll3 
            Caption         =   "*"
            Height          =   255
            Left            =   1080
            MousePointer    =   7  'Size N S
            TabIndex        =   15
            Top             =   0
            Width           =   135
         End
         Begin VB.VScrollBar VScroll1 
            Height          =   255
            Left            =   840
            Max             =   10
            Min             =   10000
            SmallChange     =   10
            TabIndex        =   14
            Top             =   0
            Value           =   32
            Width           =   255
         End
         Begin VB.TextBox txtAD 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   13
            Text            =   "32"
            Top             =   0
            Width           =   855
         End
      End
      Begin VB.PictureBox Picture1 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         ScaleHeight     =   255
         ScaleWidth      =   1215
         TabIndex        =   6
         Top             =   480
         Width           =   1275
         Begin VB.CommandButton cmdScroll2 
            Caption         =   "*"
            Height          =   255
            Left            =   1080
            MousePointer    =   7  'Size N S
            TabIndex        =   7
            Top             =   0
            Width           =   135
         End
         Begin VB.TextBox txtA 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   9
            Text            =   "255"
            Top             =   0
            Width           =   855
         End
         Begin VB.VScrollBar VScroll3 
            Height          =   255
            Left            =   840
            Max             =   0
            Min             =   255
            TabIndex        =   8
            Top             =   0
            Value           =   32
            Width           =   255
         End
      End
      Begin VB.CheckBox chkA 
         Caption         =   "Draw Additive"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1575
      End
      Begin MSComctlLib.ProgressBar progEnd 
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   3960
         Width           =   4695
         _ExtentX        =   8281
         _ExtentY        =   450
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   0
         Max             =   255
         Scrolling       =   1
      End
      Begin VB.Label lblEnd 
         AutoSize        =   -1  'True
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   33
         Top             =   4200
         Width           =   120
      End
      Begin VB.Label lblBegin 
         AutoSize        =   -1  'True
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   32
         Top             =   3480
         Width           =   120
      End
      Begin VB.Label Label8 
         Caption         =   "End Value"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   3720
         Width           =   855
      End
      Begin VB.Label Label6 
         Caption         =   "Beginning Value"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   3000
         Width           =   1455
      End
      Begin VB.Line Line1 
         X1              =   120
         X2              =   4800
         Y1              =   2880
         Y2              =   2880
      End
      Begin VB.Label Label10 
         Caption         =   "Change Additve Render Methode,Alphablending and Rotation for selected Frames"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   1035
         Index           =   1
         Left            =   2760
         TabIndex        =   25
         Top             =   240
         Width           =   1995
      End
      Begin VB.Label Label10 
         Caption         =   "Change Animation Speed ( in ms ) for selected Frames"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   435
         Index           =   0
         Left            =   120
         TabIndex        =   24
         Top             =   1920
         Width           =   3195
      End
      Begin VB.Label Label7 
         Caption         =   "To save Settings you have to push the Apply Button"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   120
         TabIndex        =   23
         Top             =   2400
         Width           =   3315
      End
      Begin VB.Label Label4 
         Caption         =   "Rotation"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1440
         TabIndex        =   17
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Animation Delay"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1440
         TabIndex        =   16
         Top             =   1680
         Width           =   1575
      End
      Begin VB.Label Label2 
         Caption         =   "Only visible in Game/Editor , no Preview"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   1320
         Width           =   3975
      End
      Begin VB.Label Label1 
         Caption         =   "Alphablending"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1440
         TabIndex        =   10
         Top             =   600
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmAdv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private AChange As Boolean

Private lY As Integer
Private lX As Integer

Private Sub Command1_Click()

    For i = 1 To Animations(CurrentAnimation).AniCount
        lstFrames.Selected(i - 1) = True
    Next i
    RefreshC
    
End Sub

Private Sub Command2_Click()

    For i = 1 To Animations(CurrentAnimation).AniCount
        lstFrames.Selected(i - 1) = False
    Next i
    RefreshC
    
End Sub

Private Sub Command3_Click()
    
    Dim SelCount As Integer
    SelCount = lstFrames.SelCount
    
    Dim CalcFactor As Double
    Dim StartValue As Integer
    Dim EndValue As Integer
    StartValue = progStart.Value
    EndValue = progEnd.Value
    
    CalcFactor = (EndValue - StartValue) / (SelCount - 1)
    Dim CalcCount As Integer
    
    For i = 1 To Animations(CurrentAnimation).AniCount
        If lstFrames.Selected(i - 1) = True Then
        
            If oAlpha.Value = True Then
                Animations(CurrentAnimation).AniCol(i).Alpha = StartValue + (CalcFactor * CalcCount)
            ElseIf oRotation.Value = True Then
                Animations(CurrentAnimation).AniCol(i).Rotation = StartValue + (CalcFactor * CalcCount)
            Else
                Animations(CurrentAnimation).AniCol(i).Interval = StartValue + (CalcFactor * CalcCount)
            End If
            
            CalcCount = CalcCount + 1
        End If
    Next i

End Sub

Private Sub Command5_Click()
    RefreshN
End Sub

Private Sub Form_Load()

    For i = 1 To Animations(CurrentAnimation).AniCount
        lstFrames.AddItem i & ": " & Animations(CurrentAnimation).AniCol(i).x & "/" & Animations(CurrentAnimation).AniCol(i).y
    Next i
    
End Sub

Private Function RefreshC()

    Dim loaded As Boolean
    Dim newloop As Integer
    
    txtA.ForeColor = vbBlack
    txtR.ForeColor = vbBlack
    txtAD.ForeColor = vbBlack
    
    chkA.Value = 0
    txtA.Text = 255
    txtR.Text = 0
    txtAD.Text = Animations(CurrentAnimation).AnimationSpeed
    
    For i = 1 To Animations(CurrentAnimation).AniCount
        If lstFrames.Selected(i - 1) = True Then
            framenone.Visible = False
            loaded = True
            newloop = i
            chkA.Value = IIf(Animations(CurrentAnimation).AniCol(i).Additive, 1, 0)
            txtA.Text = Animations(CurrentAnimation).AniCol(i).Alpha
            txtR.Text = Animations(CurrentAnimation).AniCol(i).Rotation
            txtAD.Text = Animations(CurrentAnimation).AniCol(i).Interval
            GoTo nextl
        Else
            framenone.Visible = True
        End If
    Next i
    
nextl:
    For i = newloop + 1 To Animations(CurrentAnimation).AniCount
        If lstFrames.Selected(i - 1) = True Then
            If Not Animations(CurrentAnimation).AniCol(i).Additive = Animations(CurrentAnimation).AniCol(newloop).Additive Then chkA.Value = 2
            If Not Animations(CurrentAnimation).AniCol(i).Alpha = Animations(CurrentAnimation).AniCol(newloop).Alpha Then txtA.ForeColor = vbRed
            If Not Animations(CurrentAnimation).AniCol(i).Rotation = Animations(CurrentAnimation).AniCol(newloop).Rotation Then txtR.ForeColor = vbRed
            If Not Animations(CurrentAnimation).AniCol(i).Interval = Animations(CurrentAnimation).AniCol(newloop).Interval Then txtAD.ForeColor = vbRed
        End If
    Next i

End Function

Private Function RefreshN()

    For i = 1 To Animations(CurrentAnimation).AniCount
        If lstFrames.Selected(i - 1) = True Then
        
            If Not chkA.Value = 2 Then Animations(CurrentAnimation).AniCol(i).Additive = IIf(chkA.Value = 1, True, False)
            If Not txtA.ForeColor = vbRed Then Animations(CurrentAnimation).AniCol(i).Alpha = txtA.Text
            If Not txtR.ForeColor = vbRed Then Animations(CurrentAnimation).AniCol(i).Rotation = txtR.Text
            If Not txtAD.ForeColor = vbRed Then Animations(CurrentAnimation).AniCol(i).Interval = txtAD.Text
            
        End If
    Next i

End Function

Private Sub lstFrames_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    RefreshC
End Sub

Private Sub progEnd_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    If x < 0 Then x = 0
    If x > progStart.Width Then x = progStart.Width
    progEnd.Value = (x / progEnd.Width) * 255
    lblEnd.Caption = progEnd.Value
    
End Sub

Private Sub progEnd_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    If Button = 1 Then
        If x < 0 Then x = 0
        If x > progStart.Width Then x = progStart.Width
        progEnd.Value = (x / progEnd.Width) * 255
        lblEnd.Caption = progEnd.Value
    End If
    
End Sub

Private Sub progStart_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    If x < 0 Then x = 0
    If x > progStart.Width Then x = progStart.Width
    progStart.Value = (x / progStart.Width) * 255
    lblBegin.Caption = progStart.Value
    
End Sub

Private Sub progStart_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    If Button = 1 Then
        If x < 0 Then x = 0
        If x > progStart.Width Then x = progStart.Width
        progStart.Value = (x / progStart.Width) * 255
        lblBegin.Caption = progStart.Value
    End If
    
End Sub

Private Sub txtA_Change()
        txtA.ForeColor = vbBlack
End Sub

Private Sub txtAD_Change()
    txtAD.ForeColor = vbBlack
End Sub

Private Sub txtR_Change()
    txtR.ForeColor = vbBlack
End Sub

Private Sub VScroll1_Change()
    txtAD.Text = VScroll1.Value
End Sub

Private Sub VScroll2_Change()
    txtR.Text = VScroll2.Value
End Sub

Private Sub VScroll3_Change()
    txtA.Text = VScroll3.Value
End Sub

Private Sub cmdScroll2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = True
    lY = y + (txtA.Text * Screen.TwipsPerPixelY)
End Sub

Private Sub cmdScroll2_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    If AChange = True Then
        Dim NewValue As Integer
        
        NewValue = (-y + lY) / Screen.TwipsPerPixelY
        
        If NewValue < 0 Then NewValue = 0
        If NewValue > 255 Then NewValue = 255
        txtA.Text = Snap(NewValue, 1)
        VScroll3.Value = Snap(NewValue, 1)
    End If
    
End Sub

Private Sub cmdScroll2_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = False
End Sub

Private Sub cmdScroll1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = True
    lY = y + (txtR.Text * Screen.TwipsPerPixelY)
End Sub

Private Sub cmdScroll1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    If AChange = True Then
        Dim NewValue As Integer
        
        NewValue = (-y + lY) / Screen.TwipsPerPixelY
        
        If NewValue < 0 Then NewValue = 0
        If NewValue > 360 Then NewValue = 360
        txtR.Text = Snap(NewValue, 1)
        VScroll2.Value = Snap(NewValue, 1)
    End If
    
End Sub

Private Sub cmdScroll1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = False
End Sub

Private Sub cmdScroll3_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = True
    lY = y + (txtAD.Text * Screen.TwipsPerPixelY)
End Sub

Private Sub cmdScroll3_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    If AChange = True Then
        Dim NewValue As Integer
        
        NewValue = (-y + lY) / Screen.TwipsPerPixelY
        
        If NewValue < 10 Then NewValue = 10
        If NewValue > 10000 Then NewValue = 10000
        txtAD.Text = Snap(NewValue, 10)
        VScroll1.Value = Snap(NewValue, 10)
    End If
    
End Sub

Private Sub cmdScroll3_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    AChange = False
End Sub
