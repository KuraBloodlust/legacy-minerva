#define   STRICT     
#include <windows.h> 
#include <stdlib.h>  

#include "scale2x.h"
#pragma comment(linker, "/DEF:scale2x.def")

int e,e0,e1,e2,e3,e4,e5,e6,e7,e8;
int a,b,c,d,f,g,h,i;
int x,y,tx,ty;
int rgb;
int sw2,sh2;
int pitch;

EXP_LONG SCALE2X_SetPitch(int cpitch)
{
	pitch = cpitch /4;
	return 0;
}

EXP_LONG SCALE2X_Negative(int* array1,int sw, int sh)
{
	if(pitch==0)pitch=sw;

	for(y = 0; y <= sh-1; y++)
	{
		for(x = 0; x <= sw-1; x++)
		{
			array1[y*pitch+x] = 0xff - array1[y*sw+x];
		}
	}
	return 1;
}

EXP_LONG SCALE2X_Smooth(int* array1,int sw, int sh)
{
	if(pitch==0)pitch=sw;

	for(y = 0; y <= sh-1; y++)
	{
		for(x = 0; x <= sw-1; x++)
		{
			
			if(x==0) 
			{
				x+=1;
				tx = 1;
			}

			if(y==0)
			{
				y+=1;
				ty = 1;
			}

			a=(array1[y*pitch+x]&0xff) + (array1[y*pitch+(x-1)]&0xff) + (array1[(y-1)*pitch+x]&0xff) + (array1[(y+1)*pitch+x]&0xff) + (array1[y*pitch+(x+1)]&0xff);
			a/=5;

			b=((array1[y*pitch+x]>>8)&0xff) + ((array1[y*pitch+(x-1)]>>8)&0xff) + ((array1[(y-1)*pitch+x]>>8)&0xff) + ((array1[(y+1)*pitch+x]>>8)&0xff) + ((array1[y*pitch+(x+1)]>>8)&0xff);
			b/=5;

			c=((array1[y*pitch+x]>>16)&0xff) +	((array1[y*pitch+(x-1)]>>16)&0xff) + ((array1[(y-1)*pitch+x]>>16)&0xff) + ((array1[(y+1)*pitch+x]>>16)&0xff) + ((array1[y*pitch+(x+1)]>>16)&0xff);
			c/=5;

			if(tx==1)
			{
				x-=1;
				tx = 0;
			}

			if(ty==1)
			{
				y-=1;
				ty = 0;
			}
		
			array1[y*pitch+x] = long(a|(b<<8)|(c<<16));

		}
	}
	return 1;
}

EXP_LONG SCALE2X_GreyScale(int* array1,int sw, int sh)
{
	if(pitch==0)pitch=sw;

	for(y = 0; y <= sh-1; y++)
	{
		for(x = 0; x <= sw-1; x++)
		{

			a=array1[y*pitch+x]&0xff;
			b=(array1[y*pitch+x]>>8)&0xff;
			c=(array1[y*pitch+x]>>16)&0xff;
			d=(a+b+c) / 3;

			array1[y*pitch+x] = long(d|(d<<8)|(d<<16));
		}
	}
	return 1;
}

EXP_LONG SCALE2X_Scale2x_Point(int* array1, int* array2,int sw, int sh)
{
	if(pitch==0)pitch=sw*2;

	for(y = 0; y <= sh-1; y++)
	{
		for(x = 0; x <= sw-1; x++)
		{
			e = array1[y*sw+x];

			array2[(y*2)*(pitch)+(x*2)] = e;
			array2[(y*2)*(pitch)+(x*2+1)] = e;
			array2[(y*2+1)*(pitch)+(x*2)] = e;
			array2[(y*2+1)*(pitch)+(x*2+1)] = e;
		}
	}
	return 1;
}

EXP_LONG SCALE2X_Scale2x(int* array1, int* array2,int sw, int sh)
{
	if(pitch==0)pitch=sw*2;

	for(y = 0; y <= sh-1; y++)
	{
		for(x = 0; x <= sw-1; x++)
		{
			if(x >0 && y >0) a = array1[(y-1)*sw+(x-1)];
			if(y >0) b = array1[(y-1)*sw+(x)];
			if(x < sw && y >0) c = array1[(y-1)*sw+(x+1)];
			if(x >0) d = array1[(y)*sw+(x-1)];
			e = array1[y*sw+x];
			if(x < sw) f = array1[(y)*sw+(x+1)];
			if(x >0 && y < sh) g = array1[(y+1)*sw+(x-1)];
			if(y < sh) h = array1[(y+1)*sw+(x)];
			if(x < sw && y < sh) i = array1[(y+1)*sw+(x+1)];
                
			e0 = d == b && b != h && d != f ? d : e;
			e1 = b == f && b != h && d != f ? f : e;
			e2 = d == h && b != h && d != f ? d : e;
			e3 = h == f && b != h && d != f ? f : e;
                
			array2[(y*2)*(pitch)+(x*2)] = e0;
			array2[(y*2)*(pitch)+(x*2+1)] = e1;
			array2[(y*2+1)*(pitch)+(x*2)] = e2;
			array2[(y*2+1)*(pitch)+(x*2+1)] = e3;
		}
	}
	return 1;
}

EXP_LONG SCALE2X_Scale2x_Ext(int* array1, int* array2,int sw, int sh)
{
	if(pitch==0)pitch=sw*2;

	for(y = 0; y <= sh-1; y++)
	{
		for(x = 0; x <= sw-1; x++)
		{
			if(x >0 && y >0) a = array1[(y-1)*sw+(x-1)];
			if(y >0) b = array1[(y-1)*sw+(x)];
			if(x < sw && y >0) c = array1[(y-1)*sw+(x+1)];
			if(x >0) d = array1[(y)*sw+(x-1)];
			e = array1[y*sw+x];
			if(x < sw) f = array1[(y)*sw+(x+1)];
			if(x >0 && y < sh) g = array1[(y+1)*sw+(x-1)];
			if(y < sh) h = array1[(y+1)*sw+(x)];
			if(x < sw && y < sh) i = array1[(y+1)*sw+(x+1)];
                
			if (b != h && d != f) 
			{
				e0 = d == b ? d : e;
				e1 = b == f ? f : e;
				e2 = d == h ? d : e;
				e3 = h == f ? f : e;
			}

			else

			{
				e0 = e;
				e1 = e;
				e2 = e;
				e3 = e;
			}
                
			array2[(y*2)*(pitch)+(x*2)] = e0;
			array2[(y*2)*(pitch)+(x*2+1)] = e1;
			array2[(y*2+1)*(pitch)+(x*2)] = e2;
			array2[(y*2+1)*(pitch)+(x*2+1)] = e3;
		}
	}
	return 1;
}

long conv_RGB(int r,int g,int b)
{
	return long(r|(g<<8)|(b<<16));
}

int get_RGB_pos(int x,int y,int sw, int sh)
{
	int pos;
    pos = x + (y * (sw*4));
	if(x<0 || x >= sw) pos = 0;
	if(y<0 || y >= sh) pos = 0;
	return pos;
}


EXP_LONG SCALE2X_Scale3x_Point(long* array1, long* array2,int sw, int sh)
{
	if(pitch==0)pitch=sw*3;

	for(y = 0; y <= sh-1; y++)
	{
		for(x = 0; x <= sw-1; x++)
		{
			e = array1[y*sw+x];

			array2[(y*3)*(pitch)+(x*3)] = e;
			array2[(y*3)*(pitch)+(x*3+1)] = e;
			array2[(y*3)*(pitch)+(x*3+2)] = e;

			array2[(y*3+1)*(pitch)+(x*3)] = e;
			array2[(y*3+1)*(pitch)+(x*3+1)] = e;
			array2[(y*3+1)*(pitch)+(x*3+2)] = e;

			array2[(y*3+2)*(pitch)+(x*3)] = e;
			array2[(y*3+2)*(pitch)+(x*3+1)] = e;
			array2[(y*3+2)*(pitch)+(x*3+2)] = e;

		}
	}
	return 1;
}

EXP_LONG SCALE2X_Scale3x(long* array1, long* array2,int sw, int sh)
{
	if(pitch==0)pitch=sw*3;

	for(y = 0; y <= sh-1; y++)
	{
		for(x = 0; x <= sw-1; x++)
		{
			if(x >0 && y >0) a = array1[(y-1)*sw+(x-1)];
			if(y >0) b = array1[(y-1)*sw+(x)];
			if(x < sw && y >0) c = array1[(y-1)*sw+(x+1)];
			if(x >0) d = array1[(y)*sw+(x-1)];
			e = array1[y*sw+x];
			if(x < sw) f = array1[(y)*sw+(x+1)];
			if(x >0 && y < sh) g = array1[(y+1)*sw+(x-1)];
			if(y < sh) h = array1[(y+1)*sw+(x)];
			if(x < sw && y < sh) i = array1[(y+1)*sw+(x+1)];
                
			e0 = d == b && b != f && d != h ? d : e;
			e1 = (d == b && b != f && d != h && e != c) || (b == f && b != d && f != h && e != a) ? b : e;
			e2 = b == f && b != d && f != h ? f : e;
			e3 = (d == b && b != f && d != h && e != g) || (d == b && b != f && d != h && e != a) ? d : e;
			e4 = e;
			e5 = (b == f && b != d && f != h && e != i) || (h == f && d != h && b != f && e != c) ? f : e;
			e6 = d == h && d != b && h != f ? d : e;
			e7 = (d == h && d != b && h != f && e != i) || (h == f && d != h && b != f && e != g) ? h : e;
			e8 = h == f && d != h && b != f ? f : e;
                
			array2[(y*3)*(pitch)+(x*3)] = e0;
			array2[(y*3)*(pitch)+(x*3+1)] = e1;
			array2[(y*3)*(pitch)+(x*3+2)] = e2;

			array2[(y*3+1)*(pitch)+(x*3)] = e3;
			array2[(y*3+1)*(pitch)+(x*3+1)] = e4;
			array2[(y*3+1)*(pitch)+(x*3+2)] = e5;

			array2[(y*3+2)*(pitch)+(x*3)] = e6;
			array2[(y*3+2)*(pitch)+(x*3+1)] = e7;
			array2[(y*3+2)*(pitch)+(x*3+2)] = e8;
		}
	}
	return 1;
}

EXP_LONG SCALE2X_Scale3x_Ext(long* array1, long* array2,int sw, int sh)
{
	if(pitch==0)pitch=sw*3;

	for(y = 0; y <= sh-1; y++)
	{
		for(x = 0; x <= sw-1; x++)
		{
			if(x >0 && y >0) a = array1[(y-1)*sw+(x-1)];
			if(y >0) b = array1[(y-1)*sw+(x)];
			if(x < sw && y >0) c = array1[(y-1)*sw+(x+1)];
			if(x >0) d = array1[(y)*sw+(x-1)];
			e = array1[y*sw+x];
			if(x < sw) f = array1[(y)*sw+(x+1)];
			if(x >0 && y < sh) g = array1[(y+1)*sw+(x-1)];
			if(y < sh) h = array1[(y+1)*sw+(x)];
			if(x < sw && y < sh) i = array1[(y+1)*sw+(x+1)];
                
			if (b != h && d != f) 
			{
				e0 = d == b ? d : e;
				e1 = (d == b && e != c) || (b == f && e != a) ? b : e;
				e2 = b == f ? f : e;
				e3 = (d == b && e != g) || (d == b && e != a) ? d : e;
				e4 = e;
				e5 = (b == f && e != i) || (h == f && e != c) ? f : e;
				e6 = d == h	? d : e;
				e7 = (d == g && e != i) || (h == f && e != g) ? h : e;
				e8 = h == f ? f : e;
			}
			
			else
			
			{
				e0 = e;
				e1 = e;
				e2 = e;
				e3 = e;
				e4 = e;
				e5 = e;
				e6 = e;
				e7 = e;
				e8 = e;
			}
                
			array2[(y*3)*(pitch)+(x*3)] = e0;
			array2[(y*3)*(pitch)+(x*3+1)] = e1;
			array2[(y*3)*(pitch)+(x*3+2)] = e2;

			array2[(y*3+1)*(pitch)+(x*3)] = e3;
			array2[(y*3+1)*(pitch)+(x*3+1)] = e4;
			array2[(y*3+1)*(pitch)+(x*3+2)] = e5;

			array2[(y*3+2)*(pitch)+(x*3)] = e6;
			array2[(y*3+2)*(pitch)+(x*3+1)] = e7;
			array2[(y*3+2)*(pitch)+(x*3+2)] = e8;
		}
	}
	return 1;
}