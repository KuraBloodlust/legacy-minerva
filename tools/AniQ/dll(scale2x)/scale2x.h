#ifndef _SCALE2X_
#define _SCALE2X_

#ifdef  __cplusplus
extern  "C" {  // C-Deklarationen f�r C++
#endif

#define EXP_LONG __declspec( dllexport ) long __stdcall

EXP_LONG SCALE2X_Scale2x_Point(long*array1, long*array2,int sw, int sh);
EXP_LONG SCALE2X_Scale2x(long*array1, long*array2,int sw, int sh);
EXP_LONG SCALE2X_Scale2x_Ext(long*array1, long*array2,int sw, int sh);
EXP_LONG SCALE2X_Scale3x_Point(long*array1, long*array2,int sw, int sh);
EXP_LONG SCALE2X_Scale3x(long*array1, long*array2,int sw, int sh);
EXP_LONG SCALE2X_Scale3x_Ext(long*array1, long*array2,int sw, int sh);
EXP_LONG SCALE2X_GreyScale(long*array1,int sw, int sh);
EXP_LONG SCALE2X_Negative(long* array1,int sw, int sh);
EXP_LONG SCALE2X_Smooth(long* array1,int sw, int sh);
EXP_LONG SCALE2X_SetPitch(int cpitch);

#ifdef  __cplusplus
}
#endif

#endif // _SCALE2X_
