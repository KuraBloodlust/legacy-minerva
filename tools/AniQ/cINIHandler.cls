VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cINIHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long

Private mvarFileName As String
Private mvarSection As String

Public Event DataWritten(ByVal FileName As String, ByVal Section As String, ByVal Key As String, ByVal Data As String)
Public Event DataRead(ByVal FileName As String, ByVal Section As String, ByVal Key As String, ByVal Data As String)

Public Property Let Section(ByVal vData As String)
    mvarSection = vData
End Property

Public Property Get Section() As String
    Section = mvarSection
End Property

Public Property Let FileName(ByVal vData As String)
    mvarFileName = vData
End Property

Public Property Get FileName() As String
    FileName = mvarFileName
End Property

Public Function SectionList() As Collection
If FileName = "" Then Err.Raise 52: Exit Function
If FileExists(mvarFileName) = False Then Err.Raise 53: Exit Function

Set SectionList = New Collection

Dim NewFile As Integer
NewFile = FreeFile
Open FileName For Input As NewFile
Dim TempData As String
    Do Until EOF(NewFile)
        Line Input #NewFile, TempData
        If Left(TempData, 1) = "[" And Right(TempData, 1) = "]" Then 'Valid Ini Section
            SectionList.Add Right(Left(TempData, Len(TempData) - 1), Len(TempData) - 2)
        End If
    Loop
Close NewFile
End Function

Public Function KeyList() As Collection
If FileName = "" Then Err.Raise 52: Exit Function
If FileExists(mvarFileName) = False Then Err.Raise 53: Exit Function

Set KeyList = New Collection

Dim NewFile As Integer
NewFile = FreeFile
Open FileName For Input As NewFile
Dim TempData As String
Dim TempData2() As String
    Do Until EOF(NewFile)
        Line Input #NewFile, TempData
        If TempData = "[" & Section & "]" Then 'This INI Section
            Do Until EOF(NewFile) Or Left(TempData, 1) = "[" And Right(TempData, 1) = "]" And Not TempData = "[" & Section & "]"
                Line Input #NewFile, TempData
                If Not TempData = "" Then
                    TempData2 = Split(TempData, "=", 2)
                    If Not TempData2(0) = "" Then
                        If Not Left(TempData2(0), 1) = "[" And Not Right(TempData2(0), 1) = "]" Then
                            KeyList.Add TempData2(0)
                        End If
                    End If
                End If
            Loop
        End If
    Loop
Close NewFile
End Function

Public Sub SaveINI(ByVal Key As String, ByVal Data As String)
    If FileName = "" Then Err.Raise 52: Exit Sub
    WriteIni mvarFileName, mvarSection, Key, Data
End Sub

Public Function OpenINI(ByVal Key As String, Optional ByVal Default As String) As String
    If FileName = "" Then Err.Raise 52: Exit Function
    If FileExists(mvarFileName) = False Then Err.Raise 53: Exit Function
    OpenINI = ReadIni(mvarFileName, mvarSection, Key)
    If OpenINI = "" Then OpenINI = Default
    RaiseEvent DataRead(FileName, Section, Key, OpenINI)
End Function

Private Sub WriteIni(FileName As String, Section As String, Key As String, Value As String)
    WritePrivateProfileString Section, Key, Value, FileName
    RaiseEvent DataWritten(FileName, Section, Key, Value)
End Sub

Private Function ReadIni(FileName As String, Section As String, Key As String) As String
    Dim RetVal As String * 255, v As Long
    v = GetPrivateProfileString(Section, Key, "", RetVal, 255, FileName)
    ReadIni = Left(RetVal, v)
End Function

Private Function FileExists(sFile As String) As Boolean
    If Not Dir(sFile) = "" Then FileExists = True
End Function
