VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMain 
   Caption         =   "R-PG AniQ Animation Editor"
   ClientHeight    =   4755
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   5820
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "ImgQ.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   317
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   388
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer clicktmr 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   4080
      Top             =   1560
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   4560
      Top             =   3480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":08CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":11A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":1A7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":2358
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":2C32
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":350C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":3DE6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":46C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":4F9A
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":5874
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TabStrip Tab1 
      Height          =   375
      Left            =   0
      TabIndex        =   10
      Top             =   360
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   661
      Style           =   2
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4560
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      ImageWidth      =   20
      ImageHeight     =   20
      MaskColor       =   16777215
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":614E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":62EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":6491
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":65A9
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":683E
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":6ABF
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":6D3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":6FC8
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":711D
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":7377
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":73E7
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar TB 
      Align           =   1  'Align Top
      Height          =   330
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   5820
      _ExtentX        =   10266
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "iml"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   10
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "New"
            Object.ToolTipText     =   "New AniQ"
            ImageIndex      =   14
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Load new AniQ File"
            ImageIndex      =   26
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Object.ToolTipText     =   "Save this AniQ File"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   1e-4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Preview"
            Object.ToolTipText     =   "View your Animations"
            ImageIndex      =   28
            Style           =   1
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Settings"
            Object.ToolTipText     =   "Show Settings Window"
            ImageIndex      =   27
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Import"
            Object.ToolTipText     =   "Import Animation Image"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Background"
            Object.ToolTipText     =   "Import Background for Preview"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Exit"
            Object.ToolTipText     =   "Close Program"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar Status 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   4500
      Width           =   5820
      _ExtentX        =   10266
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picTempFrame 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   120
      Left            =   6780
      Picture         =   "ImgQ.frx":7566
      ScaleHeight     =   8
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   16
      TabIndex        =   5
      Top             =   420
      Visible         =   0   'False
      Width           =   240
   End
   Begin MSComDlg.CommonDialog cd 
      Left            =   4560
      Top             =   2400
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "Image Files (*.png;*.gif;*.jpg;*.bmp)|*.png;*.gif;*.jpg;*.bmp|All Files (*.*)|*.*"
   End
   Begin VB.PictureBox picEdit 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3195
      Left            =   0
      MouseIcon       =   "ImgQ.frx":7728
      ScaleHeight     =   209
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   265
      TabIndex        =   0
      Top             =   1080
      Width           =   4035
      Begin VB.PictureBox picPrevBorder 
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2415
         Left            =   840
         ScaleHeight     =   161
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   201
         TabIndex        =   11
         Top             =   360
         Visible         =   0   'False
         Width           =   3015
         Begin VB.CheckBox chkForce 
            BackColor       =   &H00000000&
            Caption         =   "Force alternate drawing Routine"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   240
            TabIndex        =   15
            Top             =   1440
            Width           =   3375
         End
         Begin VB.CheckBox chkInter 
            BackColor       =   &H00000000&
            Caption         =   "Use Image Interpolation (Scale3X)"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   240
            TabIndex        =   14
            Top             =   1200
            Value           =   1  'Checked
            Width           =   3495
         End
         Begin VB.PictureBox picPrev 
            AutoRedraw      =   -1  'True
            AutoSize        =   -1  'True
            BackColor       =   &H00404040&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   1920
            Left            =   720
            MouseIcon       =   "ImgQ.frx":83F2
            ScaleHeight     =   128
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   132
            TabIndex        =   12
            Top             =   840
            Width           =   1980
         End
         Begin VB.Image Image1 
            Height          =   480
            Left            =   120
            Picture         =   "ImgQ.frx":90BC
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblPreview 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Preview"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   720
            TabIndex        =   13
            Top             =   240
            Width           =   675
         End
      End
      Begin VB.PictureBox tmpAni2 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1440
         ScaleHeight     =   855
         ScaleWidth      =   1035
         TabIndex        =   9
         Top             =   2880
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.PictureBox tmpAni 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   360
         ScaleHeight     =   57
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   69
         TabIndex        =   8
         Top             =   2880
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Timer Timer2 
         Interval        =   1
         Left            =   120
         Top             =   120
      End
      Begin VB.HScrollBar HScroll1 
         Enabled         =   0   'False
         Height          =   255
         Left            =   0
         Min             =   1
         SmallChange     =   100
         TabIndex        =   3
         Top             =   2940
         Value           =   1
         Width           =   3795
      End
      Begin VB.VScrollBar VScroll1 
         Enabled         =   0   'False
         Height          =   2955
         Left            =   3780
         Min             =   1
         SmallChange     =   100
         TabIndex        =   2
         Top             =   0
         Value           =   1
         Width           =   255
      End
      Begin VB.Frame command1 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3780
         TabIndex        =   7
         Top             =   2940
         Width           =   255
      End
      Begin VB.Timer Timer1 
         Interval        =   200
         Left            =   120
         Top             =   600
      End
      Begin VB.PictureBox picMain 
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   0
         Left            =   0
         MouseIcon       =   "ImgQ.frx":9986
         ScaleHeight     =   0
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   0
         TabIndex        =   4
         Top             =   0
         Width           =   0
         Begin VB.Shape shpPrev 
            BorderColor     =   &H00FFFFFF&
            BorderStyle     =   3  'Dot
            Height          =   555
            Left            =   0
            Top             =   0
            Visible         =   0   'False
            Width           =   555
         End
         Begin VB.Shape shpSelect 
            BorderColor     =   &H00FFFFFF&
            BorderWidth     =   2
            Height          =   480
            Left            =   0
            Top             =   0
            Width           =   360
         End
      End
   End
   Begin MSComctlLib.ImageList iml 
      Left            =   4560
      Top             =   1800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   28
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":A650
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":B342
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":C034
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":CD26
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":DA18
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":E70A
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":F3FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":100EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":10DE0
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":11AD2
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":127C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":134B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":141A8
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":14E9A
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":15B8C
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":1687E
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":17570
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":18262
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":18F54
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":19C46
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":1A938
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":1B62A
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":1C31C
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":1D00E
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":1DD00
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":1E9F2
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":1F6E4
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ImgQ.frx":203D6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog cd2 
      Left            =   4560
      Top             =   2880
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "AniQ Files (*.aniq)|*.aniq"
   End
   Begin MSComDlg.CommonDialog cd3 
      Left            =   5100
      Top             =   2400
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "Animated Gif Files (*.gif)|*.gif"
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuNew 
         Caption         =   "&New AniQ"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuN 
         Caption         =   "&New Animation"
         Enabled         =   0   'False
      End
      Begin VB.Menu Sep5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOA 
         Caption         =   "&Open AniQ"
      End
      Begin VB.Menu mnuSA 
         Caption         =   "&Save AniQ"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuI 
         Caption         =   "&Import Image"
      End
      Begin VB.Menu mnuImp 
         Caption         =   "&Import Animated Gif"
      End
      Begin VB.Menu Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuE 
         Caption         =   "&End"
      End
   End
   Begin VB.Menu mnuAni 
      Caption         =   "&Animation"
      Begin VB.Menu mnuPr 
         Caption         =   "&Preview Animation"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuOptions 
      Caption         =   "&Options"
      Begin VB.Menu mnuSettings 
         Caption         =   "&Settings"
      End
      Begin VB.Menu mnuAdv 
         Caption         =   "&Advanced Animation Controls"
      End
      Begin VB.Menu mnuGrid 
         Caption         =   "&View Grid"
         Checked         =   -1  'True
      End
      Begin VB.Menu msep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuC1 
         Caption         =   "&Clear Frames (current Animation)"
      End
      Begin VB.Menu mnuC2 
         Caption         =   "Clear Frames (all Animations)"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "&About"
      End
   End
   Begin VB.Menu mnuTab 
      Caption         =   "Tab"
      Visible         =   0   'False
      Begin VB.Menu mnuRename 
         Caption         =   "Rename"
      End
      Begin VB.Menu mnuDelete 
         Caption         =   "Delete"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Engine As New cS2Dmd

Public CPreview As Integer

Private PickMode As Boolean
Private Changed As Boolean
Private loaded As Boolean

Private SelectColor As Integer
Private SelectBack As Boolean

Private MyDC As Long
Private MyDC2 As Long
Private ImageData As ILinfo

Private TexID As Integer
Private FirstTexture As Boolean
Private cur_FileName As String

Public ShowGrid As Boolean
Public GridColor As Long

Public IgnoreFirstClick As Boolean
Public RightClick As Boolean

Public Function RefreshView()

    BitBlt MyDC, 0, 0, ImageData.Width, ImageData.Height, MyDC2, 0, 0, vbSrcCopy
    
    If ShowGrid Then
        RenderGrid MyDC, Animations(CurrentAnimation).SelectionW, Animations(CurrentAnimation).SelectionH, CInt(ImageData.Width), CInt(ImageData.Height), GridColor
    End If
    
    RedrawAll
    
End Function

Private Function CountFrames(x As Integer, y As Integer) As Integer
    
    Dim tCount As Integer
    
    If Animations(CurrentAnimation).AniCount > 0 Then
        For i = Animations(CurrentAnimation).AniCount To 1 Step -1
            If Animations(CurrentAnimation).AniCol(i).x = x And Animations(CurrentAnimation).AniCol(i).y = y Then tCount = tCount + 1
        Next i
    End If
    
    CountFrames = tCount
    
End Function

Private Function CheckFrame(x As Integer, y As Integer) As Integer

    If Animations(CurrentAnimation).AniCount > 0 Then
        For i = 1 To Animations(CurrentAnimation).AniCount
            If Animations(CurrentAnimation).AniCol(i).x = x And Animations(CurrentAnimation).AniCol(i).y = y Then CheckFrame = i
        Next i
    End If

End Function

Private Function SaveAniQ()

    For m = 1 To AnimationCount
        If Animations(m).AniCount = 0 Then
            MsgBox "You must have at least one Frame in every Animation to save a Animation", vbInformation, "Save File"
            Exit Function
        End If
    Next m

    cd2.ShowSave
    If cd2.FileName = "" Then MsgBox "An Error occured, please try again", vbInformation, "Save File": Exit Function
    
    Open cd2.FileName For Binary Access Write As #1

        Dim l As Integer
        l = Len(PicFile)
        
        Put #1, , l
        Put #1, , PicFile
        Put #1, , AnimationCount
        
        For m = 1 To AnimationCount
        
            Put #1, , Animations(m).TColor
            Put #1, , Animations(m).SelectionW
            Put #1, , Animations(m).SelectionH
            Put #1, , Animations(m).AnimationSpeed
            Put #1, , Animations(m).AniCount
            
            l = Len(Animations(m).Name)
            Put #1, , l
            Put #1, , Animations(m).Name
            
            For i = 1 To Animations(m).AniCount
                Put #1, , Animations(m).AniCol(i)
            Next i
            
        Next m
    
    Close #1
    Changed = False

End Function

Private Function LoadAniQ()
    cd2.ShowOpen
    
    If cd2.FileName = "" Then Exit Function
    Open cd2.FileName For Binary Access Read As #1

        Dim l As Integer
        
        Get #1, , l
        PicFile = String(l, Chr(0))
        
        Get #1, , PicFile

        Get #1, , AnimationCount
        ReDim Animations(AnimationCount)
        
        For m = 1 To AnimationCount
        
            Get #1, , Animations(m).TColor
            Get #1, , Animations(m).SelectionW
            Get #1, , Animations(m).SelectionH
            Get #1, , Animations(m).AnimationSpeed
            Get #1, , Animations(m).AniCount
            
            Get #1, , l
            Animations(m).Name = String(l, Chr(0))
            
            Get #1, , Animations(m).Name
            ReDim Animations(m).AniCol(0 To Animations(m).AniCount)
            
            For i = 1 To Animations(m).AniCount
                Get #1, , Animations(m).AniCol(i)
            Next i
        
            shpSelect.Move 0, 0, Animations(m).SelectionW + 1, Animations(m).SelectionH + 1
            
        Next m
    Close #1
    
    CurrentAnimation = 1
    OpenImage Mid(cd2.FileName, 1, Len(cd2.FileName) - Len(cd2.FileTitle)) & "\" & PicFile
        

    CurrentAnimation = 1
    PreviewMode = False
    
    picMain.Cls
    RedrawAll
    
    
    picPrevBorder.Visible = False
    shpSelect.Visible = True
    mnuPr.Checked = False
    TB.Buttons(5).Value = tbrUnpressed
   
End Function


Private Sub chkForce_Click()
        
    If chkForce.Value = 0 Then ForceAlternate = False Else ForceAlternate = True
    
    If ForceAlternate Then
        chkInter.Enabled = False
    Else
        chkInter.Enabled = True
    
        If FirstTexture Then
            Engine.UnloadTexture TexID
        End If
        
        FirstTexture = True
        
        TexID = Engine.LoadTextureFromFile(cur_FileName)
        If Not Engine.Initialize(picPrev.hWnd, Animations(CurrentAnimation).SelectionW * 3, Animations(CurrentAnimation).SelectionH * 3, True) Then Exit Sub
                
    End If
    
    INI.Section = "Preview"
    INI.SaveINI "ForceAlternativeDrawingRoutine", ForceAlternate
    
End Sub

Private Sub chkInter_Click()
    
    If chkInter.Value = 0 Then Interpolation = False Else Interpolation = True
    
    INI.Section = "Preview"
    INI.SaveINI "Interpolation", Interpolation
    
End Sub

Private Sub clicktmr_Timer()
    
    IgnoreFirstClick = False
    clicktmr.Enabled = False
    
End Sub

Private Sub Form_Load()
    
    Tab1.Tabs.Clear
    
    AnimationCount = 1
    ReDim Animations(AnimationCount)
    
    Animations(1).Name = "Animation 1"
    
    CPreview = 1
    CurrentAnimation = 1
    
    INI.FileName = App.Path & "\AniQ_Settings.ini"
    LoadINIFile
    
    Me.Caption = "R-PG AniQ Animation Editor V." & App.Major & "." & App.Minor & "." & App.Revision
    
    RedrawAll

End Sub

Public Function LoadINIFile()
    
    Dim Result As String
    
    INI.Section = "Preview"
    
        Result = INI.OpenINI("ForceAlternativeDrawingRoutine")
        If Result = "True" Then
            ForceAlternate = True
            chkForce.Value = 1
        Else
            ForceAlternate = False
            chkForce.Value = 0
        End If
        
        If Not GetColorDepth = 32 Then
        
            chkForce.Enabled = False
        
            If Not ForceAlternate Then
                MsgBox "Sorry, only 32Bit Color Depth is supported, alternative Drawing Routine will be used.", vbInformation, "Color Depth"
                
                INI.SaveINI "ForceAlternativeDrawingRoutine", "True"
                ForceAlternate = True
                chkForce = 1
            End If
            
        End If
            
        Result = INI.OpenINI("Interpolation")
        If Result = "True" Then
            Interpolation = True
            chkInter.Value = 1
        Else
            Interpolation = False
            chkInter.Value = 0
        End If
    
    INI.Section = "Globals"
    
    Animations(CurrentAnimation).SelectionW = INI.OpenINI("StandartSelectionWidth")
    Animations(CurrentAnimation).SelectionH = INI.OpenINI("StandartSelectionHeight")
    Animations(CurrentAnimation).TColor = INI.OpenINI("StandartColorKey")
    Animations(CurrentAnimation).AnimationSpeed = INI.OpenINI("StandartInterval")
    
    Result = INI.OpenINI("ShowGrid")
    If Result = "True" Then
        mnuGrid.Checked = True
        ShowGrid = True
    Else
        mnuGrid.Checked = False
        ShowGrid = False
    End If
    
    GridColor = INI.OpenINI("GridColor", vbBlack)

    shpSelect.Move 0, 0, Animations(CurrentAnimation).SelectionW + 1, Animations(CurrentAnimation).SelectionH + 1
    
End Function

Public Function SaveINIFile()
    
    Dim Result As String
    
    INI.Section = "Preview"

        INI.SaveINI "ForceAlternativeDrawingRoutine", ForceAlternate
        INI.SaveINI "Interpolation", Interpolation
    
    INI.Section = "Globals"
    
        INI.SaveINI "StandartSelectionWidth", Animations(CurrentAnimation).SelectionW
        INI.SaveINI "StandartSelectionHeight", Animations(CurrentAnimation).SelectionH
        INI.SaveINI "StandartColorKey", Animations(CurrentAnimation).TColor
        INI.SaveINI "StandartInterval", Animations(CurrentAnimation).AnimationSpeed
        INI.SaveINI "ShowGrid", ShowGrid
        INI.SaveINI "GridColor", GridColor

End Function

Private Sub Form_Resize()
 
     If Not Me.WindowState = vbMinimized Then
    
        If Me.Height < 5000 Then Me.Height = 5000
        If Me.Width < 5000 Then Me.Width = 5000

        picPrevBorder.Move 0, 0, Me.ScaleWidth, Me.ScaleHeight
        picPrev.Move Me.ScaleWidth / 2 - (Animations(CurrentAnimation).SelectionW * 3) / 2 - VScroll1.Width / 2, Me.ScaleHeight / 2 - (Animations(CurrentAnimation).SelectionH * 3) / 2 - TB.Height / 2 - HScroll1.Height / 2 - Status.Height / 2, Animations(CurrentAnimation).SelectionW * 3, Animations(CurrentAnimation).SelectionH * 3
        
        Tab1.Move 0, Tab1.Top, Me.ScaleWidth, Tab1.Height
        picEdit.Move 0, Tab1.Height + 6 + TB.Height, Me.ScaleWidth, Me.ScaleHeight - Status.Height - TB.Height - Tab1.Height
        
        If picEdit.ScaleWidth > picMain.ScaleWidth + VScroll1.Width Then
            picMain.Left = picEdit.ScaleWidth / 2 - picMain.ScaleWidth / 2 - VScroll1.Width / 2
        Else
            picMain.Left = 0
        End If
        
        If picEdit.ScaleHeight > picMain.ScaleHeight + HScroll1.Height Then
            picMain.Top = picEdit.ScaleHeight / 2 - picMain.ScaleHeight / 2 - HScroll1.Height / 2
        Else
            picMain.Top = 0
        End If
        
        If picMain.ScaleWidth > picEdit.ScaleWidth - VScroll1.Width Then
            HScroll1.Enabled = True
            HScroll1.Max = picMain.ScaleWidth - (picEdit.ScaleWidth - VScroll1.Width)
        Else
            HScroll1.Enabled = False
        End If
        
        If picMain.ScaleHeight > picEdit.ScaleHeight - HScroll1.Height Then
            VScroll1.Enabled = True
            VScroll1.Max = picMain.ScaleHeight - (picEdit.ScaleHeight - HScroll1.Height)
        Else
            VScroll1.Enabled = False
        End If
    
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If Animations(CurrentAnimation).AniCount = 0 Then End
    
    If Changed Then
        A = MsgBox("Save changes?", vbYesNo, "AniQ Editor")
        If A = vbYes Then SaveAniQ
    End If
    
    End
    
End Sub

Private Sub HScroll1_Change()
    picMain.Left = -HScroll1.Value
End Sub

Private Sub mnuAbout_Click()
    MsgBox "AniQ Animation Editor/Converter" & vbCrLf _
    & "2004-2006 by SmokingFish" & vbCrLf _
    & "mail@SmokingFish.de" & vbCrLf _
    & "www.r-pg.net", vbInformation, "AniQ Editor"
End Sub

Private Sub mnuAdv_Click()
    frmAdv.Show vbModal
End Sub

Private Sub mnuC1_Click()
    
    Animations(CurrentAnimation).AniCount = 0
    RedrawAll
    
End Sub

Public Sub mnuC2_Click()
    
    For i = 1 To AnimationCount
        Animations(i).AniCount = 0
    Next i
    
    RedrawAll
    
End Sub

Private Sub mnuDelete_Click()

    If AnimationCount = 1 Then
        MsgBox "You must have at least one Animation", vbInformation, "Cant delete"
        Exit Sub
    End If
    
    For i = 1 To AnimationCount
        If i > CurrentAnimation Then Animations(i - 1) = Animations(i)
    Next i
    
    AnimationCount = AnimationCount - 1
    ReDim Preserve Animations(AnimationCount)
    CurrentAnimation = AnimationCount
    
    If Not Tab1.Tabs.Count = 1 Then
        Tab1.Tabs.Item(AnimationCount).Selected = True
    End If
    
    RedrawAll
    
End Sub

Private Sub mnuE_Click()
    Unload Me
End Sub

Private Sub mnuGrid_Click()
    
    If mnuGrid.Checked = True Then
        mnuGrid.Checked = False
        ShowGrid = False
    Else
        mnuGrid.Checked = True
        ShowGrid = True
    End If
    
    BitBlt MyDC, 0, 0, ImageData.Width, ImageData.Height, MyDC2, 0, 0, vbSrcCopy
    
    If ShowGrid Then
        RenderGrid MyDC, Animations(CurrentAnimation).SelectionW, Animations(CurrentAnimation).SelectionH, CInt(ImageData.Width), CInt(ImageData.Height), GridColor
    End If
    
    INI.Section = "Globals"
    INI.SaveINI "ShowGrid", ShowGrid
    
    RedrawAll
    
End Sub

Private Sub mnuI_Click()
    OpenImage
End Sub

Private Sub mnuImp_Click()

    cd3.ShowOpen
    If cd3.FileName = "" Then Exit Sub
    
    Set AniGif = New cS2DAnimatedGif
    
    Dim GifHeader As String
    Dim GifLength As Integer
    
    Open cd3.FileName For Binary As #1
        GifLength = FileLen(cd3.FileName)
        GifHeader = String(GifLength, Chr(32))
        Get #1, 1, GifHeader
    Close 1
    
    If Not Left(GifHeader, 6) = "GIF89a" Then
        MsgBox "This is not a valid Animated Gif File"
        Exit Sub
    End If
    
    AniGif.Initialize cd3.FileName, picMain.hDC
    
End Sub

Private Sub mnuN_Click()
    NewAniQ
End Sub


Public Sub mnuNew_Click()

    ret = MsgBox("Do you really want to start a new AniQ File? All changes will be lost!", vbYesNo, "New AniQ")
    
    If ret = vbYes Then
        Tab1.Tabs.Clear
        
        AnimationCount = 1
        ReDim Animations(AnimationCount)
        
        Animations(1).Name = "Animation 1"
        
        CPreview = 1
        CurrentAnimation = 1
        
        INI.FileName = App.Path & "\AniQ_Settings.ini"
        LoadINIFile
        
        RedrawAll
    End If

End Sub

Private Sub mnuOA_Click()
    LoadAniQ
End Sub

Private Sub mnuPr_Click()

    Timer1.Interval = Animations(CurrentAnimation).AnimationSpeed
    
    If ForceAlternate Then
        chkForce.Value = 1
    Else
        chkForce.Value = 0
    End If
    
    If Interpolation Then
        chkInter.Value = 1
    Else
        chkInter.Value = 0
    End If
    
    If mnuPr.Checked = False Then
        TB.Buttons(5).Value = tbrPressed
        PreviewMode = True
        picPrevBorder.Visible = True
        
        picPrev.Move Me.ScaleWidth / 2 - (Animations(CurrentAnimation).SelectionW * 3) / 2 - VScroll1.Width / 2, Me.ScaleHeight / 2 - (Animations(CurrentAnimation).SelectionH * 3) / 2 - TB.Height / 2 - HScroll1.Height / 2 - Status.Height / 2, Animations(CurrentAnimation).SelectionW * 3, Animations(CurrentAnimation).SelectionH * 3
    
        mnuPr.Checked = True
        
        If Not ForceAlternate Then
            
            If FirstTexture Then
                Engine.UnloadTexture TexID
            End If
            
            FirstTexture = True
            
            
            Engine.Initialize picPrev.hWnd, Animations(CurrentAnimation).SelectionW * 3, Animations(CurrentAnimation).SelectionH * 3, True
            TexID = Engine.LoadTextureFromFile(cur_FileName)
            
        End If
    
    Else
        TB.Buttons(5).Value = tbrUnpressed
        PreviewMode = False
        
        picMain.Cls
        RedrawAll
        
        picPrevBorder.Visible = False
        shpSelect.Visible = True
        mnuPr.Checked = False
    End If
    
End Sub

Private Sub mnuRename_Click()

    ret = InputBox("Enter new Name for Animation", "Animation Name", Animations(CurrentAnimation).Name, Me.Left, Me.Top)
    If ret = "" Then ret = Animations(CurrentAnimation).Name
    Animations(CurrentAnimation).Name = ret

End Sub

Private Sub mnuSA_Click()
    SaveAniQ
End Sub

Private Sub mnuSettings_Click()
    frmOptions.Show vbModal
End Sub

Private Sub picEdit_Resize()

    HScroll1.Move 0, picEdit.ScaleHeight - HScroll1.Height - 4, picEdit.ScaleWidth - Command1.Width
    VScroll1.Move picEdit.ScaleWidth - VScroll1.Width, 0, VScroll1.Width, picEdit.ScaleHeight - Command1.Height - 4
    Command1.Move picEdit.ScaleWidth - Command1.Width, picEdit.ScaleHeight - Command1.Height - 4
    
End Sub

Private Sub picMain_KeyUp(KeyCode As Integer, Shift As Integer)
Dim oldval As Integer

    If KeyCode = vbKeyUp Or KeyCode = vbKeyDown Or KeyCode = vbKeyLeft Or KeyCode = vbKeyRight Then
        
        If Animations(CurrentAnimation).AniCount > 0 Then
            Dim ret
            ret = MsgBox("If you change Animationsize, all Data will be lost, sure?", vbYesNo, "Animationsize")
            
            If ret = vbYes Then
                mnuC2_Click
            Else
                Exit Sub
            End If
        End If
    
    End If
    
    If KeyCode = vbKeyDown Then
        oldval = Animations(CurrentAnimation).SelectionH
        
        If oldval < 4096 Then oldval = oldval + 4
        For i = 1 To AnimationCount
            Animations(i).SelectionH = oldval
        Next i
        
        frmMain.shpSelect.Height = oldval + 1
    End If
    
    If KeyCode = vbKeyUp Then
        oldval = Animations(CurrentAnimation).SelectionH
        
        If oldval > 4 Then oldval = oldval - 4
        For i = 1 To AnimationCount
            Animations(i).SelectionH = oldval
        Next i
         
        frmMain.shpSelect.Height = oldval + 1
    End If
    
    If KeyCode = vbKeyRight Then
        oldval = Animations(CurrentAnimation).SelectionW
        
        If oldval < 4096 Then oldval = oldval + 4
        For i = 1 To AnimationCount
            Animations(i).SelectionW = oldval
        Next i
        
        frmMain.shpSelect.Width = oldval + 1
    End If
    
    If KeyCode = vbKeyLeft Then
        oldval = Animations(CurrentAnimation).SelectionW
        
        If oldval > 4 Then oldval = oldval - 4
        For i = 1 To AnimationCount
            Animations(i).SelectionW = oldval
        Next i
         
        frmMain.shpSelect.Width = oldval + 1
    End If
    
    RedrawAll
    
End Sub

Private Sub picMain_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    If Not PreviewMode Then
        If PickMode Then
            picMain.MousePointer = 99
            picColor.BackColor = GetPixel(picMain.hDC, x, y)
        Else
            picMain.MousePointer = 0
            shpSelect.Move Snap(x, Animations(CurrentAnimation).SelectionW), Snap(y, Animations(CurrentAnimation).SelectionH)
        End If
    End If
    
    Dim r As Byte, g As Byte, b As Byte
    
    GetRGB picMain.Point(x, y), r, g, b
    Status.SimpleText = "RGB:" & r & "," & g & "," & b

End Sub

Private Sub picMain_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

If IgnoreFirstClick Then IgnoreFirstClick = False: Exit Sub

    If Not PreviewMode Then
    If Not PickMode Then
    
        Dim ToX As Integer
        Dim ToY As Integer
        
        ToX = Snap(x, Animations(CurrentAnimation).SelectionW)
        ToY = Snap(y, Animations(CurrentAnimation).SelectionH)

        If Button = 1 Then

            If CheckFrame(ToX, ToY) = 0 Then
                TransparentBlt picMain.hDC, ToX, ToY, 16, 10, picTempFrame.hDC, 0, 0, 16, 8, RGB(255, 0, 255)
                picMain.CurrentX = ToX
                picMain.CurrentY = ToY
                picMain.Print Animations(CurrentAnimation).AniCount + 1
            Else
                Dim tCount As Integer
                
                tCount = CountFrames(ToX, ToY)
                TransparentBlt picMain.hDC, ToX, ToY + (tCount * 8), 16, 10, picTempFrame.hDC, 0, 0, 16, 8, RGB(255, 0, 255)
                picMain.CurrentX = ToX
                picMain.CurrentY = ToY + (tCount * 8)
                picMain.Print Animations(CurrentAnimation).AniCount + 1
            End If

            Animations(CurrentAnimation).AniCount = Animations(CurrentAnimation).AniCount + 1
            ReDim Preserve Animations(CurrentAnimation).AniCol(0 To Animations(CurrentAnimation).AniCount)
            
            Animations(CurrentAnimation).AniCol(Animations(CurrentAnimation).AniCount).x = ToX
            Animations(CurrentAnimation).AniCol(Animations(CurrentAnimation).AniCount).y = ToY
            Animations(CurrentAnimation).AniCol(Animations(CurrentAnimation).AniCount).Interval = Animations(CurrentAnimation).AnimationSpeed
            Animations(CurrentAnimation).AniCol(Animations(CurrentAnimation).AniCount).Additive = False
            Animations(CurrentAnimation).AniCol(Animations(CurrentAnimation).AniCount).Alpha = 255
            Animations(CurrentAnimation).AniCol(Animations(CurrentAnimation).AniCount).Rotation = 0
            Changed = True

        End If

        If Button = 2 Then
            If Not CheckFrame(ToX, ToY) = 0 Then
                BitBlt picMain.hDC, ToX, ToY, Animations(CurrentAnimation).SelectionW, Animations(CurrentAnimation).SelectionH, MyDC, ToX, ToY, vbSrcCopy
                RemoveFrame CheckFrame(ToX, ToY)
                CPreview = 1
            End If
        End If

    Else
    
    PickMode = False
    
    End If
    End If
End Sub

Private Function RemoveFrame(ID As Integer)

    For i = 1 To Animations(CurrentAnimation).AniCount
        If i > ID Then Animations(CurrentAnimation).AniCol(i - 1) = Animations(CurrentAnimation).AniCol(i)
    Next i
    
    Animations(CurrentAnimation).AniCount = Animations(CurrentAnimation).AniCount - 1
    ReDim Preserve Animations(CurrentAnimation).AniCol(0 To Animations(CurrentAnimation).AniCount)
    
    RightClick = True
    RedrawAll
    RightClick = False
    
End Function

Public Function RedrawAll()
    
    Dim YPlus As Integer
    
    BitBlt picMain.hDC, 0, 0, ImageData.Width, ImageData.Height, MyDC, 0, 0, vbSrcCopy
    
    For i = 1 To Animations(CurrentAnimation).AniCount
        YPlus = 0
        
        For m = 1 To i - 1
            If Animations(CurrentAnimation).AniCol(i).x = Animations(CurrentAnimation).AniCol(m).x And Animations(CurrentAnimation).AniCol(i).y = Animations(CurrentAnimation).AniCol(m).y Then
            YPlus = YPlus + 8
            End If
        Next m
        
        TransparentBlt picMain.hDC, Animations(CurrentAnimation).AniCol(i).x, Animations(CurrentAnimation).AniCol(i).y + YPlus, 16, 10, picTempFrame.hDC, 0, 0, 16, 8, RGB(255, 0, 255)
        picMain.CurrentX = Animations(CurrentAnimation).AniCol(i).x
        picMain.CurrentY = Animations(CurrentAnimation).AniCol(i).y + YPlus
        picMain.Print i
    Next i
    
    Tab1.Tabs.Clear
    For i = 1 To AnimationCount
        Tab1.Tabs.Add , Animations(i).Name, Animations(i).Name, 4
    Next i
    
    picMain.Refresh
    
End Function

Public Function TransparentLine(hDC As Long, X1 As Integer, Y1 As Integer, X2 As Integer, Y2 As Integer, Alpha As Integer, Color As Long)
      
    ' Bresenham Algorithm oO
    
    Dim LWidth    As Long
    Dim LHeight   As Long
    Dim D         As Long
    Dim ix        As Long
    Dim iy        As Long
    Dim dd        As Integer
    Dim ID        As Integer
    
    Dim pixel As Long
    Dim pixel2 As Long

    LWidth = X2 - X1
    LHeight = Y2 - Y1
    D = 0
    
    If LWidth < 0 Then
        LWidth = -LWidth
        ix = -1
    Else
        ix = 1
    End If
    
    If LHeight < 0 Then
        LHeight = -LHeight
        iy = -1
    Else
        iy = 1
    End If
    
    If LWidth > LHeight Then
        dd = LWidth + LWidth
        ID = LHeight + LHeight
        
        Do
            pixel = GetPixel(hDC, X1, Y1)
            pixel2 = AlphaColor(pixel, Color, Alpha)
            SetPixel hDC, X1, Y1, pixel2
            
            If X1 = X2 Then Exit Do
            X1 = X1 + ix
            D = D + ID
            
            If D > LWidth Then
                Y1 = Y1 + iy
                D = D - dd
            End If
        Loop
    
    Else
        dd = LHeight + LHeight
        ID = LWidth + LWidth
    
        Do
            pixel = GetPixel(hDC, X1, Y1)
            pixel2 = AlphaColor(pixel, Color, Alpha)
            SetPixel hDC, X1, Y1, pixel2
            
            If Y1 = Y2 Then Exit Do
            Y1 = Y1 + iy
            D = D + ID
            
            If D > LHeight Then
                X1 = X1 + ix
                D = D - dd
            End If
        Loop
    End If
    
End Function

Public Function RenderGrid(hDC As Long, GWidth As Integer, GHeight As Integer, Width As Integer, Height As Integer, Color As Long)
    
    Dim x As Integer
    Dim y As Integer
    
    For x = 0 To Width Step GWidth
        
        TransparentLine hDC, x - 1, 0, x - 1, Height, 20, Color
        TransparentLine hDC, x, 0, x, Height, 150, Color
        TransparentLine hDC, x + 1, 0, x + 1, Height, 20, Color
        
    Next x
    
    For y = 0 To Height Step GHeight
        TransparentLine hDC, 0, y - 1, Width, y - 1, 20, Color
        TransparentLine hDC, 0, y, Width, y, 150, Color
        TransparentLine hDC, 0, y + 1, Width, y + 1, 20, Color
    Next y
    
End Function

Public Function OpenImage(Optional FileName As String = "")

    IgnoreFirstClick = True
    clicktmr.Enabled = True
    
    AniCount = 0
    CPreview = 1
    
    SelectionW = Animations(CurrentAnimation).SelectionW
    SelectionH = Animations(CurrentAnimation).SelectionH
    AnimationSpeed = Animations(CurrentAnimation).AnimationSpeed

    If FileName = "" Then
        cd.ShowOpen
        PicFile = cd.FileTitle
        FileName = cd.FileName
        If cd.FileName = "" Then Exit Function
    Else
        PicFile = GetFileNameFromPath(FileName, True)
    End If
    
    ImageData = GetImageInformation(FileName)
    
    If ImageData.Width = 1 Then
        
        Dim GifPicture As StdPicture
        Set GifPicture = LoadPicture(FileName)
        SavePicture GifPicture, App.Path & "\_aniq_temp_gif.bmp"
        FileName = App.Path & "\_aniq_temp_gif.bmp"
        ImageData = GetImageInformation(FileName)
        
        If ImageData.Width = 1 Then
            MsgBox "Error loading Image, sry", vbInformation, "Error"
            Exit Function
        End If
            
    End If
    
    cur_FileName = FileName
    picMain.Move 0, 0, ImageData.Width, ImageData.Height
    
    MyDC = CreateEmptyDC(CInt(ImageData.Width), CInt(ImageData.Height))
    MyDC2 = CreateEmptyDC(CInt(ImageData.Width), CInt(ImageData.Height))
    RenderImageToHDC FileName, MyDC, 0, 0, 0, 0, -1, -1
    RenderImageToHDC FileName, MyDC2, 0, 0, 0, 0, -1, -1
    
    If ShowGrid Then
        RenderGrid MyDC, Animations(CurrentAnimation).SelectionW, Animations(CurrentAnimation).SelectionH, CInt(ImageData.Width), CInt(ImageData.Height), GridColor
    End If

    HScroll1.Enabled = True
    VScroll1.Enabled = True
    Command1.Enabled = True

    AniCount = 0
    ReDim AniCol(0 To 1)
    
    RedrawAll

    Form_Resize
    loaded = True
    
    mnuN.Enabled = True
    mnuNew.Enabled = True
    mnuSA.Enabled = True
    
    mnuPr.Enabled = True
    
    TB.Buttons(1).Enabled = True
    TB.Buttons(3).Enabled = True
    TB.Buttons(5).Enabled = True

End Function

Private Function NewAniQ()

    AnimationCount = AnimationCount + 1
    CurrentAnimation = AnimationCount
    ReDim Preserve Animations(CurrentAnimation)
    
    Animations(CurrentAnimation).AniCount = 0
    ReDim Animations(CurrentAnimation).AniCol(0 To 1)
    
    CPreview = 1
    Animations(CurrentAnimation).AniCount = 0
    Animations(CurrentAnimation).SelectionW = Animations(1).SelectionW
    Animations(CurrentAnimation).SelectionH = Animations(1).SelectionH
    Animations(CurrentAnimation).AnimationSpeed = Animations(1).AnimationSpeed
    Animations(CurrentAnimation).Name = "Animation " & AnimationCount
    Animations(CurrentAnimation).TColor = Animations(1).TColor
    
    RedrawAll
    
    Tab1.Tabs.Item(CurrentAnimation).Selected = True
    picMain.Refresh

End Function


Private Sub picPrevBorder_Resize()
     
    chkInter.Move 5, picPrevBorder.ScaleHeight - 125
    chkForce.Move 5, chkInter.Top + 15
    
End Sub

Private Sub Tab1_Click()

    CurrentAnimation = Tab1.SelectedItem.Index
    CPreview = 1
    RedrawAll
    
End Sub

Private Sub Tab1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

    If Button = 2 Then
        Me.PopupMenu mnuTab, , x / Screen.TwipsPerPixelX, y / Screen.TwipsPerPixelY, mnuRename
    End If

End Sub

Private Sub TB_ButtonClick(ByVal Button As MSComctlLib.Button)

    If Button.Index = 2 Then
        LoadAniQ
    End If
    
    If Button.Index = 3 Then
        SaveAniQ
    End If
    
    If Button.Index = 6 Then
        frmOptions.Show vbModal
    End If
    
    If Button.Index = 5 Then
       mnuPr_Click
    End If
    
    If Button.Index = 1 Then
        NewAniQ
    End If
    
    If Button.Index = 8 Then
        OpenImage
    End If
    
    If Button.Index = 9 Then
        
        cd.ShowOpen
        BackgroundPath = cd.FileTitle
        BackgroundImage = True
        MsgBox "Background loaded", vbInformation

    End If
    
    If Button.Index = 10 Then
        Unload Me
    End If

End Sub

Private Sub Timer1_Timer()

    If PreviewMode Then
        shpSelect.Visible = False
        picPrev.Visible = True
        
        If Animations(CurrentAnimation).AniCount > 0 Then
            
            Timer1.Interval = Animations(CurrentAnimation).AniCol(CPreview).Interval
            
            If Not ForceAlternate Then
                Engine.Clear
                Engine.BeginScene
                    
                    If BackgroundImage Then
                        Engine.RenderTexture BackgroundID, 0, 0
                    End If
                    
                    Engine.RenderTexture TexID, 0, 0, Animations(CurrentAnimation).AniCol(CPreview).x, Animations(CurrentAnimation).AniCol(CPreview).y, Animations(CurrentAnimation).SelectionW, Animations(CurrentAnimation).SelectionH, Animations(CurrentAnimation).TColor, Animations(CurrentAnimation).AniCol(CPreview).Alpha
                
                Engine.EndScene
                Engine.Flip
            Else
                StretchBlt picPrev.hDC, 0, 0, Animations(CurrentAnimation).SelectionW * 3, Animations(CurrentAnimation).SelectionH * 3, MyDC, Animations(CurrentAnimation).AniCol(CPreview).x, Animations(CurrentAnimation).AniCol(CPreview).y, Animations(CurrentAnimation).SelectionW, Animations(CurrentAnimation).SelectionH, vbSrcCopy
                picPrev.Refresh
            End If
            
            If CPreview < Animations(CurrentAnimation).AniCount Then
                CPreview = CPreview + 1
            Else
                CPreview = 1
            End If
            
            lblPreview.Caption = "Preivew [" & CPreview & "/" & Animations(CurrentAnimation).AniCount & "]"
            
        Else
            picPrev.Visible = False
            picMain.Refresh
        End If
    End If
    
End Sub

Private Sub Timer2_Timer()
    
    If SelectColor = 256 Then SelectBack = False
    If SelectColor = 0 Then SelectBack = True
    
    If SelectBack Then SelectColor = SelectColor + 2
    If Not SelectBack Then SelectColor = SelectColor - 2
    
    shpSelect.BorderColor = RGB(20, SelectColor, 200)
    
End Sub

Private Sub VScroll1_Change()
    picMain.Top = -VScroll1.Value
End Sub
