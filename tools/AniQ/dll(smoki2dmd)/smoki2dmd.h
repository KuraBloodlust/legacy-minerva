#ifndef _SMOKI2DMD_
#define _SMOKI2DMD_

#ifdef  __cplusplus
extern  "C" {  // C-Deklarationen f�r C++
#endif

#define EXP_LONG __declspec( dllexport ) long __stdcall

EXP_LONG SMOKI2DMD_Initialize(int* Destination, short Width, short Height);
EXP_LONG SMOKI2DMD_LoadTextureFromFile(char* Filename);
EXP_LONG SMOKI2DMD_RenderTexture(short id, short x, short y, short sw, short sh, short sx, short sy, int clrkey , short alpha,int coloroverlay);
EXP_LONG SMOKI2DMD_RefreshDestination(int* Destination, short Pitch);
EXP_LONG SMOKI2DMD_SetClippingWindow(int X, int Y, int Width, int Height);
EXP_LONG SMOKI2DMD_Unload();
EXP_LONG SMOKI2DMD_UnloadTexture(short ID);
EXP_LONG SMOKI2DMD_ClearArray(int* array1, short sw, short sh, int color);

#ifdef  __cplusplus
}
#endif

#endif // _SMOKI2DMD_
