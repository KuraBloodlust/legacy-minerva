Attribute VB_Name = "modGlobal"

Public Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long
Public Declare Function SelectObject Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long) As Long
Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Public Declare Function CreateCompatibleBitmap Lib "gdi32" (ByVal hDC As Long, ByVal nWidth As Long, ByVal nHeight As Long) As Long
Public Declare Function CreateCompatibleDC Lib "gdi32" (ByVal hDC As Long) As Long
Public Declare Function GetDC Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function DeleteDC Lib "gdi32" (ByVal hDC As Long) As Long
Public Declare Function InvertRect Lib "user32" (ByVal hDC As Long, lpRect As RECT) As Long
Public Declare Function GetPixel Lib "gdi32" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long) As Long
Public Declare Function SetPixel Lib "gdi32" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long, ByVal crColor As Long) As Long
Public Declare Function IntersectRect Lib "user32" (lpDestRect As RECT, lpSrc1Rect As RECT, lpSrc2Rect As RECT) As Long
Public Declare Function SetRect Lib "user32" (lpRect As RECT, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Public Declare Function Pie Lib "gdi32" (ByVal hDC As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long, ByVal X3 As Long, ByVal Y3 As Long, ByVal X4 As Long, ByVal Y4 As Long) As Long
Public Declare Function Rectangle Lib "gdi32" (ByVal hDC As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Public Declare Function RoundRect Lib "gdi32" (ByVal hDC As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long, ByVal X3 As Long, ByVal Y3 As Long) As Long
Public Declare Function StretchBlt Lib "gdi32" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long
Public Declare Sub apiCopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal length As Long)
Public Declare Function DrawText Lib "user32" Alias "DrawTextA" (ByVal hDC As Long, ByVal lpStr As String, ByVal nCount As Long, lpRect As RECT, ByVal wFormat As Long) As Long
Public Declare Function GetTickCount Lib "kernel32" () As Long
Public Declare Function GetDeviceCaps& Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long)
Public Declare Function ReleaseDC& Lib "user32" (ByVal hWnd As Long, ByVal hDC As Long)
Public Declare Function SetWindowRgn Lib "user32" (ByVal hWnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Public Declare Function CreateRectRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Public Declare Function CombineRgn Lib "gdi32" (ByVal hDestRgn As Long, ByVal hSrcRgn1 As Long, ByVal hSrcRgn2 As Long, ByVal nCombineMode As Long) As Long
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Declare Function ReleaseCapture Lib "user32" () As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Public Declare Function TransparentBlt Lib "msimg32.dll" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal crTransparent As Long) As Boolean
Public Declare Function newGetObject Lib "gdi32" Alias "GetObjectA" (ByVal hObject As Long, ByVal nCount As Long, lpObject As Any) As Long
Public Const SRCCOPY = &HCC0020

Public Const SWP_NOMOVE = 2
Public Const SWP_NOSIZE = 1
Public Const FLAGS = SWP_NOMOVE Or SWP_NOSIZE
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2

Public Const RGN_OR = 2
Public Const WM_NCLBUTTONDOWN = &HA1
Public Const HTCAPTION = 2

Public Const PI                     As Double = 3.14159265358979
Private Const PLANES                As Long = 14
Private Const BITSPIXEL             As Long = 12

Public Type RECT
        Left As Long
        Top As Long
        Right As Long
        Bottom As Long
End Type

Public AniGif As cS2DAnimatedGif

Public PreviewMode As Boolean
Public PicFile As String

Public Type AniFrame
    x As Integer
    y As Integer
    Interval As Integer
    Alpha As Integer
    Rotation As Integer
    Additive As Boolean
End Type

Public Type Animation
    AniCol() As AniFrame
    AniCount As Integer
    SelectionW As Integer
    SelectionH As Integer
    AnimationSpeed As Integer
    TColor As Long
    Name As String
End Type

Public Type RGBColor
    Red As Long
    Green As Long
    Blue As Long
End Type

Public Animations() As Animation
Public AnimationCount As Integer
Public CurrentAnimation As Integer

Public Interpolation As Boolean
Public ForceAlternate As Boolean

Public INI As New cINIHandler

Public BackgroundImage As Boolean
Public BackgroundPath As String
Public BackgroundID As Integer

Public Function AlphaColor(Color1 As Long, Color2 As Long, Alpha As Integer) As Long
    
    Dim te As RGBColor
    Dim ta As RGBColor
    Dim ti As RGBColor
    Dim tAlpha As Integer

    ta = SplitRGB(Color1)
    te = SplitRGB(Color2)

    tAlpha = (255 - Alpha) '/ 255
    
    ti.Red = te.Red + (((ta.Red - te.Red) * tAlpha) / 255)
    ti.Blue = te.Blue + (((ta.Blue - te.Blue) * tAlpha) / 255)
    ti.Green = te.Green + (((ta.Green - te.Green) * tAlpha) / 255)
    
    If ti.Red < 0 Then ti.Red = 0
    If ti.Green < 0 Then ti.Green = 0
    If ti.Blue < 0 Then ti.Blue = 0
    If ti.Red > 255 Then ti.Red = 255
    If ti.Green > 255 Then ti.Green = 255
    If ti.Blue > 255 Then ti.Blue = 255

    AlphaColor = RGB(CInt(ti.Red), CInt(ti.Green), CInt(ti.Blue))
    
End Function

Public Function SplitRGB(Color As Long) As RGBColor
        
    SplitRGB.Red = Color Mod 256&
    SplitRGB.Green = ((Color And &HFF00) / 256&) Mod 256&
    SplitRGB.Blue = (Color And &HFF0000) / 65536
    
End Function

Public Function Snap(Cordinate As Variant, Dimension As Integer) As Integer
    Snap = (Cordinate \ Dimension) * Dimension
End Function

Public Function DirectoryExist(Path As String) As Boolean
dirFolder = Dir(Path, vbDirectory)

If dirFolder <> "" Then
    DirectoryExist = True
End If

End Function



Public Function GetColorDepth() As Integer
    
    Dim nPlanes As Integer, BitsPerPixel As Integer, dc As Long
    
    dc = GetDC(0)
    nPlanes = GetDeviceCaps(dc, PLANES)
    BitsPerPixel = GetDeviceCaps(dc, BITSPIXEL)
    ReleaseDC 0, dc
    GetColorDepth = nPlanes * BitsPerPixel
    
End Function

Public Function GetFileNameFromPath(ByVal FullFilePath As String, bExtension As Boolean) As String
    Dim sFileName As String, FindSlash As Long
    
    If FullFilePath = "" Then Exit Function
    FindSlash = InStrRev(FullFilePath, "\")
    sFileName = Mid(FullFilePath, FindSlash + 1, Between(FindSlash, Len(FullFilePath)))
    If bExtension = True Then
        GetFileNameFromPath = sFileName
    Else
        GetFileNameFromPath = Left(sFileName, Len(sFileName) - Between(InStrRev(sFileName, "."), Len(sFileName)))
    End If
End Function
Public Function Between(lFrom As Long, lTo As Long)
    For j = lFrom To lTo
        l = l + 1
    Next
    Between = l
End Function

Public Function MakeRGB(ByVal Red As Byte, ByVal Green As Byte, ByVal Blue As Byte) As Long
MakeRGB = (((&H100& * Green) Or Red) Or (&H10000 * Blue))
End Function

Public Sub GetRGB(ByVal Color As Long, Optional ByRef Red As Byte, Optional ByRef Green As Byte, Optional ByRef Blue As Byte)
Red = (Color And &HFF)
Green = (Color And &HFFFF&) \ &H100
Blue = (Color \ &H10000)
End Sub
