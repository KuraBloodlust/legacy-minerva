VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDevILImage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Handle As Long

Public devil As cDevIL



Public Sub BindMe()
devil.BindImg Handle
End Sub

Public Property Get Width()
BindMe

Dim inf As ILinfo
iluGetImageInfo inf
Width = inf.Width
End Property


Public Property Get Height()
BindMe

Dim inf As ILinfo
iluGetImageInfo inf
Height = inf.Height
End Property


Public Sub RenderTo(hDCTo As Long, XDest As Long, YDest As Long, xSrc As Long, ySrc As Long, lWidth As Long, lHeight As Long)
BindMe
hjpgdc = CreateCompatibleDC(0)

hJpgBmp = ilutConvertToHBitmap(0)
SelectObject hjpgdc, hJpgBmp
BitBlt hDCTo, XDest, YDest, lWidth, lHeight, hjpgdc, xSrc, ySrc, SRCCOPY
End Sub



