Attribute VB_Name = "modLogging"
Public Logging As Boolean
Public LogToFile As Boolean

Private LogFile As Integer


Public Enum eLogTypes
ltNone
ltInformation
ltQuestion
ltAlert
ltError
ltSuccess
ltAction
End Enum

Public Sub InitLogging()
LogFile = FreeFile(9)
Open App.Path & "\minerva.log" For Output As LogFile
End Sub

Public Sub Log(Message As String, logType As eLogTypes)
Print #LogFile, Now & " (" & LTToStr(logType) & "): " & Message
End Sub

Private Function LTToStr(logType As eLogTypes)
Select Case logType
Case ltNone:
LTToStr = "NONE"
Case ltInformation:
LTToStr = "INFORMATION"
Case ltQuestion:
LTToStr = "  QUESTION "
Case ltAlert:
LTToStr = "  WARNING  "
Case ltError:
LTToStr = "   ERROR   "
Case ltSuccess:
LTToStr = "  SUCCESS  "
Case ltAction:
LTToStr = "   ACTION  "

End Select

End Function

Public Sub FinishLogging()
Close LogFile
End Sub
