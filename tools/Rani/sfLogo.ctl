VERSION 5.00
Begin VB.UserControl sfLogo 
   AutoRedraw      =   -1  'True
   ClientHeight    =   1695
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2370
   ScaleHeight     =   113
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   158
   Begin VB.PictureBox picLogo 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   480
      Picture         =   "sfLogo.ctx":0000
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   1
      Top             =   840
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   1440
      Top             =   1200
   End
   Begin VB.PictureBox picBG 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   0
      ScaleHeight     =   49
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   113
      TabIndex        =   0
      Top             =   0
      Width           =   1695
   End
End
Attribute VB_Name = "sfLogo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Caption As String
Public Gradient As New clsGradient
Dim tAngle As Integer
Dim tBack As Boolean

Private Sub UserControl_Initialize()
    Caption = "Sprite Properties"
End Sub

Private Sub DrawText(Text As String)
    
    Dim tx As Integer
    Dim ty As Integer
    
    tx = 40
    ty = (UserControl.ScaleHeight / 2) - (UserControl.TextHeight(Caption) / 2)
    picBG.CurrentX = tx
    picBG.CurrentY = ty
    picBG.ForeColor = vbBlack
    picBG.FontBold = True
    
    Dim ts() As String
    ts = Split(Caption, vbCrLf)
    
    For i = 0 To UBound(ts)
        picBG.CurrentX = tx
        picBG.Print ts(i)
    Next i
    
    tx = tx - 1
    ty = ty - 1
    picBG.CurrentX = tx
    picBG.CurrentY = ty
    picBG.ForeColor = vbWhite
    
    For i = 0 To UBound(ts)
        picBG.CurrentX = tx
        picBG.Print ts(i)
        
        Dim ta As Integer
        ta = tx
        
        TransparentLine picBG.hDC, ta, picBG.CurrentY, ta + picBG.TextWidth(ts(i)), picBG.CurrentY, 200, &HC47813
        ta = tx
        TransparentLine picBG.hDC, ta, picBG.CurrentY + 1, ta + picBG.TextWidth(ts(i)), picBG.CurrentY + 1, 150, &HC47813
    Next i
     
End Sub



Private Sub DrawRect(RC As RECT, Optional Alpha As Integer = 255, Optional Color As Long = vbWhite)

    TransparentLine picBG.hDC, CInt(RC.Left), CInt(RC.Top), CInt(RC.Right), CInt(RC.Top), Alpha, Color
    TransparentLine picBG.hDC, CInt(RC.Right) - 1, CInt(RC.Top), CInt(RC.Right) - 1, CInt(RC.Bottom), Alpha, Color
    TransparentLine picBG.hDC, CInt(RC.Right), CInt(RC.Bottom) - 1, CInt(RC.Left), CInt(RC.Bottom) - 1, Alpha, Color
    TransparentLine picBG.hDC, CInt(RC.Left), CInt(RC.Bottom), CInt(RC.Left), CInt(RC.Top), Alpha, Color

End Sub

Public Sub Refresh()
    UserControl_Resize
End Sub

Private Sub UserControl_Resize()

    Dim tr As RECT

    tr.Right = UserControl.ScaleWidth
    tr.Bottom = UserControl.ScaleHeight
    
    picBG.Width = UserControl.Width
    picBG.Height = UserControl.Height
    
    Gradient.Angle = 0
    Gradient.Color1 = &HE98F16
    Gradient.Color2 = vbBlack
    
    Gradient.Draw picBG
    
    TransparentBlt picBG.hDC, 5, 5, picLogo.ScaleWidth, picLogo.ScaleHeight, picLogo.hDC, 0, 0, picLogo.ScaleWidth, picLogo.ScaleHeight, vbMagenta
  
    DrawRect tr, 255, &HC47813
    
    tr.Left = tr.Left + 1
    tr.Top = tr.Top + 1
    tr.Right = tr.Right - 1
    tr.Bottom = tr.Bottom - 1
    DrawRect tr, 150, &HC47813
    
    DrawText Caption
    
End Sub
