VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FormImageList 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Picture Database"
   ClientHeight    =   3960
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   6075
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   264
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   405
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton CommandOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   6
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Frame frmProp 
      Caption         =   "Picture Options:"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   3
      Top             =   1800
      Width           =   4215
      Begin VB.PictureBox picPreview 
         AutoRedraw      =   -1  'True
         Height          =   1455
         Left            =   120
         Picture         =   "FormImageList.frx":0000
         ScaleHeight     =   93
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   101
         TabIndex        =   5
         Top             =   240
         Width           =   1575
         Begin VB.Shape shpSelect 
            BorderColor     =   &H00FFFFFF&
            BorderStyle     =   2  'Dash
            BorderWidth     =   3
            DrawMode        =   15  'Merge Pen Not
            FillColor       =   &H00E19A66&
            FillStyle       =   0  'Solid
            Height          =   1455
            Left            =   0
            Top             =   0
            Width           =   1575
         End
      End
      Begin MSComctlLib.Slider sHeight 
         Height          =   255
         Left            =   1680
         TabIndex        =   11
         Top             =   1440
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   450
         _Version        =   393216
         Max             =   400
         TickStyle       =   3
      End
      Begin MSComctlLib.Slider sWidth 
         Height          =   255
         Left            =   1680
         TabIndex        =   10
         Top             =   960
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   450
         _Version        =   393216
         Max             =   400
         TickStyle       =   3
      End
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3360
         TabIndex        =   9
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton CommandImageReset 
         Caption         =   "Reset"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1800
         TabIndex        =   4
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Height"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1800
         TabIndex        =   8
         Top             =   1200
         Width           =   540
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Width"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1800
         TabIndex        =   7
         Top             =   720
         Width           =   480
      End
   End
   Begin MSComctlLib.StatusBar status 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   3705
      Width           =   6075
      _ExtentX        =   10716
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton CommandImageAdd 
      Caption         =   "Add Picture"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   1
      ToolTipText     =   "Erstellt ein neues Image anhand des aktuell markierten Pictures"
      Top             =   1800
      Width           =   1575
   End
   Begin VB.ListBox ListImage 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1620
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5895
   End
End
Attribute VB_Name = "FormImageList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private AktlImage As Integer
Private i As Integer

Private SelX As Integer
Private SelY As Integer

Dim tfw As Double
Dim tfh As Double
Dim tww As Integer
Dim thh As Integer
        
Public Function GetAktlImage() As Integer
    GetAktlImage = AktlImage
End Function

Public Function FillListImage()
    ListImage.Clear
    For i = 1 To Scene.GetImageCount
        ListImage.AddItem Scene.GetImageDescription(i)
    Next i
    
    status.SimpleText = "Pictures in Database : " & Scene.GetImageCount
End Function

Private Sub Command1_Click()
    On Error GoTo err

    Dim tr As RECT
    tr.Left = SelX
    tr.Top = SelY
    tr.Right = sWidth.Value
    tr.Bottom = sHeight.Value
    
    Scene.SetImageRect ListImage.ListIndex + 1, tr
    
    Dim ti As Integer
    ti = ListImage.ListIndex
    
    PicturePreview
    FillListImage
    
    ListImage.ListIndex = ti
    Exit Sub
err:
    MsgBox "An Error accured, please check your Values", vbCritical, "Error"
End Sub

Private Sub PicturePreview()
    If ListImage.ListIndex >= 0 Then
        Dim TX As Integer
        Dim Ty As Integer
        Dim tw As Integer
        Dim tH As Integer
        
        TX = Scene.GetImageLeft(ListImage.ListIndex + 1)
        Ty = Scene.GetImageTop(ListImage.ListIndex + 1)
        tw = Scene.GetImageRight(ListImage.ListIndex + 1)
        tH = Scene.GetImageBottom(ListImage.ListIndex + 1)
        
        picPreview.Cls
        'RenderImageToHDC Scene.GetImageTexturePath(ListImage.ListIndex + 1), picPreview.hDC, 0, 0, tx, ty, picPreview.ScaleWidth, picPreview.ScaleHeight, tw, tH
        RenderImageToHDC Scene.GetImageTexturePath(ListImage.ListIndex + 1), picPreview.hDC, 0, 0, 0, 0, picPreview.ScaleWidth, picPreview.ScaleHeight
        
        Dim ti As Integer
        ti = Scene.GetImageTextureID(ListImage.ListIndex + 1)
        tww = Engine.TexturePool.TextureWidth(ti)
        thh = Engine.TexturePool.TextureHeight(ti)
        
        tfw = tww / picPreview.ScaleWidth
        tfh = thh / picPreview.ScaleHeight
        
        sWidth.Min = 8
        sHeight.Min = 8
        sWidth.Max = tww
        sHeight.Max = thh
        sWidth.Value = tw
        sHeight.Value = tH
        
        shpSelect.Move TX / tfw, Ty / tfh, tw / tfw, tH / tfh

        picPreview.Refresh
    End If
End Sub

Private Sub CommandImageAdd_Click()
    Dim NewPic As String
    
    FormPictureList.Show 1
    NewPic = FormPictureList.GetPictureName
    'Unload FormPictureList
    
    If FormPictureList.ListPicture.ListIndex > -1 Then
        Scene.AddImage Scene.GetPictureID(NewPic), 0, 0, Engine.TexturePool.TextureWidth(Scene.GetPictureTextureID(Scene.GetPictureID(NewPic))), Engine.TexturePool.TextureHeight(Scene.GetPictureTextureID(Scene.GetPictureID(NewPic)))
        FillListImage
        frmMain.ComboSpriteImage.AddItem Scene.GetImageDescription(Scene.GetImageCount)
    Else
        'MsgBox "Kein Picture ausgewählt!", vbExclamation Or vbOKOnly
    End If
End Sub

Private Sub CommandImageReset_Click()
    If ListImage.ListIndex >= 0 Then
        Scene.ResetImageRect ListImage.ListIndex + 1
        FillListImage
        PicturePreview
    End If
End Sub

Private Sub CommandOK_Click()
Unload Me
End Sub

Private Sub Form_Load()
    FillListImage
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmMain.SetAktlShow ShowFrame
End Sub

Private Sub ListImage_Click()
    AktlImage = ListImage.ListIndex + 1
    frmMain.SetAktlImage ListImage.ListIndex + 1
    
    PicturePreview
    
    If ListImage.ListIndex > -1 Then frmProp.Enabled = True Else frmProp.Enabled = False
End Sub

Private Sub picPreview_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    SelX = Snap(X * tfw, sWidth.Value)
    SelY = Snap(Y * tfh, sHeight.Value)
    shpSelect.Move SelX / tfw, SelY / tfh, sWidth.Value / tfw, sHeight.Value / tfh
End Sub

Private Sub sHeight_Click()
    shpSelect.Height = sHeight.Value / tfh
End Sub

Private Sub sWidth_Click()
    shpSelect.Width = sWidth.Value / tfw
End Sub
