VERSION 5.00
Begin VB.Form frmSpriteNew 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "New Sprite"
   ClientHeight    =   795
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2670
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   53
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   178
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   15
      TabIndex        =   2
      Top             =   360
      Width           =   1215
   End
   Begin VB.CommandButton CommandOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      TabIndex        =   1
      Top             =   360
      Width           =   1215
   End
   Begin VB.TextBox TextSpriteName 
      Height          =   285
      Left            =   15
      MaxLength       =   10
      TabIndex        =   0
      Text            =   "Sprite"
      Top             =   0
      Width           =   2655
   End
End
Attribute VB_Name = "frmSpriteNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private strSpriteName As String

Public Function GetSpriteName() As String
    GetSpriteName = strSpriteName
End Function

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub CommandOK_Click()
    If TextSpriteName.Text > "" Then
        strSpriteName = TextSpriteName.Text
        Me.Hide
    Else
        MsgBox "Please name the sprite!", vbOKOnly Or vbExclamation
    End If
End Sub
