VERSION 5.00
Begin VB.Form frmOption 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Setup"
   ClientHeight    =   1065
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   2325
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1065
   ScaleWidth      =   2325
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Check 
      Caption         =   "Aktiviert"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.ComboBox ComboSpriteImage 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "frmOption.frx":0000
      Left            =   0
      List            =   "frmOption.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   480
      Visible         =   0   'False
      Width           =   1215
   End
End
Attribute VB_Name = "frmOption"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim bool_result As Boolean

Public Function GetInput(Mode As String, Optional Sprite As Integer = -1, Optional BoolValue As Boolean = False) As Variant
    Select Case Mode
    
    Case Is = "sprite":
    
        ComboSpriteImage.Visible = True
        For i = 0 To frmMain.ComboSpriteImage.ListCount - 1
            ComboSpriteImage.AddItem frmMain.ComboSpriteImage.List(i)
        Next i
        
        If Not Sprite = -1 Then ComboSpriteImage.ListIndex = Sprite
        
    Case Is = "bool":
        
        Check.Visible = True
        Check.Value = IIf(BoolValue, 1, 0)
        bool_result = BoolValue
        
    End Select
    
    Me.Show 1
    
    Select Case Mode
    
    Case Is = "sprite":
        
        GetInput = ComboSpriteImage.ListIndex
    
    Case Is = "bool":

        GetInput = bool_result
    
    End Select
    
End Function

Private Sub Check_Click()
    
    If Check.Value = 1 Then bool_result = True
    
End Sub

