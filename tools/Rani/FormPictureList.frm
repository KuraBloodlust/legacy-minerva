VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FormPictureList 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "File Database"
   ClientHeight    =   3300
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4590
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   220
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   306
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton CommandFileOK 
      Caption         =   "Accept"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2400
      TabIndex        =   5
      Top             =   120
      Width           =   2055
   End
   Begin VB.Frame frmProp 
      Caption         =   "File Options:"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   2400
      TabIndex        =   2
      Top             =   1080
      Width           =   2055
      Begin VB.PictureBox picPreview 
         AutoRedraw      =   -1  'True
         Height          =   1455
         Left            =   120
         ScaleHeight     =   93
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   117
         TabIndex        =   4
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.CommandButton CommandFileAdd 
      Caption         =   "Add File"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2400
      TabIndex        =   1
      Top             =   600
      Width           =   2055
   End
   Begin VB.ListBox ListPicture 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2790
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2055
   End
   Begin MSComctlLib.StatusBar status 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   3
      Top             =   3045
      Width           =   4590
      _ExtentX        =   8096
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FormPictureList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private j As Long

Private AktlPicture As Integer

Public Function GetPictureName() As String
    GetPictureName = ListPicture.List(AktlPicture)
End Function

Private Function FillListPicture()
    ListPicture.Clear
    For j = 1 To Scene.GetPictureCount
    
        If Not Scene.GetPictureName(j) = "" Then
            ListPicture.AddItem Scene.GetPictureName(j)
        End If
        
    Next j
    
    status.SimpleText = "Files in Database : " & Scene.GetPictureCount
End Function

Private Sub CommandFileAdd_Click()
    PictureAdd
    FillListPicture
    ListPicture.ListIndex = ListPicture.ListCount - 1
End Sub

Private Sub CommandFileOK_Click()
    AktlPicture = ListPicture.ListIndex
    Me.Hide
End Sub

Private Sub Form_Load()
    FillListPicture
End Sub

Private Sub Form_Terminate()
    AktlPicture = ListPicture.ListIndex
End Sub

Private Sub Form_Unload(Cancel As Integer)
    AktlPicture = ListPicture.ListIndex
End Sub

Private Sub ListPicture_Click()
    
    If ListPicture.ListIndex > -1 Then frmProp.Enabled = True Else frmProp.Enabled = False
    RenderImageToHDC Scene.GetPicturePath(ListPicture.ListIndex + 1), picPreview.hDC, 0, 0, 0, 0, picPreview.ScaleWidth, picPreview.ScaleHeight
    picPreview.Refresh
    
End Sub

Private Sub ListPicture_DblClick()
    AktlPicture = ListPicture.ListIndex
    Me.Hide
End Sub
