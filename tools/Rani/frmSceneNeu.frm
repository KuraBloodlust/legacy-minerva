VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSceneNew 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Create new Scene"
   ClientHeight    =   1185
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2835
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1185
   ScaleWidth      =   2835
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.Slider Slider1 
      Height          =   255
      Left            =   1440
      TabIndex        =   6
      Top             =   480
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   450
      _Version        =   393216
      SelStart        =   2
      Value           =   2
   End
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   255
      Left            =   1440
      TabIndex        =   5
      Top             =   840
      Width           =   1215
   End
   Begin VB.CommandButton CommandOK 
      Caption         =   "OK"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox TextY 
      Height          =   285
      Left            =   2160
      TabIndex        =   2
      Text            =   "128"
      Top             =   120
      Width           =   495
   End
   Begin VB.TextBox TextX 
      Height          =   285
      Left            =   1200
      TabIndex        =   1
      Text            =   "128"
      Top             =   120
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "Framedelay:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   1335
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "x"
      Height          =   255
      Left            =   1800
      TabIndex        =   3
      Top             =   120
      Width           =   255
   End
   Begin VB.Label Label1 
      Caption         =   "Scene Size"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "frmSceneNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub CommandOK_Click()
    
    Set Scene = New cls_Scene
    Set Scene.S2D = Engine
    
    If TextX.Text > 320 Then MsgBox "max breite ist 320": Exit Sub
    If TextY.Text > 240 Then MsgBox "max h�he ist 240": Exit Sub
    If TextX.Text <= 0 Then MsgBox "min breite ist 1": Exit Sub
    If TextY.Text <= 0 Then MsgBox "min h�he ist 1": Exit Sub
    
    Engine.TexturePool.Unload
    Scene.Init CLng(TextX.Text), CLng(TextY.Text)
    Engine.Initialize frmMain.PictureDest.hwnd, CLng(TextX.Text), CLng(TextY.Text)
    Set Effects = New cS2DEffects
    Set Effects.MyPool = Engine.TexturePool.Pool1
    
    Scene.SetFrameDelay Slider1.Value
    
    frmMain.ComboSprite.Clear
    frmMain.ComboSpriteImage.Clear
    frmMain.CheckSprite.Value = False
    frmMain.SpriteSelect = 0
    frmMain.ResetAll
    frmMain.AddFrame
    
    frmMain.Form_Resize
    Unload Me

    Exit Sub
errorhandler:
    Debug.Print "Fehler in CommandOK_Click"
    Debug.Print "Fehlercode: " & err.Number
    Debug.Print err.Description
    MsgBox "Eingaben �berpr�fen!", vbCritical Or vbOKOnly
    
End Sub

