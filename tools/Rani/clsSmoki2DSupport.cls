VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DSupport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private CFm_HardwareAdapterAviable               As Boolean
Private CFm_MoreHardwareAdaptersAviable          As Boolean
Private CFm_HardwareAcceleration                 As Boolean
Private CFm_SoftwareAcceleration                 As Boolean
Private CFm_SquareTextureLimitation              As Boolean
Private CFm_WindowedModeSupported                As Boolean
Private CFm_OnlySimpleText                       As Boolean
Private CFm_ForceAlternativeDrawingRoutine       As Boolean
Private CFm_UseFrameLimiter                      As Boolean
Private CFm_MaximumTextureWidth                  As Integer
Private CFm_MaximumTextureHeight                 As Integer
Private CFm_DebugText                            As String
Private CFm_MaximumPrimitives                    As Long

Public Property Get HardwareAdapterAviable() As Boolean
  HardwareAdapterAviable = CFm_HardwareAdapterAviable
End Property

Public Property Let HardwareAdapterAviable(ByVal PropVal As Boolean)
  CFm_HardwareAdapterAviable = PropVal
End Property

Public Property Get MoreHardwareAdaptersAviable() As Boolean
  MoreHardwareAdaptersAviable = CFm_MoreHardwareAdaptersAviable
End Property

Public Property Let MoreHardwareAdaptersAviable(ByVal PropVal As Boolean)
  CFm_MoreHardwareAdaptersAviable = PropVal
End Property

Public Property Get HardwareAcceleration() As Boolean
  HardwareAcceleration = CFm_HardwareAcceleration
End Property

Public Property Let HardwareAcceleration(ByVal PropVal As Boolean)
  CFm_HardwareAcceleration = PropVal
End Property

Public Property Get SoftwareAcceleration() As Boolean
  SoftwareAcceleration = CFm_SoftwareAcceleration
End Property

Public Property Let SoftwareAcceleration(ByVal PropVal As Boolean)
  CFm_SoftwareAcceleration = PropVal
End Property

Public Property Get MaximumTextureWidth() As Integer
  MaximumTextureWidth = CFm_MaximumTextureWidth
End Property

Public Property Let MaximumTextureWidth(ByVal PropVal As Integer)
  CFm_MaximumTextureWidth = PropVal
End Property

Public Property Get MaximumTextureHeight() As Integer
  MaximumTextureHeight = CFm_MaximumTextureHeight
End Property

Public Property Let MaximumTextureHeight(ByVal PropVal As Integer)
  CFm_MaximumTextureHeight = PropVal
End Property

Public Property Get MaximumPrimitives() As Long
  MaximumPrimitives = CFm_MaximumPrimitives
End Property

Public Property Let MaximumPrimitives(ByVal PropVal As Long)
  CFm_MaximumPrimitives = PropVal
End Property

Public Property Get SquareTextureLimitation() As Boolean
  SquareTextureLimitation = CFm_SquareTextureLimitation
End Property

Public Property Let SquareTextureLimitation(ByVal PropVal As Boolean)
  CFm_SquareTextureLimitation = PropVal
End Property

Public Property Get WindowedModeSupported() As Boolean
  WindowedModeSupported = CFm_WindowedModeSupported
End Property

Public Property Let WindowedModeSupported(ByVal PropVal As Boolean)
  CFm_WindowedModeSupported = PropVal
End Property

Public Property Get OnlySimpleText() As Boolean
  OnlySimpleText = CFm_OnlySimpleText
End Property

Public Property Let OnlySimpleText(ByVal PropVal As Boolean)
  CFm_OnlySimpleText = PropVal
End Property

Public Property Get ForceAlternativeDrawingRoutine() As Boolean
  ForceAlternativeDrawingRoutine = CFm_ForceAlternativeDrawingRoutine
End Property

Public Property Let ForceAlternativeDrawingRoutine(ByVal PropVal As Boolean)
  CFm_ForceAlternativeDrawingRoutine = PropVal
End Property

Public Property Get UseFrameLimiter() As Boolean
  UseFrameLimiter = CFm_UseFrameLimiter
End Property

Public Property Let UseFrameLimiter(ByVal PropVal As Boolean)
  CFm_UseFrameLimiter = PropVal
End Property

Public Property Get DebugText() As String
  DebugText = CFm_DebugText
End Property

Public Property Let DebugText(ByVal PropVal As String)
  CFm_DebugText = PropVal
End Property

Public Sub AddDebugText(strText As String)
  CFm_DebugText = CFm_DebugText & vbCrLf & " ( " & Date & " / " & time & " ) " & strText
End Sub
