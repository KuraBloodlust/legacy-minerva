Attribute VB_Name = "modRaniGlobal"
Public FN As String
Public AppPath As String
Public CurProj As String

Public Function GetRootPath()
GetRootPath = AppPath

End Function

Public Function IsInRect(ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal y2 As Long, _
    ByVal XPos As Long, ByVal YPos As Long) As Boolean
    'Hilfsfunktion
    
    If XPos >= X1 And XPos <= X2 And YPos >= Y1 And YPos <= y2 Then
        IsInRect = True
    Else
        IsInRect = False
    End If

End Function

Public Function PictureAdd()
 Dim NewID As Long

    If Scene.GetFrameCount <= 0 Then
        MsgBox "Insert a Frame first!", vbCritical, "Error!"
        Exit Function
    End If

    frmMain.CD1.Filter = "PNG-Dateien|*.png"
    frmMain.CD1.ShowOpen
    
    If frmMain.CD1.FileName = "" Then Exit Function
    
    NewID = Engine.TexturePool.AddTexture(frmMain.CD1.FileName, rgb(255, 0, 255))
    
    If NewID > 0 Then
        'wenn Textur/Bild ok, wird Picture in die Pictureliste aufgenommen
        Dim w As Long, h As Long
        
        w = Engine.TexturePool.TextureWidth(Engine.TexturePool.TextureCount)
        h = Engine.TexturePool.TextureHeight(Engine.TexturePool.TextureCount)
        If Not (Scene.AddPicture(frmMain.CD1.FileName, frmMain.CD1.FileTitle, NewID, w, h)) Then
            MsgBox "File already exists!", vbOKOnly Or vbCritical
            Exit Function
        End If
    End If
    
    NewPic = Mid(frmMain.CD1.FileName, InStrRev(frmMain.CD1.FileName, "\") + 1)

End Function

