VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   Caption         =   "RANI Animation Editor"
   ClientHeight    =   4200
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   6285
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   280
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   419
   StartUpPosition =   2  'CenterScreen
   Tag             =   "Animation Editor"
   Begin VB.Frame frmContainer 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   3495
      Left            =   3000
      TabIndex        =   4
      Top             =   360
      Width           =   2295
      Begin VB.PictureBox picContainer2 
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         Height          =   3105
         Left            =   0
         ScaleHeight     =   203
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   136
         TabIndex        =   5
         Top             =   360
         Width           =   2100
         Begin VB.OptionButton oAllFrames 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Apply to all Frames"
            Height          =   255
            Left            =   0
            TabIndex        =   22
            Top             =   2160
            Width           =   2055
         End
         Begin VB.OptionButton oOneFrame 
            BackColor       =   &H00FFFFFF&
            Caption         =   "This Frame only"
            Height          =   255
            Left            =   0
            TabIndex        =   21
            Top             =   1920
            Value           =   -1  'True
            Width           =   2055
         End
         Begin AnimationEditor.Rm2kScroll scrollAngle 
            Height          =   300
            Left            =   720
            TabIndex        =   18
            Top             =   645
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   529
            Min             =   0
            Max             =   360
            Value           =   0
         End
         Begin VB.CommandButton CommandColor 
            Height          =   255
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   11
            Top             =   1260
            Width           =   1335
         End
         Begin VB.ComboBox ComboSpriteImage 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   720
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   15
            Width           =   1335
         End
         Begin VB.CheckBox CheckSprite 
            BackColor       =   &H00FFFFFF&
            Caption         =   "True"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   720
            TabIndex        =   9
            Top             =   360
            Width           =   735
         End
         Begin AnimationEditor.Rm2kScroll scrollAlpha 
            Height          =   300
            Left            =   720
            TabIndex        =   19
            Top             =   945
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   529
            Min             =   0
            Max             =   255
            Value           =   255
         End
         Begin AnimationEditor.Rm2kScroll scrollZ 
            Height          =   300
            Left            =   720
            TabIndex        =   20
            Top             =   1530
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   529
            Min             =   -20
            Max             =   20
            Value           =   0
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Z Value"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   30
            TabIndex        =   17
            Top             =   1590
            Width           =   585
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00E0E0E0&
            Index           =   6
            X1              =   144
            X2              =   0
            Y1              =   120
            Y2              =   120
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Color"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   30
            TabIndex        =   16
            Top             =   1290
            Width           =   405
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Alpha"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   30
            TabIndex        =   15
            Top             =   990
            Width           =   435
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Angle"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   30
            TabIndex        =   14
            Top             =   690
            Width           =   435
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Visible"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   30
            TabIndex        =   13
            Top             =   390
            Width           =   525
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00E0E0E0&
            Index           =   5
            X1              =   42
            X2              =   42
            Y1              =   0
            Y2              =   120
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00E0E0E0&
            Index           =   4
            X1              =   144
            X2              =   0
            Y1              =   102
            Y2              =   102
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00E0E0E0&
            Index           =   3
            X1              =   144
            X2              =   0
            Y1              =   82
            Y2              =   82
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00E0E0E0&
            Index           =   2
            X1              =   144
            X2              =   0
            Y1              =   62
            Y2              =   62
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00E0E0E0&
            Index           =   1
            X1              =   144
            X2              =   0
            Y1              =   42
            Y2              =   42
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Picture"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   30
            TabIndex        =   12
            Top             =   60
            Width           =   540
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00E0E0E0&
            Index           =   0
            X1              =   144
            X2              =   0
            Y1              =   22
            Y2              =   22
         End
      End
      Begin VB.ComboBox ComboSprite 
         BackColor       =   &H00E19A66&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   0
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   0
         Width           =   2100
      End
   End
   Begin MSComctlLib.StatusBar status 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   3
      Top             =   3945
      Width           =   6285
      _ExtentX        =   11086
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picContainer 
      BackColor       =   &H00000000&
      Height          =   2535
      Left            =   0
      ScaleHeight     =   165
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   189
      TabIndex        =   0
      Top             =   360
      Width           =   2895
      Begin VB.PictureBox PictureDest 
         BorderStyle     =   0  'None
         Height          =   1575
         Left            =   0
         ScaleHeight     =   105
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   113
         TabIndex        =   1
         Top             =   0
         Width           =   1695
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7560
      Top             =   2160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":08CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0E1C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":136E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":18C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1E12
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2364
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":28B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2E08
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CD1 
      Left            =   7560
      Top             =   2760
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList iml 
      Left            =   7560
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   28
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":335A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":404C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":4D3E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":5A30
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":6722
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":7414
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":8106
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":8DF8
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":9AEA
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":A7DC
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":B4CE
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":C1C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":CEB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":DBA4
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":E896
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":F588
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1027A
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":10F6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":11C5E
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":12950
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":13642
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":14334
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":15026
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":15D18
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":16A0A
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":176FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":183EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":190E0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar TB 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   7
      Top             =   3615
      Width           =   6285
      _ExtentX        =   11086
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "iml"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   10
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "left"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "right"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "newframe"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "delframe"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "newsprite"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "delsprite"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "spriteprob"
            ImageIndex      =   27
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "play"
            ImageIndex      =   28
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   7800
      Top             =   3480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   28
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1947A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1A16C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1AE5E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1BB50
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1C842
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1D534
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1E226
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1EF18
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1FC0A
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":208FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":215EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":222E0
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":22FD2
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":23CC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":249B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":256A8
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2639A
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2708C
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":27D7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":28A70
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":29762
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2A454
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2B146
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2BE38
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2CB2A
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2D81C
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2E50E
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2F200
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar TB2 
      Align           =   1  'Align Top
      Height          =   330
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   6285
      _ExtentX        =   11086
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "iml"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "new"
            Object.ToolTipText     =   "New Animation"
            ImageIndex      =   14
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "open"
            Object.ToolTipText     =   "Load Animation"
            ImageIndex      =   26
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "save"
            Object.ToolTipText     =   "Save Animation"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   1e-4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "database"
            Object.ToolTipText     =   "Open Database"
            ImageIndex      =   27
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "exit"
            Object.ToolTipText     =   "Close Program"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Slider SliderFrame 
      Height          =   495
      Left            =   0
      TabIndex        =   2
      Top             =   3120
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   873
      _Version        =   393216
      LargeChange     =   1
      Min             =   1
      Max             =   2
      SelStart        =   1
      TickStyle       =   2
      Value           =   1
   End
   Begin VB.Menu MDatei 
      Caption         =   "File"
      Begin VB.Menu mnuNewScene 
         Caption         =   "New Scene"
      End
      Begin VB.Menu sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOpen 
         Caption         =   "Open Scene"
      End
      Begin VB.Menu MSceneSave 
         Caption         =   "Save Scene"
      End
      Begin VB.Menu trenn2 
         Caption         =   "-"
      End
      Begin VB.Menu MEnd 
         Caption         =   "End"
      End
   End
   Begin VB.Menu mnupic 
      Caption         =   "Database"
      Begin VB.Menu mnuimageadd 
         Caption         =   "Edit Database"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Rani Editor by MasterK
'updated by SmokingFish

Public Enum EAktlShow 'was gerade in der Render-Loop gezeigt wird
    shownothing = 0
    ShowPicture = 1
    ShowImage = 2
    ShowFrame = 3
End Enum

Public i As Integer
Public Back As Boolean

Private Ende As Boolean
Private AktlPlay As Boolean 'Animation wird abgespielt
Private Framedelay As Integer 'Hilfsvariable f�r Framedelay

Private AktlImage As Integer

Private SpriteRect As RECT
Public SpriteSelect As Integer
Private SpriteMoveStart As Point, MoveDiff As Point, MoveOldSpritePos As Point

Private AktlFrame As Long, AktlSprite As Long
Private AktlShow As EAktlShow

Private MoveSelection As Boolean
Private Limiter As New cS2DLimiter

Private Reload As Boolean
Private ScaleFactor As Double

Public Sub ResetAll()
    AktlFrame = 1
    AktlImage = 0
    AktlSprite = 0
    SpriteSelect = 0
End Sub

Public Sub SetAktlShow(ByVal NewValue As EAktlShow)
    AktlShow = NewValue
End Sub

Public Sub SetAktlImage(ByVal NewValue As Integer)
    AktlImage = NewValue
End Sub

Public Sub SetSpriteSelect(ByVal NewValue As Integer)
    SpriteSelect = NewValue
End Sub

Private Function CheckActiveScene() As Boolean
    CheckActiveScene = True
    If Not Scene.CheckInit Then
        MsgBox "Keine Animationsinformatioen vorhanden!", vbExclamation Or vbOKOnly
        CheckActiveScene = False
    End If
End Function

Public Function OpenScene()

    Set Scene = New cls_Scene
    Set Scene.S2D = Engine
    '�ffnet den Standarddialog und �ffnet die ausgew�hlte Scene
    Ende = True
    
    CD1.DialogTitle = "Scene �ffnen"
    CD1.Filter = "Rani-Dateien | *.rani"
    CD1.ShowOpen
    
    If Not Scene.LoadScene(CD1.FileName) Then
        MsgBox "Fehler beim Laden!", vbOKOnly Or vbCritical
    Else
        
        Engine.TexturePool.Unload
        
        AktlFrame = 1
        
        'die einzelnen Texturen der Scene laden
        For i = 1 To Scene.GetPictureCount
            Debug.Print Scene.GetPicturePath(i)
            Scene.SetPictureTextureID i, Engine.TexturePool.AddTexture(Scene.GetPicturePath(i), rgb(255, 0, 255))
        Next i
                
        'die Spriteimageauswahl alle Images hinzuf�gen
        ComboSpriteImage.Clear
        For i = 1 To Scene.GetImageCount
            ComboSpriteImage.AddItem Scene.GetImageDescription(i)
        Next i
        
        'die Spriteliste hinzuf�gen
        ComboSprite.Clear
        For i = 1 To Scene.GetSpriteCount
            ComboSprite.AddItem Scene.GetSpriteName(i)
        Next i
        
        'das erste Sprite schreiben
        If Scene.GetSpriteCount > 0 Then
            AktlSprite = 1
            WriteSpriteProperty Scene.GetSpriteID(ComboSprite.List(0))
        End If
                
        If Scene.GetFrameCount > 1 Then SliderFrame.Max = Scene.GetFrameCount
        SliderFrame.Min = 1
        'StatusBar.Panels(2).Text = "Frameanzahl: " & Scene.GetFrameCount
        
        AktlShow = ShowFrame
        Ende = False
        ShowScene
           
        Engine.Initialize PictureDest.hwnd, Scene.GetSceneWidth, Scene.GetSceneHeight
        Set Effects = New cS2DEffects
        Set Effects.MyPool = Engine.TexturePool.Pool1
        
        Exit Function
        
    End If

    Exit Function
errorhandler:
    Debug.Print "Fehler in OpenScene"
    Debug.Print "Fehlercode: " & err.Number
    Debug.Print err.Description

End Function

Private Function SaveScene()

    CD1.DialogTitle = "Scene speichern"
    CD1.Filter = "Rani-Dateien | *.rani"
    CD1.ShowSave
    
    If Not Scene.SaveScene(CD1.FileName) Then
        MsgBox "Fehler beim Speichern!", vbOKOnly Or vbCritical
    End If

End Function


Public Function ShowScene()
    'die Renderloop
    Dim u As Integer
    
    Dim ImageAktl As Integer
    
    Dim TexturID As Integer
    Dim DestX As Double, DestY As Double
    Dim SrcX As Integer, SrcY As Integer
    Dim SrcWidth As Integer, SrcHeight As Integer
    Dim TexturAngle As Integer
    Dim TexturAlpha As Integer
    Dim TexturColor As Long
    Dim TexturZ As Long

    Ende = False
    
    Do Until Ende
    Engine.Clear
    Engine.BeginScene
    
    If Ende Then Exit Do
    DrawBackground Scene.GetSceneWidth, Scene.GetSceneHeight
    
        If AktlPlay Then
            Framedelay = Framedelay + 1
            If Scene.GetFrameDelay = 0 Or Framedelay = Scene.GetFrameDelay Then
                AktlFrame = AktlFrame + 1
                If AktlFrame > Scene.GetFrameCount Then AktlFrame = 1
                Framedelay = -1
            End If
            SliderFrame.Value = AktlFrame
        End If
        
        If Ende Then Exit Do
        Select Case AktlShow
            Case Is = 1
        
                u = Scene.GetPictureTextureID(Scene.GetPictureID(ListPicture.List(ListPicture.ListIndex)))
                 Engine.TexturePool.RenderTexture u, 0, 0, , , , , 0, 0, , False
                 
            Case Is = 3

                For i = 1 To Scene.GetSpriteCount
                    
                    If AktlFrame > 1 Then
                    AktlFrame = AktlFrame - 1
                    If Scene.GetSpriteProperty(AktlFrame, i, bolEnabled) Then
                        TexturID = Scene.GetSpriteTextureID(AktlFrame, i)
                        DestX = Scene.GetSpriteProperty(AktlFrame, i, lXPos)
                        DestY = Scene.GetSpriteProperty(AktlFrame, i, lYPos)
                        If Scene.GetSpriteProperty(AktlFrame, i, lImage) > 0 Then SrcX = Scene.GetImageRight(Scene.GetSpriteProperty(AktlFrame, i, lImage)) ' - Scene.GetImageLeft(Scene.GetSpriteProperty(AktlFrame, i, lImage))
                        If Scene.GetSpriteProperty(AktlFrame, i, lImage) > 0 Then SrcY = Scene.GetImageBottom(Scene.GetSpriteProperty(AktlFrame, i, lImage)) ' - Scene.GetImageTop(Scene.GetSpriteProperty(AktlFrame, i, lImage))
                        TexturAngle = Scene.GetSpriteProperty(AktlFrame, i, iAngle)
                        TexturAlpha = Scene.GetSpriteProperty(AktlFrame, i, bAlpha)
                        TexturColor = Scene.GetSpriteProperty(AktlFrame, i, lColor)
                        TexturZ = Scene.GetSpriteProperty(AktlFrame, i, lZ)
                        'Debug.Print "h�he: " & SrcY
                        'Engine.TexturePool.RenderTexture TexturID, DestX, DestY, SrcX, SrcY, SrcX, SrcY, Scene.GetImageLeft(Scene.GetSpriteProperty(AktlFrame, i, lImage)), Scene.GetImageTop(Scene.GetSpriteProperty(AktlFrame, i, lImage)), CSng(TexturAlpha) / 3, , CSng(TexturColor), CSng(TexturColor), CSng(TexturColor), CSng(TexturColor), TexturAngle
                        Engine.TexturePool.RenderTexture TexturID, DestX, DestY, SrcX, SrcY, SrcX, SrcY, Scene.GetImageLeft(Scene.GetSpriteProperty(AktlFrame, i, lImage)), Scene.GetImageTop(Scene.GetSpriteProperty(AktlFrame, i, lImage)), TexturAlpha / 3, TexturAngle, , , CInt(TexturZ), TexturColor
                    End If
                    AktlFrame = AktlFrame + 1
                    End If
                
                    If Scene.GetSpriteProperty(AktlFrame, i, bolEnabled) Then
                        TexturID = Scene.GetSpriteTextureID(AktlFrame, i)
                        DestX = Scene.GetSpriteProperty(AktlFrame, i, lXPos)
                        DestY = Scene.GetSpriteProperty(AktlFrame, i, lYPos)
                        If Scene.GetSpriteProperty(AktlFrame, i, lImage) > 0 Then SrcX = Scene.GetImageRight(Scene.GetSpriteProperty(AktlFrame, i, lImage)) '- Scene.GetImageLeft(Scene.GetSpriteProperty(AktlFrame, i, lImage))
                        If Scene.GetSpriteProperty(AktlFrame, i, lImage) > 0 Then SrcY = Scene.GetImageBottom(Scene.GetSpriteProperty(AktlFrame, i, lImage)) ' - Scene.GetImageTop(Scene.GetSpriteProperty(AktlFrame, i, lImage))
                        TexturAngle = Scene.GetSpriteProperty(AktlFrame, i, iAngle)
                        TexturAlpha = Scene.GetSpriteProperty(AktlFrame, i, bAlpha)
                        TexturColor = Scene.GetSpriteProperty(AktlFrame, i, lColor)
                        TexturZ = Scene.GetSpriteProperty(AktlFrame, i, lZ)
                        'Debug.Print "h�he: " & SrcY
                        'Engine.TexturePool.RenderTexture TexturID, DestX, DestY, SrcX, SrcY, SrcX, SrcY, Scene.GetImageLeft(Scene.GetSpriteProperty(AktlFrame, i, lImage)), Scene.GetImageTop(Scene.GetSpriteProperty(AktlFrame, i, lImage)), CSng(TexturAlpha), , CSng(TexturColor), CSng(TexturColor), CSng(TexturColor), CSng(TexturColor), TexturAngle
                        Engine.TexturePool.RenderTexture TexturID, DestX, DestY, SrcX, SrcY, SrcX, SrcY, Scene.GetImageLeft(Scene.GetSpriteProperty(AktlFrame, i, lImage)), Scene.GetImageTop(Scene.GetSpriteProperty(AktlFrame, i, lImage)), TexturAlpha, TexturAngle, , , CInt(TexturZ), TexturColor
                    End If
                    
                Next i

        End Select
        DoEvents
        
        If Ende Then Exit Do
        If SpriteSelect > 0 Then
            SpriteRect.Left = Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lXPos)
            SpriteRect.Top = Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lYPos)
            If Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lImage) > 0 Then SpriteRect.Right = Scene.GetImageRight(Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lImage))
            If Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lImage) > 0 Then SpriteRect.Bottom = Scene.GetImageBottom(Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lImage))
            
            Effects.RenderRectangle CInt(SpriteRect.Left), CInt(SpriteRect.Top), CInt(SpriteRect.Right), CInt(SpriteRect.Bottom), vbBlack, vbBlack, vbRed, vbRed, 100
            Effects.RenderBorderRect CInt(SpriteRect.Left), CInt(SpriteRect.Top), CInt(SpriteRect.Right), CInt(SpriteRect.Bottom), vbWhite, 200, 2
        End If
        
        If Ende Then Exit Do
                Engine.EndScene
                Engine.Flip
                Limiter.LimitFrames 30
    Loop

End Function

Private Function DrawBackground(Width As Integer, Height As Integer)
    
    Effects.RenderRectangle 0, 0, Width, Height
    'For a = 0 To Width Step 16
        'For b = 0 To Height Step 16
        
            'Engine.Effects.RECT CSng(a), CSng(b), 8, 8, vbWhite
            'Engine.Effects.RECT CSng(a) + 8, CSng(b), 8, 8, rgb(150, 150, 150)
            'Engine.Effects.RECT CSng(a), CSng(b) + 8, 8, 8, rgb(150, 150, 150)
            'Engine.Effects.RECT CSng(a) + 8, CSng(b) + 8, 8, 8, vbWhite
        
        'Next b
    'Next a

End Function

Private Function WriteSpriteProperty(ByVal SpriteID As Long)
    'f�llt das Spritepropertyframe mit den Eigenschaften des aktuell gew�hlten Sprites
    
    If SpriteID > 0 And AktlFrame > 0 Then
    
        ComboSprite.ListIndex = SpriteID - 1
        ComboSpriteImage.ListIndex = Scene.GetSpriteProperty(AktlFrame, SpriteID, lImage) - 1
        
        If Scene.GetSpriteProperty(AktlFrame, SpriteID, bolEnabled) = True Then
            CheckSprite.Value = 1
        Else
            CheckSprite.Value = 0
        End If
        
        scrollAngle.Value = Scene.GetSpriteProperty(AktlFrame, SpriteID, iAngle)
        scrollAlpha.Value = Scene.GetSpriteProperty(AktlFrame, SpriteID, bAlpha)
        CommandColor.BackColor = Scene.GetSpriteProperty(AktlFrame, SpriteID, lColor)
        scrollZ.Value = Scene.GetSpriteProperty(AktlFrame, SpriteID, lZ)
    End If
    
End Function

Private Sub CheckSprite_Click()
    'aktuelles Sprite im aktuellen Frame aus/einschalten
    
    If CheckActiveScene Then
        
        If oOneFrame.Value = True Then
            Scene.SetSpriteProperty AktlFrame, AktlSprite, bolEnabled, CBool(CheckSprite.Value)
        Else
            For i = 1 To Scene.GetFrameCount
                Scene.SetSpriteProperty i, AktlSprite, bolEnabled, CBool(CheckSprite.Value)
            Next i
        End If

    End If
    
End Sub

Private Sub cmdAddFrame_Click()
    AddFrame
End Sub

Private Sub Combo1_Change()

End Sub

Private Sub ComboSprite_Click()
    
    If ComboSprite.ListCount > 0 Then
        AktlSprite = ComboSprite.ListIndex + 1
        SpriteSelect = AktlSprite
        WriteSpriteProperty AktlSprite
        AktlShow = ShowFrame
        picContainer2.Enabled = True
    Else
        picContainer2.Enabled = False
    End If
    
End Sub

Private Sub ComboSpriteImage_Click()
    'dem aktuellen Sprite m aktuellen Frame einImage zuweisen
    If AktlSprite > 0 And ComboSpriteImage.ListCount > 0 Then
        If oOneFrame.Value = True Then
            Scene.SetSpriteProperty AktlFrame, AktlSprite, lImage, ComboSpriteImage.ListIndex + 1
        Else
            For i = 1 To Scene.GetFrameCount
                Scene.SetSpriteProperty i, AktlSprite, lImage, ComboSpriteImage.ListIndex + 1
            Next i
        End If
    End If
    
End Sub

Private Sub CommandColor_Click()
    'dem aktuellen Sprite im aktuellen Frame eine Farbe zuweisen
    
    CD1.ShowColor
    If AktlSprite > 0 And AktlFrame > 0 Then
        
        If oOneFrame.Value = True Then
            Scene.SetSpriteProperty AktlFrame, AktlSprite, lColor, CD1.Color
            WriteSpriteProperty AktlSprite
        Else
            For i = 1 To Scene.GetFrameCount
                Scene.SetSpriteProperty i, AktlSprite, lColor, CD1.Color
                WriteSpriteProperty AktlSprite
            Next i
        End If
    End If
    
End Sub


Public Sub AddFrame()
    
    Scene.AddFrame
    AktlFrame = Scene.GetFrameCount
    If AktlFrame > 1 Then SliderFrame.Max = AktlFrame
    AktlShow = ShowFrame
    
    SliderFrame.Value = AktlFrame
    
End Sub

Private Sub Form_Load()
    
    Me.Show
    Dim Num As Integer
    Ende = False
    
    InitLogging
    
    Set Scene.S2D = Engine
    Scene.Init 128, 128
    
    Engine.Initialize PictureDest.hwnd, 128, 128
    Set Effects = New cS2DEffects
    Set Effects.MyPool = Engine.TexturePool.Pool1
    
    Scene.AddFrame

    Scene.SetFrameDelay 2
            
    AktlFrame = 1
    'SceneActive = False
    
    AktlShow = shownothing
    
    Form_Resize
    ShowScene
    
End Sub

Public Sub Form_Resize()
    
    picContainer.Move 0, TB.Height, Me.ScaleWidth - picContainer2.ScaleWidth - 6, Me.ScaleHeight - TB2.Height - TB.Height - status.Height - SliderFrame.Height
    SliderFrame.Top = picContainer.ScaleHeight + 4 + TB2.Height
    SliderFrame.Width = Me.ScaleWidth - frmContainer.Width
    frmContainer.Move picContainer.ScaleWidth + 4
    picContainer2.Height = picContainer.Height * Screen.TwipsPerPixelY - picContainer2.Top
    frmContainer.Height = picContainer.Height + SliderFrame.Height
    picContainer2.Height = picContainer2.Height + SliderFrame.Height * Screen.TwipsPerPixelY
    frmContainer.Top = TB.Height
   ' PictureDest.Left = picContainer.Width / 2 - PictureDest.Width / 2
    'PictureDest.Top = picContainer.Height / 2 - PictureDest.Height / 2
    
    If Reload Then
        Dim tw As Integer
        Dim tH As Integer
        tw = Scene.GetSceneWidth
        tH = Scene.GetSceneHeight
        
        Dim tf1 As Double
        Dim tf2 As Double
        tf1 = picContainer.ScaleWidth / tw
        tf2 = picContainer.ScaleHeight / tH
        
        ScaleFactor = 1
        If tf1 < tf2 Then
            PictureDest.Width = picContainer.Width
            PictureDest.Height = tH * tf1
            PictureDest.Left = 0
            PictureDest.Top = picContainer.Height / 2 - PictureDest.Height / 2
            ScaleFactor = tf1
        Else
            PictureDest.Height = picContainer.Height
            PictureDest.Width = tw * tf2
            PictureDest.Top = 0
            PictureDest.Left = picContainer.Width / 2 - PictureDest.Width / 2
            ScaleFactor = tf2
        End If
    End If
    Reload = True
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Ende = True
    AktlShow = shownothing
    Engine.Unload
    Unload Me
    End
    
End Sub

Private Sub Mhow_Click()
    FormImageList.Show
End Sub

Private Sub MPictureAdd_Click()
    PictureAdd
End Sub

Private Sub MSaveNeu_Click()

End Sub

Private Sub mnuAbout_Click()
MsgBox "2003 by the R-PG Team" & vbCrLf & "Code by MasterK" & vbCrLf & "Additional Code by SmokingFish & Semi", vbInformation
End Sub


Private Function AddSprite()
    If Scene.GetFrameCount > 0 Then
        frmSpriteNew.Show 1
        If frmSpriteNew.GetSpriteName = "" Then Exit Function
        If Scene.AddSprite(frmSpriteNew.GetSpriteName) Then
            ComboSprite.AddItem Scene.GetSpriteName(Scene.GetSpriteCount)
            AktlSprite = Scene.GetSpriteCount
            WriteSpriteProperty Scene.GetSpriteID(AktlSprite)
            ComboSprite.ListIndex = ComboSprite.ListCount - 1
        Else
            MsgBox "Jeder Spritename ist nur einmal erlaubt!", vbCritical Or vbOKOnly
        End If
    
        Unload frmSpriteNew
    Else
        MsgBox "Mindestens ein Frame muss vorhanden sein!", vbExclamation Or vbOKOnly
    End If
End Function

Private Sub mnuimageadd_Click()
FormImageList.Show 1
End Sub

Private Sub mnuNewScene_Click()
frmSceneNew.Show vbModal
End Sub

Private Sub mnuOpen_Click()
OpenScene
End Sub

Private Sub mnupicadd_Click()
'AddPicture
PictureAdd
End Sub

Private Sub MSceneSave_Click()
    SaveScene
End Sub

Private Sub PictureDest_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
                
                
    X = X / ScaleFactor
    Y = Y / ScaleFactor
    If AktlFrame > 0 And AktlSprite > 0 Then
        AktlShow = ShowFrame
        If SpriteSelect > 0 Then
            If Not IsInRect(Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lXPos), _
                Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lYPos), _
                Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lXPos) + Scene.GetImageRight(Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lImage)), _
                Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lYPos) + Scene.GetImageBottom(Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lImage)), X, Y) Then
                SpriteSelect = 0
            End If
        End If
        
        If SpriteSelect > 0 Then
            If IsInRect(Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lXPos), _
            Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lYPos), _
            Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lXPos) + Scene.GetImageRight(Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lImage)), _
            Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lYPos) + Scene.GetImageBottom(Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lImage)), X, Y) Then
            
                SpriteMoveStart.X = X
                SpriteMoveStart.Y = Y
                MoveOldSpritePos.X = Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lXPos)
                MoveOldSpritePos.Y = Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lYPos)
                WriteSpriteProperty SpriteSelect
            End If
        
        Else
            For i = Scene.GetSpriteCount To 1 Step -1
                If Scene.GetSpriteProperty(AktlFrame, i, bolEnabled) Then
                    If IsInRect(Scene.GetSpriteProperty(AktlFrame, i, lXPos), _
                    Scene.GetSpriteProperty(AktlFrame, i, lYPos), _
                    Scene.GetSpriteProperty(AktlFrame, i, lXPos) + Scene.GetImageRight(Scene.GetSpriteProperty(AktlFrame, i, lImage)), _
                    Scene.GetSpriteProperty(AktlFrame, i, lYPos) + Scene.GetImageBottom(Scene.GetSpriteProperty(AktlFrame, i, lImage)), X, Y) Then
                        SpriteSelect = i
                        AktlSprite = i
                        SpriteMoveStart.X = X
                        SpriteMoveStart.Y = Y
                        MoveOldSpritePos.X = Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lXPos)
                        MoveOldSpritePos.Y = Scene.GetSpriteProperty(AktlFrame, SpriteSelect, lYPos)
                        WriteSpriteProperty SpriteSelect
                        Exit For
                    End If
                End If
            Next i
        End If
    End If

End Sub

Private Sub PictureDest_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    X = X / ScaleFactor
    Y = Y / ScaleFactor
    If Button = 1 And SpriteSelect > 0 Then
        If IsInRect(Scene.GetSpriteProperty(AktlFrame, AktlSprite, lXPos), _
        Scene.GetSpriteProperty(AktlFrame, AktlSprite, lYPos), _
        Scene.GetSpriteProperty(AktlFrame, AktlSprite, lXPos) + Scene.GetImageRight(Scene.GetSpriteProperty(AktlFrame, AktlSprite, lImage)), _
        Scene.GetSpriteProperty(AktlFrame, AktlSprite, lYPos) + Scene.GetImageBottom(Scene.GetSpriteProperty(AktlFrame, AktlSprite, lImage)), X, Y) Then
            MoveDiff.X = (X - SpriteMoveStart.X)
            MoveDiff.Y = (Y - SpriteMoveStart.Y)
            Scene.SetSpriteProperty AktlFrame, SpriteSelect, lXPos, MoveOldSpritePos.X + MoveDiff.X
            Scene.SetSpriteProperty AktlFrame, SpriteSelect, lYPos, MoveOldSpritePos.Y + MoveDiff.Y
            WriteSpriteProperty SpriteSelect
        End If
    End If
    
End Sub

Private Sub scrollAlpha_Change()

    If AktlFrame > 0 And AktlSprite > 0 Then
    
        If oOneFrame.Value = True Then
            Scene.SetSpriteProperty AktlFrame, AktlSprite, bAlpha, scrollAlpha.Value
        Else
            For i = 1 To Scene.GetFrameCount
                Scene.SetSpriteProperty i, AktlSprite, bAlpha, scrollAlpha.Value
            Next i
        End If
        
    End If
    
End Sub

Private Sub scrollAngle_Change()

    If AktlFrame > 0 And AktlSprite > 0 Then
    
        If oOneFrame.Value = True Then
            Scene.SetSpriteProperty AktlFrame, AktlSprite, iAngle, scrollAngle.Value
        Else
            For i = 1 To Scene.GetFrameCount
                Scene.SetSpriteProperty i, AktlSprite, iAngle, scrollAngle.Value
            Next i
        End If
        
    End If
    
End Sub

Private Sub SliderFrame_Click()
    'ein Frame ausw�hlen
    AktlFrame = SliderFrame.Value
    If AktlFrame > Scene.GetFrameCount Then
        AktlFrame = 1
        SliderFrame.Value = 1
    End If
    AktlShow = ShowFrame
    WriteSpriteProperty AktlSprite
End Sub

Private Sub SliderFrame_Scroll()
SliderFrame_Click
End Sub


Private Sub TextSpriteAlpha_Change()
    If AktlFrame > 0 And AktlSprite > 0 Then
        If IsNumeric(TextSpriteAlpha.Text) And TextSpriteAlpha.Text <= 255 Then
            Scene.SetSpriteProperty AktlFrame, AktlSprite, bAlpha, TextSpriteAlpha.Text
        Else
            Scene.SetSpriteProperty AktlFrame, AktlSprite, bAlpha, TextSpriteAlpha.Text
        End If
    End If
End Sub

Private Sub TextSpriteAngle_Change()
    If AktlFrame > 0 And AktlSprite > 0 Then
        If TextSpriteAngle > "" Then
            Scene.SetSpriteProperty AktlFrame, AktlSprite, iAngle, TextSpriteAngle.Text
        Else
            If TextSpriteAngle.Text = "" Then Scene.SetSpriteProperty AktlFrame, AktlSprite, iAngle, 0
        End If
    End If
End Sub

Private Sub TextSpriteX_Change()
    If IsNumeric(TextSpriteX.Text) And AktlFrame > 0 And AktlSprite > 0 Then
        Scene.SetSpriteProperty AktlFrame, AktlSprite, lXPos, CInt(TextSpriteX.Text)
    End If
End Sub

Private Sub TextSpriteY_Change()
    If IsNumeric(TextSpriteY.Text) And AktlFrame > 0 And AktlSprite > 0 Then
        Scene.SetSpriteProperty AktlFrame, AktlSprite, lYPos, CInt(TextSpriteY.Text)
    End If
End Sub

Private Sub scrollZ_change()

    If AktlFrame > 0 And AktlSprite > 0 Then
        If oOneFrame.Value = True Then
            Scene.SetSpriteProperty AktlFrame, AktlSprite, lZ, scrollZ.Value
        Else
            For i = 1 To Scene.GetFrameCount
                Scene.SetSpriteProperty i, AktlSprite, lZ, scrollZ.Value
            Next i
        End If
    End If
    
End Sub

Private Sub TB_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case Button.Key
        
        Case "newframe": AddFrame
        Case "newsprite": AddSprite
        Case "play": AktlPlay = Not AktlPlay
        Case "delframe":
        
            If Scene.RemoveFrame(CInt(AktlFrame)) Then
                If AktlFrame > 1 Then AktlFrame = AktlFrame - 1 Else AktlFrame = 1
                SliderFrame.Max = SliderFrame.Max - 1
            End If
            
        Case "delsprite":
        
            If Scene.RemoveSprite(CInt(AktlSprite)) Then
                If Scene.GetSpriteCount > 0 Then AktlSprite = 1 Else AktlSprite = 0
                SpriteSelect = AktlSprite
                ComboSprite.Clear
                For i = 1 To Scene.GetSpriteCount
                    ComboSprite.AddItem Scene.GetSpriteName(i)
                Next i
                If SpriteCount = 0 Then picContainer2.Enabled = False
            End If
            
    End Select
    
End Sub

Private Sub TB2_ButtonClick(ByVal Button As MSComctlLib.Button)

    Select Case Button.Key
        Case "open"
            OpenScene
        Case "save"
            SaveScene
        Case "database"
            FormImageList.Show 1
        Case "exit"
            Unload Me
    End Select

End Sub
