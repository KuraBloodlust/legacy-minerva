Attribute VB_Name = "modS2DGlobal"
Option Explicit

Public DX As New DirectX8
Public D3D As Direct3D8
Public D3DX As New D3DX8
Public D3DDevice As Direct3DDevice8

Public BackBufferSurface As Direct3DSurface8
Public Const vFVF As Double = D3DFVF_XYZ Or D3DFVF_DIFFUSE Or D3DFVF_SPECULAR Or D3DFVF_TEX1
Public Const lFVF  As Long = (D3DFVF_XYZ Or D3DFVF_NORMAL Or D3DFVF_TEX1)
    
Public BackBufferWidth As Integer
Public BackBufferHeight As Integer
Public ScreenWidth As Integer
Public ScreenHeight As Integer
Public ScreenShiftX As Integer
Public ScreenShiftY As Integer

Public AdapterCount As Integer
Public ModeCount As Integer

Public HardwareAcceleration As Boolean
Public HardwareVertexProcessing As Boolean
Public MaximumTextureWidth As Integer
Public MaximumTextureHeight As Integer
Public MaximumPrimitives As Long
Public MaximumActiveLights As Integer
Public SquareTextureLimitation As Boolean
Public SupportsWindowedMode As Boolean

Public S2DLog As New Collection

Public Declare Function GetTickCount Lib "kernel32" () As Long
Public Const NotPI As Double = 3.14159265238 / 180
Public CurrentTexture As Integer
Public CurrentMode As BlendingMode
Public ZoomFactor As Double

Public Enum BlendingMode
    S2D_None = 0
    S2D_Masked = 1
    S2D_Alpha = 2
    S2D_Alpha_Color = 3
    S2D_Additive = 4
    S2D_Additive_Source = 5
    S2D_Additive_Destination = 6
    S2D_Exclude = 7
    S2D_Subtract = 8
    S2D_Minimize = 9
    S2D_Maximize = 10
End Enum

Public Enum RenderMode
    S2D_Solid = 0
    S2D_WireFrame = 1
    S2D_Point = 2
End Enum

Public Type ColorRGB
    r As Byte
    g As Byte
    b As Byte
End Type

Public Const SRCCOPY                As Long = &HCC0020
Public Const SWP_NOMOVE            As Integer = 2
Public Const SWP_NOSIZE            As Integer = 1
Public Const RGN_OR                 As Integer = 2

Public Type PNGHeader
    Height                            As Long
    Width                             As Long
End Type

Public Const PI                     As Double = 3.14159265358979
Public Const PLANES                As Long = 14
Public Const BITSPIXEL             As Long = 12

Public Declare Function GetAsyncKeyState Lib "user32.dll" (ByVal vKey As Long) As Integer
Public Declare Function GetFocus Lib "user32" () As Long
Public Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long
Public Declare Function SelectObject Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long) As Long
Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Public Declare Function CreateCompatibleBitmap Lib "gdi32" (ByVal hDC As Long, ByVal nWidth As Long, ByVal nHeight As Long) As Long
Public Declare Function CreateCompatibleDC Lib "gdi32" (ByVal hDC As Long) As Long
Public Declare Function GetDC Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function GetPixel Lib "gdi32" (ByVal hDC As Long, ByVal X As Long, ByVal Y As Long) As Long
Public Declare Function SetPixel Lib "gdi32" (ByVal hDC As Long, ByVal X As Long, ByVal Y As Long, ByVal crColor As Long) As Long
Public Declare Function CollisionDetect Lib "Collision.dll" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X1Width As Long, ByVal Y1Height As Long, ByVal Mask1LocX As Long, ByVal Mask1LocY As Long, ByVal Mask1Hdc As Long, ByVal X2 As Long, ByVal y2 As Long, ByVal X2Width As Long, ByVal Y2Height As Long, ByVal Mask2LocX As Long, ByVal Mask2LocY As Long, ByVal Mask2Hdc As Long) As Long
Public Declare Function GetDeviceCaps& Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long)
Public Declare Function ReleaseDC& Lib "user32" (ByVal hWnd As Long, ByVal hDC As Long)
Public Declare Function SetWindowRgn Lib "user32" (ByVal hWnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Public Declare Function CreateRectRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal y2 As Long) As Long
Public Declare Function CombineRgn Lib "gdi32" (ByVal hDestRgn As Long, ByVal hSrcRgn1 As Long, ByVal hSrcRgn2 As Long, ByVal nCombineMode As Long) As Long
Public Const GWL_EXSTYLE = -20
Public Const GWL_STYLE = -16
Public Const WS_EX_STATICEDGE = &H20000
Public Const WS_EX_CLIENTEDGE = &H200&

Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Public Declare Function GetInputState Lib "user32" () As Long

Public Const Const_TileWidth As Integer = 16
Public Const Const_TileHeight As Integer = 16

Public Function Ceil(InValue As Double, InDecimal As Integer) As Double

    Dim lDblProcess As Double
    lDblProcess = InValue * (10 ^ InDecimal)
    If Int(lDblProcess) < lDblProcess Then
    lDblProcess = Int(lDblProcess) + 1
    Else
    lDblProcess = Int(lDblProcess)
    End If
    
    Ceil = lDblProcess / (10 ^ InDecimal)
    
End Function

Public Function Floor(InValue As Double, InDecimal As Integer) As Double

    Dim lDblProcess As Double
    lDblProcess = InValue * (10 ^ InDecimal)
    Floor = Int(lDblProcess) / (10 ^ InDecimal)

End Function

Public Function MakeVector(X As Single, Y As Single, Z As Single) As D3DVECTOR

    MakeVector.X = X
    MakeVector.Y = Y
    MakeVector.Z = Z
    
End Function

Public Function Deg2Rad(Deg) As Single

    Deg2Rad = CDbl(Deg * PI / 180)

End Function

Public Function SplitColor(ByVal lngColor As Long) As ColorRGB

    With SplitColor
        .r = lngColor And &HFF
        .g = (lngColor \ &H100) And &HFF
        .b = (lngColor \ &H10000) And &HFF
    End With

End Function

Function GetFileName(Path As String) As String

    Dim i As Integer
    For i = (Len(Path)) To 1 Step -1
        If Mid(Path, i, 1) = "\" Then
            GetFileName = Mid(Path, i + 1, Len(Path) - i + 1)
            Exit For
        End If
    Next
    
End Function


Public Function DirectoryExist(ByVal Path As String) As Boolean
 
    Dim dirFolder As String
    dirFolder = Dir(Path, vbDirectory)
    If dirFolder <> "" Then
        DirectoryExist = True
    End If

End Function

Public Function GetColorDepth() As Integer

    Dim nPlanes As Integer
    Dim BitsPerPixel As Integer
    Dim dc As Long

    dc = GetDC(0)
    nPlanes = GetDeviceCaps(dc, PLANES)
    BitsPerPixel = GetDeviceCaps(dc, BITSPIXEL)
    ReleaseDC 0, dc
    GetColorDepth = nPlanes * BitsPerPixel

End Function

Public Function RandomNumber(From As Integer, ToEnd As Integer) As Integer

    Randomize Timer
    RandomNumber = Int((ToEnd * Rnd) + 1) + From

End Function

Public Function ReadPNGHeader(ByVal strFilename As String, header As PNGHeader) As Boolean

    Dim Handle As Integer
    Dim ByteArr(255) As Byte

    header.Height = 0
    header.Width = 0
    Handle = FreeFile
 
    Open strFilename For Binary Access Read As #Handle
    Get Handle, , ByteArr
    Close #Handle
    If ByteArr(0) = &H89 Then
        If ByteArr(1) = &H50 Then
            If ByteArr(2) = &H4E Then
                If ByteArr(3) = &H47 Then
                    header.Width = ByteArr(18) * 256 + ByteArr(19)
                    header.Height = ByteArr(22) * 256 + ByteArr(23)
                End If
            End If
        End If
    End If

End Function
