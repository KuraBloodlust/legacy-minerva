VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   Caption         =   "R-PG Menu Editor"
   ClientHeight    =   6915
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   9765
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   461
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   651
   StartUpPosition =   2  'CenterScreen
   Tag             =   "Animation Editor"
   Begin MSComctlLib.StatusBar status 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   6660
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   450
      Style           =   1
      SimpleText      =   "Page : 1/1"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picContainer 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   240
      ScaleHeight     =   165
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   189
      TabIndex        =   0
      Top             =   840
      Width           =   2895
      Begin VB.PictureBox PictureDest 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   0
         ScaleHeight     =   105
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   113
         TabIndex        =   1
         Top             =   0
         Width           =   1695
      End
   End
   Begin MSComDlg.CommonDialog CD1 
      Left            =   7560
      Top             =   2760
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList iml 
      Left            =   7560
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   28
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":08CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":15BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":22AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2FA0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":3C92
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":4984
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":5676
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":6368
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":705A
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":7D4C
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":8A3E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":9730
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":A422
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":B114
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":BE06
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":CAF8
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":D7EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":E4DC
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":F1CE
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":FEC0
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":10BB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":118A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":12596
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":13288
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":13F7A
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":14C6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1595E
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":16650
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar TB 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   3
      Top             =   6330
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   582
      ButtonWidth     =   609
      Style           =   1
      ImageList       =   "iml"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "last"
            Object.ToolTipText     =   "Last Menu Page"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "next"
            Object.ToolTipText     =   "Next Menu Page"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "remove"
            Object.ToolTipText     =   "Remove Page"
            ImageIndex      =   5
         EndProperty
      EndProperty
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2640
         TabIndex        =   6
         Top             =   0
         Width           =   1335
      End
      Begin MSComctlLib.Slider slideSnap 
         Height          =   255
         Left            =   1080
         TabIndex        =   5
         Top             =   0
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   450
         _Version        =   393216
         Min             =   1
         Max             =   16
         SelStart        =   8
         Value           =   8
      End
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   7800
      Top             =   3480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   28
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":169EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":176DC
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":183CE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":190C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":19DB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1AAA4
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1B796
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1C488
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1D17A
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1DE6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1EB5E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1F850
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":20542
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":21234
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":21F26
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":22C18
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2390A
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":245FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":252EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":25FE0
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":26CD2
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":279C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":286B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":293A8
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2A09A
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2AD8C
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2BA7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2C770
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar TB2 
      Align           =   1  'Align Top
      Height          =   330
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   582
      ButtonWidth     =   609
      Style           =   1
      ImageList       =   "iml"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   13
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   14
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   26
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   22
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   6
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   6
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   6
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   6
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "zoom"
            Object.ToolTipText     =   "Enable/Disable Zooming"
            ImageIndex      =   23
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   10
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuCtl 
      Caption         =   "Controls"
      Begin VB.Menu mnuAddFrame 
         Caption         =   "Add Frame"
      End
      Begin VB.Menu mnuAddButton 
         Caption         =   "Add Button"
      End
      Begin VB.Menu mnuAddText 
         Caption         =   "Add Text"
      End
      Begin VB.Menu mnuAddList 
         Caption         =   "Add List"
      End
      Begin VB.Menu mnuAddImage 
         Caption         =   "Add Image"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "About"
   End
   Begin VB.Menu mnuButtonText 
      Caption         =   "Button/Text"
      Begin VB.Menu mnuChangeButtonText 
         Caption         =   "Change Text"
      End
   End
   Begin VB.Menu mnuFrameList 
      Caption         =   "Frame/List"
      Begin VB.Menu mnuChangeColor 
         Caption         =   "Change Color"
      End
   End
   Begin VB.Menu mnuImage 
      Caption         =   "Image"
      Begin VB.Menu mnuChangeImage 
         Caption         =   "Change Image"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Reload As Boolean
Private ScaleFactor As Double

Public CurrentPage As Integer

Private lY As Integer
Private lX As Integer
Private CurrentObject As Integer
Private CObjectType As String
Private MoveObject As Boolean
Private ObjectResize As Boolean
Private ResizeDirection As String
Private ResizeBuffer As RECT
Private CObjectDif As RECT
Private CopyID As Integer
Private CopyPage As Integer
Private CopyType As String
Private PointID As Integer
Private SnapValue As Integer
Public ZoomBool As Boolean

Private Function DrawBackground(Width As Integer, Height As Integer)
    
    Effects.RenderRectangle 0, 0, Width, Height, , , , , 100
    'For a = 0 To Width Step 16
        'For b = 0 To Height Step 16
        
            'Engine.Effects.RECT CSng(a), CSng(b), 8, 8, vbWhite
            'Engine.Effects.RECT CSng(a) + 8, CSng(b), 8, 8, rgb(150, 150, 150)
            'Engine.Effects.RECT CSng(a), CSng(b) + 8, 8, 8, rgb(150, 150, 150)
            'Engine.Effects.RECT CSng(a) + 8, CSng(b) + 8, 8, 8, vbWhite
        
        'Next b
    'Next a

End Function

Private Function RenderResizeBox()

    Dim cTemp As RECT

    If CObjectType = "frame" Then
        cTemp = Menu.GetFrameDimension(CurrentPage, CurrentObject)
    End If
    
    If CObjectType = "button" Then
        cTemp = Menu.GetButtonDimension(CurrentPage, CurrentObject)
    End If
    
    If CObjectType = "list" Then
        cTemp = Menu.GetListDimension(CurrentPage, CurrentObject)
    End If
    
    If CObjectType = "image" Then
        cTemp = Menu.GetImageDimension(CurrentPage, CurrentObject)
    End If
    
    If CObjectType = "frame" Or CObjectType = "list" Or CObjectType = "image" Then
        Engine.TexturePool.RenderTexture PointID, CDbl(cTemp.Left) - 8, CDbl(cTemp.Top) - 8, 8, 8, , , , , IIf(ResizeDirection = "OL", 255, 150)
        Engine.TexturePool.RenderTexture PointID, cTemp.Left + cTemp.Right, CDbl(cTemp.Top) - 8, 8, 8, , , , , IIf(ResizeDirection = "OR", 255, 150)
        Engine.TexturePool.RenderTexture PointID, cTemp.Left + (cTemp.Right / 2) - 4, CDbl(cTemp.Top) - 8, 8, 8, , , , , IIf(ResizeDirection = "OM", 255, 150)
        
        Engine.TexturePool.RenderTexture PointID, cTemp.Left + (cTemp.Right / 2) - 4, cTemp.Top + cTemp.bottom, 8, 8, , , , , IIf(ResizeDirection = "UM", 255, 150)
        Engine.TexturePool.RenderTexture PointID, CDbl(cTemp.Left) - 8, cTemp.Top + cTemp.bottom, 8, 8, , , , , IIf(ResizeDirection = "UL", 255, 150)
        Engine.TexturePool.RenderTexture PointID, cTemp.Left + cTemp.Right, cTemp.Top + cTemp.bottom, 8, 8, , , , , IIf(ResizeDirection = "UR", 255, 150)
        
        Engine.TexturePool.RenderTexture PointID, CDbl(cTemp.Left) - 8, cTemp.Top + (cTemp.bottom / 2) - 4, 8, 8, , , , , IIf(ResizeDirection = "ML", 255, 150)
        Engine.TexturePool.RenderTexture PointID, cTemp.Left + cTemp.Right, cTemp.Top + (cTemp.bottom / 2) - 4, 8, 8, , , , , IIf(ResizeDirection = "MR", 255, 150)
    End If

End Function

Private Sub Form_Load()
       
    Engine.Initialize PictureDest.hwnd, 320, 240
    Set Effects = New cS2DEffects
    Set Effects.MyPool = Engine.TexturePool.Pool1
    Set Text.S2D = Engine
    Text.InitText App.Path & "\" & "Book Antiqua(9).PNG", App.Path & "\" & "Book Antiqua(9).txt"
    Set Menu.S2D = Engine
    Set Menu.Text = Text
    PointID = Engine.TexturePool.AddTexture(App.Path & "\particle2.png", rgb(255, 0, 255))
    SnapValue = 8
    Menu.AddPage
    CurrentPage = 1

    Form_Resize
    Me.Show
    RenderLoop
    
End Sub

Public Sub Form_Resize()
    
    picContainer.Move 0, tb.Height, Me.ScaleWidth, Me.ScaleHeight - TB2.Height - tb.Height - status.Height
    
    If Reload Then
        Dim tw As Integer
        Dim tH As Integer
        tw = ScreenWidth
        tH = ScreenHeight
        
        Dim tf1 As Double
        Dim tf2 As Double
        tf1 = picContainer.ScaleWidth / tw
        tf2 = picContainer.ScaleHeight / tH
        
        ScaleFactor = 1
        
        If ZoomBool = True Then
            If tf1 < tf2 Then
                PictureDest.Width = picContainer.Width
                PictureDest.Height = tH * tf1
                PictureDest.Left = 0
                PictureDest.Top = picContainer.Height / 2 - PictureDest.Height / 2
                ScaleFactor = tf1
            Else
                PictureDest.Height = picContainer.Height
                PictureDest.Width = tw * tf2
                PictureDest.Top = 0
                PictureDest.Left = picContainer.Width / 2 - PictureDest.Width / 2
                ScaleFactor = tf2
            End If
            
            ZoomFactor = (PictureDest.ScaleWidth / 320)
            Engine.Resize PictureDest.ScaleWidth, PictureDest.ScaleHeight
        Else
            PictureDest.Width = 320
            PictureDest.Height = 240
            ZoomFactor = 1
            Engine.Resize 320, 240
            PictureDest.Left = picContainer.Width / 2 - PictureDest.Width / 2
            PictureDest.Top = picContainer.Height / 2 - PictureDest.Height / 2
        End If
        
    End If
    Reload = True
    
End Sub

Public Sub RenderLoop()
    
    Do
    DoEvents
        
        Engine.Clear
        Engine.BeginScene
            DrawBackground ScreenWidth, ScreenHeight
            Menu.FadeIn CurrentPage
            Menu.RenderPage CurrentPage
            RenderResizeBox
        Engine.EndScene
        Engine.Flip
        
        Limiter.LimitFrames 50
    
    Loop
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Engine.Unload
    Unload Me
    End
    
End Sub

Private Sub Label6_Click()

End Sub

Private Sub mnuAddButton_Click()
    
    Menu.AddButton CurrentPage, 0, 0, "Button " & Menu.GetButtonCount(CurrentPage)
    
End Sub

Private Sub mnuAddFrame_Click()
    
    Menu.AddFrame CurrentPage, 0, 0, 100, 100, vbRed
    
End Sub



Private Sub mnuAddImage_Click()
    
    CD1.ShowOpen
    Menu.AddImage CurrentPage, CD1.FileName, 0, 0, 100, 100
    
End Sub

Private Sub mnuAddList_Click()
    
    Menu.AddList CurrentPage, 0, 0, 100, 100, vbGreen
    Menu.AddListItem CurrentPage, Menu.GetListCount(CurrentPage), "Item 1", "Item1"
    
End Sub

Private Sub mnuAddText_Click()
    
    Menu.AddText CurrentPage, 0, 0, "Text " & Menu.GetTextCount(CurrentPage)
    
End Sub

Private Sub mnuChangeButtonText_Click()
    
    If CObjectType = "button" Then
        MenuPages(CurrentPage).Buttons(CurrentObject).Text = frmText.ShowText()
        
    End If
    
End Sub

Private Sub PictureDest_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyDelete Then
        
        If CObjectType = "button" Then
            Menu.RemoveButton CurrentPage, CurrentObject
        ElseIf CObjectType = "frame" Then
            Menu.RemoveFrame CurrentPage, CurrentObject
        ElseIf CObjectType = "list" Then
            Menu.RemoveList CurrentPage, CurrentObject
        ElseIf CObjectType = "image" Then
            Menu.RemoveImage CurrentPage, CurrentObject
        ElseIf CObjectType = "text" Then
            Menu.RemoveText CurrentPage, CurrentObject
        End If
        
        CObjectType = ""
        CurrentObject = 0
        ResizeDirection = "0"
        ObjectResize = False
        MoveObject = False
        
    End If
    
End Sub

Private Sub PictureDest_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    X = X / ScaleFactor
    Y = Y / ScaleFactor
    
    Dim cTemp As RECT
    Dim cFrame As RECT
    Dim cList As RECT
    Dim i As Integer
    Dim cButton As RECT
    Dim cImage As RECT
    Dim cText As RECT
    
    If CObjectType = "frame" Or CObjectType = "list" Or CObjectType = "image" Then
    
        If CObjectType = "frame" Then cTemp = Menu.GetFrameDimension(CurrentPage, CurrentObject)
        If CObjectType = "list" Then cTemp = Menu.GetListDimension(CurrentPage, CurrentObject)
        If CObjectType = "image" Then cTemp = Menu.GetImageDimension(CurrentPage, CurrentObject)
        
        ResizeBuffer = cTemp
        
        If IsInRect(cTemp.Left - 8, cTemp.Top - 8, cTemp.Left, cTemp.Top, X, Y) Then
            ObjectResize = True
            ResizeDirection = "OL"
            Exit Sub
        End If
        If IsInRect(cTemp.Left + (cTemp.Right / 2) - 4, cTemp.Top - 8, cTemp.Left + (cTemp.Right / 2) + 4, cTemp.Top, X, Y) Then
            ObjectResize = True
            ResizeDirection = "OM"
            Exit Sub
        End If
        If IsInRect(cTemp.Left + cTemp.Right, cTemp.Top - 8, cTemp.Left + cTemp.Right + 8, cTemp.Top, X, Y) Then
            ObjectResize = True
            ResizeDirection = "OR"
            Exit Sub
        End If
        
        If IsInRect(cTemp.Left - 8, cTemp.Top + cTemp.bottom, cTemp.Left, cTemp.Top + cTemp.bottom + 8, X, Y) Then
            ObjectResize = True
            ResizeDirection = "UL"
            Exit Sub
        End If
        If IsInRect(cTemp.Left + (cTemp.Right / 2) - 4, cTemp.Top + cTemp.bottom, cTemp.Left + (cTemp.Right / 2) + 4, cTemp.Top + cTemp.bottom + 8, X, Y) Then
            ObjectResize = True
            ResizeDirection = "UM"
            Exit Sub
        End If
        If IsInRect(cTemp.Left + cTemp.Right, cTemp.Top + cTemp.bottom, cTemp.Left + cTemp.Right + 8, cTemp.Top + cTemp.bottom + 8, X, Y) Then
            ObjectResize = True
            ResizeDirection = "UR"
            Exit Sub
        End If
        
        If IsInRect(cTemp.Left + cTemp.Right, cTemp.Top + (cTemp.bottom / 2) - 4, cTemp.Left + cTemp.Right + 8, cTemp.Top + (cTemp.bottom / 2) + 4, X, Y) Then
            ObjectResize = True
            ResizeDirection = "MR"
            Exit Sub
        End If
        If IsInRect(cTemp.Left - 8, cTemp.Top + (cTemp.bottom / 2) - 4, cTemp.Left, cTemp.Top + (cTemp.bottom / 2) + 4, X, Y) Then
            ObjectResize = True
            ResizeDirection = "ML"
            Exit Sub
        End If
    End If
    
    For i = Menu.GetButtonCount(CurrentPage) To 1 Step -1
    
        cButton = Menu.GetButtonDimension(CurrentPage, i)
        If CheckRect(CInt(X), CInt(Y), cButton) Then
            CurrentObject = i
            CObjectType = "button"
            CObjectDif.Left = X - cButton.Left
            CObjectDif.Top = Y - cButton.Top
            Menu.SetButtonFocus CurrentPage, i
            MoveObject = True
            Exit Sub
        End If
    
    Next i
    
    For i = Menu.GetTextCount(CurrentPage) To 1 Step -1
        
        cText = Menu.GetTextDimension(CurrentPage, i)
        If CheckRect(CInt(X), CInt(Y), cText) Then
            CurrentObject = i
            CObjectType = "text"
            CObjectDif.Left = X - cText.Left
            CObjectDif.Top = Y - cText.Top
            MoveObject = True
            Exit Sub
        End If
    
    Next i
    
    For i = Menu.GetListCount(CurrentPage) To 1 Step -1
    
        cList = Menu.GetListDimension(CurrentPage, i)
        If CheckRect(CInt(X), CInt(Y), cList) Then
            CurrentObject = i
            CObjectType = "list"
            CObjectDif.Left = X - cList.Left
            CObjectDif.Top = Y - cList.Top
            Menu.SetButtonFocus CurrentPage, 0
            MoveObject = True
            Exit Sub
        End If
    
    Next i
    
    For i = Menu.GetImageCount(CurrentPage) To 1 Step -1
    
        cImage = Menu.GetImageDimension(CurrentPage, i)
        If CheckRect(CInt(X), CInt(Y), cImage) Then
            CurrentObject = i
            CObjectType = "image"
            CObjectDif.Left = X - cImage.Left
            CObjectDif.Top = Y - cImage.Top
            MoveObject = True
            Exit Sub
        End If
    
    Next i
    
    For i = Menu.GetFrameCount(CurrentPage) To 1 Step -1
    
        cFrame = Menu.GetFrameDimension(CurrentPage, i)
        If CheckRect(CInt(X), CInt(Y), cFrame) Then
            CurrentObject = i
            CObjectType = "frame"
            CObjectDif.Left = X - cFrame.Left
            CObjectDif.Top = Y - cFrame.Top
            MoveObject = True
            Menu.SetButtonFocus CurrentPage, 0
            Exit Sub
        End If
    
    Next i
    
    Menu.SetButtonFocus CurrentPage, 0
    CObjectType = "none"
    CurrentObject = 0

End Sub

Private Sub PictureDest_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    X = X / ScaleFactor
    Y = Y / ScaleFactor
    
    If ObjectResize Then
        If CObjectType = "frame" Or CObjectType = "list" Or CObjectType = "image" Then
        
            If ResizeDirection = "OL" Then
                If ResizeBuffer.Right + (ResizeBuffer.Left - X) < 16 Then Exit Sub
                If ResizeBuffer.bottom + (ResizeBuffer.Top - Y) < 16 Then Exit Sub
                X = Snap(X, SnapValue)
                Y = Snap(Y, SnapValue)
                If CObjectType = "frame" Then Menu.SetFrameDimension CurrentPage, CurrentObject, CDbl(X), CDbl(Y), ResizeBuffer.Right + (ResizeBuffer.Left - X), ResizeBuffer.bottom + (ResizeBuffer.Top - Y)
                If CObjectType = "list" Then Menu.SetListDimension CurrentPage, CurrentObject, CDbl(X), CDbl(Y), ResizeBuffer.Right + (ResizeBuffer.Left - X), ResizeBuffer.bottom + (ResizeBuffer.Top - Y)
                If CObjectType = "image" Then Menu.SetImageDimension CurrentPage, CurrentObject, CDbl(X), CDbl(Y), ResizeBuffer.Right + (ResizeBuffer.Left - X), ResizeBuffer.bottom + (ResizeBuffer.Top - Y)
            End If
            
            If ResizeDirection = "OM" Then
                If ResizeBuffer.bottom + (ResizeBuffer.Top - Y) < 16 Then Exit Sub
                Y = Snap(Y, SnapValue)
                If CObjectType = "frame" Then Menu.SetFrameDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(Y), CDbl(ResizeBuffer.Right), ResizeBuffer.bottom + (ResizeBuffer.Top - Y)
                If CObjectType = "list" Then Menu.SetListDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(Y), CDbl(ResizeBuffer.Right), ResizeBuffer.bottom + (ResizeBuffer.Top - Y)
                If CObjectType = "image" Then Menu.SetImageDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(Y), CDbl(ResizeBuffer.Right), ResizeBuffer.bottom + (ResizeBuffer.Top - Y)
            End If
            
            If ResizeDirection = "OR" Then
                If ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right) < 16 Then Exit Sub
                If ResizeBuffer.bottom + (ResizeBuffer.Top - Y) < 16 Then Exit Sub
                X = Snap(X, SnapValue)
                Y = Snap(Y, SnapValue)
                If CObjectType = "frame" Then Menu.SetFrameDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(Y), ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right), ResizeBuffer.bottom + (ResizeBuffer.Top - Y)
                If CObjectType = "list" Then Menu.SetListDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(Y), ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right), ResizeBuffer.bottom + (ResizeBuffer.Top - Y)
                If CObjectType = "image" Then Menu.SetImageDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(Y), ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right), ResizeBuffer.bottom + (ResizeBuffer.Top - Y)
            End If
                
            If ResizeDirection = "UL" Then
                If ResizeBuffer.Right + (ResizeBuffer.Left - X) < 16 Then Exit Sub
                If ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom) < 16 Then Exit Sub
                X = Snap(X, SnapValue)
                Y = Snap(Y, SnapValue)
                If CObjectType = "frame" Then Menu.SetFrameDimension CurrentPage, CurrentObject, CDbl(X), CDbl(ResizeBuffer.Top), ResizeBuffer.Right + (ResizeBuffer.Left - X), ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom)
                If CObjectType = "list" Then Menu.SetListDimension CurrentPage, CurrentObject, CDbl(X), CDbl(ResizeBuffer.Top), ResizeBuffer.Right + (ResizeBuffer.Left - X), ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom)
                If CObjectType = "image" Then Menu.SetImageDimension CurrentPage, CurrentObject, CDbl(X), CDbl(ResizeBuffer.Top), ResizeBuffer.Right + (ResizeBuffer.Left - X), ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom)
            End If
            
            If ResizeDirection = "UM" Then
                If ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom) < 16 Then Exit Sub
                X = Snap(X, SnapValue)
                Y = Snap(Y, SnapValue)
                If CObjectType = "frame" Then Menu.SetFrameDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(ResizeBuffer.Top), CDbl(ResizeBuffer.Right), ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom)
                If CObjectType = "list" Then Menu.SetListDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(ResizeBuffer.Top), CDbl(ResizeBuffer.Right), ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom)
                If CObjectType = "image" Then Menu.SetImageDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(ResizeBuffer.Top), CDbl(ResizeBuffer.Right), ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom)
            End If
            
            If ResizeDirection = "UR" Then
                If ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right) < 16 Then Exit Sub
                If ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom) < 16 Then Exit Sub
                X = Snap(X, SnapValue)
                Y = Snap(Y, SnapValue)
                If CObjectType = "frame" Then Menu.SetFrameDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(ResizeBuffer.Top), ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right), ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom)
                If CObjectType = "list" Then Menu.SetListDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(ResizeBuffer.Top), ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right), ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom)
                If CObjectType = "image" Then Menu.SetImageDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(ResizeBuffer.Top), ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right), ResizeBuffer.bottom - (ResizeBuffer.Top - Y + ResizeBuffer.bottom)
            End If
                
            If ResizeDirection = "ML" Then
                If ResizeBuffer.Right + (ResizeBuffer.Left - X) < 16 Then Exit Sub
                X = Snap(X, SnapValue)
                Y = Snap(Y, SnapValue)
                If CObjectType = "frame" Then Menu.SetFrameDimension CurrentPage, CurrentObject, CDbl(X), CDbl(ResizeBuffer.Top), ResizeBuffer.Right + (ResizeBuffer.Left - X), CDbl(ResizeBuffer.bottom)
                If CObjectType = "list" Then Menu.SetListDimension CurrentPage, CurrentObject, CDbl(X), CDbl(ResizeBuffer.Top), ResizeBuffer.Right + (ResizeBuffer.Left - X), CDbl(ResizeBuffer.bottom)
                If CObjectType = "image" Then Menu.SetImageDimension CurrentPage, CurrentObject, CDbl(X), CDbl(ResizeBuffer.Top), ResizeBuffer.Right + (ResizeBuffer.Left - X), CDbl(ResizeBuffer.bottom)
            End If
            
            If ResizeDirection = "MR" Then
                If ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right) < 16 Then Exit Sub
                X = Snap(X, SnapValue)
                Y = Snap(Y, SnapValue)
                If CObjectType = "frame" Then Menu.SetFrameDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(ResizeBuffer.Top), ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right), CDbl(ResizeBuffer.bottom)
                If CObjectType = "list" Then Menu.SetListDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(ResizeBuffer.Top), ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right), CDbl(ResizeBuffer.bottom)
                If CObjectType = "image" Then Menu.SetImageDimension CurrentPage, CurrentObject, CDbl(ResizeBuffer.Left), CDbl(ResizeBuffer.Top), ResizeBuffer.Right - (ResizeBuffer.Left - X + ResizeBuffer.Right), CDbl(ResizeBuffer.bottom)
            End If
        End If
    End If
        
    If MoveObject Then
        If CObjectType = "button" Then
            Menu.SetButtonDimension CurrentPage, CurrentObject, Snap(X - CObjectDif.Left, SnapValue), Snap(Y - CObjectDif.Top, SnapValue)
        End If
        
        If CObjectType = "frame" Then
            Dim cFrame As RECT
            cFrame = Menu.GetFrameDimension(CurrentPage, CurrentObject)
            Menu.SetFrameDimension CurrentPage, CurrentObject, Snap(X - CObjectDif.Left, SnapValue), Snap(Y - CObjectDif.Top, SnapValue), CDbl(cFrame.Right), CDbl(cFrame.bottom)
        End If
        
        If CObjectType = "list" Then
            Dim cList As RECT
            cList = Menu.GetListDimension(CurrentPage, CurrentObject)
            Menu.SetListDimension CurrentPage, CurrentObject, Snap(X - CObjectDif.Left, SnapValue), Snap(Y - CObjectDif.Top, SnapValue), CDbl(cList.Right), CDbl(cList.bottom)
        End If
        
        If CObjectType = "image" Then
            Dim cImage As RECT
            cImage = Menu.GetImageDimension(CurrentPage, CurrentObject)
            Menu.SetImageDimension CurrentPage, CurrentObject, Snap(X - CObjectDif.Left, SnapValue), Snap(Y - CObjectDif.Top, SnapValue), CDbl(cImage.Right), CDbl(cImage.bottom)
        End If
        
        If CObjectType = "text" Then
            Menu.SetTextDimension CurrentPage, CurrentObject, Snap(X - CObjectDif.Left, SnapValue), Snap(Y - CObjectDif.Top, SnapValue)
        End If
    End If

End Sub

Private Sub PictureDest_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    ResizeDirection = "0"
    ObjectResize = False
    MoveObject = False
End Sub

Private Sub slideSnap_Change()
    SnapValue = slideSnap.Value
End Sub

Private Sub tb_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case Button.Key
        Case "next"
            If Menu.GetPageCount > CurrentPage Then
                ResizeDirection = "0"
                ObjectResize = False
                MoveObject = False
                Menu.SetButtonFocus CurrentPage, 0
                CObjectType = "none"
                CurrentObject = 0
                CurrentPage = CurrentPage + 1
            Else
                Temp = MsgBox("Do you want to add a new Page?", vbYesNo, "New Page")
                If Temp = vbYes Then
                    ResizeDirection = "0"
                    ObjectResize = False
                    MoveObject = False
                    Menu.SetButtonFocus CurrentPage, 0
                    CObjectType = "none"
                    CurrentObject = 0
                    Menu.AddPage
                    CurrentPage = CurrentPage + 1
                End If
            End If
            status.SimpleText = "Page : " & CurrentPage & "/" & Menu.GetPageCount
        
        Case "last"
            If CurrentPage > 1 Then
                ResizeDirection = "0"
                ObjectResize = False
                MoveObject = False
                Menu.SetButtonFocus CurrentPage, 0
                CObjectType = "none"
                CurrentObject = 0
                CurrentPage = CurrentPage - 1
            End If
            status.SimpleText = "Page : " & CurrentPage & "/" & Menu.GetPageCount
            
        Case "remove"
            If Menu.GetPageCount > 1 Then
                Dim ret As Integer
                ret = MsgBox("Do you really want to remove this Page?", vbYesNo, "Remove Page")
                If ret = vbYes Then
                    Menu.RemovePage CurrentPage
                End If
            Else
                MsgBox "You need at least one Menu Page", vbInformation
            End If
            CurrentPage = 1
            status.SimpleText = "Page : " & CurrentPage & "/" & Menu.GetPageCount
    End Select
    
End Sub

Private Sub TB2_ButtonClick(ByVal Button As MSComctlLib.Button)

    Select Case Button.Key
        Case "zoom"
            ZoomBool = Not ZoomBool
            Button.Value = IIf(ZoomBool, tbrPressed, tbrUnpressed)
            Form_Resize
            
    End Select

End Sub
