VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2DMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private PageCount As Integer

Private textureID As Integer
Private CurrentPage As Integer

Private Const CornerWidth As Integer = 8
Private Const CornerHeight As Integer = 8

Public Text As cS2DText
Public S2D As cS2D_i

Public Function GetPageCount() As Integer
    GetPageCount = PageCount
End Function

Public Function SetButtonFocus(Page As Integer, ID As Integer)
    MenuPages(Page).CurrentButton = ID
End Function

Public Function GetButtonCount(Page As Integer) As Integer
    GetButtonCount = MenuPages(Page).ButtonCount
End Function

Public Function GetListCount(Page As Integer) As Integer
    GetListCount = MenuPages(Page).ListCount
End Function

Public Function GetFrameCount(Page As Integer) As Integer
    GetFrameCount = MenuPages(Page).FrameCount
End Function

Public Function GetTextCount(Page As Integer) As Integer
    GetTextCount = MenuPages(Page).TextCount
End Function

Public Function GetImageCount(Page As Integer) As Integer
    GetImageCount = MenuPages(Page).ImageCount
End Function

Public Function GetButtonDimension(Page As Integer, ID As Integer) As RECT

    GetButtonDimension.Left = MenuPages(Page).Buttons(ID).X
    GetButtonDimension.Top = MenuPages(Page).Buttons(ID).Y
    GetButtonDimension.bottom = Text.GetTextHeight(1)
    GetButtonDimension.Right = Text.GetTextWidth(MenuPages(Page).Buttons(ID).Text, 1)
    
End Function

Public Function GetFrameDimension(Page As Integer, ID As Integer) As RECT

    GetFrameDimension.Left = MenuPages(Page).Frames(ID).X
    GetFrameDimension.Top = MenuPages(Page).Frames(ID).Y
    GetFrameDimension.bottom = MenuPages(Page).Frames(ID).Height
    GetFrameDimension.Right = MenuPages(Page).Frames(ID).Width
    
End Function

Public Function GetListDimension(Page As Integer, ID As Integer) As RECT

    GetListDimension.Left = MenuPages(Page).Lists(ID).X
    GetListDimension.Top = MenuPages(Page).Lists(ID).Y
    GetListDimension.bottom = MenuPages(Page).Lists(ID).Height
    GetListDimension.Right = MenuPages(Page).Lists(ID).Width
    
End Function

Public Function GetTextDimension(Page As Integer, ID As Integer) As RECT

    GetTextDimension.Left = MenuPages(Page).Texts(ID).X
    GetTextDimension.Top = MenuPages(Page).Texts(ID).Y
    GetTextDimension.bottom = Text.GetTextHeight(1)
    GetTextDimension.Right = Text.GetTextWidth(MenuPages(Page).Texts(ID).Text, 1)
    
End Function

Public Function GetImageDimension(Page As Integer, ID As Integer) As RECT

    GetImageDimension.Left = MenuPages(Page).Images(ID).X
    GetImageDimension.Top = MenuPages(Page).Images(ID).Y
    GetImageDimension.bottom = MenuPages(Page).Images(ID).Height
    GetImageDimension.Right = MenuPages(Page).Images(ID).Width
    
End Function

Public Function SetButtonDimension(Page As Integer, ID As Integer, X As Double, Y As Double)

    MenuPages(Page).Buttons(ID).X = X
    MenuPages(Page).Buttons(ID).Y = Y
    
End Function

Public Function SetTextDimension(Page As Integer, ID As Integer, X As Double, Y As Double)

    MenuPages(Page).Texts(ID).X = X
    MenuPages(Page).Texts(ID).Y = Y
    
End Function

Public Function SetFrameDimension(Page As Integer, ID As Integer, X As Double, Y As Double, Width As Double, Height As Double)

    MenuPages(Page).Frames(ID).X = X
    MenuPages(Page).Frames(ID).Y = Y
    MenuPages(Page).Frames(ID).Width = Width
    MenuPages(Page).Frames(ID).Height = Height
    
End Function

Public Function SetListDimension(Page As Integer, ID As Integer, X As Double, Y As Double, Width As Double, Height As Double)

    MenuPages(Page).Lists(ID).X = X
    MenuPages(Page).Lists(ID).Y = Y
    MenuPages(Page).Lists(ID).Width = Width
    MenuPages(Page).Lists(ID).Height = Height
    
End Function

Public Function SetImageDimension(Page As Integer, ID As Integer, X As Double, Y As Double, Width As Double, Height As Double)

    MenuPages(Page).Images(ID).X = X
    MenuPages(Page).Images(ID).Y = Y
    MenuPages(Page).Images(ID).Width = Width
    MenuPages(Page).Images(ID).Height = Height
    
End Function

Public Function SetFocus(Page As Integer)
    CurrentPage = Page
End Function

Public Function AddPage() As Integer
    
    ReDim Preserve MenuPages(0 To PageCount + 1)
    PageCount = PageCount + 1
    AddPage = PageCount
    
    textureID = S2D.TexturePool.AddTexture(App.Path & "\system.png", rgb(255, 0, 255))
    
End Function

Public Function RemovePage(Page As Integer)

    Dim TempArray() As MenuPage
    ReDim TempArray(PageCount - 1)
    
    Dim tmp As Integer
    For i = 1 To PageCount
        If i = Page Then
            tmp = 1
            GoTo skip
        End If
        TempArray(i - tmp) = MenuPages(i)
skip:
    Next i
    
    PageCount = PageCount - 1
    ReDim MenuPages(PageCount)
    For i = 1 To PageCount
        MenuPages(i) = TempArray(i)
    Next i
    
End Function

Public Function ButtonPressed(Page As Integer, cID As Integer) As Boolean

    If CurrentPage = Page And MenuPages(Page).CurrentList = 0 Then
        If MenuPages(Page).CurrentButton = cID Then
            If Controller.CheckKey(vbKeyReturn, 8) Then
                ButtonPressed = True
            End If
        End If
    End If
    
End Function

Public Function ListPressed(Page As Integer, List As Integer, Index As Integer) As Boolean

    If CurrentPage = Page And MenuPages(Page).CurrentButton = 0 Then
        If MenuPages(Page).CurrentList = List Then
            If MenuPages(Page).Lists(List).CurrentItem = Index Then
                If Controller.CheckKey(vbKeyReturn, 8) Then
                    ListPressed = True
                End If
            End If
        End If
    End If
    
End Function

Public Function FocusList(Page As Integer, List As Integer)
    
    MenuPages(Page).CurrentList = List
    MenuPages(Page).Lists(List).CurrentItem = 1
    MenuPages(Page).Lists(List).ListStart = 1

    If MenuPages(Page).Lists(List).ItemCount < MenuPages(Page).Lists(List).Height / Text.GetTextHeight(1) - 1 Then
        MenuPages(Page).Lists(List).ListEnd = MenuPages(Page).Lists(List).ItemCount
    Else
        MenuPages(Page).Lists(List).ListEnd = MenuPages(Page).Lists(List).Height / Text.GetTextHeight(1) - 1
    End If
    
    MenuPages(Page).CurrentButton = 0
    
End Function

Public Function FadeOut(Page As Integer)

    If MenuPages(Page).Visibility > 0 Then MenuPages(Page).Visibility = MenuPages(Page).Visibility - 5
    
End Function

Public Function FadeIn(Page As Integer)

    If MenuPages(Page).Visibility < 100 Then MenuPages(Page).Visibility = MenuPages(Page).Visibility + 5
    
End Function

Public Function RenderPage(Page As Integer)

    If MenuPages(Page).Visibility <= 0 Then Exit Function

    If Page = CurrentPage And MenuPages(Page).CurrentList = 0 Then
        
        If Controller.CheckKey(vbKeyUp, 8) Then
            MenuPages(Page).CurrentButton = MenuPages(Page).CurrentButton - 1
        End If
        
        If Controller.CheckKey(vbKeyDown, 8) Then
            MenuPages(Page).CurrentButton = MenuPages(Page).CurrentButton + 1
        End If
        
        If MenuPages(Page).CurrentButton > MenuPages(Page).ButtonCount Then
            MenuPages(Page).CurrentButton = 1
            GoTo Back
        End If
        
        If Controller.CheckKey(vbKeyTab, 8) Then
            If MenuPages(Page).ListCount > 0 Then
                FocusList Page, 1
            End If
        
            If MenuPages(Page).CurrentButton < 1 Then
                MenuPages(Page).CurrentButton = MenuPages(Page).ButtonCount
                GoTo Back
            End If
        End If
        
    ElseIf Page = CurrentPage And MenuPages(Page).CurrentList > 0 Then
    
        If Controller.CheckKey(vbKeyUp, 8) Then
            If Not MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = 1 Then
                MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem - 1
                If MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart Then
                    If Not MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = 1 Then
                        MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart - 1
                        MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd - 1
                    End If
                End If
            End If
        End If
    
        If Controller.CheckKey(vbKeyDown, 8) Then
            If Not MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ItemCount Then
                MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem + 1
                If MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd Then
                    If Not MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ItemCount Then
                        MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListStart + 1
                        MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd = MenuPages(Page).Lists(MenuPages(Page).CurrentList).ListEnd + 1
                    End If
                End If
            End If
        End If
    
        If Controller.CheckKey(vbKeyTab, 8) Then
            If MenuPages(Page).ListCount > MenuPages(Page).CurrentList Then
                MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = 0
                FocusList Page, MenuPages(Page).CurrentList + 1
                GoTo Back
            End If
            If MenuPages(Page).ButtonCount = 0 Then
                FocusList Page, 1
            Else
                MenuPages(Page).Lists(MenuPages(Page).CurrentList).CurrentItem = 0
                MenuPages(Page).CurrentList = 0
                MenuPages(Page).CurrentButton = 1
            End If
        End If
    
    End If

Back:

    For b = 1 To MenuPages(Page).FrameCount
    
        Effects.RenderRectangle MenuPages(Page).Frames(b).X + 1, MenuPages(Page).Frames(b).Y + 1, MenuPages(Page).Frames(b).Width - 2, MenuPages(Page).Frames(b).Height - 2, CLng(MenuPages(Page).Frames(b).Color), CLng(MenuPages(Page).Frames(b).Color), vbBlack, vbBlack, MenuPages(Page).Visibility + 50
        DrawBorder Page, MenuPages(Page).Frames(b).X, MenuPages(Page).Frames(b).Y, MenuPages(Page).Frames(b).Width, MenuPages(Page).Frames(b).Height
    
    Next b
    
    For e = 1 To MenuPages(Page).ListCount
    
        Effects.RenderRectangle MenuPages(Page).Lists(e).X + 1, MenuPages(Page).Lists(e).Y + 1, MenuPages(Page).Lists(e).Width - 2, MenuPages(Page).Lists(e).Height - 2, CLng(MenuPages(Page).Lists(e).Color), CLng(MenuPages(Page).Lists(e).Color), vbBlack, vbBlack, MenuPages(Page).Visibility + 50
        DrawBorder Page, MenuPages(Page).Lists(e).X, MenuPages(Page).Lists(e).Y, MenuPages(Page).Lists(e).Width, MenuPages(Page).Lists(e).Height
        'S2D.Effects.RECT MenuPages(Page).Lists(e).X + CornerWidth, MenuPages(Page).Lists(e).Y + CornerHeight, MenuPages(Page).Lists(e).Width - 2 * CornerWidth, MenuPages(Page).Lists(e).Height - 2 * CornerHeight, CLng(MenuPages(Page).Lists(e).Color), MenuPages(1).Visibility
    
        If Not MenuPages(Page).Lists(e).List.Count = 0 Then
            For f = MenuPages(Page).Lists(e).ListStart To MenuPages(Page).Lists(e).ListEnd
                If Not f = MenuPages(Page).Lists(e).CurrentItem Then
                    Text.DrawText MenuPages(Page).Lists(e).X + CornerWidth, MenuPages(Page).Lists(e).Y + (f - MenuPages(Page).Lists(e).ListStart) * 10 + CornerHeight, MenuPages(Page).Lists(e).List.Item(f), 1, MenuPages(Page).Visibility + 20
                Else
                    Effects.RenderRectangle MenuPages(Page).Lists(e).X + CornerWidth, MenuPages(Page).Lists(e).Y + (f - MenuPages(Page).Lists(e).ListStart) * 10 + CornerHeight, MenuPages(Page).Lists(e).Width - 2 * CornerWidth, Text.GetTextHeight(1), CLng(MenuPages(Page).Lists(e).Color), CLng(MenuPages(Page).Lists(e).Color), CLng(MenuPages(Page).Lists(e).Color), CLng(MenuPages(Page).Lists(e).Color), MenuPages(Page).Visibility + 70
                    Text.DrawText MenuPages(Page).Lists(e).X + CornerWidth, MenuPages(Page).Lists(e).Y + (f - MenuPages(Page).Lists(e).ListStart) * 10 + CornerHeight, MenuPages(Page).Lists(e).List.Item(f), 1, MenuPages(Page).Visibility + 100
                End If
            Next f
        End If
        
    Next e
    
    
    For a = 1 To MenuPages(Page).ButtonCount
        
        If Not a = MenuPages(Page).CurrentButton Then
            Text.DrawText MenuPages(Page).Buttons(a).X, MenuPages(Page).Buttons(a).Y, MenuPages(Page).Buttons(a).Text, 1, MenuPages(Page).Visibility + 20
        Else
            Text.DrawText MenuPages(Page).Buttons(MenuPages(Page).CurrentButton).X, MenuPages(Page).Buttons(MenuPages(Page).CurrentButton).Y, MenuPages(Page).Buttons(MenuPages(Page).CurrentButton).Text, 1, MenuPages(Page).Visibility + 100
        End If
        
    Next a
    
    For C = 1 To MenuPages(Page).ImageCount
        
        Engine.TexturePool.RenderTexture MenuPages(Page).Images(C).textureID, MenuPages(Page).Images(C).X + 1, MenuPages(Page).Images(C).Y + 1, MenuPages(Page).Images(C).Width - 2, MenuPages(Page).Images(C).Height - 2, MenuPages(Page).Images(C).SourceHeight, MenuPages(Page).Images(C).SourceWidth, MenuPages(Page).Images(C).SourceX, MenuPages(Page).Images(C).SourceY, MenuPages(Page).Visibility + 100
        DrawBorder Page, MenuPages(Page).Images(C).X, MenuPages(Page).Images(C).Y, MenuPages(Page).Images(C).Width, MenuPages(Page).Images(C).Height
        
    Next C
    
    For D = 1 To MenuPages(Page).TextCount
        Text.DrawText MenuPages(Page).Texts(D).X, MenuPages(Page).Texts(D).Y, MenuPages(Page).Texts(D).Text, 1, MenuPages(Page).Visibility + 100
    Next D
    
End Function

Private Function DrawBorder(Page As Integer, X As Double, Y As Double, Width As Integer, Height As Integer)

        S2D.TexturePool.RenderTexture textureID, X, Y, CornerWidth, CornerHeight, CornerWidth, CornerHeight, , , MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, X, Y + Height - CornerHeight, CornerWidth, CornerHeight, CornerWidth, CornerHeight, , CornerHeight * 2, MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, X + Width - CornerWidth, Y + Height - CornerHeight, CornerWidth, CornerHeight, CornerWidth, CornerHeight, CornerWidth * 2, CornerHeight * 2, MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, X + Width - CornerWidth, Y, CornerWidth, CornerHeight, CornerWidth, CornerHeight, CornerWidth * 2, , MenuPages(Page).Visibility + 50
        
        S2D.TexturePool.RenderTexture textureID, X + CornerWidth, Y, Width - CornerWidth * 2, CornerHeight, CornerWidth, CornerHeight, CornerWidth, , MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, X + CornerWidth, Y + Height - CornerHeight, Width - CornerWidth * 2, CornerHeight, CornerWidth, CornerHeight, CornerWidth, CornerHeight * 2, MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, X, Y + CornerHeight, CornerWidth, Height - CornerHeight * 2, CornerWidth, CornerHeight, , CornerHeight, MenuPages(Page).Visibility + 50
        S2D.TexturePool.RenderTexture textureID, X + Width - CornerWidth, Y + CornerHeight, CornerWidth, Height - CornerHeight * 2, CornerWidth, CornerHeight, CornerWidth * 2, CornerHeight, MenuPages(Page).Visibility + 50

End Function

Public Function AddListItem(Page As Integer, List As Integer, Name As String, Key As String)

    MenuPages(Page).Lists(List).List.Add Name, Key
    MenuPages(Page).Lists(List).ItemCount = MenuPages(Page).Lists(List).ItemCount + 1
    
    If MenuPages(Page).Lists(List).ListEnd < MenuPages(Page).Lists(List).Height / Text.GetTextHeight(1) - 1 Then
        MenuPages(Page).Lists(List).ListEnd = MenuPages(Page).Lists(List).ItemCount
    Else
        MenuPages(Page).Lists(List).ListEnd = MenuPages(Page).Lists(List).Height / Text.GetTextHeight(1) - 1
    End If
    
End Function

Public Function AddList(Page As Integer, X As Double, Y As Double, Width As Double, Height As Double, Color As Long) As Integer
    
    ReDim Preserve MenuPages(Page).Lists(MenuPages(Page).ListCount + 1)
    MenuPages(Page).ListCount = MenuPages(Page).ListCount + 1
    
    AddList = MenuPages(Page).ListCount
    MenuPages(Page).Lists(MenuPages(Page).ListCount).X = X
    MenuPages(Page).Lists(MenuPages(Page).ListCount).Y = Y
    MenuPages(Page).Lists(MenuPages(Page).ListCount).Width = Width
    MenuPages(Page).Lists(MenuPages(Page).ListCount).Height = Height
    MenuPages(Page).Lists(MenuPages(Page).ListCount).Color = Color
    
    MenuPages(Page).Lists(MenuPages(Page).ListCount).ListStart = 1
    MenuPages(Page).Lists(MenuPages(Page).ListCount).ListEnd = 1

End Function

Public Function RemoveList(Page As Integer, List As Integer)
    
    Dim TempArray() As MenuList
    ReDim TempArray(MenuPages(Page).ListCount - 1)
    
    Dim tmp As Integer
    For i = 1 To MenuPages(Page).ListCount
        If i = List Then
            tmp = 1
            GoTo skip
        End If
        TempArray(i - tmp) = MenuPages(Page).Lists(i)
skip:
    Next i
    
    MenuPages(Page).ListCount = MenuPages(Page).ListCount - 1
    ReDim MenuPages(Page).Lists(MenuPages(Page).ListCount)
    For i = 1 To MenuPages(Page).ListCount
        MenuPages(Page).Lists(i) = TempArray(i)
    Next i
    
End Function

Public Function AddButton(Page As Integer, X As Double, Y As Double, Text As String) As Integer

    ReDim Preserve MenuPages(Page).Buttons(MenuPages(Page).ButtonCount + 1)
    MenuPages(Page).ButtonCount = MenuPages(Page).ButtonCount + 1
    
    If MenuPages(Page).ButtonCount = 1 Then MenuPages(Page).CurrentButton = 1
    
    AddButton = MenuPages(Page).ButtonCount
    MenuPages(Page).Buttons(MenuPages(Page).ButtonCount).X = X
    MenuPages(Page).Buttons(MenuPages(Page).ButtonCount).Y = Y
    MenuPages(Page).Buttons(MenuPages(Page).ButtonCount).Text = Text
    
End Function

Public Function RemoveButton(Page As Integer, Button As Integer)
    
    Dim TempArray() As MenuButton
    ReDim TempArray(MenuPages(Page).ButtonCount - 1)
    
    Dim tmp As Integer
    For i = 1 To MenuPages(Page).ButtonCount
        If i = Button Then
            tmp = 1
            GoTo skip
        End If
        TempArray(i - tmp) = MenuPages(Page).Buttons(i)
skip:
    Next i
    
    MenuPages(Page).ButtonCount = MenuPages(Page).ButtonCount - 1
    ReDim MenuPages(Page).Buttons(MenuPages(Page).ButtonCount)
    For i = 1 To MenuPages(Page).ButtonCount
        MenuPages(Page).Buttons(i) = TempArray(i)
    Next i
    
End Function

Public Function AddFrame(Page As Integer, X As Single, Y As Single, Width As Single, Height As Single, Color As Single) As Integer

    ReDim Preserve MenuPages(Page).Frames(MenuPages(Page).FrameCount + 1)
    MenuPages(Page).FrameCount = MenuPages(Page).FrameCount + 1
    AddFrame = MenuPages(Page).FrameCount
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).X = X
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).Y = Y
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).Width = Width
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).Height = Height
    MenuPages(Page).Frames(MenuPages(Page).FrameCount).Color = Color
    
End Function

Public Function RemoveFrame(Page As Integer, Frame As Integer)
    
    Dim TempArray() As MenuFrame
    ReDim TempArray(MenuPages(Page).FrameCount - 1)
    
    Dim tmp As Integer
    For i = 1 To MenuPages(Page).FrameCount
        If i = Frame Then
            tmp = 1
            GoTo skip
        End If
        TempArray(i - tmp) = MenuPages(Page).Frames(i)
skip:
    Next i
    
    MenuPages(Page).FrameCount = MenuPages(Page).FrameCount - 1
    ReDim MenuPages(Page).Frames(MenuPages(Page).FrameCount)
    For i = 1 To MenuPages(Page).FrameCount
        MenuPages(Page).Frames(i) = TempArray(i)
    Next i
    
End Function

Public Function AddImage(Page As Integer, File As String, X As Double, Y As Double, Width As Double, Height As Double, Optional SourceWidth As Double = -1, Optional SourceHeight As Double = -1, Optional SourceX As Double = 0, Optional SourceY As Double = 0) As Integer
    
    ReDim Preserve MenuPages(Page).Images(MenuPages(Page).ImageCount + 1)
    MenuPages(Page).ImageCount = MenuPages(Page).ImageCount + 1
    AddImage = MenuPages(Page).ImageCount
    ID = Engine.TexturePool.AddTexture(File, rgb(255, 0, 255))
    
    MenuPages(Page).Images(MenuPages(Page).ImageCount).X = X
    MenuPages(Page).Images(MenuPages(Page).ImageCount).Y = Y
    MenuPages(Page).Images(MenuPages(Page).ImageCount).Width = Width
    MenuPages(Page).Images(MenuPages(Page).ImageCount).Height = Height
    MenuPages(Page).Images(MenuPages(Page).ImageCount).textureID = ID
    
    MenuPages(Page).Images(MenuPages(Page).ImageCount).SourceWidth = SourceWidth
    MenuPages(Page).Images(MenuPages(Page).ImageCount).SourceHeight = SourceHeight
    MenuPages(Page).Images(MenuPages(Page).ImageCount).SourceX = SourceX
    MenuPages(Page).Images(MenuPages(Page).ImageCount).SourceY = SourceY
    
End Function

Public Function RemoveImage(Page As Integer, Image As Integer)
    
    Dim TempArray() As MenuImage
    ReDim TempArray(MenuPages(Page).ImageCount - 1)
    
    Dim tmp As Integer
    For i = 1 To MenuPages(Page).ImageCount
        If i = Image Then
            tmp = 1
            GoTo skip
        End If
        TempArray(i - tmp) = MenuPages(Page).Images(i)
skip:
    Next i
    
    MenuPages(Page).ImageCount = MenuPages(Page).ImageCount - 1
    ReDim MenuPages(Page).Images(MenuPages(Page).ImageCount)
    For i = 1 To MenuPages(Page).ImageCount
        MenuPages(Page).Images(i) = TempArray(i)
    Next i
    
End Function

Public Function AddText(Page As Integer, X As Double, Y As Double, Text As String) As Integer

    ReDim Preserve MenuPages(Page).Texts(MenuPages(Page).TextCount + 1)
    MenuPages(Page).TextCount = MenuPages(Page).TextCount + 1
    AddText = MenuPages(Page).TextCount
    MenuPages(Page).Texts(MenuPages(Page).TextCount).X = X
    MenuPages(Page).Texts(MenuPages(Page).TextCount).Y = Y
    MenuPages(Page).Texts(MenuPages(Page).TextCount).Text = Text
    
End Function

Public Function RemoveText(Page As Integer, Text As Integer)
    
    Dim TempArray() As MenuText
    ReDim TempArray(MenuPages(Page).TextCount - 1)
    
    Dim tmp As Integer
    For i = 1 To MenuPages(Page).TextCount
        If i = Text Then
            tmp = 1
            GoTo skip
        End If
        TempArray(i - tmp) = MenuPages(Page).Texts(i)
skip:
    Next i
    
    MenuPages(Page).TextCount = MenuPages(Page).TextCount - 1
    ReDim MenuPages(Page).Texts(MenuPages(Page).TextCount)
    For i = 1 To MenuPages(Page).TextCount
        MenuPages(Page).Texts(i) = TempArray(i)
    Next i
    
End Function
