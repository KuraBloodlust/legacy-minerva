VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cS2D_i"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public TexturePool As New cS2DTexturePool_i    'dokumentieren: VIELLEICHT
'Public Sound              As New cS2DSound3D        'dokumentieren: JA

Private CFm_FpsLimit As Integer

Public Engine_i As Integer
Private Engine1 As New cS2D

'dokumentieren: JA
Public Property Get FpsLimit() As Integer
    FpsLimit = CFm_FpsLimit
End Property

Public Property Let FpsLimit(ByVal PropVal As Integer)
    CFm_FpsLimit = PropVal
End Property

Public Property Get ScreenWidthTiles() As Long
    
Select Case Engine_i
Case 1:
    ScreenWidthTiles = ScreenWidth / Const_TileWidth + 1
Case 2, 3:
    ScreenWidthTiles = ScreenWidth / Const_TileWidth + 1
End Select
    
End Property

Public Property Get ScreenHeightTiles() As Long

Select Case Engine_i
Case 1:
    ScreenHeightTiles = ScreenHeight / Const_TileHeight + 1
Case 2, 3:
    ScreenHeightTiles = ScreenHeight / Const_TileHeight + 1
End Select

End Property

Public Sub BeginScene()

Select Case Engine_i
Case 1:
    Engine1.BeginScene
Case 2, 3:
   
End Select

End Sub

Public Sub Clear()

Select Case Engine_i
Case 1:
    Engine1.Clear
Case 2, 3:
    
End Select

End Sub

Public Sub EndScene()

Select Case Engine_i
Case 1:
    Engine1.EndScene
Case 2, 3:
   
End Select

End Sub

'dokumentieren: NEIN
Public Sub Flip(Optional ByVal dest As Long)

  Select Case Engine_i
Case 1:
    Engine1.Flip dest
Case 2, 3:

End Select

End Sub

'dokumentieren: NEIN
Public Sub Initialize(dest As Long, Width As Integer, Height As Integer)

Select Case Engine_i
Case 1:
    Engine1.Initialize dest, False
    Engine1.Resize Width, Height
    Set TexturePool.t_engine = Me
    TexturePool.Pool1.SetBlending S2D_Alpha
Case 2, 3:

End Select

End Sub

Public Sub Resize(Width As Integer, Height As Integer)

    Select Case Engine_i
    Case 1:
        Engine1.Resize Width, Height
    End Select

End Sub

'dokumentieren: NEIN
Public Sub Unload()

Select Case Engine_i
Case 1:
    Engine1.Unload
Case 2, 3:
   
End Select

End Sub

Private Sub Class_Initialize()
    Engine_i = 1
End Sub
