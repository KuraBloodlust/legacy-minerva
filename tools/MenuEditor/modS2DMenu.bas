Attribute VB_Name = "modS2DMenu"
Public Type MenuFrame
    
    X As Double
    Y As Double
    Width As Integer
    Height As Integer
    Color As Long
    
End Type

Public Type MenuList
    
    X As Double
    Y As Double
    Width As Integer
    Height As Integer
    List As New Collection
    Color As Long
    CurrentItem As Integer
    ItemCount As Integer
    ListStart As Integer
    ListEnd As Integer
    
End Type

Public Type MenuButton
    
    X As Double
    Y As Double
    Text As String
    
End Type

Public Type MenuImage
    
    X As Double
    Y As Double
    Width As Integer
    Height As Integer
    SourceWidth As Integer
    SourceHeight As Integer
    SourceX As Integer
    SourceY As Integer
    textureID As Integer
    
End Type

Public Type MenuText
    
    X As Double
    Y As Double
    Text As String
    
End Type

Public Type MenuPage
    
    Frames() As MenuFrame
    FrameCount As Integer
    Buttons() As MenuButton
    ButtonCount As Integer
    Images() As MenuImage
    ImageCount As Integer
    Texts() As MenuText
    TextCount As Integer
    Lists() As MenuList
    ListCount As Integer
    BackgroundColor As Long
    CurrentButton As Integer
    CurrentList As Integer
    Visibility As Integer
    
End Type

Public MenuPages() As MenuPage
